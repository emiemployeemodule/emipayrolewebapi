﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using EMIPayrolModel;
using System.Security.Cryptography;
using System.IO;
using System.Data;
using System.Reflection;

namespace EMIPayrolData
{
    public static class CommonHelper
    {
        private static Random random = new Random();
        public static void SendEmail(string toEmail, string Subject, string Body)
        {
            try
            {
                MailMessage message = new MailMessage();
                message.From = new MailAddress(ConfigurationManager.AppSettings["mailFrom"], ConfigurationManager.AppSettings["mailFromDisplayName"]);
                if (toEmail.Contains(","))
                {
                    foreach (var email in toEmail.Split(','))
                    {
                        if (!string.IsNullOrEmpty(email))
                            message.To.Add(email);
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(toEmail))
                        message.To.Add(toEmail);
                }
                var ccmail = ConfigurationManager.AppSettings["ErrorMailAddressCC"] + "";
                if (string.IsNullOrEmpty(ccmail))
                {
                    message.CC.Add(ccmail);
                }
                message.Subject = Subject;
                message.Body = Body;
                message.IsBodyHtml = true;
                var smtp_host = ConfigurationManager.AppSettings["smtp_server"];
                var smtp_port = Convert.ToInt32(ConfigurationManager.AppSettings["smtp_port"]);
                SmtpClient SmtpServer = new SmtpClient(smtp_host, smtp_port)
                {
                    Host = smtp_host,
                    Port = smtp_port,
                    EnableSsl = true,
                    UseDefaultCredentials = true,
                    Credentials = new NetworkCredential(ConfigurationManager.AppSettings["smtp_email"], ConfigurationManager.AppSettings["smtp_password"])
                };
                if (message.To != null && message.To.Count > 0)
                    SmtpServer.Send(message);
            }
            catch (Exception ex)
            {
                CommonHelper.SendEmail(ConfigurationManager.AppSettings["ErrorMailAddress"], "Email Not Sent(" + Constants.MailErrorSubject + ")", "<b>Email Send CASE: Exception : </b>" + ex.ToString());
                throw ex;
            }
        }
    
        public static string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }

        public static string Decrypt(string cipherText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cipherText = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cipherText;
        }

        public static string AutogenratePassord()
        {
            return Encrypt(RandomString());
        }

        public static string RandomString()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, 8)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        public static string DateToString(object dt)
        {
            string str = (dt == null || dt.ToString().Length <= 2) ? "" : Convert.ToDateTime(dt).ToString("MM/dd/yyyy");
            if (str == "01-01-1900"|| str == "01/01/1900")
                str = "";
            return str;
        }


        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }
        public static List<T> ToList<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }

        public static void CopyFiles(string sourceDirectory, string targetDirectory)
        {
            DirectoryInfo source = new DirectoryInfo(sourceDirectory);
            DirectoryInfo target = new DirectoryInfo(targetDirectory);

            if (Directory.Exists(target.FullName) == false)
            {
                Directory.CreateDirectory(target.FullName);
            }

            // Copy each file into it's new directory.
            foreach (FileInfo fi in source.GetFiles())
            {
                fi.CopyTo(Path.Combine(target.ToString(), fi.Name), true);
            }
            //delete temp files
            System.IO.DirectoryInfo di = new DirectoryInfo(sourceDirectory);
            var filelist = di.GetFiles();
            if (filelist != null && filelist.Count() > 0)
            {
                foreach (FileInfo file in filelist)
                {
                    file.Delete();
                }
            }
        }
    }
}
