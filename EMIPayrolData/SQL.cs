﻿using EMIPayrolModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace EMIPayrolData
{
    public static class SQL
    {
       
        public enum ConnectionName
        {
            DefaultConnection
        }
        static public string Connectionstring
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString;
            }
        }
        static public int ExecuteQuery(string sql)
        {
            int result = -1;
            using (SqlConnection con = new SqlConnection(Connectionstring))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                using (SqlCommand cmd = new SqlCommand(sql, con))
                {
                    try
                    {

                        cmd.CommandTimeout = 0;
                        result = cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        CommonHelper.SendEmail(ConfigurationManager.AppSettings["ErrorMailAddress"], Constants.MailErrorSubject, "<b>SqlQuery : </b>" + sql + "<b>Exception : </b>" + ex.ToString());
                    }
                    finally
                    {
                        cmd.Dispose();
                        con.Close();
                        con.Dispose();
                    }
                }
            }
            return result;

        }
        static public int ExecuteQuery(string Sql, SqlParameter[] cmdparams)
        {
            string parameterswithvalues = "";
            int result = -1;
            using (SqlConnection con = new SqlConnection(Connectionstring))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                using (SqlCommand cmd = new SqlCommand(Sql, con))
                {
                    try
                    {
                        cmd.CommandTimeout = 0;
                        if (cmdparams != null)
                        {
                            foreach (SqlParameter sp in cmdparams)
                            {
                                parameterswithvalues += "<br />Parameter: " + sp.ParameterName + " Value:" + (sp.Value == null ? "" : sp.Value.ToString());
                                cmd.Parameters.Add(sp);
                            }
                        }
                        result = cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        CommonHelper.SendEmail(ConfigurationManager.AppSettings["ErrorMailAddress"], Constants.MailErrorSubject, "<b>SqlQuery : </b>" + Sql + "<br /><b>Parameterswithvalues : </b>" + parameterswithvalues + "<br /><br /><b>Exception : </b>" + ex.ToString());
                    }
                    finally
                    {
                        cmd.Dispose();
                        con.Close();
                        con.Dispose();
                    }
                }
            }
            return result;

        }
        static public object ExecuteScalar(string Sql)
        {
            Object obj = new object();
            using (SqlConnection con = new SqlConnection(Connectionstring))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                using (SqlCommand cmd = new SqlCommand(Sql, con))
                {
                    try
                    {
                        cmd.CommandTimeout = 0;
                        obj = cmd.ExecuteScalar();
                    }
                    catch (Exception ex)
                    {
                        CommonHelper.SendEmail(ConfigurationManager.AppSettings["ErrorMailAddress"], Constants.MailErrorSubject, "<b>SqlQuery : </b>" + Sql + "<b>Exception : </b>" + ex.ToString());
                    }
                    finally
                    {
                        cmd.Dispose();
                        con.Close();
                        con.Dispose();
                    }
                }
            }
            return obj;
        }
        static public object ExecuteScalar(string Sql, SqlParameter[] cmdparams)
        {
            string parameterswithvalues = "";
            Object obj = new object();
            using (SqlConnection con = new SqlConnection(Connectionstring))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                using (SqlCommand cmd = new SqlCommand(Sql, con))
                {
                    try
                    {
                        cmd.CommandTimeout = 0;
                        if (cmdparams != null)
                        {
                            foreach (SqlParameter sp in cmdparams)
                            {
                                parameterswithvalues += "<br />Parameter: " + sp.ParameterName + " Value:" + (sp.Value == null ? "" : sp.Value.ToString());
                                cmd.Parameters.Add(sp);
                            }
                        }
                        obj = cmd.ExecuteScalar();
                    }
                    catch (Exception ex)
                    {
                        CommonHelper.SendEmail(ConfigurationManager.AppSettings["ErrorMailAddress"], Constants.MailErrorSubject, "<b>SqlQuery : </b>" + Sql + "<br /><b>Parameterswithvalues : </b>" + parameterswithvalues + "<br /><br /><b>Exception : </b>" + ex.ToString());
                    }
                    finally
                    {
                        cmd.Dispose();
                        con.Close();
                        con.Dispose();
                    }
                }
            }
            return obj;
        }
        static public void ExceuteStoreProcedure(string StoreProcedureName)
        {
            using (SqlConnection con = new SqlConnection(Connectionstring))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                using (SqlCommand cmd = new SqlCommand(StoreProcedureName, con))
                {

                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 0;
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        CommonHelper.SendEmail(ConfigurationManager.AppSettings["ErrorMailAddress"], Constants.MailErrorSubject, "<b>Sql StoreProcedure : </b>" + StoreProcedureName + "<b>Exception : </b>" + ex.ToString());
                    }
                    finally
                    {
                        cmd.Dispose();
                        con.Close();
                        con.Dispose();
                    }

                }
            }
        }
        static public int ExceuteStoreProcedure(string StoreProcedureName, SqlParameter[] commandparams, bool returnParamater)
        {
            string parameterswithvalues = "";
            string retrunparametname = "";
            int result = 0;
            using (SqlConnection con = new SqlConnection(Connectionstring))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                using (SqlCommand cmd = new SqlCommand(StoreProcedureName, con))
                {
                    try
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 0;
                        if (commandparams != null)
                        {
                            foreach (SqlParameter sp in commandparams)
                            {
                                parameterswithvalues += "<br />Parameter: " + sp.ParameterName + " Value:" + (sp.Value == null ? "" : sp.Value.ToString());
                                cmd.Parameters.Add(sp);
                                if (returnParamater)
                                {
                                    if (sp.Direction == ParameterDirection.ReturnValue)
                                    {
                                        retrunparametname = sp.ParameterName;
                                    }
                                }

                            }
                        }
                        result = cmd.ExecuteNonQuery();
                        if (returnParamater && commandparams != null)
                        {
                            result = Convert.ToInt32(cmd.Parameters[retrunparametname].Value);
                        }

                    }
                    catch (Exception ex)
                    {
                        CommonHelper.SendEmail(ConfigurationManager.AppSettings["ErrorMailAddress"], Constants.MailErrorSubject, "<b>Sql StoreProcedure : </b>" + StoreProcedureName + " <br /><b>Parameterswithvalues : </b>" + parameterswithvalues + "<br /><br /><b>Exception : </b>" + ex.ToString());
                    }
                    finally
                    {
                        cmd.Dispose();
                        con.Close();
                        con.Dispose();
                    }
                }
            }
            return result;
        }
        static public SqlDataReader GetDataReader(string Sql, bool IsStoreProcedure)
        {
            SqlConnection con = new SqlConnection(Connectionstring);

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            using (SqlCommand cmd = new SqlCommand(Sql, con))
            {
                try
                {
                    if (IsStoreProcedure)
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                    }
                    cmd.CommandTimeout = 0;
                    return cmd.ExecuteReader(CommandBehavior.CloseConnection);

                }
                catch (Exception ex)
                {
                    CommonHelper.SendEmail(ConfigurationManager.AppSettings["ErrorMailAddress"], Constants.MailErrorSubject, "<b>SqlQuery : </b>" + Sql + "<b>Exception : </b>" + ex.ToString());
                    return null;
                }
                finally
                {
                    //   cmd.Dispose();
                    //  con.Close();
                    //  con.Dispose();
                }
            }


        }
        static public SqlDataReader GetDataReader(string Sql, SqlParameter[] commandparams, bool IsStoreProcedure)
        {
            string parameterswithvalues = "";
            SqlConnection con = new SqlConnection(Connectionstring);

            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            using (SqlCommand cmd = new SqlCommand(Sql, con))
            {
                try
                {
                    if (IsStoreProcedure)
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                    }

                    cmd.CommandTimeout = 0;
                    if (commandparams != null)
                    {
                        foreach (SqlParameter sp in commandparams)
                        {
                            parameterswithvalues += "<br />Parameter: " + sp.ParameterName + " Value:" + (sp.Value == null ? "" : sp.Value.ToString());
                            cmd.Parameters.Add(sp);
                        }
                    }
                    return cmd.ExecuteReader(CommandBehavior.CloseConnection);
                }
                catch (Exception ex)
                {
                    CommonHelper.SendEmail(ConfigurationManager.AppSettings["ErrorMailAddress"], Constants.MailErrorSubject, "<b>SqlQuery : </b>" + Sql + "<br /><b>Parameterswithvalues : </b>" + parameterswithvalues + "<br /><br /><b>Exception : </b>" + ex.ToString());
                    return null;
                }
                finally
                {
                    //   cmd.Dispose();
                    // con.Close();
                    // con.Dispose();
                }
            }


        }
        static public DataSet GetDs(string Sql, bool IsStoreProcedure)
        {
            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(Connectionstring))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                using (SqlCommand cmd = new SqlCommand(Sql, con))
                {
                    try
                    {
                        if (IsStoreProcedure)
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                        }
                        cmd.CommandTimeout = 0;
                        SqlDataAdapter adp = new SqlDataAdapter(cmd);
                        adp.Fill(ds);

                    }
                    catch (Exception ex)
                    {
                        CommonHelper.SendEmail(ConfigurationManager.AppSettings["ErrorMailAddress"], Constants.MailErrorSubject, "<b>SqlQuery : </b>" + Sql + "<b>Exception : </b>" + ex.ToString());
                    }
                    finally
                    {
                        cmd.Dispose();
                        con.Close();
                        con.Dispose();
                    }
                }
            }
            return ds;
        }
        static public DataSet GetDs(string Sql, SqlParameter[] commandparams, bool IsStoreProcedure)
        {
            string parameterswithvalues = "";
            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection(Connectionstring))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                using (SqlCommand cmd = new SqlCommand(Sql, con))
                {
                    try
                    {
                        if (IsStoreProcedure)
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                        }
                        if (commandparams != null)
                        {
                            foreach (SqlParameter sp in commandparams)
                            {
                                parameterswithvalues += "<br />Parameter: " + sp.ParameterName + " Value:" + (sp.Value == null ? "" : sp.Value.ToString());
                                cmd.Parameters.Add(sp);
                            }
                        }
                        cmd.CommandTimeout = 0;
                        SqlDataAdapter adp = new SqlDataAdapter(cmd);
                        adp.Fill(ds);

                    }
                    catch (Exception ex)
                    {
                        CommonHelper.SendEmail(ConfigurationManager.AppSettings["ErrorMailAddress"], Constants.MailErrorSubject, "<b>SqlQuery : </b>" + Sql + " <br /><b>Parameterswithvalues : </b>" + parameterswithvalues + "<br /><br /><b>Exception : </b>" + ex.ToString());
                    }
                    finally
                    {
                        cmd.Dispose();
                        con.Close();
                        con.Dispose();
                    }
                }
            }
            return ds;
        }
        static public DataTable GetDt(string Sql, bool IsStoreProcedure)
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(Connectionstring))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                using (SqlCommand cmd = new SqlCommand(Sql, con))
                {
                    try
                    {
                        if (IsStoreProcedure)
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                        }
                        cmd.CommandTimeout = 0;
                        SqlDataAdapter adp = new SqlDataAdapter(cmd);
                        adp.Fill(dt);

                    }
                    catch (Exception ex)
                    {
                        CommonHelper.SendEmail(ConfigurationManager.AppSettings["ErrorMailAddress"], Constants.MailErrorSubject, "<b>SqlQuery : </b>" + Sql + "<b>Exception : </b>" + ex.ToString());
                    }
                    finally
                    {
                        cmd.Dispose();
                        con.Close();
                        con.Dispose();
                    }
                }
            }
            return dt;
        }
        static public DataTable GetDt(string Sql, SqlParameter[] commandparams, bool IsStoreProcedure)
        {
            string parameterswithvalues = "";
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(Connectionstring))
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                using (SqlCommand cmd = new SqlCommand(Sql, con))
                {
                    try
                    {
                        if (IsStoreProcedure)
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                        }
                        if (commandparams != null)
                        {
                            foreach (SqlParameter sp in commandparams)
                            {
                                parameterswithvalues += "<br />Parameter: " + sp.ParameterName + " Value:" + (sp.Value == null ? "" : sp.Value.ToString());
                                cmd.Parameters.Add(sp);
                            }
                        }
                        cmd.CommandTimeout = 0;
                        SqlDataAdapter adp = new SqlDataAdapter(cmd);
                        adp.Fill(dt);

                    }
                    catch (Exception ex)
                    {
                        CommonHelper.SendEmail(ConfigurationManager.AppSettings["ErrorMailAddress"], Constants.MailErrorSubject, "<b>SqlQuery : </b>" + Sql + " <br /><b>Parameterswithvalues : </b>" + parameterswithvalues + "<br /><br /><b>Exception : </b>" + ex.ToString());
                    }
                    finally
                    {
                        cmd.Dispose();
                        con.Close();
                        con.Dispose();
                    }
                }
            }
            return dt;
        }
        static public string DBSquote(string s)
        {

            return ("'" + s.Replace("'", "''") + "'");
        }
        static public String GetSqlS(String Sql)
        {
            String S = String.Empty;

            using (SqlDataReader dr = GetDataReader(Sql, false))
            {
                if (dr.Read())
                {
                    S = dr["S"].ToString();
                    if (S.Equals(DBNull.Value))
                    {
                        S = String.Empty;
                    }
                }
            }
            return S;
        }
        static public int GetSqlN(String Sql)
        {
            int i = 0;
            String S = String.Empty;
            using (SqlDataReader dr = GetDataReader(Sql, false))
            {
                if (dr.Read())
                {
                    S = dr["N"].ToString();
                    if (!S.Equals(DBNull.Value))
                    {
                        int.TryParse(S, out i);
                    }
                }
            }
            return i;
        }


    }
}
