﻿using EMIPayrol.Helpers;
using EMIPayrol.Models;
using EMIPayrolModel;
using EMIPayrolServices;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace EMIPayrol.Controllers
{
    [Authorize]
    [GlobalExceptionAttribute]
    [RoutePrefix("api/company")]
    public class CompanyController : ApiController
    {
        private readonly ICompanyServices _companyServices;
        public CompanyController(ICompanyServices companyServices)
        {
            _companyServices = companyServices;
        }

        readonly ICompanyServices companyServices = new CompanyServices();

        public CompanyController()
        {
            _companyServices = companyServices;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("saveCompany")]
        public async Task<IHttpActionResult> saveCompany(Company model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null)
            {
                var result = _companyServices.SaveCompanyDetails(model);
                if (result > 0 && model.ID == 0)
                {
                    response.Result = Constants.CompanySignUp; response.StatusCode = HttpStatusCode.Created;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.ErrorMessages = new List<string>() { Constants.AlreadyCompanyExist };
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("emailExist")]
        public async Task<IHttpActionResult> emailExist(CommonClass model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.Email))
            {
                var result = _companyServices.IsEmailExist(model.Email);
                if (result)
                {
                    response.Result = Constants.AlreadyRegisterdEmail; response.StatusCode = HttpStatusCode.Conflict;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.Continue;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("verifyCompanyacount")]
        public async Task<IHttpActionResult> verifyCompanyAccount(CommonClass model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.Guidtoken) && !string.IsNullOrEmpty(model.Password))
            {
                var result = _companyServices.verifyCompanyAccount(model);
                if (result)
                {
                    response.Result = Constants.LinkVerifiedMessage; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = Constants.Expirelink;
                    response.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("employeRange")]
        public async Task<IHttpActionResult> employeRange()
        {
            ServiceStatus<List<EmployeRange>> response = new ServiceStatus<List<EmployeRange>>();
            var model = _companyServices.EmployeRange();
            if (model != null)
            {
                response.Result = model; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("companyLogin")]
        public async Task<IHttpActionResult> companyLogin(CommonClass model)
        {
            ServiceStatus<AuthToken> response = new ServiceStatus<AuthToken>();
            if (model!=null&&!string.IsNullOrEmpty(model.Email) && !string.IsNullOrEmpty(model.Password))
            {
                var result = _companyServices.CompanyLogin(model);
                if (result > 0)
                {
                    var companyDetail = _companyServices.companyDetail(result);
                    AuthToken authToken = new AuthToken();
                    authToken.Uat = Common.createToken(companyDetail);
                    response.Result = authToken; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.ErrorMessages = new List<string>() { Constants.InvalidCredtional };
                    response.StatusCode = HttpStatusCode.BadRequest;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpPost]
        [Route("UpdateCompany")]
        public async Task<IHttpActionResult> UpdateCompany(Company model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null)
            {
                var result = _companyServices.SaveCompanyDetails(model);
                if (model.ID > 0 && result > 0) { response.Result = Constants.CompanyUpdate; response.StatusCode = HttpStatusCode.Accepted; }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.Result = Constants.AlreadyCompanyExist;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("companyForgotPassword")]
        public async Task<IHttpActionResult> companyForgotPassword(CommonClass model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.Email))
            {
                var result = _companyServices.companyForgotPassword(model.Email);
                if (result)
                {
                    response.Result = Constants.FogotEmailSent; response.StatusCode = HttpStatusCode.OK;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("changePassword")]
        public async Task<IHttpActionResult> changePassword(ChangePassword model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && model.ID > 0 && !string.IsNullOrEmpty(model.OldPassword) && !string.IsNullOrEmpty(model.Password))
            {
                int id = _companyServices.ChangePassword(model);
                if (id > 0)
                {
                    response.Result = Constants.ChangePassword; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.ErrorMessages = new List<string>() { Constants.MismatchOldPassword };
                    response.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("updateCompanyPassword")]
        public async Task<IHttpActionResult> updateCompanyPassword(CommonClass model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.Guidtoken) && !string.IsNullOrEmpty(model.Password))
            {
                var result = _companyServices.updateCompanyPassword(model);
                if (result)
                {
                    response.Result = Constants.NewPassword; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = Constants.Expirelink;
                    response.StatusCode = HttpStatusCode.Unauthorized;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpGet]
        [Route("companyDetail/{CompanyID}")]
        public async Task<IHttpActionResult> companyDetail(int CompanyID)
        {
            ServiceStatus<Company> response = new ServiceStatus<Company>();
            if (CompanyID > 0)
            {
                var model = _companyServices.companyDetail(CompanyID);
                if (model != null)
                {
                    response.Result = model; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
   
        #region department
        [HttpPost]
        [Route("SaveCompanyDepartment")]
        public async Task<IHttpActionResult> SaveCompanyDepartment(List<CompanyDepartment> model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && model.Count > 0)
            {
                _companyServices.SaveCompanyDepartment(model);
                response.Result = Constants.AddCompanyDepartment; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getCompanyDepartment/{CompanyID}")]
        public async Task<IHttpActionResult> getCompanyDepartment(int CompanyID)
        {
            ServiceStatus<List<CompanyDepartment>> response = new ServiceStatus<List<CompanyDepartment>>();
            if (CompanyID > 0)
            {
                var model = _companyServices.GetCompanyDepartment(CompanyID);
                if (model != null)
                {
                    response.Result = model; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("deleteCompanyDepartment")]
        public async Task<IHttpActionResult> deleteCompanyDepartment(DeleteDepartment model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && model.CompanyID > 0 && model.DepartmentID > 0)
            {
                _companyServices.DeleteCompanyDepartment(model);
                response.Result = Constants.DeleteDepartment; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region industry, appmodule, rolepermission
        [HttpPost]
        [Route("SaveCompanyIndustryInfo")]
        public async Task<IHttpActionResult> SaveCompanyIndustryInfo(CompanyIndustry model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && model.IndustryQuestion!=null&& model.IndustryQuestion.Count>0)
            {
                _companyServices.SaveCompanyIndustryInfo(model);
                response.Result = Constants.AddAnswer; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getCompanyAppModule/{CompanyID}/{RoleID}/{DepartMentID}")]
        public async Task<IHttpActionResult> getCompanyAppModule(int CompanyID, int RoleID, int DepartMentID)
        {
            ServiceStatus<List<AppModule>> response = new ServiceStatus<List<AppModule>>();
            var model = _companyServices.getCompanyAppModule(CompanyID, RoleID, DepartMentID);
            if (model != null)
            {
                response.Result = model; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);

        }
        
        [HttpPost]
        [Route("SaveCompanyRolePermission")]
        public async Task<IHttpActionResult> SaveCompanyRolePermission(List<DepartmentRolePermission> model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && model.Count >0)
            {
               var result= _companyServices.SaveCompanyRolePermission(model);
                if (result)
                {
                    response.Result = Constants.Addpermission; response.StatusCode = HttpStatusCode.OK;
                }
                else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        
        [HttpGet]
        [Route("getCompanyRolePermission/{CompanyID}")]
        public async Task<IHttpActionResult> getCompanyRolePermission(int CompanyID)
        {
            ServiceStatus<List<DepartmentRolePermission>> response = new ServiceStatus<List<DepartmentRolePermission>>();
            var model = _companyServices.getCompanyRolePermission(CompanyID);
            if (model != null)
            {
                response.Result = model; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        #endregion

        #region CompanyHoliday
        [HttpPost]
        [Route("saveCompanyHoliday")]
        public async Task<IHttpActionResult> saveCompanyHoliday(CompanyHoliday model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null)
            {
                var result = _companyServices.SaveUpdateCompanyHoliday(model);
                if (result > 0 && model.ID == 0)
                {
                    response.Result = Constants.AddCompanyHoliday; response.StatusCode = HttpStatusCode.OK;
                }
                else if (result > 0 && model.ID > 0)
                {
                    response.Result = Constants.EditCompanyHoliday; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.ErrorMessages = new List<string>() { Constants.AlreadyCompanyHoliday };
                }

            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpGet]
        [Route("getCompanyHolidayByCompanyID/{CompanyID}")]
        public async Task<IHttpActionResult> getCompanyHolidayByCompanyID(int CompanyID)
        {
            ServiceStatus<List<CompanyHoliday>> response = new ServiceStatus<List<CompanyHoliday>>();
            if (CompanyID > 0)
            {
                var result = _companyServices.GetCompanyHolidayByCompanyID(CompanyID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpGet]
        [Route("getCompanyHolidayByID/{ID}")]
        public async Task<IHttpActionResult> getCompanyHolidayByID(int ID)
        {
            ServiceStatus<CompanyHoliday> response = new ServiceStatus<CompanyHoliday>();
            if (ID > 0)
            {
                var result = _companyServices.GetCompanyHolidayByID(ID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpPost]
        [Route("DeleteCompanyHoliday/{ID}")]
        public async Task<IHttpActionResult> deleteCompanyHolidayByID(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _companyServices.DeleteCompanyHoliday(ID);
                response.Result = Constants.DeleteCompanyHoliday; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region VactionPolicy
        [HttpPost]
        [Route("saveVactionPolicy")]
        public async Task<IHttpActionResult> saveVactionPolicy(VactionPolicy model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.PolicyType) && model.CompanyID > 0)
            {
                var result = _companyServices.SaveVactionPolicy(model);
                if (result > 0 && model.ID == 0)
                {
                    response.Result = Constants.AddVactionPolicy; response.StatusCode = HttpStatusCode.Created;
                }
                else if (result > 0 && model.ID > 0) { response.Result = Constants.EditVactionPolicy; response.StatusCode = HttpStatusCode.Accepted; }
                else
                {

                    response.StatusCode = HttpStatusCode.Conflict;
                    response.ErrorMessages = new List<string>() { Constants.AlreadyVactionPolicyExist };
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getVactionPolicy/{CompanyID}")]
        public async Task<IHttpActionResult> getVactionPolicy(int CompanyID)
        {
            ServiceStatus<List<VactionPolicy>> response = new ServiceStatus<List<VactionPolicy>>();
            if (CompanyID > 0)
            {
                var model = _companyServices.getVactionPolicy(CompanyID);
                if (model != null)
                {
                    response.Result = model; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getVactionPolicyByID/{PolicyID}")]
        public async Task<IHttpActionResult> getVactionPolicyByID(int PolicyID)
        {
            ServiceStatus<VactionPolicy> response = new ServiceStatus<VactionPolicy>();
            if (PolicyID > 0)
            {
                var model = _companyServices.getVactionPolicy(0, PolicyID);
                if (model != null && model.Count > 0)
                {
                    response.Result = model.FirstOrDefault(); response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else
            {
                response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest;
            }
            return Ok(response);
        }
        [HttpPost]
        [Route("deleteVactionPolicy")]
        public async Task<IHttpActionResult> deleteVactionPolicy(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _companyServices.DeleteVactionPolicy(ID);
                response.Result = Constants.DeleteVactionPolicy; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region CompanyPolicy
        [HttpPost]
        [Route("saveCompanyPolicy")]
        public async Task<IHttpActionResult> saveCompanyPolicy(CompanyPolicy model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.PolicyType) && model.CompanyID > 0)
            {
                var result = _companyServices.SaveCompanyPolicy(model);
                if (result > 0 && model.ID == 0)
                {
                    response.Result = Constants.AddCompanyPolicy; response.StatusCode = HttpStatusCode.Created;
                }
                else if (result > 0 && model.ID > 0) { response.Result = Constants.EditCompanyPolicy; response.StatusCode = HttpStatusCode.Accepted; }
                else
                {

                    response.StatusCode = HttpStatusCode.Conflict;
                    response.ErrorMessages = new List<string>() { Constants.AlreadyCompanyPolicyExist };
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getCompanyPolicy/{CompanyID}")]
        public async Task<IHttpActionResult> getCompanyPolicy(int CompanyID)
        {
            ServiceStatus<List<CompanyPolicy>> response = new ServiceStatus<List<CompanyPolicy>>();
            if (CompanyID > 0)
            {
                var model = _companyServices.getCompanyPolicy(CompanyID);
                if (model != null)
                {
                    response.Result = model; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getCompanyPolicyByID/{PolicyID}")]
        public async Task<IHttpActionResult> getCompanyPolicyByID(int PolicyID)
        {
            ServiceStatus<CompanyPolicy> response = new ServiceStatus<CompanyPolicy>();
            if (PolicyID > 0)
            {
                var model = _companyServices.getCompanyPolicy(0, PolicyID);
                if (model != null && model.Count > 0)
                {
                    response.Result = model.FirstOrDefault(); response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else
            {
                response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest;
            }
            return Ok(response);
        }
        [HttpPost]
        [Route("deleteCompanyPolicy")]
        public async Task<IHttpActionResult> deleteCompanyPolicy(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _companyServices.DeleteCompanyPolicy(ID);
                response.Result = Constants.DeleteCompanyPolicy; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region Invsetting
        
        [HttpPost]
        [Route("saveInvoiceSettings")]
        public async Task<IHttpActionResult> saveInvoiceSettings()
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            var request = HttpContext.Current.Request;

            if (request != null && !string.IsNullOrEmpty(request.Form["InvPerfix"]) && !string.IsNullOrEmpty(request.Form["CompanyID"]))
            {
                InvoiceSettings model = new InvoiceSettings
                {
                    CompanyID = Convert.ToInt32(request.Form["CompanyID"]),
                    InvPerfix = request.Form["InvPerfix"],
                    ImageUrl = request.Form["ImageUrl"],
                    Path = request.Form["Path"]
                };
                var file = HttpContext.Current.Request.Files.Count > 0 ?
                          HttpContext.Current.Request.Files[0] : null;
                string filePath = string.Empty;
                if (file != null && file.ContentLength > 0)
                {
                    string extention = file.FileName.Split('.').LastOrDefault();
                    model.Path = "/Files/Inv/" + Guid.NewGuid().ToString() + "." + extention;
                    filePath = Path.Combine(HttpContext.Current.Server.MapPath("~" + model.Path));
                    file.SaveAs(filePath);
                }
                var result = _companyServices.SaveInvoiceSettings(model);
                if (result > 0)
                {
                    response.Result = Constants.AddInviceSettings; response.StatusCode = HttpStatusCode.OK;
                }
                else
                { response.Result = Constants.editInviceSettings; response.StatusCode = HttpStatusCode.OK; }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        
        [HttpGet]
        [Route("getInvoiceSettings/{CompanyID}")]
        public async Task<IHttpActionResult> getInvoiceSettings(int CompanyID)
        {
            ServiceStatus<InvoiceSettings> response = new ServiceStatus<InvoiceSettings>();
            if (CompanyID > 0)
            {
                var model = _companyServices.GetInvoiceSettings(CompanyID);
                if (model != null)
                {
                    response.Result = model; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region Document Library 
        [HttpGet]
        [Route("getFolderList")]
        public async Task<IHttpActionResult> getFolderList()
        {
            ServiceStatus<List<Filedto>> response = new ServiceStatus<List<Filedto>>();
            string baseurl = Convert.ToString(ConfigurationManager.AppSettings["APIBaseUrl"]);
            List<Filedto> model = new List<Filedto>();
            model.Add(new Filedto { Name = "Employee Manual", Value = "EmployProfile",IconUrl=baseurl+"/files/employeeicon.png" });
            model.Add(new Filedto { Name = "Health & Safety Manual", Value = "HeathSafety", IconUrl = baseurl + "/files/healthsafetyicon.png" });
            model.Add(new Filedto { Name = "Document Templates", Value = "DocumentTemplate", IconUrl = baseurl + "/files/documenticon.png" });
            response.Result = model; response.StatusCode = HttpStatusCode.OK;
            return Ok(response);
        }

        [HttpGet]
        [Route("getDocumentList/{CompanyID}/{FolderName?}")]
        public async Task<IHttpActionResult> getDocumentList(int CompanyID,string FolderName=null)
        {
            ServiceStatus<List<FileDocument>> response = new ServiceStatus<List<FileDocument>>();
            if (CompanyID> 0)
            {
                response.Result = _companyServices.GetDocumentList(CompanyID, FolderName + "");
                response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getDocumentByID/{ID}")]
        public async Task<IHttpActionResult> getDocumentByID(int ID)
        {
            ServiceStatus<FileDocument> response = new ServiceStatus<FileDocument>();
            if (ID > 0)
            {
                response.Result = _companyServices.GetDocumentByID(ID);
                 response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("saveDocument")]
        public async Task<IHttpActionResult> saveDocument()
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            var request = HttpContext.Current.Request;

            if (request != null && !string.IsNullOrEmpty(request.Form["CompanyID"]))
            {
                int CompanyID = Convert.ToInt32(request.Form["CompanyID"]);
                int ID = 0, PrivacyType = 0;
                int.TryParse(request.Form["ID"], out ID);
                int.TryParse(request.Form["PrivacyType"], out PrivacyType);
                var file = HttpContext.Current.Request.Files.Count > 0 ?
                          HttpContext.Current.Request.Files[0] : null;
                
                string folderName = request.Form["FolderName"];
                string filepath = request.Form["FilePath"];
                if (file != null && file.ContentLength > 0)
                {
                    string extention = file.FileName.Split('.').LastOrDefault();
                    filepath = string.IsNullOrEmpty(filepath)?("/Files/" + folderName.Trim()+"/" + Guid.NewGuid().ToString() + "." + extention): filepath;
                    var fullPath = Path.Combine(HttpContext.Current.Server.MapPath("~" + filepath));
                    file.SaveAs(fullPath);
                }
                FileDocument filedoc = new FileDocument {
                    ID=ID,
                    FilePath=filepath,
                    FileName= folderName,
                    PublicPrivacy =Convert.ToBoolean(PrivacyType),
                    Title=request.Form["Title"],
                    Description = request.Form["Description"],
                };
                _companyServices.SaveDocument(CompanyID, filedoc);
                response.Result = Constants.DefaultUpdateConst; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion
    }
}
