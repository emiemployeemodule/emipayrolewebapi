﻿using EMIPayrol.Helpers;
using EMIPayrolModel;
using EMIPayrolServices;
using NReco.PdfGenerator;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace EMIPayrol.Controllers
{
    //[Authorize]
    [GlobalExceptionAttribute]
    [RoutePrefix("api/healthsafty")]
    public class HealthSaftyController : ApiController
    {
        
        private readonly IHealthSaftyServices _healthSaftyServices;
        public HealthSaftyController(IHealthSaftyServices healthSaftyServices)
        {
            _healthSaftyServices = healthSaftyServices;
        }

        readonly IHealthSaftyServices healthSaftyServices = new HealthSaftyServices();

        public HealthSaftyController()
        {
            _healthSaftyServices = healthSaftyServices;
        }

        #region JHSCmember
        [HttpPost]
        [Route("saveUpdateJHSCMember")]
        public async Task<IHttpActionResult> saveUpdateJHSCMember(JHSCMember model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null)
            {
                var result = _healthSaftyServices.SaveUpdateJHSCMember(model);
                if (result > 0 && model.ID == 0)
                {
                    response.Result = Constants.AddJHSCMember; response.StatusCode = HttpStatusCode.OK;
                }
                else if (result > 0 && model.ID > 0)
                {
                    response.Result = Constants.EditJHSCMember; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.ErrorMessages = new List<string>() { Constants.AlreadyJHSCMemberExist};
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getJHSCMemberBYCompanyID/{CompanyID}")]
        public async Task<IHttpActionResult> GetJHSCMemberBYCompanyID(int CompanyID)
        {
            ServiceStatus<List<JHSCMember>> response = new ServiceStatus<List<JHSCMember>>();
            if (CompanyID > 0)
            {
                var result = _healthSaftyServices.GetJHSCMemberBYCompanyID(CompanyID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("deleteJHSCMemberByID/{ID}")]
        public async Task<IHttpActionResult> deleteJHSCMemberByID(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _healthSaftyServices.deleteJHSCMemberByID(ID);
                response.Result = Constants.DeleteJHSCMember; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpPost]
        [Route("saveBulkJHSCMember")]
        public async Task<IHttpActionResult> saveBulkJHSCMember(BulkJHSCMemberdto model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && model.CompanyID > 0 &&(model.ParticipantsList != null && model.ParticipantsList.Count > 0))
            {
                _healthSaftyServices.SaveBulkJHSCMember(model);
                response.Result = Constants.AddJHSCMember; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getBulkJHSCMember/{CompanyID}")]
        public async Task<IHttpActionResult> getBulkJHSCMember(int CompanyID)
        {
            ServiceStatus<BulkJHSCMemberdto> response = new ServiceStatus<BulkJHSCMemberdto>();
            var result = _healthSaftyServices.GetBulkJHSCMember(CompanyID);
            if (result != null)
            {
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [HttpGet]
        [Route("getNonJhscMember/{CompanyID}")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<List<EmployeeEmail>>))]
        public async Task<IHttpActionResult> getNonJhscMember(int CompanyID)
        {
            ServiceStatus<List<EmployeeEmail>> response = new ServiceStatus<List<EmployeeEmail>>();
            if (CompanyID > 0)
            {
                var result = _healthSaftyServices.GetNonJhscMember(CompanyID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region Health & Safty Email
        [HttpPost]
        [Route("saveHealthSaftyEmail")]
        public async Task<IHttpActionResult> saveHealthSaftyEmail(List<HealthAndSaftyEmail> model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null)
            {
                _healthSaftyServices.SaveHealthSaftyEmail(model);
                response.Result = Constants.AddHealthSaftyEmail; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getHealthAndSaftyEmail/{CompanyID}")]
        public async Task<IHttpActionResult> GetHealthAndSaftyEmail(int CompanyID)
        {
            ServiceStatus<List<HealthAndSaftyEmail>> response = new ServiceStatus<List<HealthAndSaftyEmail>>();
            if (CompanyID > 0)
            {
                var result = _healthSaftyServices.GetHealthAndSaftyEmailBYCompanyID(CompanyID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getNonHealthAndSaftyEmail/{CompanyID}")]
        public async Task<IHttpActionResult> getNonHealthAndSaftyEmail(int CompanyID)
        {
            ServiceStatus<List<HealthAndSaftyEmail>> response = new ServiceStatus<List<HealthAndSaftyEmail>>();
            if (CompanyID > 0)
            {
                var result = _healthSaftyServices.GetNonHealthAndSaftyEmail(CompanyID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("deleteHealthAndSaftyEmail/{ID}")]
        public async Task<IHttpActionResult> deleteHealthAndSaftyEmail(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _healthSaftyServices.deleteHealthAndSaftyEmail(ID);
                response.Result = Constants.DeleteHealthSaftyEmail; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region Inspection
        [HttpPost]
        [Route("saveUpdateInspectionModule")]
        public async Task<IHttpActionResult> saveUpdateInspectionModule(InspectionModule model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null)
            {
                var result = _healthSaftyServices.SaveUpdateInspectionModule(model);
                if (result > 0 && model.ID == 0)
                {
                    response.Result = Constants.AddInspectionModule; response.StatusCode = HttpStatusCode.OK;
                }
                else if (result > 0 && model.ID > 0)
                {
                    response.Result = Constants.EditInspectionModule; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.ErrorMessages = new List<string>() { Constants.AlreadyInspectionModuleExist };
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getInspectionModule")]
        public async Task<IHttpActionResult> getInspectionModule()
        {
            ServiceStatus<List<InspectionModule>> response = new ServiceStatus<List<InspectionModule>>();
            var result = _healthSaftyServices.GetInspectionModule();
            if (result != null && result.Count > 0)
            {
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [HttpPost]
        [Route("saveUpdateInspectionModuleCategory")]
        public async Task<IHttpActionResult> saveUpdateInspectionModuleCategory(InspectionModuleCategory model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null)
            {
                var result = _healthSaftyServices.SaveUpdateInspectionModuleCategory(model);
                if (result > 0 && model.ID == 0)
                {
                    response.Result = Constants.AddInspectionModuleCategory; response.StatusCode = HttpStatusCode.OK;
                }
                else if (result > 0 && model.ID > 0)
                {
                    response.Result = Constants.EditInspectionModuleCategory; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.ErrorMessages = new List<string>() { Constants.AlreadyInspectionModuleExistCategory };
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpGet]
        [Route("getInsModuleCategoryByModuleID/{ModuleID}")]
        public async Task<IHttpActionResult> getInsModuleCategoryByModuleID(int ModuleID)
        {
            ServiceStatus<List<InspectionModuleCategory>> response = new ServiceStatus<List<InspectionModuleCategory>>();
            if (ModuleID > 0)
            {
                var result = _healthSaftyServices.GetInsModuleCategoryByModuleID(ModuleID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("saveUpdateInspectionForm")]
        public async Task<IHttpActionResult> saveUpdateInspectionForm(InspectionForm model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null)
            {
                var result = _healthSaftyServices.SaveUpdateInspectionForm(model);
                if (result > 0 && model.ID == 0)
                {
                    response.Result = Constants.AddInspectionForm; response.StatusCode = HttpStatusCode.OK;
                }
                else if (result > 0 && model.ID > 0)
                {
                    response.Result = Constants.EditInspectionForm; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.ErrorMessages = new List<string>() { Constants.AlreadyInspectionFormExist};
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getInspectionFormByCompanyID/{ModuleID}/{CompanyID}/{CategoryID}")]
        public async Task<IHttpActionResult> getInspectionFormByCompanyID(int ModuleID,int CompanyID,int CategoryID)
        {
            ServiceStatus<List<InspectionForm>> response = new ServiceStatus<List<InspectionForm>>();
            if (ModuleID > 0 && CompanyID>0 && CategoryID>0)
            {
                var result = _healthSaftyServices.GetInspectionFormByCompanyID(ModuleID,CategoryID,CompanyID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpPost]
        [Route("saveInspectionFormQuestion")]
        public async Task<IHttpActionResult> saveInspectionFormQuestion(InspectionFormQuestion model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null)
            {
                var result = _healthSaftyServices.SaveInspectionFormQuestion(model);
                if (result > 0 && model.ID == 0)
                {
                    response.Result = Constants.AddInspectionFormQuestion; response.StatusCode = HttpStatusCode.OK;
                }
                else if (result > 0 && model.ID > 0)
                {
                    response.Result = Constants.EditInspectionFormQuestion; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.ErrorMessages = new List<string>() { Constants.AlreadyInspectionFormExistQuestion };
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpPost]
        [Route("setQuestionActive")]
        public async Task<IHttpActionResult> setQuestionActive(QuestionActive model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && model.InspectionFormQuestionID > 0)
            {
                _healthSaftyServices.setQuestionActive(model);
                response.Result = Constants.EditInspectionFormQuestion; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("GetCompanyInspectionFormQuestion/{CompanyID}/{ModuleID}/{CategoryID}")]
        public async Task<IHttpActionResult> GetCompanyInspectionFormQuestion(int CompanyID,int ModuleID, int CategoryID)
        {
            ServiceStatus<List<CompanyInspectionForm>> response = new ServiceStatus<List<CompanyInspectionForm>>();
            if (ModuleID > 0)
            {
                var result = _healthSaftyServices.GetCompanyInspectionFormQuestion(CompanyID,ModuleID, CategoryID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        
        #endregion

        #region Saftyboard
        [HttpPost]
        [Route("saveSaftyBoard")]
        public async Task<IHttpActionResult> saveSaftyBoard()
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            var request = HttpContext.Current.Request;

            if (request != null && !string.IsNullOrEmpty(request.Form["Title"]) && !string.IsNullOrEmpty(request.Form["Decription"])
                && !string.IsNullOrEmpty(request.Form["CompanyID"]) 
                )
            {
                SaftyBoard model = new SaftyBoard
                {
                    ID = Convert.ToInt32(request.Form["ID"]),
                    Title = request.Form["Title"],
                    ExpirationDate = request.Form["ExpirationDate"],
                    Decription = request.Form["Decription"],
                    Location = request.Form["Location"],
                    CompanyID = Convert.ToInt32(request.Form["CompanyID"]),
                    FileName = request.Form["FileName"],
                    CreatedBy = Convert.ToInt32(request.Form["CreatedBy"])
                };
                var file = HttpContext.Current.Request.Files.Count > 0 ?
                          HttpContext.Current.Request.Files[0] : null;
                string filePath = string.Empty;
                if (file != null && file.ContentLength > 0)
                {
                    string extention = file.FileName.Split('.').LastOrDefault();
                    if (string.IsNullOrEmpty(model.FileName))
                    {
                        model.FileName = "/Files/HeathSafety/SDB" + Convert.ToString(request.Form["CompanyID"]) + Guid.NewGuid().ToString() + "." + extention;
                    }
                    filePath = Path.Combine(HttpContext.Current.Server.MapPath("~" + model.FileName));
                    file.SaveAs(filePath);
                }
                var result = _healthSaftyServices.SaveSaftyBoard(model);
                if (result > 0)
                {
                    response.Result = Constants.Addsaftyboard; response.StatusCode = HttpStatusCode.OK;
                }
                else
                { response.Result = Constants.Editsaftyboard; response.StatusCode = HttpStatusCode.OK; }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getSaftyBoard/{CompanyID}")]
        public async Task<IHttpActionResult> getSaftyBoard(int CompanyID)
        {
            ServiceStatus<List<SaftyBoard>> response = new ServiceStatus<List<SaftyBoard>>();
            if (CompanyID > 0)
            {
                var result = _healthSaftyServices.GetSaftyBoard(CompanyID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getSaftyBoardbyID/{ID}")]
        public async Task<IHttpActionResult> getSaftyBoardbyID(int ID)
        {
            ServiceStatus<SaftyBoard> response = new ServiceStatus<SaftyBoard>();
            if (ID> 0)
            {
                var result = _healthSaftyServices.GetSaftyBoardbyID(ID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("deleteSaftyBoard")]
        public async Task<IHttpActionResult> deleteSaftyBoard(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _healthSaftyServices.DeleteSaftyBoard(ID);
                response.Result = Constants.DeleteHealthSaftyEmail; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region Safety Board Announcment
        [HttpPost]
        [Route("saveSaftyBoardAnnouncement")]
        public async Task<IHttpActionResult> saveSaftyBoardAnnouncement(SaftyBoardAnnouncement model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null)
            {
                var result = _healthSaftyServices.SaveSaftyBoardAnnouncement(model);
                if (result > 0 && model.ID == 0)
                {
                    response.Result = Constants.Addsaftyboard; response.StatusCode = HttpStatusCode.OK;
                }
                else if (result > 0 && model.ID > 0)
                {
                    response.Result = Constants.Editsaftyboard; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.ErrorMessages = new List<string>() { Constants.Alreadysaftyboard };
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getSaftyBoardAnnouncement/{CompanyID}")]
        public async Task<IHttpActionResult> getSaftyBoardAnnouncement(int CompanyID)
        {
            ServiceStatus<List<SaftyBoardAnnouncement>> response = new ServiceStatus<List<SaftyBoardAnnouncement>>();
            if (CompanyID > 0)
            {
                var result = _healthSaftyServices.GetSaftyBoardAnnouncement(CompanyID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getSaftyBoardAnnouncementbyID/{ID}")]
        public async Task<IHttpActionResult> getSaftyBoardAnnouncementbyID(int ID)
        {
            ServiceStatus<SaftyBoardAnnouncement> response = new ServiceStatus<SaftyBoardAnnouncement>();
            if (ID>0)
            {
                var result = _healthSaftyServices.GetSaftyBoardAnnouncementbyID(ID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("deleteSaftyBoardAnnouncement")]
        public async Task<IHttpActionResult> deleteSaftyBoardAnnouncement(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _healthSaftyServices.DeleteSaftyBoardAnnouncement(ID);
                response.Result = Constants.DeleteHealthSaftyEmail; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region Safety Data sheet
        [HttpPost]
        [Route("saveSaftyDataSheet")]
        public async Task<IHttpActionResult> saveSaftyDataSheet()
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            var request = HttpContext.Current.Request;

            if (request != null && !string.IsNullOrEmpty(request.Form["Supplier"]) && !string.IsNullOrEmpty(request.Form["ChemicalName"])
                && !string.IsNullOrEmpty(request.Form["CompanyID"]) && !string.IsNullOrEmpty(request.Form["ExpirationDate"])
                )
            {
                SaftyDataSheet model = new SaftyDataSheet
                {
                    ID = Convert.ToInt32(request.Form["ID"]),
                    ChemicalName= request.Form["ChemicalName"],
                    ExpirationDate = request.Form["ExpirationDate"],
                    Supplier= request.Form["Supplier"],
                    CompanyID = Convert.ToInt32(request.Form["CompanyID"]),
                    FileName= request.Form["FileName"],
                    CreatedBy = Convert.ToInt32(request.Form["CreatedBy"])
                };
                var file = HttpContext.Current.Request.Files.Count > 0 ?
                          HttpContext.Current.Request.Files[0] : null;
                string filePath = string.Empty;
                if (file != null && file.ContentLength > 0)
                {
                    string extention = file.FileName.Split('.').LastOrDefault();
                    if (string.IsNullOrEmpty(model.FileName))
                    {
                        model.FileName= "/Files/HeathSafety/SDS" + Convert.ToString(request.Form["CompanyID"]) + Guid.NewGuid().ToString() + "." + extention;
                    }
                    filePath = Path.Combine(HttpContext.Current.Server.MapPath("~" + model.FileName));
                    file.SaveAs(filePath);
                }
                var result = _healthSaftyServices.SaveSaftyDataSheet(model);
                if (result > 0)
                {
                    response.Result = Constants.Addsaftyboard; response.StatusCode = HttpStatusCode.OK;
                }
                else
                { response.Result = Constants.Editsaftyboard; response.StatusCode = HttpStatusCode.OK; }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("getSaftyDataSheet/{CompanyID}")]
        public async Task<IHttpActionResult> getSaftyDataSheet(int CompanyID)
        {
            ServiceStatus<List<SaftyDataSheet>> response = new ServiceStatus<List<SaftyDataSheet>>();
            if (CompanyID > 0)
            {
                var result = _healthSaftyServices.GetSaftyDataSheet(CompanyID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getSaftyDataSheetbyID/{ID}")]
        public async Task<IHttpActionResult> getSaftyDataSheetbyID(int ID)
        {
            ServiceStatus<SaftyDataSheet> response = new ServiceStatus<SaftyDataSheet>();
            if (ID > 0)
            {
                var result = _healthSaftyServices.GetSaftyDataSheetbyID(ID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("deleteSaftyDataSheet")]
        public async Task<IHttpActionResult> deleteSaftyDataSheet(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _healthSaftyServices.DeleteSaftyDataSheet(ID);
                response.Result = Constants.DeleteHealthSaftyEmail; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region Lifting Device Certification
        [HttpPost]
        [Route("saveLiftingDeviceCertification")]
        public async Task<IHttpActionResult> saveLiftingDeviceCertification()
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            var request = HttpContext.Current.Request;

            if (request != null && Convert.ToInt32(request.Form["EquipmentTypeID"]) > 0
                && !string.IsNullOrEmpty(request.Form["Supplier"]) && !string.IsNullOrEmpty(request.Form["Model"])
                && !string.IsNullOrEmpty(request.Form["CompanyID"]) && !string.IsNullOrEmpty(request.Form["ExpirationDate"])
                )
            {
                LiftingDeviceCertification model = new LiftingDeviceCertification
                {
                    ID = Convert.ToInt32(request.Form["ID"]),
                    EquipmentTypeID = Convert.ToInt32(request.Form["EquipmentTypeID"]),
                    ExpirationDate = request.Form["ExpirationDate"],
                    Model = request.Form["Model"],
                    Supplier = request.Form["Supplier"],
                    CompanyID = Convert.ToInt32(request.Form["CompanyID"]),
                    FileName= request.Form["FileName"],
                    CreatedBy = Convert.ToInt32(request.Form["CreatedBy"])
                };
                var file = HttpContext.Current.Request.Files.Count > 0 ?
                          HttpContext.Current.Request.Files[0] : null;
                string filePath = string.Empty;
                if (file != null && file.ContentLength > 0)
                {
                    string extention = file.FileName.Split('.').LastOrDefault();
                    if (string.IsNullOrEmpty(model.FileName))
                    {
                        model.FileName= "/Files/HeathSafety/LDC" + Convert.ToString(request.Form["CompanyID"]) + Guid.NewGuid().ToString() + "." + extention;
                    }
                    filePath = Path.Combine(HttpContext.Current.Server.MapPath("~" + model.FileName));
                    file.SaveAs(filePath);
                }
                var result = _healthSaftyServices.SaveLiftingDeviceCertification(model);
                if (model.ID == 0 && result > 0)
                {
                    response.Result = Constants.AddLDCertificate; response.StatusCode = HttpStatusCode.OK;
                }
                else if (model.ID > 0 && result > 0)
                { response.Result = Constants.UpdateLDCertificate; response.StatusCode = HttpStatusCode.OK; }
                else
                {
                    response.Result = Constants.AlreadyExistLDCertificate; response.StatusCode = HttpStatusCode.Conflict;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("getLiftingDeviceCertification/{CompanyID}")]
        public async Task<IHttpActionResult> getLiftingDeviceCertification(int CompanyID)
        {
            ServiceStatus<List<LiftingDeviceCertification>> response = new ServiceStatus<List<LiftingDeviceCertification>>();
            if (CompanyID > 0)
            {
                var result = _healthSaftyServices.GetLiftingDeviceCertification(CompanyID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getLiftingDeviceCertificationbyID/{ID}")]
        public async Task<IHttpActionResult> getLiftingDeviceCertificationbyID(int ID)
        {
            ServiceStatus<LiftingDeviceCertification> response = new ServiceStatus<LiftingDeviceCertification>();
            if (ID > 0)
            {
                var result = _healthSaftyServices.GetLiftingDeviceCertificationbyID(ID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("deleteLiftingDeviceCertification")]
        public async Task<IHttpActionResult> deleteLiftingDeviceCertification(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _healthSaftyServices.DeleteLiftingDeviceCertification(ID);
                response.Result = Constants.DeleteLDCertificate; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region Equipment Type
        [HttpPost]
        [Route("saveEquipmentType")]
        public async Task<IHttpActionResult> saveEquipmentType(EquipmentType model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.Type) && model.CompanyID > 0)
            {
                var result = _healthSaftyServices.SaveEquipmentType(model);
                if (model.ID == 0 && result > 0)
                {
                    response.Result = Constants.AddEquipmentType; response.StatusCode = HttpStatusCode.OK;
                }
                else if (model.ID > 0 && result > 0)
                { response.Result = Constants.UpdateEquipmentType; response.StatusCode = HttpStatusCode.OK; }
                else
                { response.Result = Constants.AlreadyExistEquipmentType; response.StatusCode = HttpStatusCode.OK; }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("getEquipmentType/{CompanyID}")]
        public async Task<IHttpActionResult> getEquipmentType(int CompanyID)
        {
            ServiceStatus<List<EquipmentType>> response = new ServiceStatus<List<EquipmentType>>();
            if (CompanyID > 0)
            {
                var result = _healthSaftyServices.GetEquipmentType(CompanyID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getEquipmentTypebyID/{ID}")]
        public async Task<IHttpActionResult> getEquipmentTypebyID(int ID)
        {
            ServiceStatus<EquipmentType> response = new ServiceStatus<EquipmentType>();
            if (ID > 0)
            {
                var result = _healthSaftyServices.GetEquipmentTypebyID(ID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("deleteEquipmentType")]
        public async Task<IHttpActionResult> deleteEquipmentType(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _healthSaftyServices.DeleteEquipmentType(ID);
                response.Result = Constants.DeleteEquipmentType; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region inspection(h&s/Vehicle/lifting vehicle/machinery) details

        /// <summary>
        /// Get Category list by module name
        /// </summary> 
        /// <param name="ModuleName">HealthSafety/Vehicle/LiftingVehicle/Machinery/Incident/Accident</param>
        /// <returns></returns>        
        [HttpGet]
        [Route("getInsModuleCategoryByName/{ModuleName}")]
        public async Task<IHttpActionResult> getInsModuleCategoryByName(string ModuleName)
        {
            ServiceStatus<List<InspectionModuleCategory>> response = new ServiceStatus<List<InspectionModuleCategory>>();
            if (!string.IsNullOrEmpty(ModuleName))
            {
                var result = _healthSaftyServices.GetInsModuleCategoryByName(ModuleName);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        /// <summary>
        /// Get form list by module name
        /// </summary> 
        /// <param name="CompanyID">Passed Company ID</param>
        /// <param name="ModuleName">HealthSafety/Vehicle/LiftingVehicle/Machinery/Accident</param>
        /// <returns></returns>        
        [HttpGet]
        [Route("getFormListByByName/{CompanyID}/{ModuleName}")]
        public async Task<IHttpActionResult> getFormListByByName(int CompanyID,string ModuleName)
        {
            ServiceStatus<List<InspectionForm>> response = new ServiceStatus<List<InspectionForm>>();
            if (CompanyID>0 && !string.IsNullOrEmpty(ModuleName))
            {
                var result = _healthSaftyServices.GetInsFormlistByName(CompanyID,ModuleName);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getFormListByCategoryId/{CompanyID}/{CategoryID?}")]
        public async Task<IHttpActionResult> getFormListByCategoryId(int CompanyID, int CategoryID=0)
        {
            ServiceStatus<List<InspectionForm>> response = new ServiceStatus<List<InspectionForm>>();
            if (CompanyID > 0 && CategoryID > 0)
            {
                var result = _healthSaftyServices.GetFormListByCategoryId(CategoryID, CompanyID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpGet]
        [Route("getInspectionPriority")]
        public async Task<IHttpActionResult> getInspectionPriority()
        {
            ServiceStatus<List<InspectionPriority>> response = new ServiceStatus<List<InspectionPriority>>();
            var result = _healthSaftyServices.GetInspectionPriority();
            if (result != null && result.Count > 0)
            {
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        /// <summary>
        /// save inspection basic details. 
        /// </summary>     
        /// <param name="model">  
        /// Type: HealthSafety/Vehicle/LiftingVehicle/Machinery/Incident/Accident
        /// EmployeeID: Pass when type Accident 
        /// </param>
        /// <returns></returns>
        ///<response code="200">OK</response>
        [HttpPost]
        [Route("saveInspectionDetails")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<string>))]
        public async Task<IHttpActionResult> saveInspectionDetails(InspectionDetailsRequest model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.Name) && model.CompanyID > 0)
            {
                var result = _healthSaftyServices.SaveInspectionDetails(model);
                if (model.ID == 0 && result > 0)
                {
                    response.ID = result;
                    response.Result = Constants.AddEquipmentType; response.StatusCode = HttpStatusCode.OK;
                }
                else if (model.ID > 0 && result > 0)
                { response.Result = Constants.UpdateEquipmentType; response.StatusCode = HttpStatusCode.OK; }
                else
                { response.Result = Constants.AlreadyExistEquipmentType; response.StatusCode = HttpStatusCode.OK; }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        /// <summary>
        /// Get Inspection by ID
        /// </summary> 
        /// <param name="ID">Passed UniqueID ID</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getInspectionbyID/{ID}")]
        public async Task<IHttpActionResult> getInspectionbyID(int ID)
        {
            ServiceStatus<InspectionDetailsResponse> response = new ServiceStatus<InspectionDetailsResponse>();
            if (ID > 0)
            {
                Inspectiondto model = new Inspectiondto { ID = ID };
                var result = _healthSaftyServices.GetInspectionDetails(model);
                if (result != null && result.Count > 0)
                {
                    response.Result = result.FirstOrDefault(); response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        /// <summary>
        /// Get inspection list
        /// </summary>     
        /// <param name="model">  
        /// Type: HealthSafety/Vehicle/LiftingVehicle/Machinery/Incident/Accident
        /// </param>
        [HttpPost]
        [Route("getInspectionList")]
        public async Task<IHttpActionResult> getInspectionList(Inspectiondto model)
        {
            ServiceStatus<List<InspectionDetailsResponse>> response = new ServiceStatus<List<InspectionDetailsResponse>>();
            if (model != null && !string.IsNullOrEmpty(model.Type) && model.CompanyID > 0)
            {
                var result = _healthSaftyServices.GetInspectionDetails(model);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("saveTempImage")]
        public async Task<IHttpActionResult> saveTempImage()
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            var request = HttpContext.Current.Request;
            string FilePath = "";
            if (request != null && request.Files.Count > 0 && Convert.ToInt32(request.Form["InspectionID"]) > 0 && Convert.ToInt32(request.Form["CompanyID"]) > 0)
            {
                String location = "/Files/tmpimg_" + request.Form["InspectionID"] + "_" + request.Form["CompanyID"] + "/";
                Common.ClearTempDirectory(location, false);
                var file = HttpContext.Current.Request.Files[0];
                if (file != null && file.ContentLength > 0)
                {
                    string extention = file.FileName.Split('.').LastOrDefault();
                    FilePath = string.IsNullOrEmpty(request.Form["FilePath"]) ? (location + Guid.NewGuid().ToString() + "." + extention) : request.Form["FilePath"];
                    location = Path.Combine(HttpContext.Current.Server.MapPath("~" + FilePath));
                    file.SaveAs(location);
                }
                response.Result = FilePath; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("saveInspectionQuestionAnswer/{InspectionID}/{CompanyID}/{FormID}/{CreatedBy}/{ModuleCategoryID?}")]
        public async Task<IHttpActionResult> saveInspectionQuestionAnswer(List<InsQuestion> model,int InspectionID, int CompanyID, int FormID, int CreatedBy, int ModuleCategoryID=0)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && model.Count > 0)
            {
                _healthSaftyServices.SaveInspectionQuestionAnswer(model, InspectionID, CompanyID, FormID, CreatedBy, ModuleCategoryID);
                response.Result = Constants.AddRecord.Replace("{Title}", "Record"); response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
       
        [HttpGet]
        [Route("getInspectionQuestionAnswer/{CompanyID}/{InspectionID}/{FormID}/{CategoryID?}")]
        public async Task<IHttpActionResult> GetInspectionQuestionAnswer(int CompanyID,int InspectionID,int FormID,int CategoryID=0)
        {
            ServiceStatus<List<InsQuestion>> response = new ServiceStatus<List<InsQuestion>>();
            if (FormID > 0 && CompanyID > 0 && InspectionID> 0)
            {
                var result = _healthSaftyServices.GetInspectionQuestionAnswer(CompanyID, InspectionID, FormID,CategoryID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        /// <summary>
        /// Get Incpection detail by Inspectionid
        /// </summary> 
        /// <param name="CompanyID">Passed Company UniqueID</param>
        /// <param name="InspectionID">Passed Inspection -UniqueID.</param>
        /// <param name="Type">HealthSafety/Vehicle/LiftingVehicle/Machinery</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getInspectionDetailsByID/{CompanyID}/{InspectionID}/{Type}")]
        public async Task<IHttpActionResult> getInspectionDetailsByID(int CompanyID, int InspectionID, string Type)
        {
            ServiceStatus<InspectionDetails> response = new ServiceStatus<InspectionDetails>();
            if (!string.IsNullOrEmpty(Type) && CompanyID > 0 && InspectionID > 0)
            {
                var result = _healthSaftyServices.GetInspectionDetailsByID(CompanyID, InspectionID, Type);
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPut]
        [Route("inspectionComplete/{InspectionID}/{CreatedBy}/{Type}")]
        public async Task<IHttpActionResult> inspectionComplete(int InspectionID, int CreatedBy, string Type)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (InspectionID > 0 && CreatedBy > 0)
            {
                 _healthSaftyServices.InspectionComplete(InspectionID, CreatedBy,Type);
                response.Result = Constants.DefaultUpdateConst; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("deleteInspection/{InspectionID}")]
        public async Task<IHttpActionResult> deleteInspection(int InspectionID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (InspectionID > 0)
            {
                _healthSaftyServices.DeleteInspection(InspectionID);
                response.Result = Constants.DefaultDelateConst; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getNonInspectionParticipants/{CompanyID}")]
        public async Task<IHttpActionResult> getNonInspectionParticipants(int CompanyID)
        {
            ServiceStatus<List<HealthAndSaftyEmail>> response = new ServiceStatus<List<HealthAndSaftyEmail>>();
            if (CompanyID > 0)
            {
                var result = _healthSaftyServices.GetNonInspectionParticipants(CompanyID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("saveInspectionParticipants")]
        public async Task<IHttpActionResult> saveInspectionParticipants(Participants model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.Type) && model.CompanyID > 0 && model.InspectionID > 0 && (model.ParticipantsList != null && model.ParticipantsList.Count > 0))
            {
                _healthSaftyServices.saveInspectionParticipants(model);
                response.Result = Constants.AddEquipmentType; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        /// <summary>
        /// Get inspection participant list by each Inspectionid
        /// </summary> 
        /// <param name="CompanyID">Passed Company UniqueID</param>
        /// <param name="InspectionID">Passed Inspection UniqueID.</param>
        /// <param name="Type">HealthSafety/Vehicle/LiftingVehicle/Machinery/Incident/Accident</param>
        /// <returns></returns>       
        [HttpGet]
        [Route("getInspectionParticipants/{CompanyID}/{InspectionID}/{Type}")]
        public async Task<IHttpActionResult> getInspectionParticipants(int CompanyID, int InspectionID, string Type)
        {
            ServiceStatus<Participants> response = new ServiceStatus<Participants>();
            var result = _healthSaftyServices.GetInspectionParticipants(CompanyID, InspectionID, Type);
            if (result != null)
            {
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        /// <summary>
        /// </summary> 
        /// <param name="IsEmailParticipant">Email participant Pass 1 else 0</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getParticipantByInspection/{CompanyID}/{InspectionID}/{IsEmailParticipant?}")]
        public async Task<IHttpActionResult> getParticipantByInspection(int CompanyID, int InspectionID,int IsEmailParticipant = 0)
        {
            ServiceStatus<List<Participantdto>> response = new ServiceStatus<List<Participantdto>>();
            var result = _healthSaftyServices.GetParticipantByInspection(CompanyID, InspectionID, IsEmailParticipant>0);
            if (result != null)
            {
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [HttpPost]
        [Route("sendInspectionParticipantMail")]
        public async Task<IHttpActionResult> sendInspectionParticipantMail(Participants model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.Type) && model.CompanyID > 0 && model.InspectionID > 0 && (model.ParticipantsList != null && model.ParticipantsList.Count > 0))
            {
                _healthSaftyServices.sendInspectionParticipantMail(model);
                response.Result = Constants.MailSent; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("sendEmailParticipantMail")]
        public async Task<IHttpActionResult> sendEmailParticipantMail(Participants model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.Type) && model.CompanyID > 0 && model.InspectionID > 0 && (model.ParticipantsList != null && model.ParticipantsList.Count > 0))
            {
                _healthSaftyServices.sendInspectionParticipantMail(model,0);
                response.Result = Constants.MailSent; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region Incident

        [HttpPost]
        [Route("saveGernralInformation")]
        public async Task<IHttpActionResult> saveGernralInformation(GernralInformationRequestdto model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ModelState.IsValid && model.InspectionID > 0 && model.FormID > 0)
            {
                _healthSaftyServices.SaveGernralInformation(model);
                response.Result = Constants.DefaultUpdateConst; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("saveAllegation")]
        public async Task<IHttpActionResult> saveAllegation(AllegationRequestdto model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ModelState.IsValid && model.InspectionID > 0 && model.FormID > 0)
            {
                _healthSaftyServices.SaveAllegation(model);
                response.Result = Constants.DefaultUpdateConst; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("saveWitness")]
        public async Task<IHttpActionResult> saveWitness(WitnessRequestdto model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ModelState.IsValid && model.InspectionID > 0 && model.FormID > 0)
            {
                _healthSaftyServices.SaveWitness(model);
                response.Result = Constants.DefaultUpdateConst; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("saveResponse")]
        public async Task<IHttpActionResult> saveResponse(ResponseRequestdto model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ModelState.IsValid && model.InspectionID > 0 && model.FormID > 0)
            {
                _healthSaftyServices.SaveResponse(model);
                response.Result = Constants.DefaultUpdateConst; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        
        [HttpGet]
        [Route("getGenralInfomtaion/{InspectionID}/{FormID}")]
        public async Task<IHttpActionResult> getGenralInfomtaion(int InspectionID, int FormID)
        {
            ServiceStatus<System.Data.DataTable> response = new ServiceStatus<System.Data.DataTable>();
            if (InspectionID > 0)
            {
                response.Result = _healthSaftyServices.GetIncidentFormDetail(InspectionID, FormID, 1);
                response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getAlligationDetail/{InspectionID}/{FormID}")]
        public async Task<IHttpActionResult> getAlligationDetail(int InspectionID, int FormID)
        {
            ServiceStatus<System.Data.DataTable> response = new ServiceStatus<System.Data.DataTable>();
            if (InspectionID > 0)
            {
                response.Result = _healthSaftyServices.GetIncidentFormDetail(InspectionID, FormID, 2);
                response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getIncidentWitness/{InspectionID}/{FormID}")]
        public async Task<IHttpActionResult> getIncidentWitness(int InspectionID, int FormID)
        {
            ServiceStatus<System.Data.DataTable> response = new ServiceStatus<System.Data.DataTable>();
            if (InspectionID > 0)
            {
                response.Result = _healthSaftyServices.GetIncidentFormDetail(InspectionID, FormID, 3);
                response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getincidentResponse/{InspectionID}/{FormID}")]
        public async Task<IHttpActionResult> getincidentResponse(int InspectionID, int FormID)
        {
            ServiceStatus<System.Data.DataTable> response = new ServiceStatus<System.Data.DataTable>();
            if (InspectionID > 0)
            {
                response.Result = _healthSaftyServices.GetIncidentFormDetail(InspectionID, FormID);
                response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        /// <summary>
        /// Get Incident list by each Inspectionid
        /// </summary> 
        /// <param name="CompanyID">Passed Company UniqueID</param>
        /// <param name="InspectionID">Passed Inspection -UniqueID.</param>
        /// <param name="Type">Incident</param>
        /// <returns></returns>
        [HttpGet]
        [Route("getIncidentDetailsByID/{CompanyID}/{InspectionID}/{Type}")]
        public async Task<IHttpActionResult> getIncidentDetailsByID(int CompanyID, int InspectionID, string Type)
        {
            ServiceStatus<InspectionDetails> response = new ServiceStatus<InspectionDetails>();
            if (!string.IsNullOrEmpty(Type) && CompanyID > 0 && InspectionID > 0)
            {
                var result = _healthSaftyServices.GetIncidentDetailsByID(CompanyID, InspectionID, Type);
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region Accident
        [HttpPost]
        [Route("saveAccidentDatedetaildto")]
        public async Task<IHttpActionResult> saveAccidentDatedetaildto(AccidentDatedetaildto model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && ModelState.IsValid)
            {
                var returnValue = _healthSaftyServices.SaveAccidentDatedetail(model);
                if (returnValue > 0)
                { response.Result = Constants.DefaultUpdateConst; response.StatusCode = HttpStatusCode.OK; }
                else
                { response.Result = Constants.InternalServerError; response.StatusCode = HttpStatusCode.InternalServerError; }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getAccidentDatedetailByInspectionID/{InspectionID}")]
        public async Task<IHttpActionResult> getAccidentDatedetailByInspectionID(int InspectionID)
        {
            ServiceStatus<AccidentDatedetaildto> response = new ServiceStatus<AccidentDatedetaildto>();
            if (InspectionID > 0)
            {
                var result = _healthSaftyServices.GetAccidentDatedetailByInspectionID(InspectionID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpPost]
        [Route("saveLocationWitness")]
        public async Task<IHttpActionResult> saveLocationWitness(LocationWitnessdto model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && ModelState.IsValid)
            {
                var returnValue = _healthSaftyServices.SaveLocationWitness(model);
                if (returnValue > 0)
                { response.Result = Constants.DefaultUpdateConst; response.StatusCode = HttpStatusCode.OK; }
                else
                { response.Result = Constants.InternalServerError; response.StatusCode = HttpStatusCode.InternalServerError; }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getLocationWitnessByInspectionID/{InspectionID}")]
        public async Task<IHttpActionResult> getLocationWitnessByInspectionID(int InspectionID)
        {
            ServiceStatus<LocationWitnessdto> response = new ServiceStatus<LocationWitnessdto>();
            if (InspectionID > 0)
            {
                var result = _healthSaftyServices.GetLocationWitnessByInspectionID(InspectionID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpPost]
        [Route("saveAccidentHealtCare")]
        public async Task<IHttpActionResult> saveAccidentHealtCare(AccidentHealtCare model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && ModelState.IsValid)
            {
                var returnValue = _healthSaftyServices.SaveAccidentHealtCare(model);
                if (returnValue > 0)
                { response.Result = Constants.DefaultUpdateConst; response.StatusCode = HttpStatusCode.OK; }
                else
                { response.Result = Constants.InternalServerError; response.StatusCode = HttpStatusCode.InternalServerError; }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getAccidentHealtCareByInspectionID/{InspectionID}")]
        public async Task<IHttpActionResult> getAccidentHealtCareByInspectionID(int InspectionID)
        {
            ServiceStatus<AccidentHealtCare> response = new ServiceStatus<AccidentHealtCare>();
            if (InspectionID > 0)
            {
                var result = _healthSaftyServices.GetAccidentHealtCareByInspectionID(InspectionID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
        }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpPost]
        [Route("saveReturnWorkDetail")]
        public async Task<IHttpActionResult> saveReturnWorkDetail(ReturnWorkDetail model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && ModelState.IsValid)
            {
                var returnValue = _healthSaftyServices.SaveReturnWorkDetail(model);
                if (returnValue > 0)
                { response.Result = Constants.DefaultUpdateConst; response.StatusCode = HttpStatusCode.OK; }
                else
                { response.Result = Constants.InternalServerError; response.StatusCode = HttpStatusCode.InternalServerError; }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getReturnWorkDetailByID/{InspectionID}")]
        public async Task<IHttpActionResult> getReturnWorkDetailByID(int InspectionID)
        {
            ServiceStatus<ReturnWorkDetail> response = new ServiceStatus<ReturnWorkDetail>();
            if (InspectionID > 0)
            {
                var result = _healthSaftyServices.GetReturnWorkDetailByID(InspectionID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpPost]
        [Route("saveAccidentWagesInfo")]
        public async Task<IHttpActionResult> saveAccidentWagesInfo(AccidentWagesInfo model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && ModelState.IsValid)
            {
                var returnValue = _healthSaftyServices.SaveAccidentWagesInfo(model);
                if (returnValue > 0)
                { response.Result = Constants.DefaultUpdateConst; response.StatusCode = HttpStatusCode.OK; }
                else
                { response.Result = Constants.InternalServerError; response.StatusCode = HttpStatusCode.InternalServerError; }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getAccidentWagesInfoByID/{InspectionID}")]
        public async Task<IHttpActionResult> getAccidentWagesInfoByID(int InspectionID)
        {
            ServiceStatus<AccidentWagesInfo> response = new ServiceStatus<AccidentWagesInfo>();
            if (InspectionID > 0)
            {
                var result = _healthSaftyServices.GetAccidentWagesInfoByID(InspectionID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpPost]
        [Route("saveAccidentWorkScheduleInfo")]
        public async Task<IHttpActionResult> saveAccidentWorkScheduleInfo(WorkSchedule model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && ModelState.IsValid)
            {
                var returnValue = _healthSaftyServices.SaveAccidentWorkScheduleInfo(model);
                if (returnValue > 0)
                { response.Result = Constants.DefaultUpdateConst; response.StatusCode = HttpStatusCode.OK; }
                else
                { response.Result = Constants.InternalServerError; response.StatusCode = HttpStatusCode.InternalServerError; }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getAccidentWorkScheduleByID/{InspectionID}")]
        public async Task<IHttpActionResult> getAccidentWorkScheduleByID(int InspectionID)
        {
            ServiceStatus<WorkSchedule> response = new ServiceStatus<WorkSchedule>();
            if (InspectionID > 0)
            {
                var result = _healthSaftyServices.GetAccidentWorkScheduleByID(InspectionID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpPost]
        [Route("saveAccidentSignOffInfo")]
        public async Task<IHttpActionResult> saveAccidentSignOffInfo()
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            var request = HttpContext.Current.Request;

            if (request != null && !string.IsNullOrEmpty(request.Form["Title"]) && !string.IsNullOrEmpty(request.Form["Name"])
                && Convert.ToInt32(request.Form["CompanyID"]) > 0 && Convert.ToInt32(request.Form["InspectionID"])>0
                && Convert.ToInt32(request.Form["FormID"]) > 0 && Convert.ToInt32(request.Form["CreatedBy"]) > 0
                && !string.IsNullOrEmpty(request.Form["Telephone"]) && !string.IsNullOrEmpty(request.Form["Ext"])
                )
            {
                AccidentSignOff model = new AccidentSignOff
                {
                    InspectionID = Convert.ToInt32(request.Form["InspectionID"]),
                    FormID = Convert.ToInt32(request.Form["FormID"]),
                    Title = request.Form["Title"],
                    Name = request.Form["Name"],
                    Telephone = request.Form["Telephone"],
                    Ext = request.Form["Ext"],
                    CompanyID = Convert.ToInt32(request.Form["CompanyID"]),
                    SignOffDate=request.Form["SignOffDate"],
                    FilePath = request.Form["FilePath"],
                    AdditionalInfo = request.Form["AdditionalInfo"],
                    CreatedBy = Convert.ToInt32(request.Form["CreatedBy"])
                };
                var file = HttpContext.Current.Request.Files.Count > 0 ?
                          HttpContext.Current.Request.Files[0] : null;
                string filePath = string.Empty;
                if (file != null && file.ContentLength > 0)
                {
                    string extention = file.FileName.Split('.').LastOrDefault();
                    if (string.IsNullOrEmpty(model.FilePath))
                    {
                        model.FilePath = "/Files/HeathSafety/" + Guid.NewGuid().ToString() + "." + extention;
                    }
                    filePath = Path.Combine(HttpContext.Current.Server.MapPath("~" + model.FilePath));
                    file.SaveAs(filePath);
                }
                var result = _healthSaftyServices.SaveAccidentSignOffInfo(model);
                if (result > 0)
                {
                    response.Result = Constants.Addsaftyboard; response.StatusCode = HttpStatusCode.OK;
                }
                else
                { response.Result = Constants.InternalServerError; response.StatusCode = HttpStatusCode.InternalServerError; }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        
        [HttpGet]
        [Route("getAccidentSignOffByID/{InspectionID}")]
        public async Task<IHttpActionResult> getAccidentSignOffByID(int InspectionID)
        {
            ServiceStatus<AccidentSignOff> response = new ServiceStatus<AccidentSignOff>();
            if (InspectionID > 0)
            {
                var result = _healthSaftyServices.GetAccidentSignOffByID(InspectionID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpGet]
        [Route("getAccidentDetailsByID/{CompanyID}/{InspectionID}")]
        public async Task<IHttpActionResult> getAccidentDetailsByID(int CompanyID, int InspectionID)
        {
            ServiceStatus<InspectionDetails> response = new ServiceStatus<InspectionDetails>();
            if (InspectionID > 0)
            {
                var result = _healthSaftyServices.GetAccidentDetailsByID(CompanyID,InspectionID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region JHSC meeting
        [HttpPost]
        [Route("saveJHSCMeeting")]
        public async Task<IHttpActionResult> saveJHSCMeeting(JHSCMeeting model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && ModelState.IsValid)
            {
                var returnValue = _healthSaftyServices.SaveJHSCMeeting(model);
                if (returnValue > 0)
                {
                    response.ID = returnValue;
                    response.Result = Constants.DefaultUpdateConst; response.StatusCode = HttpStatusCode.OK; }
                else
                { response.Result = Constants.InternalServerError; response.StatusCode = HttpStatusCode.InternalServerError; }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getJHSCMeetingbyCompanyID/{CompanyID}")]
        public async Task<IHttpActionResult> getJHSCMeetingbyCompanyID(int CompanyID)
        {
            ServiceStatus<List<JHSCMeeting>> response = new ServiceStatus<List<JHSCMeeting>>();
            if (CompanyID > 0)
            {
                var result = _healthSaftyServices.GetJHSCMeetingbyCompanyID(CompanyID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getJHSCMeetingbyID/{ID}")]
        public async Task<IHttpActionResult> getJHSCMeetingbyID(int ID)
        {
            ServiceStatus<JHSCMeeting> response = new ServiceStatus<JHSCMeeting>();
            if (ID > 0)
            {
                var result = _healthSaftyServices.GetJHSCMeetingbyID(ID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpPost]
        [Route("deleteJHSCMeeting/{ID}")]
        public async Task<IHttpActionResult> deleteJHSCMeeting(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _healthSaftyServices.DeleteJHSCMeeting(ID);
                response.Result = Constants.DefaultDelateConst; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getJHSCUnsafeQuestion/{CompanyID}/{JHSCMeetingID?}")]
        public async Task<IHttpActionResult> GetJHSCUnsafeQuestion(int CompanyID, int JHSCMeetingID = 0)
        {
            ServiceStatus<JHSCMeetingUnsafeQuestion> response = new ServiceStatus<JHSCMeetingUnsafeQuestion>();
            if (CompanyID > 0)
            {
                var result = _healthSaftyServices.GetJHSCUnsafeQuestion(CompanyID,JHSCMeetingID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("saveJHSCUnsafeQuestion/{CompanyID}")]
        public async Task<IHttpActionResult> saveJHSCUnsafeQuestion(List<InspectionUnsafeQuestion> model,int CompanyID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (CompanyID > 0 && model != null)
            {
                _healthSaftyServices.SaveJHSCUnsafeQuestion(model, CompanyID);
                response.Result = Constants.DefaultUpdateConst; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPut]
        [Route("completeJHSCMeeting/{JHSCMeetingID}/{CreatedBy}")]
        public async Task<IHttpActionResult> completeJHSCMeeting(int JHSCMeetingID, int CreatedBy)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (JHSCMeetingID> 0 && CreatedBy > 0)
            {
                _healthSaftyServices.CompleteJHSCMeeting(JHSCMeetingID, CreatedBy);
                response.Result = Constants.DefaultUpdateConst; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("shareInspectionWithjHSCMember/{CompanyID}/{InspectionID}")]
        public async Task<IHttpActionResult> shareInspectionWithjHSCMember(List<InsParticipant> model,int CompanyID, int InspectionID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (CompanyID > 0 && InspectionID > 0 && model != null)
            {
                _healthSaftyServices.ShareInspectionWithjHSCMember(model,CompanyID,InspectionID);
                response.Result = Constants.ShareInspectionWithMember; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPut]
        [Route("shareInspectionWithjHSCMeeting/{CompanyID}/{InspectionID}/{JHSCMeetingID}")]
        public async Task<IHttpActionResult> shareInspectionWithjHSCMember(int CompanyID, int InspectionID, int JHSCMeetingID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (CompanyID > 0 && InspectionID > 0 && JHSCMeetingID>0)
            {
                _healthSaftyServices.ShareInspectionWithjHSCMeeting(CompanyID, InspectionID,JHSCMeetingID);
                response.Result = Constants.ShareInspectionWithMeeting; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region Dashboard API
        [HttpGet]
        [Route("getDashboardDetails/{CompanyID}")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<Dashboard>))]
        public async Task<IHttpActionResult> getDashboardDetails(int CompanyID)
        {
            ServiceStatus<Dashboard> response = new ServiceStatus<Dashboard>();
            if (CompanyID > 0)
            {
                var result = _healthSaftyServices.GetDashboardDetails(CompanyID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region WSIB Form
        [HttpGet]
        [Route("genrateWSIB/{ID}")]
        public async Task<IHttpActionResult> genrateWSIB(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                Common.ClearTempDirectory("/Files/tmp/");
                DataSet ds = _healthSaftyServices.GetHealthSafetyWSIBDetails(ID);
                DataRow dr = ds.Tables[0].Rows[0];
                string fileName = "WSIB" + ID + "_" + DateTime.Now.ToString("MMM_dd_yyyy");

                string path = HttpContext.Current.Server.MapPath("~/Files/Template/WSIB.html");
                string strFileData = File.ReadAllText(path);

                #region Replace Token
                var DOB = Common.ConvertDateToDDMMYY(dr["DateOfBirth"] + "");
                var DOH = Common.ConvertDateToDDMMYY(dr["HireDate"] + "");
                var DOI = Common.ConvertDateToDDMMYY(dr["DateOfInspection"] + "");
                var DOR = Common.ConvertDateToDDMMYY(dr["DateReported"] + "");
                var RHC = Common.ConvertDateToDDMMYY(dr["HealthCareDate"] + "");
                var ERHC = Common.ConvertDateToDDMMYY(dr["EmployerlearnReceivedHealthCare"] + "");
                var WLT = Common.ConvertDateToDDMMYY(dr["WorkerLostTimeDate"] + "");
                var RWD = Common.ConvertDateToDDMMYY(dr["ReturnToWorkDate"] + "");
                var LWD = Common.ConvertDateToDDMMYY(dr["LastWorkDate"] + "");
                string strDiseasesType = "", strAccidentType = "", strWorktreated = "", strAwarenessOfIllness = "";
                foreach (DataRow row in ds.Tables[1].Rows)
                {
                    strDiseasesType += @"<tr>
                                            <td>
                                                <input type='checkbox' " + (row["Checked"].ToString() == "0" ? "" : "checked='checked'") + @">
                                                <small style='font-weight:600; margin:0;'> " + row["Type"] + @" </small>
                                            </td>
                                        </tr>";
                }
                foreach (DataRow row in ds.Tables[2].Rows)
                {
                    if ("0,3,6".Contains(ds.Tables[2].Rows.IndexOf(row).ToString()))
                    { strAccidentType += "<tr>"; }
                    if (ds.Tables[2].Rows.IndexOf(row) != 8)
                    {
                        strAccidentType += @"<td>
                                            <input type='checkbox' " + (row["Checked"].ToString() == "0" ? "" : "checked='checked'") + @">
                                            <small style='font-weight:600; margin:0;'> " + row["Type"] + @"  </small>
                                        </td>";
                    }
                    else
                    {
                        strAccidentType += @"<td>
                                            <small style='font-weight:600; margin:0;'> Other </small>
                                            <input style='box-sizing: border-box;height:22px;border:0;background:#e6f2fb; width:80%; padding:4px;' type='text' value='" + dr["AccidentTypeOther"] + "" + @"'>
                                        </td>";
                    }
                    if ("2,5,8".Contains(ds.Tables[2].Rows.IndexOf(row).ToString()))
                    { strAccidentType += "</tr>"; }
                }
                foreach (DataRow row in ds.Tables[3].Rows)
                {
                    strWorktreated += @"<td><input type='checkbox' " + (row["Checked"].ToString() == "0" ? "" : "checked='checked'") + @">
                                            <small style='font-weight:600'> " + row["PlaceType"] + @" </small>
                                       </td>";
                }
                foreach (DataRow row in ds.Tables[4].Rows)
                {
                    strAwarenessOfIllness += @"<tr>
                                                <td style='padding: 5px;'><input type='checkbox' style='float: left;' " + (row["Checked"].ToString() == "0" ? "" : "checked='checked'") + @">
                                                <small style='font-weight:600; margin:0;float: left;padding: 2px 0 0 0;'> " + row["Name"] + @" </small></td>
                                            </tr>";
                }
                strFileData = strFileData.Replace("{LastName}", dr["LastName"] + "").Replace("{FirstName}", dr["FirstName"] + "").Replace("{Address}", dr["EmpAddress"] + "").Replace("{EmployeeName}", dr["FirstName"] + "" + " " + dr["LastName"] + "")
                    .Replace("{City}", dr["EmpCity"] + "").Replace("{Province}", dr["EmpState"] + "").Replace("{PostalCode}", dr["EmpPostalCode"] + "").Replace("{Male}", (dr["Sex"] + "").Trim() == "M" ? "Checked='Checked'" : "")
                    .Replace("{Female}", (dr["Sex"] + "").Trim() == "F" ? "checked='checked'" : "").Replace("{Telephone}", dr["EmpPhone"] + "").Replace("{CompanyName}", dr["CompanyName"] + "")
                    .Replace("{DOB_dd}", DOB.Item1).Replace("{DOB_mm}", DOB.Item2).Replace("{DOB_yy}", DOB.Item3)
                    .Replace("{DOH_dd}", DOH.Item1).Replace("{DOH_mm}", DOH.Item2).Replace("{DOH_yy}", DOH.Item3).Replace("{CompanyAddress}", dr["CompAddress"] + "").Replace("{CompanyCity}", dr["CompCity"] + "")
                    .Replace("{CompnayProvince}", dr["CompState"] + "").Replace("{CompnayPostalCode}", dr["CompPostalCode"] + "").Replace("{CompnayTelephone}", dr["CompTelephone"] + "").Replace("{MoreThen20}", "")
                    .Replace("{LessThen20}", "").Replace("{CompanyFax}", dr["CompFax"] + "").Replace("{DOI_dd}", DOI.Item1).Replace("{DOI_mm}", DOI.Item2).Replace("{DOI_yy}", DOI.Item3)
                    .Replace("{DOI_Time}", dr["TimeOfInspection"] + "").Replace("{DOI_AM}", dr["InspectionTimePeriod"] + "" == "AM" ? "checked='checked'" : "").Replace("{DOI_PM}", dr["InspectionTimePeriod"] + "" == "PM" ? "checked='checked'" : "")
                    .Replace("{DOI_ResponsiblePersone}", dr["DOI_ResponsiblePersone"] + "").Replace("{DOR_dd}", DOR.Item1).Replace("{DOR_mm}", DOR.Item2).Replace("{DOR_yy}", DOR.Item3)
                    .Replace("{DOR_Time}", dr["TimeReported"] + "")
                    .Replace("{DiseasesType}", strDiseasesType).Replace("{AccidentType}", strAccidentType).Replace("{AccidentDescription}", "").Replace("{AccidentHappendInsidePremises_Yes}", dr["AccidentHappendInsidePremises"] + "" == "0" ? "" : "checked='checked'")
                    .Replace("{AccidentHappendInsidePremises_No}", dr["AccidentHappendInsidePremises"] + "" == "0" ? "checked='checked'" : "").Replace("{AccidentHappendInsidePremisesDescription}", dr["AccidentHappendInsidePremisesDescription"] + "")
                    .Replace("{AccidentOutSideProvince_Yes}", dr["AccidentOutSideProvince"] + "" == "0" ? "" : "checked='checked'").Replace("{AccidentOutSideProvince_No}", dr["AccidentOutSideProvince"] + "" == "0" ? "checked='checked'" : "")
                    .Replace("{AccidentOutSideProvinceDescription}", dr["AccidentOutSideProvinceDescription"] + "").Replace("{AwarenessAnyWitness_Yes}", dr["AwarenessAnyWitness"] + "" == "0" ? "" : "checked='checked'").Replace("{AwarenessAnyWitness_No}", dr["AwarenessAnyWitness"] + "" == "0" ? "checked='checked'" : "")
                    .Replace("{AwarenessWitnessDescription}", dr["AwarenessWitnessDescription"] + "").Replace("{ResonposibleForAccident_Yes}", dr["ResonposibleForAccident"] + "" == "0" ? "" : "checked='checked'")
                    .Replace("{ResonposibleForAccident_No}", dr["ResonposibleForAccident"] + "" == "0" ? "checked='checked'" : "").Replace("{ResonposibleDescription}", dr["ResonposibleDescription"] + "")
                    .Replace("{AwareAnyInjury_Yes}", dr["AwareAnyInjury"] + "" == "0" ? "" : "checked='checked'").Replace("{AwareAnyInjury_No}", dr["AwareAnyInjury"] + "" == "0" ? "checked='checked'" : "")
                    .Replace("{AwareAnyInjuryDescription}", dr["AwareAnyInjuryDescription"] + "").Replace("{ConcernClaim}", dr["ConcernClaim"] + "" == "0" ? "" : "checked='checked'")
                    .Replace("{ReceiveHealthCareForInjury_Yes}", dr["ReceiveHealthCareForInjury"] + "" == "0" ? "" : "checked='checked'").Replace("{ReceiveHealthCareForInjury_No}", dr["ReceiveHealthCareForInjury"] + "" == "0" ? "checked='checked'" : "")
                    .Replace("{RHC_dd}", RHC.Item1).Replace("{RHC_mm}", RHC.Item2).Replace("{RHC_yy}", RHC.Item3).Replace("{RHC_time}", "").Replace("{ERHC_dd}", ERHC.Item1).Replace("{ERHC_mm}", ERHC.Item2).Replace("{ERHC_yy}", ERHC.Item3).Replace("{ERHC_time}", "")
                    .Replace("{WorkerTreated}", strWorktreated).Replace("{AddressOfTreatedPerson}", dr["AddressOfTreatedPerson"] + "").Replace("{WorkerTreated_Other}", "").Replace("{AwarenessOfIllnessID}", strAwarenessOfIllness)
                    .Replace("{WLT_dd}", WLT.Item1).Replace("{WLT_mm}", WLT.Item2).Replace("{WLT_yy}", WLT.Item3).Replace("{RWD_dd}", RWD.Item1).Replace("{RWD_mm}", RWD.Item2).Replace("{RWD_yy}", RWD.Item3)
                    .Replace("{WorkType_regular}", dr["WorkType"] + "" == "1" ? "checked='checked'" : "").Replace("{WorkType_modified}", dr["WorkType"] + "" == "0" ? "checked='checked'" : "").Replace("{ConfirmBy}", dr["ConfirmBy"] + "" == "0" ? "" : "checked='checked'")
                    .Replace("{HealthCare_Name}", dr["HealthCare_Name"] + "").Replace("{HealthCare_Telephone}", dr["HealthCare_Telephone"] + "").Replace("{HealthCare_Ext}", "")
                    .Replace("{WorkLimitationForInjury_Yes}", dr["WorkLimitationForInjury"] + "" == "1" ? "checked='checked'" : "").Replace("{WorkLimitationForInjury_No}", dr["WorkLimitationForInjury"] + "" == "0" ? "checked='checked'" : "")
                    .Replace("{ModifiedworkDiscuss_Yes}", dr["ModifiedworkDiscuss"] + "" == "1" ? "checked='checked'" : "").Replace("{ModifiedworkDiscuss_No}", dr["ModifiedworkDiscuss"] + "" == "0" ? "checked='checked'" : "")
                    .Replace("{OfferAnyWork_Yes}", dr["OfferAnyWork"] + "" == "1" ? "checked='checked'" : "").Replace("{OfferAnyWork_No}", dr["OfferAnyWork"] + "" == "0" ? "checked='checked'" : "")
                    .Replace("{WorkAccepted_Yes}", dr["WorkAccepted"] + "" == "1" ? "checked='checked'" : "").Replace("{WorkAccepted_No}", dr["WorkAccepted"] + "" == "0" ? "checked='checked'" : "")
                    .Replace("{ResponsibleForArangingWorker_MySelf}", dr["ResponsibleForArangingWorker"] + "" == "1" ? "checked='checked'" : "").Replace("{ResponsibleForArangingWorker_Other}", dr["ResponsibleForArangingWorker"] + "" == "2" ? "checked='checked'" : "")
                    .Replace("{ReturnWorkDetail_Name}", dr["ReturnWorkDetail_Name"] + "").Replace("{ReturnWorkDetail_Telephone}", dr["ReturnWorkDetail_Telephone"] + "").Replace("{ReturnWorkDetail_Ext}", "").Replace("{Federal}", dr["Federal"] + "").Replace("{Provincial}", dr["Provincial"] + "")
                    .Replace("{VactionPayCheck_Yes}", dr["VactionPayCheck"] + "" == "1" ? "checked='checked'" : "").Replace("{VactionPayCheck_No}", dr["VactionPayCheck"] + "" == "0" ? "checked='checked'" : "").Replace("{VactionPayCheckPercentage}", dr["VactionPayCheckPercentage"] + "")
                    .Replace("{LWD_dd}", LWD.Item1).Replace("{LWD_mm}", LWD.Item2).Replace("{LWD_yy}", LWD.Item3).Replace("{LastDayWorkFrom}", dr["LastworkTime"] + "").Replace("{LastDayWorkTo}", dr["LastDayWorkTo"] + "")
                    .Replace("{LastDayWorkTo_AM}", dr["LastDayWorkToPeriod"] + "" == "AM" ? "checked='checked'" : "").Replace("{LastDayWorkTo_PM}", dr["LastDayWorkToPeriod"] + "" == "PM" ? "checked='checked'" : "")
                    .Replace("{ActualEarning}", dr["ActualEarning"] + "").Replace("{NormalEarning}", dr["NormalEarning"] + "").Replace("{AdvanceWagePaid_Yes}", dr["AdvanceWagePaid"] + "" == "1" ? "checked='checked'" : "").Replace("{AdvanceWagePaid_No}", dr["AdvanceWagePaid"] + "" == "0" ? "checked='checked'" : "")
                    .Replace("{FullRegular_Yes}", dr["FullRegular"] + "" == "0" ? "" : "checked='checked'").Replace("{Other_Yes}", dr["FullRegular"] + "" == "0" ? "" : "checked='checked'")
                    .Replace("{Wages_other}", dr["Wages_other"] + "").Replace("{Week1FromDate}", Common.DateToString(dr["Week1FromDate"])).Replace("{Week1ToDate}", Common.DateToString(dr["Week1ToDate"])).Replace("{MandatoryOverTimePay1}", dr["MandatoryOverTimePay1"] + "")
                    .Replace("{VoluntaryOverTimePay1}", dr["VoluntaryOverTimePay1"] + "").Replace("{Week2FromDate}", Common.DateToString(dr["Week2FromDate"])).Replace("{Week2ToDate}", Common.DateToString(dr["Week2ToDate"])).Replace("{MandatoryOverTimePay2}", dr["MandatoryOverTimePay2"] + "")
                    .Replace("{VoluntaryOverTimePay2}", dr["VoluntaryOverTimePay2"] + "").Replace("{Week3FromDate}", Common.DateToString(dr["Week3FromDate"])).Replace("{Week3ToDate}", Common.DateToString(dr["Week3ToDate"])).Replace("{MandatoryOverTimePay3}", dr["MandatoryOverTimePay3"] + "")
                    .Replace("{VoluntaryOverTimePay3}", dr["VoluntaryOverTimePay3"] + "").Replace("{Week4FromDate}", Common.DateToString(dr["Week4FromDate"])).Replace("{Week4ToDate}", Common.DateToString(dr["Week4ToDate"])).Replace("{MandatoryOverTimePay4}", dr["MandatoryOverTimePay4"] + "")
                    .Replace("{VoluntaryOverTimePay4}", dr["VoluntaryOverTimePay4"] + "").Replace("{SundayHour}", dr["SundayHour"] + "").Replace("{MondayHour}", dr["ModayHour"] + "").Replace("{TuesdayHour}", dr["TuesdayHour"] + "").Replace("{WednesdayHour}", dr["Wednesday"] + "")
                    .Replace("{ThrusdayHour}", dr["ThrusdayHour"] + "").Replace("{FridayHour}", dr["FridayHour"] + "").Replace("{SaturdayHour}", dr["SaturdayHour"] + "").Replace("{DayOn}", dr["DayOn"] + "").Replace("{DayOff}", dr["DayOff"] + "").Replace("{HourPerShift}", dr["HourPerShift"] + "")
                    .Replace("{NumberOfWeek}", dr["NumberOfWeek"] + "")
                    .Replace("{Week1FromDate}", Common.DateToString(dr["Week1FromDate"])).Replace("{Week1ToDate}", Common.DateToString(dr["Week1ToDate"])).Replace("{Week1TotalHourWork}", dr["Week1TotalHourWork"] + "").Replace("{Week1TotalShiftWork}", dr["Week1TotalShiftWork"] + "")
                    .Replace("{Week2FromDate}", Common.DateToString(dr["Week2FromDate"])).Replace("{Week2ToDate}", Common.DateToString(dr["Week2ToDate"])).Replace("{Week2TotalHourWork}", dr["Week2TotalHourWork"] + "").Replace("{Week2TotalShiftWork}", dr["Week2TotalShiftWork"] + "")
                    .Replace("{Week3FromDate}", Common.DateToString(dr["Week3FromDate"])).Replace("{Week3ToDate}", Common.DateToString(dr["Week3ToDate"])).Replace("{Week3TotalHourWork}", dr["Week3TotalHourWork"] + "").Replace("{Week3TotalShiftWork}", dr["Week3TotalShiftWork"] + "")
                    .Replace("{Week4FromDate}", Common.DateToString(dr["Week4FromDate"])).Replace("{Week4ToDate}", Common.DateToString(dr["Week4ToDate"])).Replace("{Week4TotalHourWork}", dr["Week4TotalHourWork"] + "").Replace("{Week4TotalShiftWork}", dr["Week4TotalShiftWork"] + "")
                    .Replace("{CompletedReportedBy}", "").Replace("{OfficialTitle}", "").Replace("{CompletedTelephone}", "").Replace("{CompletedExt}", "")
                    .Replace("{Completed_dd}", "").Replace("{Completed_mm}", "").Replace("{Completed_yy}", "").Replace("{DOR_AM}", dr["TimeReportedPeriod"] + "" == "AM" ? "checked='checked'" : "").Replace("{DOR_PM}", dr["TimeReportedPeriod"] + "" == "PM" ? "checked='checked'" : "")
                    .Replace("{LastDayWorkFrom_AM}", dr["LastDayWorkFromPeriod"] + "" == "AM" ? "checked='checked'" : "").Replace("{LastDayWorkFrom_PM}", dr["LastDayWorkFromPeriod"] + "" == "PM" ? "checked='checked'" : "")
                    ;
                #endregion

                var htmlToPdf = new HtmlToPdfConverter();
                var pdfBytes = htmlToPdf.GeneratePdf(strFileData);
                System.IO.File.WriteAllBytes(HttpContext.Current.Server.MapPath("~/Files/tmp/") + fileName + ".pdf", pdfBytes);
                response.Result = Convert.ToString(ConfigurationManager.AppSettings["APIBaseUrl"])+ "/api/common/downlaod/" + fileName + "/pdf"; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion
    }
}
