﻿using EMIPayrol.Helpers;
using EMIPayrolModel;
using EMIPayrolServices;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace EMIPayrol.Controllers
{
    //[Authorize]
    [GlobalExceptionAttribute]
    [RoutePrefix("api/common")]
    public class CommonController : ApiController
    {
        private readonly ICommonServices _commonServices;
        public CommonController(ICommonServices commonServices)
        {
            _commonServices = commonServices;
        }

        readonly ICommonServices commonServices = new CommonServices();
        
        public CommonController()
        {
            _commonServices = commonServices;
        }

        [HttpPost]        [Route("UploadFile")]        public async Task<IHttpActionResult> UploadFile()        {            //CResponse lobjResponse = new CResponse();            var request = HttpContext.Current.Request;            string path = HttpContext.Current.Server.MapPath("~/UploadedFiles/");            if (!Directory.Exists(path))            {                Directory.CreateDirectory(path);            }            Guid guid = Guid.NewGuid();

            var file = HttpContext.Current.Request.Files.Count > 0 ?
                        HttpContext.Current.Request.Files[0] : null;
            string filePath = string.Empty;
            if (file != null && file.ContentLength > 0)
            {
                string FileName = Path.GetFileNameWithoutExtension(file.FileName);
                string NewFileName = FileName + "_" + guid + Path.GetExtension(file.FileName);
                string OrgFileName = file.FileName;

                var fileSavePath = "/UploadedFiles/" + NewFileName;

                file.SaveAs(path + NewFileName);

                //lobjResponse.Message = NewFileName;
            }

            return Ok();        }

        #region country-state-city
        [AllowAnonymous]
        [HttpGet]
        [Route("getCountry")]
        public async Task<IHttpActionResult> getCountryList()
        {
            ServiceStatus<List<Country>> response = new ServiceStatus<List<Country>>();
            var modelCountry = _commonServices.getCounteryList();
            response.Result = modelCountry;
            response.StatusCode = HttpStatusCode.OK;
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("saveCountry")]
        public async Task<IHttpActionResult> saveCountry(Country model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.Name))
            {
                var result = _commonServices.SaveCountry(model);
                if (result > 0 && model.ID == 0)
                {
                    response.Result = Constants.AddRecord.Replace("{Title}","Country"); response.StatusCode = HttpStatusCode.OK;
                }
                else if (result > 0 && model.ID >0)
                {
                    response.Result = Constants.AddRecord.Replace("{Title}", "Country"); response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.Result = Constants.AlreadyRecordExist.Replace("{Title}", "country");
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("getStateByCountryID/{CountryID}")]
        public async Task<IHttpActionResult> getStateList(int CountryID)
        {
            ServiceStatus<List<State>> response = new ServiceStatus<List<State>>();
            var modelState = _commonServices.getStateList(CountryID);
            response.Result = modelState;
            response.StatusCode = HttpStatusCode.OK;
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("saveState")]
        public async Task<IHttpActionResult> saveState(State model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.Name)&& model.CountryID>0)
            {
                var result = _commonServices.SaveState(model);
                if (result > 0 && model.ID == 0)
                {
                    response.Result = Constants.AddRecord.Replace("{Title}", "State"); response.StatusCode = HttpStatusCode.OK;
                }
                else if (result > 0 && model.ID > 0)
                {
                    response.Result = Constants.AddRecord.Replace("{Title}", "State"); response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.Result = Constants.AlreadyRecordExist.Replace("{Title}", "state");
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpGet]
        [Route("getCityByStateID/{StateID}")]
        public async Task<IHttpActionResult> getCityByStateID(int StateID)
        {
            ServiceStatus<List<City>> response = new ServiceStatus<List<City>>();
            var modelCity = _commonServices.getCityByStateID(StateID);
            response.Result = modelCity;
            response.StatusCode = HttpStatusCode.OK;
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("saveCity")]
        public async Task<IHttpActionResult> saveCity(City model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.Name) && model.StateID > 0)
            {
                var result = _commonServices.SaveCIty(model);
                if (result > 0 && model.ID == 0)
                {
                    response.Result = Constants.AddRecord.Replace("{Title}", "City"); response.StatusCode = HttpStatusCode.OK;
                }
                else if (result > 0 && model.ID > 0)
                {
                    response.Result = Constants.AddRecord.Replace("{Title}", "City"); response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.Result = Constants.AlreadyRecordExist.Replace("{Title}", "city");
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        #endregion country-state-city

        #region Department
        [HttpPost]
        [Route("saveDepartment")]
        public async Task<IHttpActionResult> saveDepartment(Department model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.Name) && model.IndustryID > 0)
            {
                var result = _commonServices.SaveDepartment(model);
                if (result > 0 && model.ID == 0)
                {
                    response.Result = Constants.AddDepartment; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.Result = Constants.AlreadyDepartmentExist;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("updateDepartment")]
        public async Task<IHttpActionResult> updateDepartment(Department model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.Name) && model.ID > 0 && model.IndustryID > 0)
            {
                var result = _commonServices.SaveDepartment(model);
                if (result > 0 && model.ID > 0)
                { response.Result = Constants.EditDepartment; response.StatusCode = HttpStatusCode.OK; }
                else
                {response.StatusCode = HttpStatusCode.Conflict;response.Result = Constants.AlreadyDepartmentExist;}
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpGet]
        [Route("getDepartment/{IdustryID}")]
        public async Task<IHttpActionResult> getDepartment(int IdustryID)
        {
            ServiceStatus<List<Department>> response = new ServiceStatus<List<Department>>();
            if (IdustryID > 0)
            {
                var model = _commonServices.getDepartment(IdustryID);
                if (model != null)
                {response.Result = model; response.StatusCode = HttpStatusCode.OK;}
                else
                {response.Result = null;response.StatusCode = HttpStatusCode.NoContent;}
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getDepartmentByID/{ID}")]
        public async Task<IHttpActionResult> getDepartmentByID(int ID)
        {
            ServiceStatus<Department> response = new ServiceStatus<Department>();
            if (ID > 0)
            {
                var model = _commonServices.getDepartment(0, ID);
                if (model != null)
                {response.Result = model.FirstOrDefault(); response.StatusCode = HttpStatusCode.OK;}
                else
                {response.Result = null;response.StatusCode = HttpStatusCode.NoContent;}
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("deleteDepartment")]
        public async Task<IHttpActionResult> deleteDepartment(CommonClass model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && model.ID > 0)
            {
                _commonServices.DeleteDepartment(model.ID);
                response.Result = Constants.DeleteDepartment; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion Department

        #region Role
        [HttpPost]
        [Route("SaveRole")]
        public async Task<IHttpActionResult> saveRole(Role model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.Name)&&model.IndustryID>0)
            {
                var result = commonServices.SaveUpdateRoles(model);
                if (result > 0 && model.ID == 0)
                {
                    response.Result = Constants.AddRole; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.ErrorMessages = new List<string>() { Constants.AlreadyRoleExist};
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("UpdateRole")]
        public async Task<IHttpActionResult> UpdateRole(Role model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.Name) && model.ID> 0 && model.IndustryID > 0)
            {
                var result = _commonServices.SaveUpdateRoles(model);
                if (result > 0 && model.ID > 0) { response.Result = Constants.EditRole; response.StatusCode = HttpStatusCode.OK; }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.ErrorMessages = new List<string>() { Constants.AlreadyRoleExist};
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpGet]
        [Route("getRolelist/{IndustryID}")]
        public async Task<IHttpActionResult> getRole(int IndustryID)
        {
            ServiceStatus<List<Role>> response = new ServiceStatus<List<Role>>();
            if (IndustryID > 0)
            {
                var model = _commonServices.getRoles(IndustryID);
                if (model != null)
                {
                    response.Result = model; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getRoleByID/{ID}")]
        public async Task<IHttpActionResult> getRoleByID(int ID)
        {
            ServiceStatus<Role> response = new ServiceStatus<Role>();
            if (ID > 0)
            {
                var model = _commonServices.getRoles(ID);
                if (model != null && model.Count > 0)
                {
                    response.Result = model.FirstOrDefault(); response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("deleteRole")]
        public async Task<IHttpActionResult> deleteRole(CommonClass model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && model.ID > 0)
            {
                _commonServices.DeleteRole(model.ID);
                response.Result = Constants.DeletRole; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion Role

        #region AppModule
        [HttpPost]
        [Route("SaveAppModule")]
        public async Task<IHttpActionResult> saveAppModule(AppModule model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.Name))
            {
                var result = commonServices.SaveUpdateAppModule(model);
                if (result > 0 && model.ID == 0)
                {
                    response.Result = Constants.AddAppModule; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.ErrorMessages = new List<string>() { Constants.AlreadyAppModuleExist };
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("UpdateAppModule")]
        public async Task<IHttpActionResult> UpdateAppModule(AppModule model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.Name) && model.ID > 0)
            {
                var result = _commonServices.SaveUpdateAppModule(model);
                if (result > 0 && model.ID > 0) { response.Result = Constants.EditAppModule; response.StatusCode = HttpStatusCode.OK; }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.ErrorMessages = new List<string>() { Constants.AlreadyAppModuleExist };
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getAppModulelist")]
        public async Task<IHttpActionResult> getAppModule()
        {
            ServiceStatus<List<AppModule>> response = new ServiceStatus<List<AppModule>>();
            var model = _commonServices.getAppModule();
            if (model != null)
            {
                response.Result = model; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [HttpGet]
        [Route("getAppModuleByID/{ID?}")]
        public async Task<IHttpActionResult> getAppModuleByID(int ID)
        {
            ServiceStatus<AppModule> response = new ServiceStatus<AppModule>();
            var model = _commonServices.getAppModule(ID);
            if (model != null && model.Count > 0)
            {
                response.Result = model.FirstOrDefault(); response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [HttpPost]
        [Route("deleteAppModule")]
        public async Task<IHttpActionResult> deleteAppModule(CommonClass model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && model.ID > 0)
            {
                _commonServices.DeleteAppModule(model.ID);
                response.Result = Constants.DeletAppModule; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion AppModule

        #region Industry
        
        [HttpPost]
        [Route("SaveIndustry")]
        public async Task<IHttpActionResult> saveIndustry(Industry model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.Name))
            {
                var result = commonServices.SaveUpdateIndustry(model);
                if (result > 0 && model.ID == 0)
                {
                    response.Result = Constants.AddIndustry; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.ErrorMessages = new List<string>() { Constants.AlreadyIndustryExist };
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        
        [HttpPost]
        [Route("UpdateIndustry")]
        public async Task<IHttpActionResult> UpdateIndustry(Industry model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.Name) && model.ID > 0)
            {
                var result = _commonServices.SaveUpdateIndustry(model);
                if (result > 0 && model.ID > 0) { response.Result = Constants.EditIndustry; response.StatusCode = HttpStatusCode.OK; }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.ErrorMessages = new List<string>() { Constants.AlreadyIndustryExist };
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpGet]
        [Route("getIndustrylist/{CompanyID?}")]
        public async Task<IHttpActionResult> getIndustry(int CompanyID = 0)
        {
            ServiceStatus<List<Industry>> response = new ServiceStatus<List<Industry>>();
            var model = _commonServices.getIndustry(0,CompanyID);
            if (model != null)
            {
                response.Result = model; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpGet]
        [Route("getIndustryByID/{ID}")]
        public async Task<IHttpActionResult> getIndustryByID(int ID)
        {
            ServiceStatus<Industry> response = new ServiceStatus<Industry>();
            if (ID > 0)
            {
                var model = _commonServices.getIndustry(ID);
                if (model != null && model.Count > 0)
                {
                    response.Result = model.FirstOrDefault(); response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        
        [HttpPost]
        [Route("deleteIndustry")]
        public async Task<IHttpActionResult> deleteIndustry(CommonClass model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && model.ID > 0)
            {
                _commonServices.DeleteIndustry(model.ID);
                response.Result = Constants.DeletIndustry; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion Industry

        #region EmailSetting
        [HttpPost]
        [Route("saveEmailSetting")]
        public async Task<IHttpActionResult> saveEmailSetting(EmailSetting model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null)
            {
                var result = _commonServices.SaveUpdateEmailSetting(model);
                if (result > 0 && model.ID == 0) { response.Result = Constants.AddEmailSetting; response.StatusCode = HttpStatusCode.OK; }
                else if (result > 0 && model.ID >0) { response.Result = Constants.EditEmailSetting; response.StatusCode = HttpStatusCode.OK; }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.ErrorMessages = new List<string>() { Constants.AlreadyEmailSettingExist };
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getEmailSetting/{CompanyID}")]
        public async Task<IHttpActionResult> getEmailSetting(int CompanyID)
        {
            ServiceStatus<EmailSetting> response = new ServiceStatus<EmailSetting>();
            if (CompanyID > 0)
            {
                var model = _commonServices.getEmailSetting(CompanyID);
                if (model != null)
                {
                    response.Result = model; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion EmailSetting

        #region SalarySetting
        [HttpPost]
        [Route("saveSalarySetting")]
        public async Task<IHttpActionResult> saveSalarySetting(SalarySetting model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null)
            {
                var result = _commonServices.SaveUpdateSalarySetting(model);
                if (result > 0 && model.ID == 0) { response.Result = Constants.AddSalarySetting; }
                else { response.Result = Constants.EditSalarySetting; }
                response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getSalarySetting/{CompanyID}")]
        public async Task<IHttpActionResult> getSalarySetting(int CompanyID)
        {
            ServiceStatus<SalarySetting> response = new ServiceStatus<SalarySetting>();
            if (CompanyID > 0)
            {
                var model = _commonServices.getSalarySetting(CompanyID);
                if (model != null)
                {
                    response.Result = model; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion SalarySetting

        #region AdditionalRequirment
        [HttpPost]
        [Route("SaveAdditionalRequirment")]
        public async Task<IHttpActionResult> saveAdditionalRequirment(AdditionalRequirment model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.Name))
            {
                var result = commonServices.SaveUpdateAdditionalRequirment(model);
                if (result > 0 && model.ID == 0)
                {
                    response.Result = Constants.AddAdditionalRequirment; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.ErrorMessages = new List<string>() { Constants.AlreadyAdditionalRequirmentExist };
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("UpdateAdditionalRequirment")]
        public async Task<IHttpActionResult> UpdateAdditionalRequirment(AdditionalRequirment model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.Name) && model.ID > 0)
            {
                var result = _commonServices.SaveUpdateAdditionalRequirment(model);
                if (result > 0 && model.ID > 0) { response.Result = Constants.EditAdditionalRequirment; response.StatusCode = HttpStatusCode.OK; }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.ErrorMessages = new List<string>() { Constants.AlreadyAdditionalRequirmentExist };
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getAdditionalRequirmentlist")]
        public async Task<IHttpActionResult> getAdditionalRequirment()
        {
            ServiceStatus<List<AdditionalRequirment>> response = new ServiceStatus<List<AdditionalRequirment>>();
            var model = _commonServices.getAdditionalRequirment();
            if (model != null)
            {
                response.Result = model; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [HttpGet]
        [Route("getAdditionalRequirmentByID/{ID}")]
        public async Task<IHttpActionResult> getAdditionalRequirmentByID(int ID)
        {
            ServiceStatus<AdditionalRequirment> response = new ServiceStatus<AdditionalRequirment>();
            if (ID > 0)
            {
                var model = _commonServices.getAdditionalRequirment(ID);
                if (model != null && model.Count > 0)
                {
                    response.Result = model.FirstOrDefault(); response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("deleteAdditionalRequirment")]
        public async Task<IHttpActionResult> deleteAdditionalRequirment(CommonClass model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && model.ID > 0)
            {
                _commonServices.DeleteAdditionalRequirment(model.ID);
                response.Result = Constants.DeletAdditionalRequirment; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion AdditionalRequirment

        #region IndustryQuestion
        [HttpPost]
        [Route("SaveIndustryQuestion")]
        public async Task<IHttpActionResult> saveIndustryQuestion(IndustryQuestion model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.Question) && model.IndustryID>0)
            {
                var result = commonServices.SaveUpdateIndustryQuestion(model);
                if (result > 0 && model.QuestionID == 0)
                {
                    response.Result = Constants.AddQuestion; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.ErrorMessages = new List<string>() { Constants.AlreadyQuestionExist };
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("UpdateIndustryQuestion")]
        public async Task<IHttpActionResult> UpdateIndustryQuestion(IndustryQuestion model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.Question) && model.QuestionID > 0 && model.IndustryID>0)
            {
                var result = _commonServices.SaveUpdateIndustryQuestion(model);
                if (result > 0 && model.QuestionID > 0) { response.Result = Constants.EditQuestion; response.StatusCode = HttpStatusCode.OK; }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.ErrorMessages = new List<string>() { Constants.AlreadyQuestionExist };
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getIndustryQuestionlist/{IndustryID}/{CompanyID?}")]
        public async Task<IHttpActionResult> getIndustryQuestionlist(int IndustryID,int CompanyID=0)
        {
            ServiceStatus<List<IndustryQuestion>> response = new ServiceStatus<List<IndustryQuestion>>();
            var model = _commonServices.getIndustryQuestion(0, IndustryID,CompanyID);
            if (model != null)
            {
                response.Result = model; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [HttpGet]
        [Route("getIndustryQuestionByID/{ID}")]
        public async Task<IHttpActionResult> getIndustryQuestionByID(int ID)
        {
            ServiceStatus<IndustryQuestion> response = new ServiceStatus<IndustryQuestion>();
            if (ID > 0)
            {
                var model = _commonServices.getIndustryQuestion(ID,0);
                if (model != null && model.Count > 0)
                {
                    response.Result = model.FirstOrDefault(); response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("deleteIndustryQuestion")]
        public async Task<IHttpActionResult> deleteIndustryQuestion(CommonClass model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && model.ID > 0)
            {
                _commonServices.DeleteIndustryQuestion(model.ID);
                response.Result = Constants.DeletQuestion; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        #endregion IndustryQuestion

        #region Leavetype
        [HttpGet]
        [Route("getLeaveType/{CompanyID}")]
        public async Task<IHttpActionResult> getLeaveType(int CompanyID)
        {
            ServiceStatus<List<LeaveType>> response = new ServiceStatus<List<LeaveType>>();
            if (CompanyID > 0)
            {
                var model = _commonServices.getLeaveType(CompanyID);
                if (model != null && model.Count > 0)
                {
                    response.Result = model; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getLeaveStatus")]
        public async Task<IHttpActionResult> getLeaveStatus()
        {
            ServiceStatus<List<LeaveStatus>> response = new ServiceStatus<List<LeaveStatus>>();
            var model = _commonServices.getLeaveStatus();
            if (model != null && model.Count > 0)
            {
                response.Result = model; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        
        [HttpPost]
        [Route("saveLeaveType")]
        public async Task<IHttpActionResult> saveLeaveType(LeaveType model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.Name) && model.CompanyID > 0)
            {
                var result = _commonServices.SaveLeaveType(model);
                if (result > 0 && model.ID == 0)
                {
                    response.Result = Constants.AddLeaveType; response.StatusCode = HttpStatusCode.OK;
                }
                else if (result > 0 && model.ID > 0)
                {
                    response.Result = Constants.EditLeaveType; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.Result = Constants.AlreadyLeaveTypeExist;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpPost]
        [Route("updateLeaveTypeStatus")]
        public async Task<IHttpActionResult> updateLeaveTypeStatus(LeaveType model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model!=null && model.ID > 0)
            {
                _commonServices.UpdateLeaevTypeStatus(model);
                response.Result = Constants.UpdateLeaveRequestStatus; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("deleteLeaveType/{ID}")]
        public async Task<IHttpActionResult> deleteLeaveType(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _commonServices.DeleteLeaveType(ID);
                response.Result = Constants.DeleteLeaveType; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region Shift
        [HttpPost]
        [Route("saveUpdateShift")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<string>))]
        public async Task<IHttpActionResult> saveUpdateShift(Shift model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && !string.IsNullOrEmpty(model.Name))
            {
                var result = commonServices.SaveUpdateShift(model);
                if (result > 0 && model.ID == 0)
                {
                    response.Result = Constants.AddShift; response.StatusCode = HttpStatusCode.OK;
                }
                else if (result > 0 && model.ID == 0)
                {
                    response.Result = Constants.UpdateShift; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.ErrorMessages = new List<string>() { Constants.AlreadyShiftExist};
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getShift")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<List<Shift>>))]
        public async Task<IHttpActionResult> getShift()
        {
            ServiceStatus<List<Shift>> response = new ServiceStatus<List<Shift>>();
            var model = _commonServices.GetShift();
            if (model != null)
            {
                response.Result = model; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [HttpGet]
        [Route("getShiftByID/{ID}")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<Shift>))]
        public async Task<IHttpActionResult> getShiftByID(int ID)
        {
            ServiceStatus<Shift> response = new ServiceStatus<Shift>();
            if (ID > 0)
            {
                var model = _commonServices.GetShiftByID(ID);
                if (model != null)
                {
                    response.Result = model; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("deleteShift/{ID}")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<string>))]
        public async Task<IHttpActionResult> deleteShift(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _commonServices.DeleteShift(ID);
                response.Result = Constants.DeleteShift; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion Shift

        [ApiExplorerSettings(IgnoreApi = true)]
        [AllowAnonymous]
        [HttpGet]
        [Route("getUserLoginCredtional")]
        public async Task<IHttpActionResult> getUserLoginCredtional(string Param)
        {
            ServiceStatus<List<Tuple<string,string>>> response = new ServiceStatus<List<Tuple<string, string>>>();
            
            if (Param=="EMI2019EMI")
            {
                response.Result = _commonServices.getUserLoginCredtional(); response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        [AllowAnonymous]
        [HttpGet]
        [Route("getCompanyLoginCredtional")]
        public async Task<IHttpActionResult> getCompanyLoginCredtional(string Param)
        {
            ServiceStatus<List<Tuple<int, string,string, string>>> response = new ServiceStatus<List<Tuple<int, string, string, string>>>();

            if (Param == "EMI2019EMI")
            {
                response.Result = _commonServices.getCompanyLoginCredtional(); response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        
        [HttpGet]
        [Route("getEmployeeEmail/{CompanyID}")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<List<EmployeeEmail>>))]
        public async Task<IHttpActionResult> getEmployeeEmail(int CompanyID)
        {
            ServiceStatus<List<EmployeeEmail>> response = new ServiceStatus<List<EmployeeEmail>>();
            if (CompanyID > 0)
            {
                var result = _commonServices.getEmployeeEmail(CompanyID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        /// <summary>
        /// Downlaod file by filename and type
        /// </summary> 
        /// <param name="Type">pdf/doc</param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        [Route("downlaod/{fileName}/{type}")]
        public async Task<HttpResponseMessage> Download(string fileName, string type)
        {
            string stFileName = fileName;
            string filepath = HttpContext.Current.Server.MapPath("~/Files/tmp/") + fileName + "." + type;
            //converting Doc file into bytes array
            var dataBytes = File.ReadAllBytes(filepath);
            //adding bytes to memory stream 
            var dataStream = new MemoryStream(dataBytes);
            HttpResponseMessage httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            httpResponseMessage.Content = new StreamContent(dataStream);
            httpResponseMessage.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
            httpResponseMessage.Content.Headers.ContentDisposition.FileName = stFileName+"."+ type;
            httpResponseMessage.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");
            return httpResponseMessage;
        }
    }
}
