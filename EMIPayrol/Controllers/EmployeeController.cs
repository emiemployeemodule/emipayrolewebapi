﻿using EMIPayrol.Helpers;
using EMIPayrolModel;
using EMIPayrolServices;
using NReco.PdfGenerator;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace EMIPayrol.Controllers
{
    //[Authorize]
    [GlobalExceptionAttribute]
    [RoutePrefix("api/employee")]
    public class EmployeeController : ApiController
    {
        private readonly IEmployeeServices _employeeServices;
        public EmployeeController(IEmployeeServices employeeServices)
        {
            _employeeServices = employeeServices;
        }

        readonly IEmployeeServices employeeServices = new EmployeeServices();

        public EmployeeController()
        {
            _employeeServices = employeeServices;
        }
        
        #region employee
        [HttpPost]
        [Route("saveEmployee")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<string>))]
        public async Task<IHttpActionResult> saveEmployee(Employee model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null)
            {
                var result = _employeeServices.SaveEmployeeDetails(model);
                if (result > 0 && model.ID == 0)
                {
                    response.ID = result;
                    response.Result = Constants.AddEmployee; response.StatusCode = HttpStatusCode.OK;
                }
                else if (result > 0 && model.ID > 0)
                {
                    response.ID = result;
                    response.Result = Constants.EditEmployee; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.ErrorMessages = new List<string>() { Constants.AlreadyEmployeeExist };
                }

            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        
        [HttpGet]
        [Route("getEmployeeByCompanyID/{CompanyID}")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<List<Employee>>))]
        public async Task<IHttpActionResult> getEmployeeByCompanyID(int CompanyID)
        {
            ServiceStatus<List<Employee>> response = new ServiceStatus<List<Employee>>();
            if (CompanyID > 0)
            {
                var result = _employeeServices.getEmployeeByCompanyID(CompanyID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpPost]
        [Route("getEmployeeByFilter")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<List<Employee>>))]
        public async Task<IHttpActionResult> getEmployeeByFilter(EmployeSearchRequest searchRequest)
        {
            ServiceStatus<List<Employee>> response = new ServiceStatus<List<Employee>>();
            if (searchRequest!=null&&searchRequest.CompanyID > 0)
            {
                var result = _employeeServices.GetEmployeeByFilter(searchRequest);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getEmployeeByID/{ID}")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<Employee>))]
        public async Task<IHttpActionResult> getEmployeeByID(int ID)
        {
            ServiceStatus<Employee> response = new ServiceStatus<Employee>();
            if (ID > 0)
            {
                var result = _employeeServices.getEmployeeByID(ID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpPost]
        [Route("deleteEmployeeByID/{ID}")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<string>))]
        public async Task<IHttpActionResult> deleteEmployeeByID(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _employeeServices.DeleteEmployeeByID(ID);
                response.Result = Constants.DeleteEmployee; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("employeeLogin")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<AuthToken>))]
        public async Task<IHttpActionResult> employeeLogin(CommonClass model)
        {
            ServiceStatus<AuthToken> response = new ServiceStatus<AuthToken>();
            if (model != null && !string.IsNullOrEmpty(model.Email) && !string.IsNullOrEmpty(model.Password))
            {
                var result = _employeeServices.EmployeeLogin(model);
                if (result > 0)
                {
                    var emoployeeDetail = _employeeServices.getEmployeeByID(result);
                    AuthToken authToken = new AuthToken();
                    authToken.Uat = Common.createEmployeeToken(emoployeeDetail);
                    response.Result = authToken; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.ErrorMessages = new List<string>() { Constants.InvalidCredtional };
                    response.StatusCode = HttpStatusCode.BadRequest;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getStatusList")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<List<Status>>))]
        public async Task<IHttpActionResult> getStatusList()
        {
            ServiceStatus<List<Status>> response = new ServiceStatus<List<Status>>();
            var result = _employeeServices.GetStatusList();
            if (result != null)
            {
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [HttpPost]
        [Route("updateEmployeeStatus")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<string>))]
        public async Task<IHttpActionResult> updateEmployeeStatus(EmployeeStatus model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && model.EmployeeID > 0 && model.StatusID > 0)
            {
                _employeeServices.UpdateEmployeeStatus(model);
                response.Result = Constants.DefaultUpdateConst; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getStatusHistory/{EmployeeID}")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<List<EmployeeStatus>>))]
        public async Task<IHttpActionResult> getStatusHistory(int EmployeeID)
        {
            ServiceStatus<List<EmployeeStatus>> response = new ServiceStatus<List<EmployeeStatus>>();
            if (EmployeeID > 0)
            {
                var result = _employeeServices.GetStatusHistory(EmployeeID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getReportedUser/{CompanyID}")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<List<ReportedUser>>))]
        public async Task<IHttpActionResult> getReportedUser(int CompanyID)
        {
            ServiceStatus<List<ReportedUser>> response = new ServiceStatus<List<ReportedUser>>();
            if (CompanyID > 0)
            {
                var result = _employeeServices.getReportedUser(CompanyID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region LeaveRequest
        [HttpPost]
        [Route("saveLeaveRequest")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<string>))]
        public async Task<IHttpActionResult> saveLeaveRequest(LeaveRequest model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null)
            {
                var result = _employeeServices.SaveUpdateLeaevRequest(model);
                if (result > 0 && model.ID == 0)
                {
                    response.Result = Constants.AddLeaveRequest; response.StatusCode = HttpStatusCode.OK;
                }
                else if (result > 0 && model.ID > 0)
                {
                    response.Result = Constants.EditLeaveRequest; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.ErrorMessages = new List<string>() { Constants.AlreadyLeaveRequest };
                }

            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("updateLeaveRequestStatus")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<string>))]
        public async Task<IHttpActionResult> updateLeaveRequestStatus(int ID,int LeaveStatusID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _employeeServices.UpdateLeaevRequestStatus(ID, LeaveStatusID);
                response.Result = Constants.UpdateLeaveRequestStatus; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        
        [HttpPost]
        [Route("getLeaveRequest")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<List<LeaveRequest>>))]
        public async Task<IHttpActionResult> getLeaveRequest(LeaveSearch model)
        {
            ServiceStatus<List<LeaveRequest>> response = new ServiceStatus<List<LeaveRequest>>();
            if (model.CompanyID > 0)
            {
                var result = _employeeServices.GetLeaevRequestByCompanyID(model);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpGet]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<LeaveRequest>))]
        [Route("getLeaveRequestByID/{ID}")]
        public async Task<IHttpActionResult> getLeaveRequestByID(int ID)
        {
            ServiceStatus<LeaveRequest> response = new ServiceStatus<LeaveRequest>();
            if (ID > 0)
            {
                var result = _employeeServices.GetLeaevRequestByID(ID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpPost]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<string>))]
        [Route("deleteLeaveRequestByID/{ID}")]
        public async Task<IHttpActionResult> deleteLeaveRequestByID(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _employeeServices.DeleteLeaveRequest(ID);
                response.Result = Constants.DeleteLeaveRequest; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion
        
        #region Designation
        [HttpPost]
        [Route("saveDesignation")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<string>))]
        public async Task<IHttpActionResult> saveDesignation(Designation model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null)
            {
                var result = _employeeServices.SaveUpdateDesignation(model);
                if (result > 0 && model.ID == 0)
                {
                    response.Result = Constants.AddDesignation; response.StatusCode = HttpStatusCode.OK;
                }
                else if (result > 0 && model.ID > 0)
                {
                    response.Result = Constants.EditDesignation; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.ErrorMessages = new List<string>() { Constants.AlreadyDesignation };
                }

            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpGet]
        [Route("getDesignationByCompanyID/{CompanyID}")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<List<Designation>>))]
        public async Task<IHttpActionResult> getDesignationByCompanyID(int CompanyID)
        {
            ServiceStatus<List<Designation>> response = new ServiceStatus<List<Designation>>();
            if (CompanyID > 0)
            {
                var result = _employeeServices.GetDesignationByCompanyID(CompanyID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpGet]
        [Route("getDesignationByDepartment/{CompanyID}/{DepartmentID}")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<List<Designation>>))]
        public async Task<IHttpActionResult> getDesignationByDepartment(int CompanyID,int DepartmentID)
        {
            ServiceStatus<List<Designation>> response = new ServiceStatus<List<Designation>>();
            if (CompanyID > 0&& DepartmentID>0)
            {
                var result = _employeeServices.GetDesignationByDepartment(CompanyID, DepartmentID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpGet]
        [Route("getDesignationByID/{ID}")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<Designation>))]
        public async Task<IHttpActionResult> getDesignationByID(int ID)
        {
            ServiceStatus<Designation> response = new ServiceStatus<Designation>();
            if (ID > 0)
            {
                var result = _employeeServices.GetDesignationByID(ID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpPost]
        [Route("DeleteDesignation/{ID}")]
        public async Task<IHttpActionResult> deleteDesignationByID(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _employeeServices.DeleteDesignation(ID);
                response.Result = Constants.DeleteDesignation; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("updateEmployeeDesignation")]
        public async Task<IHttpActionResult> updateEmployeeDesignation(EmployeRole employeerole)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (employeerole != null && employeerole.ID>0&& employeerole.DesignationID > 0)
            {
                employeeServices.UpdateEmployeeDesignation(employeerole);
                response.StatusCode = HttpStatusCode.OK;
                response.Result = Constants.EditDesignation;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region upload employee photo & Genrate employeeid
        [HttpGet]
        [Route("gerateEmployeeID/{CompanyID}")]
        public async Task<IHttpActionResult> gerateEmployeeID(int CompanyID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (CompanyID > 0)
            {
                var result = _employeeServices.GerateEmployeeID(CompanyID);
                if (!string.IsNullOrEmpty(result))
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.ErrorMessages = new List<string>() { Constants.EmployeeIDNotGenrateReason }; ;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        
        [HttpPost]
        [Route("uploadEmployeePhoto/{EmployeeID}")]
        public async Task<IHttpActionResult> uploadEmployeePhoto(int EmployeeID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            var file = HttpContext.Current.Request.Files.Count > 0 ?
                        HttpContext.Current.Request.Files[0] : null;
            string filePath = string.Empty;
            if (EmployeeID > 0 && file != null && file.ContentLength > 0)
            {
                string extention = file.FileName.Split('.').LastOrDefault();
                string filename = "/Files/EmployProfile/" + Guid.NewGuid().ToString() + "." + extention;
                filePath = Path.Combine(HttpContext.Current.Server.MapPath("~" + filename));
                file.SaveAs(filePath);
                employeeServices.UpdateEployeePhoto(EmployeeID,filename);
                response.Result = ConfigurationManager.AppSettings["APIBaseUrl"].ToString() + filename;
                response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region emergency info
        [HttpPost]
        [Route("saveEmergenyInfo")]
        public async Task<IHttpActionResult> saveEmergenyInfo(EmergenyInfo model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null&& model.EmployeeID>0)
            {
                 _employeeServices.SaveEmergenyInfo(model);
                response.Result = Constants.AddEmergenyInfo; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpGet]
        [Route("getEmergenyInfo/{EmployeeID}")]
        public async Task<IHttpActionResult> getEmergenyInfo(int EmployeeID)
        {
            ServiceStatus<EmergenyInfo> response = new ServiceStatus<EmergenyInfo>();
            if (EmployeeID > 0)
            {
                response.Result = _employeeServices.GetEmergenyInfo(EmployeeID);
                response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region PositionInfo
        [HttpPost]
        [Route("savePositionInfo")]
        public async Task<IHttpActionResult> savePositionInfo(PositionInfo model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null)
            {
                var result = _employeeServices.SavePositionInfo(model);
                if (result > 0 && model.ID == 0)
                {
                    response.Result = Constants.AddPositionInfo; response.StatusCode = HttpStatusCode.OK;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getPositionInfo/{EmployeeID}")]
        public async Task<IHttpActionResult> getPositionInfo(int EmployeeID)
        {
            ServiceStatus<List<PositionInfo>> response = new ServiceStatus<List<PositionInfo>>();
            if (EmployeeID > 0)
            {
                var result = _employeeServices.GetPositionInfo(EmployeeID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpGet]
        [Route("getPositionInfoByID/{ID}")]
        public async Task<IHttpActionResult> getPositionInfoByID(int ID)
        {
            ServiceStatus<PositionInfo> response = new ServiceStatus<PositionInfo>();
            if (ID > 0)
            {
                var result = _employeeServices.GetPositionInfoByID(ID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpPost]
        [Route("DeletePositionInfo/{ID}")]
        public async Task<IHttpActionResult> deletePositionInfoByID(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _employeeServices.DeletePositionInfo(ID);
                response.Result = Constants.DeletePositionInfo; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region EmpWagesInfo
        [HttpGet]
        [Route("getPayFrequencyList")]
        public async Task<IHttpActionResult> getPayFrequencyList()
        {
            ServiceStatus<List<PayFrequency>> response = new ServiceStatus<List<PayFrequency>>();

            var result = _employeeServices.GetPayFrequencyList();
            if (result != null)
            {
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }

        [HttpPost]
        [Route("saveEmpWagesInfo")]
        public async Task<IHttpActionResult> saveEmpWagesInfo(EmpWagesInfo model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null)
            {
                _employeeServices.SaveEmpWagesInfo(model);
                response.Result = Constants.AddEmpWagesInfo; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getEmpWagesInfo/{EmployeeID}")]
        public async Task<IHttpActionResult> getEmpWagesInfo(int EmployeeID)
        {
            ServiceStatus<List<EmpWagesInfo>> response = new ServiceStatus<List<EmpWagesInfo>>();
            if (EmployeeID > 0)
            {
                var result = _employeeServices.GetEmpWagesInfo(EmployeeID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpGet]
        [Route("getEmpWagesInfoByID/{ID}")]
        public async Task<IHttpActionResult> getEmpWagesInfoByID(int ID)
        {
            ServiceStatus<EmpWagesInfo> response = new ServiceStatus<EmpWagesInfo>();
            if (ID > 0)
            {
                var result = _employeeServices.GetEmpWagesInfoByID(ID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpPost]
        [Route("DeleteEmpWagesInfo/{ID}")]
        public async Task<IHttpActionResult> deleteEmpWagesInfoByID(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _employeeServices.DeleteEmpWagesInfo(ID);
                response.Result = Constants.DeleteEmpWagesInfo; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region DependentInfo
     
        [HttpPost]
        [Route("saveDependentInfo")]
        public async Task<IHttpActionResult> saveDependentInfo(DependentInfo model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null)
            {
                var result=_employeeServices.SaveDependentInfo(model);
                if (result > 0 && model.ID == 0)
                {
                    response.Result = Constants.AddDependentInfo; response.StatusCode = HttpStatusCode.OK;
                }
                else if (result > 0 && model.ID > 0)
                {
                    response.Result = Constants.EditDependentInfo; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.ErrorMessages = new List<string>() { "Internal server error" }; response.StatusCode = HttpStatusCode.InternalServerError;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getDependentInfo/{EmployeeID}")]
        public async Task<IHttpActionResult> getDependentInfo(int EmployeeID)
        {
            ServiceStatus<List<DependentInfo>> response = new ServiceStatus<List<DependentInfo>>();
            if (EmployeeID > 0)
            {
                var result = _employeeServices.GetDependentInfo(EmployeeID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpGet]
        [Route("getDependentInfoByID/{ID}")]
        public async Task<IHttpActionResult> getDependentInfoByID(int ID)
        {
            ServiceStatus<DependentInfo> response = new ServiceStatus<DependentInfo>();
            if (ID > 0)
            {
                var result = _employeeServices.GetDependentInfoByID(ID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpPost]
        [Route("deleteDependentInfoByID/{ID}")]
        public async Task<IHttpActionResult> deleteDependentInfoByID(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _employeeServices.DeleteDependentInfo(ID);
                response.Result = Constants.DeleteDependentInfo; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region EmployeeCertificateInfo
        [HttpGet]
        [Route("getCertificateType")]
        public async Task<IHttpActionResult> getCertificateType()
        {
            ServiceStatus<List<TrainingCertificate>> response = new ServiceStatus<List<TrainingCertificate>>();
            var result = _employeeServices.GetCertificateType();
            if (result != null)
            {
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [HttpPost]
        [Route("saveEmployeeCertificateInfo")]
        public async Task<IHttpActionResult> saveEmployeeCertificateInfo()
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            var request = HttpContext.Current.Request;

            if (request != null && !string.IsNullOrEmpty(request.Form["EmployeeID"]) && !string.IsNullOrEmpty(request.Form["TrainingCertificateID"])
                && !string.IsNullOrEmpty(request.Form["EmployeeID"])
                )
            {
                EmployeeCertificateInfo model = new EmployeeCertificateInfo
                {
                    ID = Convert.ToInt32(request.Form["ID"]),
                    TrainingCertificateID = Convert.ToInt32(request.Form["TrainingCertificateID"]),
                    Acquireddate = request.Form["Acquireddate"],
                    Expirydate = request.Form["Expirydate"],
                    RefNumber = request.Form["RefNumber"],
                    IssuedBy = request.Form["IssuedBy"],
                    FIlePath = request.Form["FIlePath"],
                    EmployeeID = Convert.ToInt32(request.Form["EmployeeID"]),
                    CreatedBy = Convert.ToInt32(request.Form["CreatedBy"])
                };
                var file = HttpContext.Current.Request.Files.Count > 0 ?
                          HttpContext.Current.Request.Files[0] : null;
                string filePath = string.Empty;
                if (file != null && file.ContentLength > 0)
                {
                    string extention = file.FileName.Split('.').LastOrDefault();
                    if (string.IsNullOrEmpty(model.FIlePath))
                        model.FIlePath = "/Files/Certificate/" + Convert.ToString(request.Form["EmployeeID"]) + "__" + Guid.NewGuid().ToString() + "." + extention;
                    filePath = Path.Combine(HttpContext.Current.Server.MapPath("~" + model.FIlePath));
                    file.SaveAs(filePath);
                }
                var result = _employeeServices.SaveEmployeeCertificateInfo(model);
                if (model.ID ==0 && result > 0)
                {
                    response.Result = Constants.AddCertificateInfo; response.StatusCode = HttpStatusCode.OK;
                }
                else if(model.ID>0 && result > 0)
                { response.Result = Constants.EditCertificateInfo; response.StatusCode = HttpStatusCode.OK; }
                else {
                    response.Result = Constants.InternalServerError; response.StatusCode = HttpStatusCode.InternalServerError;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getEmployeeCertificateInfo/{EmployeeID}")]
        public async Task<IHttpActionResult> getEmployeeCertificateInfo(int EmployeeID)
        {
            ServiceStatus<List<EmployeeCertificateInfo>> response = new ServiceStatus<List<EmployeeCertificateInfo>>();
            if (EmployeeID > 0)
            {
                var result = _employeeServices.GetEmployeeCertificateInfo(EmployeeID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpGet]
        [Route("getEmployeeCertificateInfoByID/{ID}")]
        public async Task<IHttpActionResult> getEmployeeCertificateInfoByID(int ID)
        {
            ServiceStatus<EmployeeCertificateInfo> response = new ServiceStatus<EmployeeCertificateInfo>();
            if (ID > 0)
            {
                var result = _employeeServices.GetEmployeeCertificateInfoByID(ID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpPost]
        [Route("deleteEmployeeCertificateInfoByID/{ID}")]
        public async Task<IHttpActionResult> deleteEmployeeCertificateInfoByID(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _employeeServices.DeleteEmployeeCertificateInfo(ID);
                response.Result = Constants.DeleteCertificateInfo; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("setCertificateClossOff")]
        public async Task<IHttpActionResult> setCertificateClossOff(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID>0)
            {
                _employeeServices.SetCertificateClossOff(ID);
                response.Result = Constants.EditCertificateInfo; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("renewCertificate")]
        public async Task<IHttpActionResult> renewCertificate(RenewCertificate model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && model.ID > 0)
            {
                _employeeServices.RenewCertificate(model);
                response.Result = Constants.EditCertificateInfo; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region EmployeeVicationInfo
        [HttpGet]
        [Route("getVicationType")]
        public async Task<IHttpActionResult> getVicationType()
        {
            ServiceStatus<List<VicationType>> response = new ServiceStatus<List<VicationType>>();
            var result = _employeeServices.GetVicationType();
            if (result != null)
            {
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [HttpPost]
        [Route("saveVicationInfo")]
        public async Task<IHttpActionResult> saveVicationInfo(EmployeeVicationInfo model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && model.YearFrom>0&& model.DayPaid>0&& model.DayAllow>0&& model.EmployeeID>0)
            {   var result = _employeeServices.SaveEmployeeVicationInfo(model);
                if (result > 0)
                {
                    response.Result = Constants.AddVicationInfo; response.StatusCode = HttpStatusCode.OK;
                }
                else if (model.ID > 0 && result > 0)
                { response.Result = Constants.EditVicationInfo; response.StatusCode = HttpStatusCode.OK; }
                else
                {
                    response.Result = Constants.InternalServerError; response.StatusCode = HttpStatusCode.InternalServerError;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getVicationInfo/{EmployeeID}")]
        public async Task<IHttpActionResult> getVicationInfo(int EmployeeID)
        {
            ServiceStatus<List<EmployeeVicationInfo>> response = new ServiceStatus<List<EmployeeVicationInfo>>();
            if (EmployeeID > 0)
            {
                var result = _employeeServices.GetEmployeeVicationInfo(EmployeeID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpGet]
        [Route("getVicationInfoByID/{ID}")]
        public async Task<IHttpActionResult> getVicationInfoByID(int ID)
        {
            ServiceStatus<EmployeeVicationInfo> response = new ServiceStatus<EmployeeVicationInfo>();
            if (ID > 0)
            {
                var result = _employeeServices.GetEmployeeVicationInfoByID(ID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpPost]
        [Route("deleteVicationInfoByID/{ID}")]
        public async Task<IHttpActionResult> deleteVicationInfoByID(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _employeeServices.DeleteEmployeeVicationInfo(ID);
                response.Result = Constants.DeleteVicationInfo; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region EmployeeDisciplinaryDetail
        [HttpGet]
        [Route("getDisciplinaryInfractionType")]
        public async Task<IHttpActionResult> getDisciplinaryInfractionType()
        {
            ServiceStatus<List<DisciplinaryInfraction>> response = new ServiceStatus<List<DisciplinaryInfraction>>();
            var result = _employeeServices.GetDisciplinaryInfractionType();
            if (result != null)
            {
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [HttpGet]
        [Route("getDisciplinaryWarningType")]
        public async Task<IHttpActionResult> getDisciplinaryWarningType()
        {
            ServiceStatus<List<DisciplinaryWarning>> response = new ServiceStatus<List<DisciplinaryWarning>>();
            var result = _employeeServices.GetDisciplinaryWarningType();
            if (result != null)
            {
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [HttpPost]
        [Route("saveDisciplinaryDetail")]
        public async Task<IHttpActionResult> saveDisciplinaryDetail()
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            var request = HttpContext.Current.Request;

            if (request != null && !string.IsNullOrEmpty(request.Form["EmployeeID"]) && !string.IsNullOrEmpty(request.Form["DisciplinaryInfractionID"])
                && !string.IsNullOrEmpty(request.Form["WarningTypeID"]) && !string.IsNullOrEmpty(request.Form["DisciplinaryDate"])
                )
            {
                EmployeeDisciplinaryDetail model = new EmployeeDisciplinaryDetail
                {
                    ID = Convert.ToInt32(request.Form["ID"]),
                    DisciplinaryDate = request.Form["DisciplinaryDate"],
                    DisciplinaryInfractionID = Convert.ToInt32(request.Form["DisciplinaryInfractionID"]),
                    Specify = request.Form["Specify"],
                    Expectations = request.Form["Expectations"],
                    WarningTypeID = Convert.ToInt32(request.Form["WarningTypeID"]),
                    InfractionDetails = request.Form["InfractionDetails"],
                    Status = request.Form["Status"],
                    FilePath = request.Form["FilePath"],
                    NoOfWeek = string.IsNullOrEmpty(request.Form["NoOfWeek"])?0:Convert.ToInt32(request.Form["NoOfWeek"]),
                    NumberOfRepetition = string.IsNullOrEmpty(request.Form["NumberOfRepetition"]) ? 0 : Convert.ToInt32(request.Form["NumberOfRepetition"]),
                    EmployeeID = Convert.ToInt32(request.Form["EmployeeID"]),
                    CreatedBy = Convert.ToInt32(request.Form["CreatedBy"])
                };
                var file = HttpContext.Current.Request.Files.Count > 0 ?
                          HttpContext.Current.Request.Files[0] : null;
                string filePath = string.Empty;
                if (file != null && file.ContentLength > 0)
                {
                    string extention = file.FileName.Split('.').LastOrDefault();
                    if (string.IsNullOrEmpty(model.FilePath))
                        model.FilePath = "/Files/Disciplinary/" + Convert.ToString(request.Form["EmployeeID"]) + "__" + Guid.NewGuid().ToString() + "." + extention;
                    filePath = Path.Combine(HttpContext.Current.Server.MapPath("~" + model.FilePath));
                    file.SaveAs(filePath);
                }
                var result = _employeeServices.SaveEmployeeDisciplinaryDetail(model);
                if (result > 0)
                {
                    response.Result = Constants.AddDisciplinaryDetail; response.StatusCode = HttpStatusCode.OK;
                }
                else if (model.ID > 0 && result > 0)
                { response.Result = Constants.EditDisciplinaryDetail; response.StatusCode = HttpStatusCode.OK; }
                else
                {
                    response.Result = Constants.InternalServerError; response.StatusCode = HttpStatusCode.InternalServerError;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getDisciplinaryDetailByCompanyID/{CompanyID}")]
        public async Task<IHttpActionResult> GetDisciplinaryDetailByCompanyID(int CompanyID)
        {
            ServiceStatus<List<EmployeeDisciplinaryDetail>> response = new ServiceStatus<List<EmployeeDisciplinaryDetail>>();
            if (CompanyID > 0)
            {
                var result = _employeeServices.GetDisciplinaryDetailByCompanyID(CompanyID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getDisciplinaryDetail/{EmployeeID}")]
        public async Task<IHttpActionResult> getDisciplinaryDetail(int EmployeeID)
        {
            ServiceStatus<List<EmployeeDisciplinaryDetail>> response = new ServiceStatus<List<EmployeeDisciplinaryDetail>>();
            if (EmployeeID > 0)
            {
                var result = _employeeServices.GetEmployeeDisciplinaryDetail(EmployeeID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("getFilterDisciplinaryDetail")]
        public async Task<IHttpActionResult> getFilterDisciplinaryDetail(FilterDto model)
        {
            ServiceStatus<List<EmployeeDisciplinaryDetail>> response = new ServiceStatus<List<EmployeeDisciplinaryDetail>>();
            if (model!=null&& model.EmoplyeeID> 0)
            {
                var result = _employeeServices.GetFilterDisciplinaryDetail(model);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpGet]
        [Route("getDisciplinaryDetailByID/{ID}")]
        public async Task<IHttpActionResult> getDisciplinaryDetailByID(int ID)
        {
            ServiceStatus<EmployeeDisciplinaryDetail> response = new ServiceStatus<EmployeeDisciplinaryDetail>();
            if (ID > 0)
            {
                var result = _employeeServices.GetEmployeeDisciplinaryDetailByID(ID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpPost]
        [Route("deleteDisciplinaryDetailByID/{ID}")]
        public async Task<IHttpActionResult> deleteDisciplinaryDetailByID(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _employeeServices.DeleteEmployeeDisciplinaryDetail(ID);
                response.Result = Constants.DeleteDisciplinaryDetail; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("disciplinaryDetailCompleted")]
        public async Task<IHttpActionResult> disciplinaryDetailCompleted(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _employeeServices.DisciplinaryDetailCompleted(ID);
                response.Result = Constants.DefaultUpdateConst; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region EmployeeTerminationDetail
        [HttpGet]
        [Route("sendNotoficationTermination/{EmployeeID}")]
        public async Task<IHttpActionResult> sendNotoficationTermination(int EmployeeID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (EmployeeID > 0)
            {
                if (_employeeServices.SendNotoficationTermination(EmployeeID))
                {
                    response.Result = Constants.TerminationNotification; response.StatusCode = HttpStatusCode.OK;
                }
                else { response.ErrorMessages = new List<string> { "HR information not found"}; response.StatusCode = HttpStatusCode.NoContent; }
            }
            else
            {
                response.Result = Constants.NullableParametre;
                response.StatusCode = HttpStatusCode.BadRequest;
            }
            return Ok(response);
        }

        [HttpGet]
        [Route("getTerminationReasonType")]
        public async Task<IHttpActionResult> getTerminationReasonType()
        {
            ServiceStatus<List<TerminationReasonType>> response = new ServiceStatus<List<TerminationReasonType>>();
            var result = _employeeServices.GetTerminationReasonType();
            if (result != null)
            {
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [HttpPost]
        [Route("saveTerminationDetail")]
        public async Task<IHttpActionResult> saveTerminationDetail()
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            var request = HttpContext.Current.Request;

            if (request != null && !string.IsNullOrEmpty(request.Form["LastDayWorkDate"]) && !string.IsNullOrEmpty(request.Form["BenefitsCeaseDate"])
                && !string.IsNullOrEmpty(request.Form["TerminationTypeID"]) && !string.IsNullOrEmpty(request.Form["FinalPay"])
                && !string.IsNullOrEmpty(request.Form["TerminationPayPaid"]) && !string.IsNullOrEmpty(request.Form["EmployeeID"])
                )
            {
                EmployeeTerminationDetail model = new EmployeeTerminationDetail
                {
                    ID = Convert.ToInt32(request.Form["ID"]),
                    LastDayWorkDate = request.Form["LastDayWorkDate"],
                    BenefitsCeaseDate = request.Form["BenefitsCeaseDate"],
                    TerminationPay = string.IsNullOrEmpty(request.Form["TerminationPay"]) ? 0 : Convert.ToInt32(request.Form["TerminationPay"]),
                    TerminationPayPaid = string.IsNullOrEmpty(request.Form["TerminationPayPaid"]) ? 0 : Convert.ToInt32(request.Form["TerminationPayPaid"]),
                    Severance = Convert.ToInt32(request.Form["Severance"]),
                    SeverancePaid = string.IsNullOrEmpty(request.Form["SeverancePaid"]) ? 0 : Convert.ToInt32(request.Form["SeverancePaid"]),
                    Status = request.Form["Status"],
                    TerminationTypeID = string.IsNullOrEmpty(request.Form["TerminationTypeID"]) ? 0 : Convert.ToInt32(request.Form["TerminationTypeID"]),
                    FinalPay = string.IsNullOrEmpty(request.Form["FinalPay"]) ? 0 : Convert.ToInt32(request.Form["FinalPay"]),
                    EmployeeID = Convert.ToInt32(request.Form["EmployeeID"]),
                    CreatedBy = Convert.ToInt32(request.Form["CreatedBy"])
                };
                var file = HttpContext.Current.Request.Files.Count > 0 ?
                          HttpContext.Current.Request.Files[0] : null;
                string filePath = string.Empty;
                if (file != null && file.ContentLength > 0)
                {
                    string extention = file.FileName.Split('.').LastOrDefault();
                    model.FilePath = "/Files/Disciplinary/" + Guid.NewGuid().ToString() + "." + extention;
                    filePath = Path.Combine(HttpContext.Current.Server.MapPath("~" + model.FilePath));
                    file.SaveAs(filePath);
                }
                var result = _employeeServices.SaveEmployeeTerminationDetail(model);
                if (result > 0)
                {
                    response.Result = Constants.AddTerminationDetail; response.StatusCode = HttpStatusCode.OK;
                }
                else if (model.ID > 0 && result > 0)
                { response.Result = Constants.EditTerminationDetail; response.StatusCode = HttpStatusCode.OK; }
                else
                {
                    response.Result = Constants.InternalServerError; response.StatusCode = HttpStatusCode.InternalServerError;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpGet]
        [Route("GetTerminationDetailByCompanyID/{CompanyID}")]
        public async Task<IHttpActionResult> GetTerminationDetailByCompanyID(int CompanyID)
        {
            ServiceStatus<List<EmployeeTerminationDetail>> response = new ServiceStatus<List<EmployeeTerminationDetail>>();
            if (CompanyID > 0)
            {
                var result = _employeeServices.GetTerminationDetailByCompanyID(CompanyID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpGet]
        [Route("getTerminationDetail/{EmployeeID}")]
        public async Task<IHttpActionResult> getTerminationDetail(int EmployeeID)
        {
            ServiceStatus<List<EmployeeTerminationDetail>> response = new ServiceStatus<List<EmployeeTerminationDetail>>();
            if (EmployeeID > 0)
            {
                var result = _employeeServices.GetEmployeeTerminationDetail(EmployeeID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpGet]
        [Route("getTerminationDetailByID/{ID}")]
        public async Task<IHttpActionResult> getTerminationDetailByID(int ID)
        {
            ServiceStatus<EmployeeTerminationDetail> response = new ServiceStatus<EmployeeTerminationDetail>();
            if (ID > 0)
            {
                var result = _employeeServices.GetEmployeeTerminationDetailByID(ID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("getFilterTerminationDetail")]
        public async Task<IHttpActionResult> getFilterTerminationDetail(FilterDto model)
        {
            ServiceStatus<List<EmployeeTerminationDetail>> response = new ServiceStatus<List<EmployeeTerminationDetail>>();
            if (model != null && model.EmoplyeeID > 0)
            {
                var result = _employeeServices.GetFilterTerminationDetail(model);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("deleteTerminationDetailByID/{ID}")]
        public async Task<IHttpActionResult> deleteTerminationDetailByID(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _employeeServices.DeleteEmployeeTerminationDetail(ID);
                response.Result = Constants.DeleteTerminationDetail; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("terminationDetailCompleted")]
        public async Task<IHttpActionResult> terminationDetailCompleted(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _employeeServices.TerminationDetailCompleted(ID);
                response.Result = Constants.DefaultUpdateConst; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region Layoffs
        [HttpPost]
        [Route("saveLayoffs")]
        public async Task<IHttpActionResult> saveLayoffs()
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            var request = HttpContext.Current.Request;

            if (request != null && !string.IsNullOrEmpty(request.Form["EmployeeID"]) && !string.IsNullOrEmpty(request.Form["LastDayWork"])
                && !string.IsNullOrEmpty(request.Form["ReturnDate"]))
            {
                LayOffs model = new LayOffs
                {
                    ID = Convert.ToInt32(request.Form["ID"]),
                    LastDayWork = request.Form["LastDayWork"],
                    ReturnDate = request.Form["ReturnDate"],
                    PayBenifit = Convert.ToBoolean(request.Form["PayBenifit"]),
                    BenifitTypeID = Convert.ToBoolean(request.Form["PayBenifit"]) ? Convert.ToInt32(request.Form["BenifitTypeID"]) : 0,
                    FilePath = request.Form["FilePath"],
                    EmployeeID = Convert.ToInt32(request.Form["EmployeeID"]),
                    CreatedBy = Convert.ToInt32(request.Form["CreatedBy"])
                };
                var file = HttpContext.Current.Request.Files.Count > 0 ?
                          HttpContext.Current.Request.Files[0] : null;
                string filePath = string.Empty;
                if (file != null && file.ContentLength > 0)
                {
                    string extention = file.FileName.Split('.').LastOrDefault();
                    if (string.IsNullOrEmpty(model.FilePath))
                        model.FilePath = "/Files/Layoffs/" + Convert.ToString(request.Form["EmployeeID"]) + "__" + Guid.NewGuid().ToString() + "." + extention;
                    filePath = Path.Combine(HttpContext.Current.Server.MapPath("~" + model.FilePath));
                    file.SaveAs(filePath);
                }
                var result = _employeeServices.SaveLayoffsDetail(model);
                if (result > 0)
                {
                    response.Result = Constants.AddLayOffs; response.StatusCode = HttpStatusCode.OK;
                }
                else if (model.ID > 0 && result > 0)
                { response.Result = Constants.EditLayOffs; response.StatusCode = HttpStatusCode.OK; }
                else
                {
                    response.Result = Constants.InternalServerError; response.StatusCode = HttpStatusCode.InternalServerError;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpGet]
        [Route("getLayoffsByCompanyID/{CompanyID}")]
        public async Task<IHttpActionResult> getLayoffsByCompanyID(int CompanyID)
        {
            ServiceStatus<List<LayOffs>> response = new ServiceStatus<List<LayOffs>>();
            if (CompanyID > 0)
            {
                var result = _employeeServices.getLayoffsByCompanyID(CompanyID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getLayoffsDetail/{EmployeeID}")]
        public async Task<IHttpActionResult> getLayoffsDetail(int EmployeeID)
        {
            ServiceStatus<List<LayOffs>> response = new ServiceStatus<List<LayOffs>>();
            if (EmployeeID > 0)
            {
                var result = _employeeServices.getLayoffsDetail(EmployeeID);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        
        [HttpGet]
        [Route("getLayoffsByID/{ID}")]
        public async Task<IHttpActionResult> getLayoffsByID(int ID)
        {
            ServiceStatus<LayOffs> response = new ServiceStatus<LayOffs>();
            if (ID > 0)
            {
                var result = _employeeServices.getLayoffsByID(ID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpPost]
        [Route("deleteLayoffsByID/{ID}")]
        public async Task<IHttpActionResult> deleteLayoffsByID(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _employeeServices.DeleteLayoffsByID(ID);
                response.Result = Constants.DeleteLayOffs; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("layoffCompleted")]
        public async Task<IHttpActionResult> layoffCompleted(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _employeeServices.LayoffsCompleted(ID);
                response.Result = Constants.DefaultUpdateConst; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region EmployeeOfferDetail
        [HttpGet]
        [Route("getOfferBenefitsType")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<List<OfferBenefitsType>>))]
        public async Task<IHttpActionResult> getOfferBenefitsType()
        {
            ServiceStatus<List<OfferBenefitsType>> response = new ServiceStatus<List<OfferBenefitsType>>();
            var result = _employeeServices.GetOfferBenefitsType();
            if (result != null)
            {
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [HttpGet]
        [Route("getOfferVacationType")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<List<OfferVacationType>>))]
        public async Task<IHttpActionResult> getOfferVacationType()
        {
            ServiceStatus<List<OfferVacationType>> response = new ServiceStatus<List<OfferVacationType>>();
            var result = _employeeServices.GetOfferVacationType();
            if (result != null)
            {
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [HttpGet]
        [Route("getEmploymentType")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<List<EmploymentType>>))]
        public async Task<IHttpActionResult> getEmploymentType()
        {
            ServiceStatus<List<EmploymentType>> response = new ServiceStatus<List<EmploymentType>>();
            var result = _employeeServices.GetEmploymentType();
            if (result != null)
            {
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [HttpGet]
        [Route("getCompensationType")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<List<CompensationType>>))]
        public async Task<IHttpActionResult> getCompensationType()
        {
            ServiceStatus<List<CompensationType>> response = new ServiceStatus<List<CompensationType>>();
            var result = _employeeServices.GetCompensationType();
            if (result != null)
            {
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        /// <summary>
        /// Get termination clauses list
        /// </summary>
        [HttpGet]
        [Route("getTerminationClause")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<List<TerminationClause>>))]
        public async Task<IHttpActionResult> getTerminationClause()
        {
            ServiceStatus<List<TerminationClause>> response = new ServiceStatus<List<TerminationClause>>();
            var result = _employeeServices.GetTerminationClause();
            if (result != null)
            {
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        /// <summary>
        /// Get working day type list
        /// </summary>
        [HttpGet]
        [Route("getWorkingDayType")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<List<WorkingDayType>>))]
        public async Task<IHttpActionResult> getWorkingDayType()
        {
            ServiceStatus<List<WorkingDayType>> response = new ServiceStatus<List<WorkingDayType>>();
            var result = _employeeServices.GetWorkingDayType();
            if (result != null)
            {
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        /// <summary>
        /// save offer detail record
        /// </summary>
        [HttpPost]
        [Route("saveOfferDetail")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<string>))]
        public async Task<IHttpActionResult> saveOfferDetail()
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            var request = HttpContext.Current.Request;

            if (request != null && !string.IsNullOrEmpty(request.Form["Startdate"]) && !string.IsNullOrEmpty(request.Form["EmploymentTypeID"])
                && !string.IsNullOrEmpty(request.Form["CompensationTypeID"]) && !string.IsNullOrEmpty(request.Form["CompensationAmount"])
                && !string.IsNullOrEmpty(request.Form["WorkingDayTypeID"]) && !string.IsNullOrEmpty(request.Form["StartTime"])
                && !string.IsNullOrEmpty(request.Form["EndTime"]) && !string.IsNullOrEmpty(request.Form["OfferBenefitsTypeID"])
                && !string.IsNullOrEmpty(request.Form["OfferVacationTypeID"]) && !string.IsNullOrEmpty(request.Form["EmployeeID"])
                )
            {
                EmployeeOfferDetail model = new EmployeeOfferDetail
                {
                    ID = Convert.ToInt32(request.Form["ID"]),
                    EmploymentTypeID = string.IsNullOrEmpty(request.Form["EmploymentTypeID"]) ? 0 : Convert.ToInt32(request.Form["EmploymentTypeID"]),
                    CompensationTypeID = string.IsNullOrEmpty(request.Form["CompensationTypeID"]) ? 0 : Convert.ToInt32(request.Form["CompensationTypeID"]),
                    CompensationAmount = string.IsNullOrEmpty(request.Form["CompensationAmount"]) ? 0 : Convert.ToDecimal(request.Form["CompensationAmount"]),
                    BenefitsCeaseDate = request.Form["BenefitsCeaseDate"],
                    WorkingDayTypeID = Convert.ToInt32(request.Form["WorkingDayTypeID"]),
                    OtherInfo = request.Form["WorkingDayOther"],
                    Startdate = request.Form["Startdate"],
                    StartTime = request.Form["StartTime"],
                    EndTime = request.Form["EndTime"],
                    Status = request.Form["Status"],
                    JobTitle = request.Form["JobTitle"],
                    OfferBenefitsTypeID = string.IsNullOrEmpty(request.Form["OfferBenefitsTypeID"]) ? 0 : Convert.ToInt32(request.Form["OfferBenefitsTypeID"]),
                    OfferVacationTypeID = string.IsNullOrEmpty(request.Form["OfferVacationTypeID"]) ? 0 : Convert.ToInt32(request.Form["OfferVacationTypeID"]),
                    OfferAcceptingDate = request.Form["OfferAcceptingDate"],
                    ReportingManagerID = string.IsNullOrEmpty(request.Form["ReportingManagerID"]) ? 0 : Convert.ToInt32(request.Form["ReportingManagerID"]),
                    FilePath = request.Form["FilePath"],
                    EmployeeID = Convert.ToInt32(request.Form["EmployeeID"]),
                    CreatedBy = Convert.ToInt32(request.Form["CreatedBy"]),
                    PayCommission = Convert.ToString(request.Form["PayCommission"]) == "1" ? true : false,
                    CommissionPrevision = Convert.ToString(request.Form["CommissionPrevision"]),
                    TerminationClauseID = Convert.ToInt32(request.Form["TerminationClauseID"]),
                    TerminationDescription = Convert.ToString(request.Form["TerminationDescription"]),
                    OtherClause = Convert.ToString(request.Form["OtherClause"]) == "1" ? true : false,
                    ExpiryDate = request.Form["ExpiryDate"],
                    OnBoarding= Convert.ToInt32(request.Form["OnBoarding"]+""),
                };
                if (model.OnBoarding > 0)
                {
                    model.FirstName = Convert.ToString(request.Form["FirstName"]);
                    model.LastName= Convert.ToString(request.Form["LastName"]);
                    model.CityID = Convert.ToInt32(request.Form["CityID"]);
                    model.StateID= Convert.ToInt32(request.Form["StateID"]);
                    model.CountryID= Convert.ToInt32(request.Form["CountryID"]);
                    model.Email= Convert.ToString(request.Form["Email"]);
                }
                var file = HttpContext.Current.Request.Files.Count > 0 ?
                          HttpContext.Current.Request.Files[0] : null;
                string filePath = string.Empty;
                if (file != null && file.ContentLength > 0)
                {
                    string extention = file.FileName.Split('.').LastOrDefault();
                    if (string.IsNullOrEmpty(model.FilePath))
                        model.FilePath = "/Files/OfferLetter/" + Convert.ToString(request.Form["EmployeeID"]) + "__" + Guid.NewGuid().ToString() + "." + extention;
                    filePath = Path.Combine(HttpContext.Current.Server.MapPath("~" + model.FilePath));
                    file.SaveAs(filePath);
                }
                var result = _employeeServices.SaveEmployeeOfferDetail(model);
                if (result > 0)
                {
                    response.Result = Constants.AddOfferDetail; response.StatusCode = HttpStatusCode.OK;
                }
                else if (model.ID > 0 && result > 0)
                { response.Result = Constants.EditOfferDetail; response.StatusCode = HttpStatusCode.OK; }
                else
                {
                    response.Result = Constants.InternalServerError; response.StatusCode = HttpStatusCode.InternalServerError;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        /// <summary>
        /// Get offer details by companyid with filter param
        /// </summary>
        [HttpGet]
        [Route("getEmployeeOfferDetailsByCompanyID/{CompanyID}/{SearcthText?}")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<List<EmployeeOfferDetail>>))]
        public async Task<IHttpActionResult> GetEmployeeOfferDetailsByCompanyID(int CompanyID,string SearcthText=null)
        {
            ServiceStatus<List<EmployeeOfferDetail>> response = new ServiceStatus<List<EmployeeOfferDetail>>();
            if (CompanyID > 0)
            {
                var result = _employeeServices.GetEmployeeOfferDetailsByCompanyID(CompanyID, SearcthText);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        /// <summary>
        /// Get offer details by employeeid with filter param
        /// </summary>
        [HttpGet]
        [Route("getOfferDetail/{EmployeeID}/{SearcthText?}")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<List<EmployeeOfferDetail>>))]
        public async Task<IHttpActionResult> getOfferDetail(int EmployeeID,string SearcthText=null)
        {
            ServiceStatus<List<EmployeeOfferDetail>> response = new ServiceStatus<List<EmployeeOfferDetail>>();
            if (EmployeeID > 0)
            {
                var result = _employeeServices.GetEmployeeOfferDetails(EmployeeID, SearcthText);
                if (result != null && result.Count > 0)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        /// <summary>
        /// Get offer detail by ID
        /// </summary>
        [HttpGet]
        [Route("getOfferDetailByID/{ID}")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<EmployeeOfferDetail>))]
        public async Task<IHttpActionResult> getOfferDetailByID(int ID)
        {
            ServiceStatus<EmployeeOfferDetail> response = new ServiceStatus<EmployeeOfferDetail>();
            if (ID > 0)
            {
                var result = _employeeServices.GetEmployeeOfferDetailsByID(ID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        /// <summary>
        /// Delete employment offer 
        /// </summary>
        [HttpPost]
        [Route("deleteOfferDetailByID/{ID}")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<string>))]
        public async Task<IHttpActionResult> deleteOfferDetailByID(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _employeeServices.DeleteEmployeeOfferDetails(ID);
                response.Result = Constants.DeleteOfferDetail; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        /// <summary>
        /// Accept employment offer 
        /// </summary>
        [HttpPost]
        [Route("offerAccepted")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<string>))]
        public async Task<IHttpActionResult> offerAccepted(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _employeeServices.OfferAccepted(ID);
                response.Result = Constants.DefaultUpdateConst; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        /// <summary>
        /// Decline employment offer 
        /// </summary>
        [HttpPost]
        [Route("offerDecline")]
        [SwaggerResponse(200, "OK", typeof(ServiceStatus<string>))]
        public async Task<IHttpActionResult> offerDecline(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _employeeServices.OfferDecline(ID);
                response.Result = Constants.DefaultUpdateConst; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        #endregion

        #region accident details
        [HttpGet]
        [Route("getAccidentType")]
        public async Task<IHttpActionResult> getAccidentType()
        {
            ServiceStatus<List<AccidentType>> response = new ServiceStatus<List<AccidentType>>();
            var result = _employeeServices.GetAccidentType();
            if (result != null)
            {
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [HttpGet]
        [Route("getDiseasesType")]
        public async Task<IHttpActionResult> getDiseasesType()
        {
            ServiceStatus<List<DiseasesType>> response = new ServiceStatus<List<DiseasesType>>();
            var result = _employeeServices.GetDiseasesType();
            if (result != null)
            {
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [HttpGet]
        [Route("getBodyPartType")]
        public async Task<IHttpActionResult> getBodyPartType()
        {
            ServiceStatus<List<BodyPartType>> response = new ServiceStatus<List<BodyPartType>>();
            var result = _employeeServices.GetBodyPartType();
            if (result != null)
            {
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [HttpGet]
        [Route("getAccidentDetailByCompanyID/{CompanyID}/{SearchText?}")]
        public async Task<IHttpActionResult> getAccidentDetailByCompanyID(int CompanyID, string SearchText = null)
        {
            ServiceStatus<List<AccidentDetail>> response = new ServiceStatus<List<AccidentDetail>>();
            if (CompanyID > 0)
            {
                var result = _employeeServices.GetAccidentDetailByCompanyID(CompanyID, SearchText);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getAccidentDetail/{EmployeeID}/{SearchText?}")]
        public async Task<IHttpActionResult> getAccidentDetail(int EmployeeID, string SearchText=null)
        {
            ServiceStatus<List<AccidentDetail>> response = new ServiceStatus<List<AccidentDetail>>();
            if (EmployeeID > 0)
            {
                var result = _employeeServices.GetAccidentDetail(EmployeeID, SearchText);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getAccidentDetailByID/{ID}")]
        public async Task<IHttpActionResult> getAccidentDetailByID(int ID)
        {
            ServiceStatus<AccidentDetail> response = new ServiceStatus<AccidentDetail>();
            if (ID > 0)
            {
                var result = _employeeServices.GetAccidentDetailByID(ID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("saveAccidentDetail")]
        public async Task<IHttpActionResult> saveAccidentDetail(AccidentDetail model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && model.EmployeeID>0)
            {
                response.ID = _employeeServices.SaveAccidentDetail(model);
                if (model.ID == 0)
                {
                    response.Result = Constants.AddAccidentDetail; response.StatusCode = HttpStatusCode.OK;
                }
                else
                { response.Result = Constants.EditAccidentDetail; response.StatusCode = HttpStatusCode.OK; }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getTreatmentPlace")]
        public async Task<IHttpActionResult> getTreatmentPlace()
        {
            ServiceStatus<List<TreatmentPlace>> response = new ServiceStatus<List<TreatmentPlace>>();
            var result = _employeeServices.GetTreatmentPlace();
            if (result != null)
            {
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [HttpGet]
        [Route("getMedicalAwarnessList")]
        public async Task<IHttpActionResult> getMedicalAwarnessList()
        {
            ServiceStatus<List<MedicalAwarness>> response = new ServiceStatus<List<MedicalAwarness>>();
            var result = _employeeServices.GetMedicalAwarnessList();
            if (result != null)
            {
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [HttpGet]
        [Route("getRateType")]
        public async Task<IHttpActionResult> getRateType()
        {
            ServiceStatus<List<RateType>> response = new ServiceStatus<List<RateType>>();
            var result = _employeeServices.GetRateType();
            if (result != null)
            {
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [HttpGet]
        [Route("getWorkerJobType")]
        public async Task<IHttpActionResult> getWorkerJobType()
        {
            ServiceStatus<List<WorkerJobType>> response = new ServiceStatus<List<WorkerJobType>>();
            var result = _employeeServices.GetWorkerJobType();
            if (result != null)
            {
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [HttpGet]
        [Route("getHealthcare")]
        public async Task<IHttpActionResult> getHealthcare(int AccidentDetailID)
        {
            ServiceStatus<List<HealthCare>> response = new ServiceStatus<List<HealthCare>>();
            if (AccidentDetailID > 0)
            {
                var result = _employeeServices.GetHealthcare(AccidentDetailID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("saveHealthCare")]
        public async Task<IHttpActionResult> saveHealthCare(HealthCare model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && model.AccidentDetailID > 0)
            {
                _employeeServices.SaveHealthCare(model);
                response.Result = Constants.DefaultUpdateConst; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getAccidentTabLabel")]
        public async Task<IHttpActionResult> getAccidentTabLabel()
        {
            ServiceStatus<AccidentLable> response = new ServiceStatus<AccidentLable>();
            AccidentLable model = new AccidentLable();
            response.Result = model; response.StatusCode = HttpStatusCode.OK;
            return Ok(response);
        }
        [HttpPost]
        [Route("accidentCompleted")]
        public async Task<IHttpActionResult> accidentCompleted(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _employeeServices.AccidentCompleted(ID);
                response.Result = Constants.DefaultUpdateConst; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region Miscellaneous 
        [HttpGet]
        [Route("getMiscellaneous")]
        public async Task<IHttpActionResult> getMiscellaneous()
        {
            ServiceStatus<List<Filedto>> response = new ServiceStatus<List<Filedto>>();
            List<Filedto> model = new List<Filedto>();
            model.Add(new Filedto { Name = "Certificate", Value = "Certificate" });
            model.Add(new Filedto { Name = "Discipline", Value = "Disciplinary" });
            model.Add(new Filedto { Name = "Offer Of Employment", Value = "OfferLetter" });
            model.Add(new Filedto { Name = "Performanc Review", Value = "PerformancReview" });
            response.Result = model; response.StatusCode = HttpStatusCode.OK;
            return Ok(response);
        }
        [HttpGet]
        [Route("getFilesFromFolder/{EmployeeID}/{FolderName?}")]
        public async Task<IHttpActionResult> getFilesFromFolder(int EmployeeID, string FolderName=null)
        {
            ServiceStatus<List<FileDetail>> response = new ServiceStatus<List<FileDetail>>();
            if (EmployeeID>0)
            {
                List<FileDetail> result = employeeServices.GetFilesFromFolderByEmployeeID(EmployeeID, FolderName+"");
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("updateFilePrivacy")]
        public async Task<IHttpActionResult> updateFilePrivacy(FilePrivacy model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null && model.EmployeeID>0)
            {
                _employeeServices.UpdateFilePrivacy(model);
                response.Result = Constants.DefaultUpdateConst; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("savePublicFile")]
        public async Task<IHttpActionResult> savePublicFile()
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            var request = HttpContext.Current.Request;

            if (request != null && !string.IsNullOrEmpty(request.Form["EmployeeID"]))
            {
                int EmployeeID = Convert.ToInt32(request.Form["EmployeeID"]);
                var file = HttpContext.Current.Request.Files.Count > 0 ?
                          HttpContext.Current.Request.Files[0] : null;
                string filePath = string.Empty;
                string folderName = string.IsNullOrEmpty(request.Form["Folder"] + "") ? "" : (request.Form["Folder"].ToString() + "/");
                if (file != null && file.ContentLength > 0)
                {
                    string extention = file.FileName.Split('.').LastOrDefault();
                    filePath = "/Files/"+folderName.Trim()+ Convert.ToString(request.Form["EmployeeID"]) + "__" + Guid.NewGuid().ToString() + "." + extention;
                    var fullPath = Path.Combine(HttpContext.Current.Server.MapPath("~" + filePath));
                    file.SaveAs(fullPath);
                }
                _employeeServices.SavePublicFile(EmployeeID, filePath, request.Form["Folder"]+"");
                response.Result = Constants.DefaultUpdateConst; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion

        #region PerformenceReview
        [HttpGet]
        [Route("getPerformanceReviewContent")]
        public async Task<IHttpActionResult> getPerformanceReviewContent()
        {
            ServiceStatus<List<ReviewSectionContent>> response = new ServiceStatus<List<ReviewSectionContent>>();
            var result = _employeeServices.GetPerformanceReviewContent();
            if (result != null)
            {
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [HttpGet]
        [Route("getEvaluatorIndicatedType")]
        public async Task<IHttpActionResult> getEvaluatorIndicatedType()
        {
            ServiceStatus<List<EvaluatorIndicatedType>> response = new ServiceStatus<List<EvaluatorIndicatedType>>();
            var result = _employeeServices.GetPerEvaluatorIndicatedType();
            if (result != null)
            {
                response.Result = result; response.StatusCode = HttpStatusCode.OK;
            }
            else
            {
                response.Result = null;
                response.StatusCode = HttpStatusCode.NoContent;
            }
            return Ok(response);
        }
        [HttpGet]
        [Route("getPerformanceTabLabel")]
        public async Task<IHttpActionResult> getPerformanceTabLabel()
        {
            ServiceStatus<PerformanceLable> response = new ServiceStatus<PerformanceLable>();
            PerformanceLable model = new PerformanceLable();
            response.Result = model; response.StatusCode = HttpStatusCode.OK;
            return Ok(response);
        }
        [HttpGet]
        [Route("getPerformenceReview/{EmployeeID}")]
        public async Task<IHttpActionResult> getPerformenceReview(int EmployeeID)
        {
            ServiceStatus<List<PerformenceReview>> response = new ServiceStatus<List<PerformenceReview>>();
            if (EmployeeID > 0)
            {
                var result = _employeeServices.GetPerformenceReview(EmployeeID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }

            return Ok(response);
        }
        [HttpGet]
        [Route("getPerformenceReviewByID/{ID}")]
        public async Task<IHttpActionResult> getPerformenceReviewByID(int ID)
        {
            ServiceStatus<PerformenceReview> response = new ServiceStatus<PerformenceReview>();
            if (ID > 0)
            {
                var result = _employeeServices.GetPerformenceReviewByID(ID);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("getFilterPerformenceReview")]
        public async Task<IHttpActionResult> getFilterPerformenceReview(FilterDto model)
        {
            ServiceStatus<List<PerformenceReview>> response = new ServiceStatus<List<PerformenceReview>>();
            if (model != null && (model.EmoplyeeID > 0|| model.CompanyID> 0))
            {
                var result = _employeeServices.getFilterPerformenceReview(model);
                if (result != null)
                {
                    response.Result = result; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("savePerformenceReview")]
        public async Task<IHttpActionResult> savePerformenceReview()
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            var request = HttpContext.Current.Request;

            if (request != null && !string.IsNullOrEmpty(request.Form["EmployeeID"])
                )
            {
                PerformenceReview model = new PerformenceReview();
                
                    model.ID = Convert.ToInt32(request.Form["ID"]);
                    model.EmployeeID = Convert.ToInt32(request.Form["EmployeeID"]);
                    model.EvaluatorID = Convert.ToInt32(request.Form["EvaluatorID"]);
                    model.EvaluatorPossition = Convert.ToString(request.Form["EvaluatorPossition"]);
                    model.DateOfReview = Convert.ToString(request.Form["DateOfReview"]);
                    model.FilePath = Convert.ToString(request.Form["FilePath"]);
                    model.Status = Convert.ToInt32(request.Form["Status"]);
                    model.ExcusableAbsenteeism = Convert.ToInt32(request.Form["ExcusableAbsenteeism"]);
                    model.InexcusableAbsenteeism = Convert.ToInt32(request.Form["InexcusableAbsenteeism"]);
                    model.LateDepartures = Convert.ToInt32(request.Form["LateDepartures"]);
                    model.Occupational = Convert.ToInt32(request.Form["Occupational"]);
                    model.EmployeeRelations = Convert.ToInt32(request.Form["EmployeeRelations"]);
                    model.QualityAssurance = Convert.ToInt32(request.Form["QualityAssurance"]);
                    model.JobRequirements = Convert.ToInt32(request.Form["JobRequirements"]);
                    model.AdherenceProcedure = Convert.ToInt32(request.Form["AdherenceProcedure"]);
                    model.JobTasks = Convert.ToInt32(request.Form["JobTasks"]);
                    model.OrganizationTasks = Convert.ToInt32(request.Form["OrganizationTasks"]);
                    model.AbilityProblemSolve = Convert.ToInt32(request.Form["AbilityProblemSolve"]);
                    model.Responsibility = Convert.ToInt32(request.Form["Responsibility"]);
                    model.Initiative = Convert.ToInt32(request.Form["Initiative"]);
                    model.AbilityLearn = Convert.ToInt32(request.Form["AbilityLearn"]);
                    model.Adaptability = Convert.ToInt32(request.Form["Adaptability"]);
                    model.AbilityWork = Convert.ToInt32(request.Form["AbilityWork"]);
                    model.CooperationCoWorkers = Convert.ToInt32(request.Form["CooperationCoWorkers"]);
                    model.AttitudeSupervision = Convert.ToInt32(request.Form["AttitudeSupervision"]);
                    model.EmployeeStrengths = Convert.ToString(request.Form["EmployeeStrengths"]);
                    model.ImprovementDesc = Convert.ToString(request.Form["ImprovementDesc"]);
                    model.GaolDesc = Convert.ToString(request.Form["GaolDesc"]);
                    model.Comments = Convert.ToString(request.Form["Comments"]);
                model.CreatedBy = Convert.ToInt32(request.Form["CreatedBy"]);
                
                var file = HttpContext.Current.Request.Files.Count > 0 ?
                          HttpContext.Current.Request.Files[0] : null;
                string filePath = string.Empty;
                if (file != null && file.ContentLength > 0)
                {
                    string extention = file.FileName.Split('.').LastOrDefault();
                    if (string.IsNullOrEmpty(model.FilePath))
                        model.FilePath = "/Files/PerformancReview/" + Convert.ToString(request.Form["EmployeeID"]) + "__" + Guid.NewGuid().ToString() + "." + extention;
                    filePath = Path.Combine(HttpContext.Current.Server.MapPath("~" + model.FilePath));
                    file.SaveAs(filePath);
                }
                var result = _employeeServices.SavePerformenceReview(model);
                if (result > 0)
                {
                    response.ID = result;
                    response.Result = Constants.AddPerformanceReview; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.ID = result;
                    response.Result = Constants.EditPerformanceReview; response.StatusCode = HttpStatusCode.OK; }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("UpdatePerReviewStatus/{ID}")]
        public async Task<IHttpActionResult> UpdatePerReviewStatus(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _employeeServices.UpdatePerReviewStatus(ID);
                response.Result = Constants.DeleteOfferDetail; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("performenceReviewCompleted")]
        public async Task<IHttpActionResult> performenceReviewCompleted(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                _employeeServices.PerformenceReviewCompleted(ID);
                response.Result = Constants.DefaultUpdateConst; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        #endregion

        #region export excel
        [HttpGet]
        [Route("exportPositionInfo/{EmployeeID}")]
        public HttpResponseMessage exportPositionInfo(int EmployeeID)
        {
            HttpResponseMessage httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            if (EmployeeID > 0)
            {
                var positionInfo = _employeeServices.GetPositionInfo(EmployeeID);
                DataTable dt = new DataTable();
                dt.Columns.Add("Position", typeof(string));
                dt.Columns.Add("Start Date", typeof(string));
                dt.Columns.Add("End Date", typeof(string));
                dt.Columns.Add("Notes", typeof(string));
                foreach (var obj in positionInfo)
                {
                    var dr = dt.NewRow();
                    dr["Position"] = obj.Position;
                    dr["Start Date"] = Convert.ToDateTime(obj.StartDate).ToString("MMM dd, yyyy");
                    dr["End Date"] = Convert.ToDateTime(obj.EndDate).ToString("MMM dd, yyyy");
                    dr["Notes"] = obj.Notes + "";
                    dt.Rows.Add(dr);
                }
                dt.TableName ="EmployeePositionListing";
                var dataBytes = Common.ConvertDataTableToByteArray(dt);
                string fileName = "EmployeePositionListing_" + DateTime.Now.Date + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".xlsx";
                Common.ExportFunction(httpResponseMessage, dataBytes, fileName);
            }
            return httpResponseMessage;
        }
        [HttpGet]
        [Route("exportWagesInfo/{EmployeeID}")]
        public HttpResponseMessage exportWagesInfo(int EmployeeID)
        {
            HttpResponseMessage httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            if (EmployeeID > 0)
            {
                var positionInfo = _employeeServices.GetEmpWagesInfo(EmployeeID);
                DataTable dt = new DataTable();
                dt.Columns.Add("Start Date", typeof(string));
                dt.Columns.Add("Amount", typeof(string));
                dt.Columns.Add("Frequency", typeof(string));
                dt.Columns.Add("Notes", typeof(string));
                foreach (var obj in positionInfo)
                {
                    var dr = dt.NewRow();
                    dr["Start Date"] = Convert.ToDateTime(obj.StartDate).ToString("MMM dd, yyyy");
                    dr["Amount"] =Constants.Currency+obj.PayPerPeriod.ToString("0.00");
                    dr["Frequency"] = obj.PayFrequency + "";
                    dr["Notes"] = obj.Notes + "";
                    dt.Rows.Add(dr);
                }
                dt.TableName = "EmployeeWageListing";
                var dataBytes = Common.ConvertDataTableToByteArray(dt);
                string fileName = "EmployeeWageListing_" + DateTime.Now.Date + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".xlsx";
                Common.ExportFunction(httpResponseMessage, dataBytes, fileName);
            }
            return httpResponseMessage;
        }
        [HttpGet]
        [Route("exportDependentInfo/{EmployeeID}")]
        public HttpResponseMessage exportDependentInfo(int EmployeeID)
        {
            HttpResponseMessage httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            if (EmployeeID > 0)
            {
                var dependentInfo = _employeeServices.GetDependentInfo(EmployeeID);
                DataTable dt = new DataTable();
                dt.Columns.Add("Employee Name", typeof(string));
                dt.Columns.Add("DependName", typeof(string));
                dt.Columns.Add("Relationship", typeof(string));
                dt.Columns.Add("Birth Date", typeof(string));
                dt.Columns.Add("Address", typeof(string));
                dt.Columns.Add("City", typeof(string));
                dt.Columns.Add("Province", typeof(string));
                dt.Columns.Add("Country", typeof(string));
                dt.Columns.Add("PostalCode", typeof(string));
                dt.Columns.Add("Phone", typeof(string));
                dt.Columns.Add("Cell", typeof(string));
                dt.Columns.Add("Email", typeof(string));
                dt.Columns.Add("Creation Date", typeof(string));
                dt.Columns.Add("Last Modified Date", typeof(string));
                foreach (var obj in dependentInfo)
                {
                    var dr = dt.NewRow();
                    dr["Employee Name"] = obj.EmployeeName;
                    dr["DependName"] = obj.Name;
                    dr["Relationship"] = obj.Relation;
                    dr["Birth Date"] = obj.DatOfBirth;
                    dr["Address"] = obj.Address;
                    dr["City"] = obj.CityName;
                    dr["Province"] = obj.StateName;
                    dr["Country"] = obj.CountryName;
                    dr["PostalCode"] = obj.PostalCode;
                    dr["Phone"] = obj.Phone;
                    dr["Cell"] = obj.Cell;
                    dr["Email"] = obj.Email;
                    dr["Creation Date"] = Convert.ToDateTime(obj.CreatedDate).ToString("MMM dd, yyyy");
                    dr["Last Modified Date"] = string.IsNullOrEmpty(obj.ModifiedDate)?"":Convert.ToDateTime(obj.ModifiedDate).ToString("MMM dd, yyyy");
                    dt.Rows.Add(dr);
                }
                dt.TableName = "EmployeeDependListing";
                var dataBytes = Common.ConvertDataTableToByteArray(dt);
                string fileName = "EmployeeDependListing_" + DateTime.Now.Date + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".xlsx";
                Common.ExportFunction(httpResponseMessage, dataBytes, fileName);
            }
            return httpResponseMessage;
        }
        [HttpGet]
        [Route("exportTrainingCertInfo/{EmployeeID}")]
        public HttpResponseMessage exportTrainingCertInfo(int EmployeeID)
        {
            HttpResponseMessage httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            if (EmployeeID > 0)
            {
                var certificateInfo = _employeeServices.GetEmployeeCertificateInfo(EmployeeID);
                DataTable dt = new DataTable();
                dt.Columns.Add("Requirement", typeof(string));
                dt.Columns.Add("Expiry Date", typeof(string));
                dt.Columns.Add("Acquired Date", typeof(string));
                dt.Columns.Add("IssuedBy", typeof(string));
                foreach (var obj in certificateInfo)
                {
                    var dr = dt.NewRow();
                    dr["Requirement"] = obj.TrainingCertificate;
                    dr["Expiry Date"] = obj.Expirydate;
                    dr["Acquired Date"] = obj.Acquireddate;
                    dr["IssuedBy"] = obj.IssuedBy;
                    dt.Rows.Add(dr);
                }
                dt.TableName = "EmployeeTraingListing";
                var dataBytes = Common.ConvertDataTableToByteArray(dt);
                string fileName = "EmployeeTraingListing_" + DateTime.Now.Date + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".xlsx";
                Common.ExportFunction(httpResponseMessage, dataBytes, fileName);
            }
            return httpResponseMessage;
        }
        [HttpGet]
        [Route("exportVactionPolicyListing/{EmployeeID}")]
        public HttpResponseMessage exportVactionPolicyListing(int EmployeeID)
        {
            HttpResponseMessage httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            if (EmployeeID > 0)
            {
                var vacationInfo = _employeeServices.GetEmployeeVicationInfo(EmployeeID);
                DataTable dt = new DataTable();
                dt.Columns.Add("Type", typeof(string));
                dt.Columns.Add("Year (From)", typeof(string));
                dt.Columns.Add("Year (To)", typeof(string));
                dt.Columns.Add("# of Days Allowed", typeof(string));
                dt.Columns.Add("# of Paid Days", typeof(string));
                dt.Columns.Add("Creation Date", typeof(string));
                dt.Columns.Add("Created By", typeof(string));
                foreach (var obj in vacationInfo)
                {
                    var dr = dt.NewRow();
                    dr["Type"] = obj.VicationType;
                    dr["Year (From)"] = obj.YearFrom;
                    dr["Year (To)"] = obj.YearTo;
                    dr["# of Days Allowed"] = obj.DayAllow;
                    dr["# of Paid Days"] = obj.DayPaid;
                    dr["Creation Date"] = obj.CreatedDate;
                    dr["Created By"] = obj.CreatedUser;
                    dt.Rows.Add(dr);
                }
                dt.TableName = "VactionPolicyListing";
                var dataBytes = Common.ConvertDataTableToByteArray(dt);
                string fileName = "VactionPolicyListing_" + DateTime.Now.Date + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".xlsx";
                Common.ExportFunction(httpResponseMessage, dataBytes, fileName);
            }
            return httpResponseMessage;
        }
        [HttpGet]
        [Route("exportEmployeeDisciplinaryDetail/{EmployeeID?}/{CompanyID?}")]
        public HttpResponseMessage exportEmployeeDisciplinaryDetail(int EmployeeID=0,int CompanyID=0)
        {
            HttpResponseMessage httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            if (EmployeeID > 0||CompanyID>0)
            {
                List<EmployeeDisciplinaryDetail> lstDisciplinary = new List<EmployeeDisciplinaryDetail>();

                if (EmployeeID > 0)
                    lstDisciplinary = _employeeServices.GetEmployeeDisciplinaryDetail(EmployeeID);
                else
                    lstDisciplinary = _employeeServices.GetDisciplinaryDetailByCompanyID(CompanyID);

                DataTable dt = new DataTable();
                dt.Columns.Add("CompanyName", typeof(string));
                dt.Columns.Add("Employee Name", typeof(string));
                dt.Columns.Add("Nature Of Infraction", typeof(string));
                dt.Columns.Add("Infraction Date", typeof(string));
                dt.Columns.Add("Warning Type", typeof(string));
                dt.Columns.Add("Consequences", typeof(string));
                dt.Columns.Add("Status", typeof(string));
                dt.Columns.Add("Infraction Details", typeof(string));
                dt.Columns.Add("Created On", typeof(string));
                foreach (var obj in lstDisciplinary)
                {
                    var dr = dt.NewRow();
                    dr["CompanyName"] = obj.CompanyName;
                    dr["Employee Name"] = obj.EmployeeName;
                    dr["Nature Of Infraction"] = obj.Infraction;
                    dr["Infraction Date"] = obj.DisciplinaryDate;
                    dr["Warning Type"] = obj.WarningType;
                    dr["Consequences"] = obj.Expectations;
                    dr["Status"] = obj.Status;
                    dr["Infraction Details"] = obj.InfractionDetails;
                    dr["Created On"] = Convert.ToDateTime(obj.CreatedDate).ToString("MMM dd, yyyy");
                    dt.Rows.Add(dr);
                }
                dt.TableName = "EmployeeDisciplinaryListing";
                var dataBytes = Common.ConvertDataTableToByteArray(dt);
                string fileName = "EmployeeDisciplinaryListing_" + DateTime.Now.Date + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".xlsx";
                Common.ExportFunction(httpResponseMessage, dataBytes, fileName);
            }
            return httpResponseMessage;
        }
        [HttpGet]
        [Route("exportTerminationList/{EmployeeID?}/{CompanyID?}")]
        public HttpResponseMessage exportTerminationList(int EmployeeID = 0, int CompanyID = 0)
        {
            HttpResponseMessage httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            if (EmployeeID > 0 || CompanyID > 0)
            {
                List<EmployeeTerminationDetail> lstTermination = new List<EmployeeTerminationDetail>();

                if (EmployeeID > 0)
                    lstTermination = _employeeServices.GetEmployeeTerminationDetail(EmployeeID);
                else
                    lstTermination = _employeeServices.GetTerminationDetailByCompanyID(CompanyID);

                DataTable dt = new DataTable();
                dt.Columns.Add("Employee Name", typeof(string));
                dt.Columns.Add("Creator Name", typeof(string));
                dt.Columns.Add("Modifier Name", typeof(string));
                dt.Columns.Add("Created On", typeof(string));
                dt.Columns.Add("Last Modified", typeof(string));
                dt.Columns.Add("Status", typeof(string));
                foreach (var obj in lstTermination)
                {
                    var dr = dt.NewRow();
                    dr["Employee Name"] = obj.EmployeeName;
                    dr["Creator Name"] = obj.CreatedUser;
                    dr["Modifier Name"] = obj.ModifiedUser;
                    dr["Creaed On"] = Convert.ToDateTime(obj.CreatedDate).ToString("MMM dd, yyyy");
                    dr["Last Modified"] = Convert.ToDateTime(obj.ModifiedDate).ToString("MMM dd, yyyy");
                    dr["Status"] = obj.Status;
                    dt.Rows.Add(dr);
                }
                dt.TableName = "EmployeeTerminationListing";
                var dataBytes = Common.ConvertDataTableToByteArray(dt);
                string fileName = "EmployeeTerminationListing_" + DateTime.Now.Date + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".xlsx";
                Common.ExportFunction(httpResponseMessage, dataBytes, fileName);
            }
            return httpResponseMessage;
        }
        [HttpGet]
        [Route("exportEmployeeOfferDetail/{EmployeeID?}/{CompanyID?}")]
        public HttpResponseMessage exportEmployeeOfferDetail(int EmployeeID=0,int CompanyID=0)
        {
            HttpResponseMessage httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            if (EmployeeID > 0||CompanyID>0)
            {
                List<EmployeeOfferDetail> lstOfferDetails = new List<EmployeeOfferDetail>();
                if (EmployeeID > 0)
                    lstOfferDetails = _employeeServices.GetEmployeeOfferDetails(EmployeeID);
                else
                    lstOfferDetails = _employeeServices.GetEmployeeOfferDetailsByCompanyID(CompanyID);

                DataTable dt = new DataTable();
                dt.Columns.Add("Candidate Name", typeof(string));
                dt.Columns.Add("Job Title", typeof(string));
                dt.Columns.Add("Type of Employment", typeof(string));
                dt.Columns.Add("Status", typeof(string));
                dt.Columns.Add("Creation Date", typeof(string));
                foreach (var obj in lstOfferDetails)
                {
                    var dr = dt.NewRow();
                    dr["Candidate Name"] = obj.EmployeeName;
                    dr["Job Title"] = obj.JobTitle;
                    dr["Type of Employment"] = obj.EmploymentType;
                    dr["Status"] = obj.Status;
                    dr["Creation Date"] = Convert.ToDateTime(obj.CreatedDate).ToString("MMM dd, yyyy");
                    dt.Rows.Add(dr);
                }
                dt.TableName = "EmployeeOfferList";
                var dataBytes = Common.ConvertDataTableToByteArray(dt);
                string fileName = "LetterOfOffer_" + DateTime.Now.Date + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".xlsx";
                Common.ExportFunction(httpResponseMessage, dataBytes, fileName);
            }
            return httpResponseMessage;
        }
        [HttpGet]
        [Route("exportPerformanceList/{EmployeeID?}/{CompanyID?}")]
        public HttpResponseMessage exportPerformanceList(int EmployeeID=0,int CompanyID=0)
        {
            HttpResponseMessage httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            if (EmployeeID > 0||CompanyID>0)
            {
                List<PerformenceReview> lstReview = new List<PerformenceReview>();
                if (EmployeeID > 0)
                    lstReview = _employeeServices.GetPerformenceReview(EmployeeID);
                else
                {
                    FilterDto model = new FilterDto { CompanyID = CompanyID };
                    lstReview = _employeeServices.getFilterPerformenceReview(model);
                }
                DataTable dt = new DataTable();
                dt.Columns.Add("Employee Name", typeof(string));
                dt.Columns.Add("Creator Name", typeof(string));
                dt.Columns.Add("Modifier Name", typeof(string));
                dt.Columns.Add("Modified", typeof(string));
                dt.Columns.Add("Creation On", typeof(string));
                dt.Columns.Add("Status", typeof(string));
                foreach (var obj in lstReview)
                {
                    var dr = dt.NewRow();
                    dr["Employee Name"] = obj.EmployeeName;
                    dr["Creator Name"] = obj.CreatedUser;
                    dr["Modifier Name"] = obj.ModifiedUser;
                    dr["Modified"] = obj.ModifiedDate==""?"":Convert.ToDateTime(obj.ModifiedDate).ToString("MMM dd, yyyy"); ;
                    dr["Creation On"] = Convert.ToDateTime(obj.CreatedDate).ToString("MMM dd, yyyy");
                    dr["Status"] = obj.StatusValue;
                    dt.Rows.Add(dr);
                }
                dt.TableName = "EmployeePerformanceTrackerList";
                var dataBytes = Common.ConvertDataTableToByteArray(dt);
                string fileName = "EmployeePerformanceTrackerList_" + DateTime.Now.Date + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".xlsx";
                Common.ExportFunction(httpResponseMessage, dataBytes, fileName);
            }
            return httpResponseMessage;
        }
        [HttpGet]
        [Route("exportEmployeeAccidentList/{EmployeeID?}/{CompanyID?}")]
        public HttpResponseMessage exportEmployeeAccidentList(int EmployeeID=0,int CompanyID=0)
        {
            HttpResponseMessage httpResponseMessage = Request.CreateResponse(HttpStatusCode.OK);
            if (EmployeeID > 0||CompanyID>0)
            {
                List<AccidentDetail> objlist = new List<AccidentDetail>();
                if (EmployeeID > 0)
                    objlist = _employeeServices.GetAccidentDetail(EmployeeID);
                else
                    objlist = _employeeServices.GetAccidentDetailByCompanyID(CompanyID);

                DataTable dt = new DataTable();
                dt.Columns.Add("Employee Name", typeof(string));
                dt.Columns.Add("Date of Accident", typeof(string));
                dt.Columns.Add("Accident Reported To", typeof(string));
                dt.Columns.Add("Created By", typeof(string));
                dt.Columns.Add("Creation On", typeof(string));
                dt.Columns.Add("Status", typeof(string));
                foreach (var obj in objlist)
                {
                    var dr = dt.NewRow();
                    dr["Employee Name"] = obj.EmployeeName;
                    dr["Date of Accident"] = Convert.ToDateTime(obj.DateOfAccident).ToString("MMM dd, yyyy");
                    dr["Accident Reported To"] = obj.ReportedManager;
                    dr["Created By"] = obj.CreatedUser;
                    dr["Creation On"] = Convert.ToDateTime(obj.CreatedDate).ToString("MMM dd, yyyy");
                    dr["Status"] = obj.StatusValue;
                    dt.Rows.Add(dr);
                }
                dt.TableName = "EmployeeAccidentTrackerList";
                var dataBytes = Common.ConvertDataTableToByteArray(dt);
                string fileName = "EmployeeAccidentTrackerList_" + DateTime.Now.Date + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".xlsx";
                Common.ExportFunction(httpResponseMessage, dataBytes, fileName);
            }
            return httpResponseMessage;
        }
        #endregion

        #region genrate word doc
        [HttpGet]
        [Route("genrateDisciplinaryEventletter/{ID}/{type?}")]
        public async Task<IHttpActionResult> genrateDisciplinaryEventletter(int ID,string type= null)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                Common.ClearTempDirectory("/Files/tmp/");
                var obj = _employeeServices.GetEmployeeDisciplinaryDetailByID(ID);
                if (obj != null)
                {
                    string fileName = "DisciplinaryEvent_letter_" + ID + "_" + DateTime.Now.ToString("MMM_dd_yyyy");

                    string path = HttpContext.Current.Server.MapPath("~/Files/Template/DisciplinaryTemplate.html");
                    string strFileData = File.ReadAllText(path);
                    strFileData = strFileData.Replace("{{CurrentDate}}", DateTime.Now.ToString("MMM dd, yyyy"))
                        .Replace("{{EmployeeName}}", obj.EmployeeName).Replace("{{Address}}", obj.Address).Replace("{{City}}", obj.City).Replace("{{State}}", obj.State)
                        .Replace("{{PostalCode}}", obj.PostalCode).Replace("{{WarningType}}", obj.WarningType).Replace("{{CompanyName}}", obj.CompanyName)
                        .Replace("{{NatureOfInfraction}}", obj.Infraction).Replace("{{InfractionDetails}}", obj.InfractionDetails)
                        .Replace("{{Expectations}}", obj.Expectations).Replace("{{CreatedUser}}", obj.CreatedUser)
                        .Replace("{{PhoneNumber}}", obj.EmployeePhone).Replace("{{CompanyAddress}}", obj.CompanyAddress);

                    SaveTempFile(type, fileName, strFileData);
                    response.Result = fileName; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        [HttpGet]
        [Route("genrateTerminationletter/{ID}/{type?}")]
        public async Task<IHttpActionResult> genrateTerminationletter(int ID,string type=null)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                Common.ClearTempDirectory("/Files/tmp/");
                var obj = _employeeServices.GetEmployeeTerminationDetailByID(ID);
                if (obj != null)
                {
                    string fileName = "Termination_letter_" + ID + "_" + DateTime.Now.ToString("MMM_dd_yyyy");
                    string path = "";
                    if (obj.TerminationType.ToUpper().Trim() == Constants.JustCause)
                        path = HttpContext.Current.Server.MapPath("~/Files/Template/Termination_JustCause.html");
                    else if(obj.TerminationType.ToUpper().Trim() == Constants.UnsatisfactoryProbationaryPeriod)
                        path = HttpContext.Current.Server.MapPath("~/Files/Template/Termination_UnsatisfactoryProbationaryPeriod.html");
                    else if(obj.TerminationType.ToUpper().Trim() == Constants.WithoutCauseRelease)
                        path = HttpContext.Current.Server.MapPath("~/Files/Template/Termination_WithoutCauseRelease.html");
                    else
                        path = HttpContext.Current.Server.MapPath("~/Files/Template/Termination_Other.html");

                    string strFileData = File.ReadAllText(path);
                    strFileData = strFileData.Replace("{{CurrentDate}}", DateTime.Now.ToString("MMM dd, yyyy"))
                        .Replace("{{EmployeeName}}", obj.EmployeeName).Replace("{{Address}}", obj.Address).Replace("{{City}}", obj.City)
                        .Replace("{{PostalCode}}", obj.PostalCode).Replace("{{TerminationType}}", obj.TerminationType).Replace("{{FinalPay}}", obj.FinalPayValue)
                        .Replace("{{TerminationPayPaidValue}}", obj.TerminationPayPaidValue).Replace("{{TerminationPay}}", obj.TerminationPay.ToString()+(obj.TerminationPay>1?" weeks":" week"))
                        .Replace("{{LastDayWorkDate}}", Convert.ToDateTime(obj.LastDayWorkDate).ToString("MMM dd, yyyy"))
                        .Replace("{{PhoneNumber}}",obj.Phone).Replace("{{CompanyAddress}}", obj.CompanyAddress)
                        .Replace("{{CompanyName}}", obj.CompanyName).Replace("{{OtherInfo}}", obj.OtherInfo).Replace("{{Designation}}", obj.Designation)
                        .Replace("{{DATEofBENEFITS}}",obj.BenefitsCeaseDate).Replace("{{display}}", string.IsNullOrEmpty(obj.OtherInfo)?"none":"")
                        .Replace("{{ReportingManager}}", obj.ReportingManager);
                    SaveTempFile(type, fileName, strFileData);
                    response.Result = fileName; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("genrateLayOffletter/{ID}")]
        public async Task<IHttpActionResult> genrateLayOffletter(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                Common.ClearTempDirectory("/Files/tmp/");
                var obj = _employeeServices.GetLayOffEmpDetails(ID);
                if (obj != null)
                {
                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.Charset = "";
                    HttpContext.Current.Response.ContentType = "application/msword";
                    string strFileName = "Layoffs_letter_" + ID + "_" + DateTime.Now.ToString("MMM_dd_yyyy");
                    HttpContext.Current.Response.AddHeader("Content-Disposition", "inline;filename=" + strFileName + ".doc");
                    string path = HttpContext.Current.Server.MapPath("~/Files/Template/Layoff.html");

                    string strFileData = File.ReadAllText(path);
                    strFileData = strFileData.Replace("{{CurrentDate}}", DateTime.Now.ToString("MMM dd, yyyy"))
                        .Replace("{{EmployeeName}}", (obj.FirstName + " " + obj.LastName)).Replace("{{Address}}", obj.Address).Replace("{{City}}", obj.City)
                        .Replace("{{PostalCode}}", obj.PinCode).Replace("{{State}}", obj.State)
                        .Replace("{{Phone}}", obj.Phone).Replace("{{CompanyAddress}}", obj.CompanyAddress)
                        .Replace("{{CompanyName}}", obj.CompanyName).Replace("{{Owner}}", obj.Owner).Replace("{{LastWorkDate}}", obj.LastWorkDate);
                    string filePathSave = HttpContext.Current.Server.MapPath("~/Files/tmp/") + strFileName + ".doc";
                    File.WriteAllText(filePathSave, strFileData); // is supposed to overwrite the file if it is there but doesn't
                    response.Result = strFileName; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("genrateOfferletter/{ID}")]
        public async Task<IHttpActionResult> genrateOfferletter(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                Common.ClearTempDirectory("/Files/tmp/");
                var obj = _employeeServices.GetEmployeeOfferDetailsByID(ID);
                if (obj != null)
                {
                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.Charset = "";
                    HttpContext.Current.Response.ContentType = "application/msword";
                    string strFileName = "Offerletter_" + ID + "_" + DateTime.Now.ToString("MMM_dd_yyyy");
                    HttpContext.Current.Response.AddHeader("Content-Disposition", "inline;filename=" + strFileName + ".doc");
                    string path = HttpContext.Current.Server.MapPath("~/Files/Template/OfferLetterTemplate.html");
                    string strFileData = File.ReadAllText(path);
                    strFileData = strFileData.Replace("{{CurrentDate}}", DateTime.Now.ToString("MMM dd, yyyy"))
                     .Replace("{{EmployeeName}}", obj.EmployeeName).Replace("{{Address}}", obj.Address).Replace("{{City}}", obj.City).Replace("{{State}}", obj.State)
                     .Replace("{{PostalCode}}", obj.PostalCode).Replace("{{ReportingManager}}", obj.ReportingManager).Replace("{{CompanyName}}", obj.CompanyName)
                     .Replace("{{Startdate}}", obj.Startdate).Replace("{{CompensationType}}", obj.CompensationType)
                     .Replace("{{CompensationAmount}}", obj.CompensationAmount.ToString("0.00")).Replace("{{WorkingDay}}", obj.WorkingDayType + (string.IsNullOrEmpty(obj.OtherInfo) ? "" : (" (" + obj.OtherInfo + ")")))
                     .Replace("{{Vacation}}", obj.OfferVacationsType).Replace("{{Benifits}}", obj.OfferBenefitsType).Replace("{{OfferAcceptingDate}}", obj.OfferAcceptingDate)
                     ;
                    string filePathSave = HttpContext.Current.Server.MapPath("~/Files/tmp/") + strFileName + ".doc";
                    File.WriteAllText(filePathSave, strFileData); // is supposed to overwrite the file if it is there but doesn't
                    response.Result = strFileName; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("genratePerformanceletter/{ID}")]
        public async Task<IHttpActionResult> genratePerformanceletter(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                Common.ClearTempDirectory("/Files/tmp/");
                var obj = _employeeServices.GetPerformenceReviewByID(ID);
                if (obj != null)
                {
                    var reviewContent= _employeeServices.GetPerformanceReviewContent();
                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.Charset = "";
                    HttpContext.Current.Response.ContentType = "application/msword";
                    string strFileName = "PerformanceReviewTemplate_" + ID + "_" + DateTime.Now.ToString("MMM_dd_yyyy");
                    HttpContext.Current.Response.AddHeader("Content-Disposition", "inline;filename=" + strFileName + ".doc");
                    string path = HttpContext.Current.Server.MapPath("~/Files/Template/PerformanceReviewTemplate.html");
                    string strFileData = File.ReadAllText(path);
                    string strList = "",sublist = "",strSectionA="";
                    int count = 0;
                    foreach (var sectionContent in reviewContent)
                    {
                        count++;
                        strList += @"<TD WIDTH=321 STYLE='border-top: 1.50pt solid #000000; border-bottom: 1px solid #000000; border-left: 1.50pt solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
                                    <OL START=" + count + @">
                                        <LI>
                                            <P CLASS='western' STYLE='margin-bottom: 0in'>
                                                <FONT SIZE=1 STYLE='font-size: 8pt'><B>" + sectionContent.Title + @" : </B></FONT><FONT SIZE=1 STYLE='font-size: 8pt'>
                                                    <I>" + sectionContent.Title + @"</I>
                                                </FONT>
                                            </P>
                                    </OL>
                                    <UL>";
                        sublist = "";
                        foreach (var subContent in sectionContent.Content)
                        {
                            sublist += @"<LI>
                                            <P CLASS='western' STYLE='margin-bottom: 0in'>
                                                <FONT SIZE=1 STYLE='font-size: 8pt'>" + subContent + @"</FONT>
                                            </P>
                                        </LI>";
                        }
                        strList = strList + sublist + "</UL></TD>";
                        if (count % 2 == 0)
                        {
                            strSectionA += "<TR VALIGN=TOP>" + strList + "</TR>";
                            strList = "";
                        
                        }
                    }
                    string strSectionContent2A= SectionCOntent2A(obj);
                    strFileData = strFileData.Replace("{{EmployeeName}}", obj.EmployeeName).Replace("{{EmployeeID}}", obj.EmployeeNumber)
                     .Replace("{{EmployeePosition}}", obj.EmployeePosition).Replace("{{EvaluatorName}}", obj.EvaluatorName)
                     .Replace("{{EvalutorPosition}}", obj.EvaluatorPossition).Replace("{{DateOfReview}}", obj.DateOfReview)
                     .Replace("{{Section2}}", strSectionA).Replace("{{EmployeeStrengths}}", obj.EmployeeStrengths)
                     .Replace("{{ImprovementDesc}}", obj.ImprovementDesc).Replace("{{GaolDesc}}", obj.GaolDesc)
                     .Replace("{{Comments}}", obj.Comments).Replace("{{SectionContent2A}}", strSectionContent2A);
                    string filePathSave = HttpContext.Current.Server.MapPath("~/Files/tmp/") + strFileName + ".doc";
                    File.WriteAllText(filePathSave, strFileData); // is supposed to overwrite the file if it is there but doesn't
                    response.Result = strFileName; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("genrateAccidentReport/{ID}")]
        public async Task<IHttpActionResult> genrateAccidentReport(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                Common.ClearTempDirectory("/Files/tmp/");
                var obj = _employeeServices.GetAccidentDetailByID(ID);
                if (obj != null)
                {
                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.Charset = "";
                    HttpContext.Current.Response.ContentType = "application/msword";
                    string strFileName = "EmployeeAccidentTracker_" + ID + "_" + DateTime.Now.ToString("MMM_dd_yyyy");
                    HttpContext.Current.Response.AddHeader("Content-Disposition", "inline;filename=" + strFileName + ".doc");
                    string path = HttpContext.Current.Server.MapPath("~/Files/Template/AccidentTemplate.html.html");
                    string strFileData = File.ReadAllText(path);
                    strFileData = strFileData.Replace("{{EmployeeName}}",obj.EmployeeName).Replace("{{EmployeeCode}}",obj.EmployeeCode)
                     .Replace("{{DateOfAccident}}", obj.DateOfAccident).Replace("{{TimeOfAccident}}", obj.TimeOfAccident)
                     .Replace("{{AccidentReportedTo}}", obj.ReportedManager).Replace("{{DateOfReported}}", obj.DateOfReported)
                     .Replace("{{TimeOfReported}}", obj.TimeOfReported)
                     .Replace("{{Accident}}", string.Join(",",obj.AccidentTypeList.Select(x=>x.AccidentType)))
                     .Replace("{{AccidentIllness}}", string.Join(",", obj.DiseasesList.Select(x => x.DiseasesType)))
                     .Replace("{{BodyPart}}", string.Join(",", obj.BodyPartMapList.Select(x => x.BodyPartType)))
                     .Replace("{{Description}}", obj.Description)
                     .Replace("{{AccidentHappendEmployeerPremises}}", obj.AccidentHappendEmployeerPremises==1?"Yes":"No")
                     .Replace("{{AccidentHappendEmployeerPremisesDesc}}", obj.AccidentHappendEmployeerPremisesDesc)
                     .Replace("{{AccidentOutSideProvince}}", obj.AccidentOutSideProvince==1 ? "Yes" : "No")
                     .Replace("{{AccidentOutSideProvinceDesc}}", obj.AccidentOutSideProvinceDesc)
                     .Replace("{{AnyWitness}}", obj.AnyWitness==1 ? "Yes" : "No")
                     .Replace("{{WitnessDesc}}", obj.WitnessDesc)
                     .Replace("{{ResonposibleAccident}}", obj.ResonposibleForAccident==1? "Yes" : "No")
                     .Replace("{{ResonposibleDescritption}}", obj.ResonposibleDescritption)
                     .Replace("{{AwareAnyInjury}}", obj.AwareAnyInjury==1 ? "Yes" : "No")
                     .Replace("{{AwareAnyInjuryDesc}}", obj.AwareAnyInjuryDesc)
                     .Replace("{{ConcernClaim}}", obj.ConcernClaim==1?"Yes":"No")
                     .Replace("{{NameOfPerson}}",obj.NameOfPerson).Replace("{{OfficeTitle}}", obj.OfficeTitle).Replace("{{Telephone}}",(obj.Ext+"-"+obj.Telephone))
                     .Replace("{{RegisterDate}}", obj.RegisterDate)
                     ;
                    string filePathSave = HttpContext.Current.Server.MapPath("~/Files/tmp/") + strFileName + ".doc";
                    File.WriteAllText(filePathSave, strFileData); // is supposed to overwrite the file if it is there but doesn't
                    response.Result = strFileName; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        
        #endregion

        #region Private
        private string SectionCOntent2A(PerformenceReview obj)
        {
            return @"<TR>
		<TD WIDTH=147 HEIGHT=10 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1.50pt solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<OL>
				<LI><P CLASS='western' STYLE='margin-bottom: 0in'><FONT SIZE=1 STYLE='font-size: 8pt'>Excusable</FONT></P>
			</OL>
			<P CLASS='western' STYLE='margin-left: 0.25in'><FONT SIZE=1 STYLE='font-size: 8pt'>Absenteeism</FONT></P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.ExcusableAbsenteeism == 1 ? "✔" : "") + @"</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.ExcusableAbsenteeism == 2 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.ExcusableAbsenteeism == 3 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=122 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.ExcusableAbsenteeism == 4 ? "✔" : "") + @"
			</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=147 HEIGHT=10 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1.50pt solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<OL START=2>
				<LI><P CLASS='western' STYLE='margin-bottom: 0in'><FONT SIZE=1 STYLE='font-size: 8pt'>Inexcusable</FONT></P>
			</OL>
			<P CLASS='western' STYLE='margin-left: 0.25in'><FONT SIZE=1 STYLE='font-size: 8pt'>Absenteeism</FONT></P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.InexcusableAbsenteeism == 1 ? "✔" : "") + @"</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.InexcusableAbsenteeism == 2 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.InexcusableAbsenteeism == 3 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=122 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.InexcusableAbsenteeism == 4 ? "✔" : "") + @"
			</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=147 HEIGHT=10 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1.50pt solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<OL START=3>
				<LI><P CLASS='western'><FONT SIZE=1 STYLE='font-size: 8pt'>Late
				Arrivals/Early Departures</FONT></P>
			</OL>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.LateDepartures == 1 ? "✔" : "") + @"</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.LateDepartures == 2 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.LateDepartures == 3 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=122 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.LateDepartures == 4 ? "✔" : "") + @"
			</P>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=5 WIDTH=670 BGCOLOR='#e5e5e5' STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1.50pt solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in'>
			<OL START=2>
				<LI><P CLASS='western'><FONT SIZE=1 STYLE='font-size: 8pt'><B>JOB
				KNOWLEDGE </B></FONT><FONT SIZE=1 STYLE='font-size: 6pt'>(based
				on the applicable section of the appropriate </FONT><FONT SIZE=1 STYLE='font-size: 6pt'><B>Performance
				Review Checklist</B></FONT><FONT SIZE=1 STYLE='font-size: 6pt'>)</FONT></P>
			</OL>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=147 HEIGHT=17 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1.50pt solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<OL>
				<LI><P CLASS='western'><FONT SIZE=1 STYLE='font-size: 8pt'>Occupational
				             Health and Safety </FONT>
				</P>
			</OL>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.Occupational == 1 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.Occupational == 2 ? "✔" : "") + @"</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.Occupational == 3 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=122 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.Occupational == 4 ? "✔" : "") + @"
			</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=147 HEIGHT=8 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1.50pt solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<OL START=2>
				<LI><P CLASS='western'><FONT SIZE=1 STYLE='font-size: 8pt'>Employee
				Relations</FONT></P>
			</OL>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.EmployeeRelations == 1 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.EmployeeRelations == 2 ? "✔" : "") + @"</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.EmployeeRelations == 3 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=122 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.EmployeeRelations == 4 ? "✔" : "") + @"
			</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=147 HEIGHT=8 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1.50pt solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<OL START=3>
				<LI><P CLASS='western'><FONT SIZE=1 STYLE='font-size: 8pt'>Quality
				Assurance</FONT></P>
			</OL>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.QualityAssurance == 1 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.QualityAssurance == 2 ? "✔" : "") + @"</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.QualityAssurance == 3 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=122 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.QualityAssurance == 4 ? "✔" : "") + @"
			</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=147 HEIGHT=8 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1.50pt solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<OL START=4>
				<LI><P CLASS='western'><FONT SIZE=1 STYLE='font-size: 8pt'>Job
				Requirements</FONT><FONT SIZE=1 STYLE='font-size: 8pt'><B> </B></FONT>
				</P>
			</OL>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.JobRequirements == 1 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.JobRequirements == 2 ? "✔" : "") + @"</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.JobRequirements == 3 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=122 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.JobRequirements == 4 ? "✔" : "") + @"
			</P>
		</TD>
	</TR>
	<TR>
		<TD COLSPAN=5 WIDTH=670 VALIGN=TOP BGCOLOR='#e5e5e5' STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1.50pt solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in'>
			<OL START=3>
				<LI><H2 CLASS='western'>WORK CONDUCT</H2>
			</OL>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=147 HEIGHT=9 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1.50pt solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<OL>
				<LI><P CLASS='western' STYLE='margin-bottom: 0in'><FONT SIZE=1 STYLE='font-size: 8pt'>Adherence
				to Procedures</FONT></P>
			</OL>
			<P CLASS='western'><FONT SIZE=1 STYLE='font-size: 6pt'>	(including
			documentation)</FONT></P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.AdherenceProcedure == 1 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.AdherenceProcedure == 2 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.AdherenceProcedure == 3 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=122 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.AdherenceProcedure == 4 ? "✔" : "") + @"
			</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=147 HEIGHT=9 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1.50pt solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<OL START=2>
				<LI><P CLASS='western'><FONT SIZE=1 STYLE='font-size: 8pt'>Thoroughness/Accuracy
				of Job Tasks </FONT>
				</P>
			</OL>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.JobTasks == 1 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.JobTasks == 2 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.JobTasks == 3 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=122 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.JobTasks == 4 ? "✔" : "") + @"
			</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=147 HEIGHT=9 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1.50pt solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<OL START=3>
				<LI><P CLASS='western' STYLE='margin-bottom: 0in'><FONT SIZE=1 STYLE='font-size: 8pt'>Organization
				of Job Tasks</FONT></P>
			</OL>
			<P CLASS='western'><FONT SIZE=1 STYLE='font-size: 6pt'>	(i.e.
			finished goods)</FONT></P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.OrganizationTasks == 1 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.OrganizationTasks == 2 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.OrganizationTasks == 3 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=122 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.OrganizationTasks == 4 ? "✔" : "") + @"
			</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=147 HEIGHT=9 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1.50pt solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<OL START=4>
				<LI><P CLASS='western' STYLE='margin-bottom: 0in'><FONT SIZE=1 STYLE='font-size: 8pt'>Ability
				to Problem Solve</FONT></P>
			</OL>
			<P CLASS='western'><FONT SIZE=1 STYLE='font-size: 6pt'>	(including
			troubleshooting)</FONT></P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.AbilityProblemSolve == 1 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.AbilityProblemSolve == 2 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.AbilityProblemSolve == 3 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=122 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.AbilityProblemSolve == 4 ? "✔" : "") + @"
			</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=147 HEIGHT=9 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1.50pt solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<OL START=5>
				<LI><P CLASS='western'><FONT SIZE=1 STYLE='font-size: 8pt'>Acceptance
				of Responsibility</FONT></P>
			</OL>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.Responsibility == 1 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.Responsibility == 2 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.Responsibility == 3 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=122 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.Responsibility == 4 ? "✔" : "") + @"
			</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=147 HEIGHT=9 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1.50pt solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<OL START=6>
				<LI><P CLASS='western'><FONT SIZE=1 STYLE='font-size: 8pt'>Initiative</FONT></P>
			</OL>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.Initiative == 1 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.Initiative == 2 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.Initiative == 3 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=122 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.Initiative == 4 ? "✔" : "") + @"
			</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=147 HEIGHT=9 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1.50pt solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<OL START=7>
				<LI><P CLASS='western'><FONT SIZE=1 STYLE='font-size: 8pt'>Ability
				to Learn</FONT></P>
			</OL>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.AbilityLearn == 1 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.AbilityLearn == 2 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.AbilityLearn == 3 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=122 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.AbilityLearn == 4 ? "✔" : "") + @"
			</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=147 HEIGHT=9 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1.50pt solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<OL START=8>
				<LI><H2 CLASS='western'><SPAN STYLE='font-weight: normal'>Adaptability
				</SPAN><FONT SIZE=1 STYLE='font-size: 6pt'><SPAN STYLE='font-weight: normal'>(i.e.
				acceptance of temporary work assignments)</SPAN></FONT></H2>
			</OL>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.Adaptability == 1 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.Adaptability == 2 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.Adaptability == 3 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=122 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.Adaptability == 4 ? "✔" : "") + @"
			</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=147 HEIGHT=9 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1.50pt solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<OL START=9>
				<LI><H4 CLASS='western' STYLE='font-style: normal'>Ability to
				Work Independently 
				</H4>
			</OL>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.AbilityWork == 1 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.AbilityWork == 2 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.AbilityWork == 3 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=122 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.AbilityWork == 4 ? "✔" : "") + @"
			</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=147 HEIGHT=9 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1.50pt solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<OL START=10>
				<LI><P CLASS='western' STYLE='margin-bottom: 0in'><FONT SIZE=1 STYLE='font-size: 8pt'>Cooperation
				with </FONT>
				</P>
			</OL>
			<P CLASS='western'><FONT SIZE=1 STYLE='font-size: 8pt'>	Co-workers</FONT></P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.CooperationCoWorkers == 1 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.CooperationCoWorkers == 2 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.CooperationCoWorkers == 3 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=122 STYLE='border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.CooperationCoWorkers == 4 ? "✔" : "") + @"
			</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=147 HEIGHT=7 STYLE='border-top: 1px solid #000000; border-bottom: 1.50pt solid #000000; border-left: 1.50pt solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<OL START=11>
				<LI><P CLASS='western'><FONT SIZE=1 STYLE='font-size: 8pt'>Attitude
				towards Supervision</FONT></P>
			</OL>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1.50pt solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.AttitudeSupervision == 1 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1.50pt solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.AttitudeSupervision == 2 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=115 STYLE='border-top: 1px solid #000000; border-bottom: 1.50pt solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0.08in; padding-right: 0in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.AttitudeSupervision == 3 ? "✔" : "") + @"
			</P>
		</TD>
		<TD WIDTH=122 STYLE='border-top: 1px solid #000000; border-bottom: 1.50pt solid #000000; border-left: 1px solid #000000; border-right: 1.50pt solid #000000; padding: 0in 0.08in'>
			<P CLASS='western' ALIGN=CENTER>" + (obj.AttitudeSupervision == 4 ? "✔" : "") + @"
			</P>
		</TD>
	</TR>";

        }
        private static void SaveTempFile(string type, string fileName, string strFileData)
        {
            if (type == "PDF")
            {
                var htmlToPdf = new HtmlToPdfConverter();
                var pdfBytes = htmlToPdf.GeneratePdf(strFileData);
                System.IO.File.WriteAllBytes(HttpContext.Current.Server.MapPath("~/Files/tmp/") + fileName + ".pdf", pdfBytes);
            }
            else
            {
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.Charset = "";
                HttpContext.Current.Response.ContentType = "application/msword";
                HttpContext.Current.Response.AddHeader("Content-Disposition", "inline;filename=" + fileName + ".doc");
                string filePathSave = HttpContext.Current.Server.MapPath("~/Files/tmp/") + fileName + ".doc";
                File.WriteAllText(filePathSave, strFileData);
            }
        }
        #endregion

        #region Bulk Upload & Genrate employeeid
        [HttpGet]
        [Route("getBulkRequiredColumn")]
        public async Task<IHttpActionResult> getBulkRequiredColumn()
        {
            ServiceStatus<List<string>> response = new ServiceStatus<List<string>>();
            response.Result.Add("FirstName");
            response.Result.Add("LastName");
            response.Result.Add("Email");
            response.Result.Add("DateOfBirth");
            response.Result.Add("Gender");
            response.Result.Add("JoiningDate");
            response.Result.Add("Phone");
            response.Result.Add("Country");
            response.Result.Add("State");
            response.Result.Add("City");
            response.Result.Add("PostalCode");
            response.Result.Add("Address");
            response.StatusCode = HttpStatusCode.OK;
            return Ok(response);
        }

        [HttpPost]
        [Route("GetFileColumn")]
        public async Task<IHttpActionResult> GetFileColumn()
        {
            ServiceStatus<List<string>> response = new ServiceStatus<List<string>>();
            var request = HttpContext.Current.Request;
            var file = request.Files.Count > 0 ? request.Files[0] : null;
            string filePath = string.Empty;
            if (file != null && file.ContentLength > 0)
            {
                var fileName = Path.GetFileName("Import" + DateTime.Now.ToString("yyyyMmddhhmmss") + ".xlsx");
                filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Files/BulkEmployee/"), fileName);
                file.SaveAs(filePath);
                string extention = Path.GetExtension(filePath);
                var dtRecord = Security.ReadExcelFile(filePath, extention);
                if (dtRecord != null && dtRecord.Rows.Count > 0)
                {
                    response.Result = new List<string>();
                    foreach (DataColumn column in dtRecord.Columns)
                    {
                        response.Result.Add(column.ColumnName);
                    }
                    response.StatusCode = HttpStatusCode.OK;
                    File.Delete(filePath);
                }
                else
                {
                    response.StatusCode = HttpStatusCode.NoContent;
                    response.ErrorMessages = new List<string>() { Constants.FileColumnInValid };
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        
        [HttpPost]
        [Route("bulkUpload/{CompanyID}")]
        public async Task<IHttpActionResult> bulkUpload(int CompanyID)
        {
            ServiceStatus<BulkResult> response = new ServiceStatus<BulkResult>();
            var file = HttpContext.Current.Request.Files.Count > 0 ?
                        HttpContext.Current.Request.Files[0] : null;
            string filePath = string.Empty;
            if (CompanyID > 0 && file != null && file.ContentLength > 0)
            {
                var fileName = Path.GetFileName(file.FileName);
                filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/Files/BulkEmployee/"), fileName);
                file.SaveAs(filePath);
                string extention = Path.GetExtension(filePath);
                var dtRecord = Security.ReadExcelFile(filePath, extention);
                if (dtRecord != null && dtRecord.Rows.Count > 0)
                {
                    var columnarray = "FirstName,LastName,Email,DateOfBirth,Gender,JoiningDate,Phone,Country,State,City,Address,PostalCode".ToLower().Split(',').ToList();
                    //Validate Column
                    var count = Security.ValidateExcelFileColumn(dtRecord, columnarray);
                    if (count == columnarray.Count)
                    {
                        var bulkResult = employeeServices.BulkUpload(dtRecord, CompanyID);
                        response.Result = bulkResult;
                        response.StatusCode = HttpStatusCode.OK;
                    }
                    else
                    {
                        response.StatusCode = HttpStatusCode.NoContent;
                        response.ErrorMessages = new List<string>() { Constants.FileColumnInValid };
                    }
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }

        #endregion

        #region Asset
        [HttpPost]
        [Route("saveAssetsType")]
        public async Task<IHttpActionResult> saveAssetsType(AssetsType model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null)
            {
                var result = _employeeServices.SaveAssetsType(model);
                if (result > 0 && model.ID == 0)
                {
                    response.Result = Constants.AddAssets; response.StatusCode = HttpStatusCode.OK;
                }
                else if (result > 0 && model.ID > 0)
                {
                    response.Result = Constants.UpdateAssets; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.Result = Constants.AssetsAlreadyExist;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getAssetTypeList")]
        public async Task<IHttpActionResult> getAssetTypeList()
        {
            ServiceStatus<List<AssetsType>> response = new ServiceStatus<List<AssetsType>>();
            var result = _employeeServices.GetAssetTypeList();
            if (result.Count>0) {
                response.Result = result;
                response.StatusCode = HttpStatusCode.OK;
            }
            else
            { response.Result = null; response.StatusCode = HttpStatusCode.NoContent; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getAssetStatusList")]
        public async Task<IHttpActionResult> getAssetStatusList()
        {
            ServiceStatus<List<KeyValuePair<string, int>>> response = new ServiceStatus<List<KeyValuePair<string, int>>>();
            response.Result =Common.GetEnumList<AssetStatus>();
            response.StatusCode = HttpStatusCode.OK;
            return Ok(response);
        }
        
        [HttpPost]
        [Route("saveAssets")]
        public async Task<IHttpActionResult> saveAssets(Assets model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model != null)
            {
                var result = _employeeServices.SaveAssets(model);
                if (result > 0 && model.ID == 0)
                {
                    response.Result = Constants.AddAssets; response.StatusCode = HttpStatusCode.OK;
                }
                else if (result > 0 && model.ID > 0)
                {
                    response.Result = Constants.UpdateAssets; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.StatusCode = HttpStatusCode.Conflict;
                    response.Result = Constants.AssetsAlreadyExist;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("getAssetList")]
        public async Task<IHttpActionResult> GetAssetList(AssetsFilter assetsFilter)
        {
            ServiceStatus<List<Assets>> response = new ServiceStatus<List<Assets>>();
            if (assetsFilter != null && assetsFilter.CompanyID > 0)
            {
                var model = _employeeServices.GetAssetList(assetsFilter);
                if (model != null)
                { response.Result = model; response.StatusCode = HttpStatusCode.OK; }
                else
                { response.Result = null; response.StatusCode = HttpStatusCode.NoContent; }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getAssetByID/{ID}")]
        public async Task<IHttpActionResult> getAssetByID(int ID)
        {
            ServiceStatus<Assets> response = new ServiceStatus<Assets>();
            if (ID> 0)
            {
                var model = _employeeServices.GetAssetList(ID);
                if (model != null)
                { response.Result = model; response.StatusCode = HttpStatusCode.OK; }
                else
                { response.Result = null; response.StatusCode = HttpStatusCode.NoContent; }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("deleteAsset")]
        public async Task<IHttpActionResult> deleteAsset(int ID)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (ID > 0)
            {
                employeeServices.DeleteAssets(ID);
                response.Result = Constants.DeleteAssets; response.StatusCode = HttpStatusCode.OK;                
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpPost]
        [Route("updateAssetStatus")]
        public async Task<IHttpActionResult> updateAssetStatus(AssetStatusDto model)
        {
            ServiceStatus<string> response = new ServiceStatus<string>();
            if (model!=null && model.ID > 0&&model.StatusID>0)
            {
                employeeServices.UpdateAssetStatus(model);
                response.Result = Constants.UpdateAssets; response.StatusCode = HttpStatusCode.OK;
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        #endregion
    }
}
