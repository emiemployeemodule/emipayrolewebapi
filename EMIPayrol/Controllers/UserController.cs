﻿using EMIPayrol.Helpers;
using EMIPayrolModel;
using EMIPayrolServices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace EMIPayrol.Controllers
{
    //[Authorize]
    [RoutePrefix("api/user")]
    public class UserController : ApiController
    {
        private readonly IUserServices _userServices;
        private readonly ICompanyServices _companyServices;
        public UserController(IUserServices userServices, ICompanyServices companyServices)
        {
            _userServices = userServices;
            _companyServices = companyServices;
        }
        readonly IUserServices userServices = new UserServices();
        readonly ICompanyServices companyServices = new CompanyServices();
        public UserController()
        {
            _userServices = userServices;
            _companyServices = companyServices;
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("Login")]
        public async Task<IHttpActionResult> Login(login model)
        {
            ServiceStatus<AuthToken> response = new ServiceStatus<AuthToken>();
            if (model != null && !string.IsNullOrEmpty(model.Email) && !string.IsNullOrEmpty(model.Password))
            {
                User result = _userServices.Login(model);
                if (result !=null)
                {
                    AuthToken authToken = new AuthToken();
                    authToken.Uat = Common.CreateUserToken(result);
                    response.Result = authToken; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.ErrorMessages = new List<string>() { Constants.InvalidCredtional };
                    response.StatusCode = HttpStatusCode.BadRequest;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
        [HttpGet]
        [Route("getCompanyList/{UserID}")]
        public async Task<IHttpActionResult> getCompanyList(int UserID)
        {
            ServiceStatus<List<Company>> response = new ServiceStatus<List<Company>>();
            if (UserID > 0)
            {
                var model = _companyServices.GetCompanyList(UserID);
                if (model != null)
                {
                    response.Result = model; response.StatusCode = HttpStatusCode.OK;
                }
                else
                {
                    response.Result = null;
                    response.StatusCode = HttpStatusCode.NoContent;
                }
            }
            else { response.ErrorMessages = new List<string>() { Constants.NullableParametre }; response.StatusCode = HttpStatusCode.BadRequest; }
            return Ok(response);
        }
    }
}
