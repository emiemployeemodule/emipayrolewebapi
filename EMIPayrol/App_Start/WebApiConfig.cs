﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Configuration;
using EMIPayrol.Models;

namespace EMIPayrol
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            //config.Filters.Add(new LoggingFilter());
            //config.Filters.Add(new AuthCheckFilter());

            //config.MessageHandlers.Add(new CorsHandler());
            // Web API routes
            //            var CorsBaseUrl = ConfigurationManager.AppSettings["CorsOriginBaseUrl"].ToString();
            //#if DEBUG
            //            var cors = new EnableCorsAttribute(CorsBaseUrl, headers: "*", methods: "*");
            //#else
            //            var cors = new EnableCorsAttribute(CorsBaseUrl, "*", "*");
            //#endif

            //            config.EnableCors(cors);
            // Web API routes

            EnableCorsAttribute cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);
            config.MapHttpAttributeRoutes();
            config.MessageHandlers.Add(new TokenValidationHandler());
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
