﻿using EMIPayrolModel;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Configuration;
using System.Data;
using System.IO;
using ClosedXML.Excel;
using System.Net.Http;

namespace EMIPayrol
{
    public static class Common
    {
        public static string createToken(Company model)
        {
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Name, model.CompanyOwner),
                 new Claim(ClaimTypes.NameIdentifier, model.ID.ToString()),
                 new Claim(ClaimTypes.Role, "Admin"),
                 new Claim("CompanyID", model.ID.ToString()),
                 new Claim("CompanyName", model.CompanyName),
                new Claim("Email", model.Email),
                new Claim("Password", model.Password+""),
                new Claim("PostalCode", model.PostalCode+""),
                new Claim("PhoneNo", model.PhoneNo+""),
                new Claim("MobilePhone", model.MobileNo+""),
                new Claim("Country", model.CountryName),
                new Claim("StateOrProvince", model.StateName),
                new Claim("City", model.CityName),
                new Claim("StreetAddress", model.Address+""),
                new Claim("Fax", model.Fax+""),
                new Claim("CompanyGuid", model.CompanyGuid+""),
                new Claim("WebSiteUrl", model.WebSiteUrl+""),
                new Claim("EmpRange",model.EmpRange+""),
                new Claim("IndustryID", model.IndustryID.ToString()),
            });
            return CreateIdentityToken(claimsIdentity);
        }
        public static string createEmployeeToken(Employee model)
        {
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Name, model.UserName),
                 new Claim(ClaimTypes.NameIdentifier, model.ID.ToString()),
                 new Claim(ClaimTypes.Role, model.DesignationName),
                 new Claim("DesignationID", model.DesignationID.ToString()),
                 new Claim("CompanyName", model.CompanyName),
                new Claim("Email", model.Email),
                new Claim("Password", model.Password+""),
                new Claim("PostalCode", model.PinCode+""),
                new Claim("PhoneNo", model.Phone+""),
                new Claim("CountryID", model.CountryID.ToString()),
                new Claim("StateID", model.StateID.ToString()),
                new Claim("CityID", model.CityID.ToString()),
                new Claim("Country", model.Country),
                new Claim("State", model.State),
                new Claim("City", model.City),
                new Claim("ImageProfilePath", model.ImageProfilePath),
                new Claim("ImageProfileUrl", model.ImageProfileUrl),
                new Claim("StreetAddress", model.Address+""),
                new Claim("EmployeeID", model.EmployeeID+""),
                new Claim("JoiningDate", model.JoiningDate),
                new Claim("CompanyID", model.CompanyID.ToString()),
                new Claim("CreatedDate", model.CreatedDate),
                new Claim("ModifiedDate", model.ModifiedDate),
            });
            return CreateIdentityToken(claimsIdentity);
        }
        public static string CreateUserToken(User model)
        {
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Name, model.FirstName+" "+model.LastName),
                 new Claim(ClaimTypes.NameIdentifier, model.ID.ToString()),
                 new Claim(ClaimTypes.Role, "Admin"),
                new Claim("Email", model.Email),
                new Claim("Password", model.Password+"")
            });
            return CreateIdentityToken(claimsIdentity);
        }
        private static string CreateIdentityToken(ClaimsIdentity claimsIdentity)
        {
            //Set issued at date
            string Local_Domain = ConfigurationManager.AppSettings["TokenhandlerLocalUrl"].ToString();
            string Live_Domain = ConfigurationManager.AppSettings["TokenhandlerLiveUrl"].ToString();
            string token_Audience = string.Empty;
            DateTime issuedAt = DateTime.UtcNow;
            //set the time when it expires
            DateTime expires = DateTime.UtcNow.AddHours(24);

            //http://stackoverflow.com/questions/18223868/how-to-encrypt-jwt-security-token
            var tokenHandler = new JwtSecurityTokenHandler();

            const string sec = "401b09eab3c013d4ca54922bb802bec8fd5318192b0a75f201d8b3727429090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae731f5a65ed1";
            var now = DateTime.UtcNow;
            var securityKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(System.Text.Encoding.Default.GetBytes(sec));
            var signingCredentials = new Microsoft.IdentityModel.Tokens.SigningCredentials(securityKey, Microsoft.IdentityModel.Tokens.SecurityAlgorithms.HmacSha256Signature);
#if DEBUG
            token_Audience = Local_Domain;
#else
            token_Audience = Live_Domain;
#endif

            //create the jwt
            var token =
                (JwtSecurityToken)
                    tokenHandler.CreateJwtSecurityToken(issuer: token_Audience, audience: token_Audience,
                        subject: claimsIdentity, notBefore: issuedAt, expires: expires, signingCredentials: signingCredentials);
            var tokenString = tokenHandler.WriteToken(token);
            return tokenString;
        }
        public static byte[] ConvertDataTableToByteArray(DataTable dt)
        {
            byte[] streamArray = null;
            using (XLWorkbook wb = new XLWorkbook())
            {
                //Add the DataTable as Excel Worksheet.
                wb.Worksheets.Add(dt);
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    //Save the Excel Workbook to MemoryStream.
                    wb.SaveAs(memoryStream);
                    //Convert MemoryStream to Byte array.
                    streamArray = memoryStream.ToArray();
                    memoryStream.Close();
                }
            }
            return streamArray;
        }
        public static void ExportFunction(HttpResponseMessage httpResponseMessage, byte[] dataBytes, string bookName)
        {
            var dataStream = new MemoryStream(dataBytes);
            httpResponseMessage.Content = new StreamContent(dataStream);
            httpResponseMessage.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
            httpResponseMessage.Content.Headers.ContentDisposition.FileName = bookName;
            httpResponseMessage.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");
        }
        
        //this function are Use to craete temp folder if not exists. if exist then craete empty.
        public static void ClearTempDirectory(string location, bool deleteEstingFile = true)
        {
            String Path = HttpContext.Current.Server.MapPath("~" + location);
            if (Directory.Exists(Path))
            {
                if (deleteEstingFile)
                {
                    DirectoryInfo di = new DirectoryInfo(Path);
                    foreach (FileInfo file in di.GetFiles())
                    {
                        file.Delete();
                    }
                }
            }
            else { Directory.CreateDirectory(Path); }
        }
        
        public static List<KeyValuePair<string, int>> GetEnumList<T>()
        {
            var list = new List<KeyValuePair<string, int>>();
            foreach (var e in Enum.GetValues(typeof(T)))
            {
                list.Add(new KeyValuePair<string, int>(e.ToString(), (int)e));
            }
            return list;
        }

        public static Tuple<string, string, string> ConvertDateToDDMMYY(string dt)
        {
            string str = (dt == null || dt.ToString().Length <= 2) ? "" : Convert.ToDateTime(dt).ToString("MM/dd/yyyy");
            if (str == "01-01-1900" || str == "01/01/1900")
                str = "";
            string dd = "", mm = "", yy = "";
            if (str != "") { dd = str.Split('/')[0]; mm = str.Split('/')[1]; yy = str.Split('/')[2]; }
            return new Tuple<string, string, string>(dd, mm, yy);
        }
        public static string DateToString(object dt)
        {
            string str = (dt == null || dt.ToString().Length <= 2) ? "" : Convert.ToDateTime(dt).ToString("MM/dd/yyyy");
            if (str == "01-01-1900" || str == "01/01/1900")
                str = "";
            return str;
        }
    }
}