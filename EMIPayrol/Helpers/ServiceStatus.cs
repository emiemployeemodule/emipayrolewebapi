﻿using System;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;

namespace EMIPayrol.Helpers
{
    #region Status Object Class
    /// <summary>
    /// Public class to return input status
    /// </summary>
    [Serializable]
    [DataContract(Name = "Response")]
    public class ServiceStatus<T>
    {
        #region Public properties.
        /// <summary>
        /// Get/Set property for accessing Status Code
        /// </summary>
        [JsonProperty("StatusCode")]
        [DataMember(Name = "StatusCode")]
        public HttpStatusCode StatusCode { get; set; }
        [JsonProperty("id")]
        [DataMember(Name = "id")]
        public int ID { get; set; }
        /// <summary>
        /// Get/Set property for accessing Status Message
        /// </summary>
        [JsonProperty("ErrorMessage")]
        [DataMember(Name = "ErrorMessage")]
        public List<string> ErrorMessages { get; set; }
        /// <summary>
        /// Get/Set property for accessing Status Message
        /// </summary>
        //[JsonProperty("ReasonPhrase")]
        //[DataMember]
        //public string ReasonPhrase { get; set; }

        [JsonProperty("Result")]
        [DataMember(Name = "Result")]
        public T Result { get; set; }

        public ServiceStatus()
        {
            this.ErrorMessages = new List<string>();
        }
        #endregion
    }

    #endregion
}