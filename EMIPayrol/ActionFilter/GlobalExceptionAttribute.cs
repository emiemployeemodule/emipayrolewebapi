﻿using System;
using System.Web.Http.Filters;
using System.Web.Http;
using System.Web.Http.Tracing;
using System.ComponentModel.DataAnnotations;
using System.Net.Http;
using System.Net;
using System.Collections.Generic;
using EMIPayrol.Helpers;
using System.Runtime.Serialization;

namespace EMIPayrol
{
    public class GlobalExceptionAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            ServiceStatus<NoResultStatus> status = new ServiceStatus<NoResultStatus>();

            var exceptionType = context.Exception.GetType();

            if (exceptionType == typeof(ValidationException))
            {
                var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError) { Content = new StringContent(context.Exception.Message), ReasonPhrase = "ValidationException", };
                throw new HttpResponseException(resp);

            }
            else if (exceptionType == typeof(UnauthorizedAccessException))
            {
                throw new HttpResponseException(context.Request.CreateResponse(HttpStatusCode.Unauthorized));
            }
            else if (exceptionType == typeof(ApiException))
            {
                var webapiException = context.Exception as ApiException;
                if (webapiException != null)
                {
                    status.StatusCode = webapiException.HttpStatus;
                    status.ErrorMessages = new List<string>() { webapiException.Message };
                    throw new HttpResponseException(context.Request.CreateResponse(status.StatusCode, status));
                }

            }
            else
            {
                status.StatusCode = HttpStatusCode.InternalServerError;
                status.ErrorMessages = new List<string>() { context.Exception.Message };
                throw new HttpResponseException(context.Request.CreateResponse(status.StatusCode, status));
            }
        }
    }
    /// <summary>  
    /// Api Exception  
    /// </summary>  
    [Serializable]
    [DataContract]
    public class ApiException : Exception
    {
        #region Public Serializable properties.  
        [DataMember]
        public int ErrorCode { get; set; }
        [DataMember]
        public string ErrorDescription { get; set; }
        [DataMember]
        public HttpStatusCode HttpStatus { get; set; }

        string reasonPhrase = "ApiException";

        [DataMember]
        public string ReasonPhrase
        {
            get { return this.reasonPhrase; }

            set { this.reasonPhrase = value; }
        }
        #endregion
    }
    public class NoResultStatus
    { }
}