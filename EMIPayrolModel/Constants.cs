﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace EMIPayrolModel
{
    public static class Constants
    {
        public const string Currency= "$ ";
        public const string LinkVerifiedMessage = "Your account is verified successfully.";
        public const string FogotEmailSent = "Your submission has been received. Please check your email to Reset  Password.";
        public const string AlreadyRegisterdEmail = "An user already registered with this email. Please use other email.";
        public const string VerificationEmailSent = "The Verification email has been sent, Please check your MailBox.";
        public const string NewPassword = "Your password has been changed successfully. Please click here to Login";
        public const string ChangePassword = "Your password has been changed successfully.";
        public const string MailErrorSubject = "EMI Web Api Application Error";
        public const string CompanySignUp= "Your submission has been received.The Verification email has been sent, Please check your MailBox.";
        public const string CompanyUpdate = "Company Profile has been updated successfully";
        public const string InternalServerError = "Internal Server Error";
        public const string NullableParametre= "Paramter not passed or Null Value exist";
        public const string Expirelink = "This Link has been expired.";
        public const string InvalidCredtional = "The email or password you entered is invalid.";
        public const string MismatchOldPassword = "Old password you entered is invalid.";
        public const string CompanySignupTemplate= "CompanySignupTemplate";
        public const string CompanyResetLink = "CompanyResetLink";
        public const string AlreadyCompanyExist = "An company already registered with this Name. Please use other Name.";
        public const string TerminationNotification="HR Manager has been notified and will contact shortly.";
        // Default
        public const string AddRecord= "{Title} has been added successfully.";
        public const string EditRedorc = "{Title} has been updated successfully.";
        public const string AlreadyRecordExist = "An {Title} already exist with this Name. Please use other Name.";
        public const string DeleteRecord = "{Title} has been deleted successfully.";
        // Department
        public const string AddDepartment = "Department has been added successfully.";
        public const string EditDepartment = "Department has been updated successfully.";
        public const string AlreadyDepartmentExist = "An department already exist with this Name. Please use other Name.";
        public const string DeleteDepartment = "Department has been deleted successfully.";
        public const string AddCompanyDepartment = "Department has been saved successfully.";
        // LeaveType
        public const string AddLeaveType = "LeaveType has been added successfully.";
        public const string EditLeaveType = "LeaveType has been updated successfully.";
        public const string AlreadyLeaveTypeExist = "An LeaveType already exist with this Name. Please use other Name.";
        public const string DeleteLeaveType = "LeaveType has been deleted successfully.";
        public const string AddCompanyLeaveType = "LeaveType has been saved successfully.";
        //VactionPolicy
        public const string AddVactionPolicy = "Policy has been added successfully.";
        public const string EditVactionPolicy = "Policy has been updated successfully.";
        public const string AlreadyVactionPolicyExist = "An policy already registered with this Name. Please use other Name.";
        public const string DeleteVactionPolicy = "Policy has been deleted successfully.";
        //CompanyPolicy
        public const string AddCompanyPolicy = "Policy has been added successfully.";
        public const string EditCompanyPolicy = "Policy has been updated successfully.";
        public const string AlreadyCompanyPolicyExist = "An policy already registered with this Name. Please use other Name.";
        public const string DeleteCompanyPolicy = "Policy has been deleted successfully.";
        //Role
        public const string AddRole = "Role has been added successfully.";
        public const string EditRole = "Role has been updated successfully.";
        public const string AlreadyRoleExist = "An Role already registered with this Name. Please use other Name.";
        public static readonly string DeletRole = "Role has been deleted successfully.";
        //Appmodule
        public const string AddAppModule = "AppModule has been added successfully.";
        public const string EditAppModule = "AppModule has been updated successfully.";
        public const string AlreadyAppModuleExist = "An AppModule already registered with this Name. Please use other Name.";
        public static readonly string DeletAppModule= "AppModule has been deleted successfully.";
        //Industry
        public const string AddIndustry = "Industry has been added successfully.";
        public const string EditIndustry = "Industry has been updated successfully.";
        public const string AlreadyIndustryExist = "An Industry already registered with this Name. Please use other Name.";
        public static readonly string DeletIndustry = "Industry has been deleted successfully.";
        //EmailSetting
        public const string AddEmailSetting = "Email Setting has been added successfully.";
        public const string EditEmailSetting = "Email Setting has been updated successfully.";
        public const string AlreadyEmailSettingExist = "An Email Setting already registered with this Email. Please use other Email.";
        public static readonly string DeletEmailSetting = "Email Setting has been deleted successfully.";
        //SalarySetting
        public const string AddSalarySetting = "Salary Setting has been added successfully.";
        public const string EditSalarySetting = "Salary Setting has been updated successfully.";
        public static readonly string DeletSalarySetting = "Salary Setting has been deleted successfully.";
        //AdditionalRequirment
        public const string AddAdditionalRequirment = "AdditionalRequirment has been added successfully.";
        public const string EditAdditionalRequirment = "AdditionalRequirment has been updated successfully.";
        public const string AlreadyAdditionalRequirmentExist = "An AdditionalRequirment already registered with this Name. Please use other Name.";
        public static readonly string DeletAdditionalRequirment = "AdditionalRequirment has been deleted successfully.";

        //IndustryQuestion
        public const string AddQuestion = "Question has been added successfully.";
        public const string EditQuestion = "Question has been updated successfully.";
        public const string AlreadyQuestionExist = "An Question already registered with this Name. Please use other Name.";
        public static readonly string DeletQuestion = "Question has been deleted successfully.";
        public static readonly string AddAnswer = "Answer has been added successfully.";
        public static readonly string AddPermission = "Permission has been added successfully.";
        public static readonly string Addpermission= "Permission added successfully.";
        //Employee
        public const string AddEmployee = "Employee has been added successfully.";
        public const string EditEmployee = "Employee has been updated successfully.";
        public const string DeleteEmployee = "Employee has been deleted successfully.";
        public const string AlreadyEmployeeExist = "An Employee already registered with this email.";
        //JHSCMember
        public const string AddJHSCMember = "JHSC Member has been added successfully.";
        public const string EditJHSCMember = "JHSC Member has been updated successfully.";
        public const string DeleteJHSCMember = "JHSC Member has been deleted successfully.";
        public const string AlreadyJHSCMemberExist = "JHSC member is already part of the list";
        //Health & Safty Email
        public const string AddHealthSaftyEmail = "Email list added successfully.";
        public const string DeleteHealthSaftyEmail = "Record delete successfully.";
        //InspectionModule
        public const string AddInspectionModule = "Record added successfully.";
        public const string EditInspectionModule = "Record update successfully.";
        public const string DeleteInspectionModule = "Record delete successfully.";
        public const string AlreadyInspectionModuleExist = "This Record is already part of the list";
        //InspectionModuleCategory
        public const string AddInspectionModuleCategory = "Record added successfully.";
        public const string EditInspectionModuleCategory = "Record update successfully.";
        public const string DeleteInspectionModuleCategory = "Record delete successfully.";
        public const string AlreadyInspectionModuleExistCategory = "This Record is already part of the list";
        //InspectionForm
        public const string AddInspectionForm = "Add Form successfully.";
        public const string EditInspectionForm = "Update Form successfully.";
        public const string DeleteInspectionForm = "Form delete successfully.";
        public const string AlreadyInspectionFormExist = "Same name Form already part of the list";
        //InspectionFormQuestions
        public const string AddInspectionFormQuestion = "Question added successfully.";
        public const string EditInspectionFormQuestion = "Question update successfully.";
        public const string DeleteInspectionFormQuestion = "Question delete successfully.";
        public const string AlreadyInspectionFormExistQuestion = "This Question  already part of the list";
        //LeaveRequest
        public const string AddLeaveRequest = "Request added successfully.";
        public const string EditLeaveRequest = "Request update successfully.";
        public const string DeleteLeaveRequest = "Request delete successfully.";
        public const string UpdateLeaveRequestStatus = "Status update successfully.";
        public const string AlreadyLeaveRequest = "This request already part of the list";
        //Designation
        public const string AddDesignation = "Designation added successfully.";
        public const string EditDesignation = "Designation update successfully.";
        public const string DeleteDesignation = "Designation delete successfully.";
        public const string AlreadyDesignation = "This designation already part of the list";
        //CompanyHoliday
        public const string AddCompanyHoliday = "Record added successfully.";
        public const string EditCompanyHoliday = "Record update successfully.";
        public const string DeleteCompanyHoliday = "Record delete successfully.";
        public const string AlreadyCompanyHoliday = "This record already part of the list";
        //Invoice setting
        public const string AddInviceSettings = "Record added successfully";
        public const string editInviceSettings = "Record update successfully";
        public const string FileColumnInValid = "File Column are eqaul to sample file column!";
        public const string EmployeeIDNotGenrateReason = "CompanyID not exist or deleted.";
        public const string AddEmergenyInfo = "Information added successfully.";
        //Employee tab 
        public const string AddPositionInfo = "Your information has been saved successfully!";
        public const string DeletePositionInfo = "Information delete successfully.";
        public const string AddEmpWagesInfo = "Your information has been saved successfully!";
        public const string DeleteEmpWagesInfo = "Information delete successfully.";
        public static string AddDependentInfo = "Your information has been saved successfully! ";
        public static string EditDependentInfo = "Your information has been updated successfully!";
        public static string DeleteDependentInfo = "Your information has been deleted successfully!";
        public static string AddCertificateInfo= "Your information has been saved successfully! ";
        public static string EditCertificateInfo = "Your information has been updated successfully! ";
        public static string DeleteCertificateInfo = "Your information has been deleted successfully! ";
        public static string DeleteVicationInfo = "Your information has been deleted successfully! ";
        public static string AddVicationInfo = "Your information has been saved successfully! ";
        public static string EditVicationInfo = "Your information has been updated successfully! ";
        public static string DeleteDisciplinaryDetail = "Your information has been deleted successfully! ";
        public static string EditDisciplinaryDetail = "Your information has been updated successfully! ";
        public static string AddDisciplinaryDetail = "Your information has been saved successfully! ";
        public static string DeleteTerminationDetail = "Your information has been deleted successfully! ";
        public static string AddTerminationDetail = "Your information has been saved successfully! ";
        public static string EditTerminationDetail = "Your information has been updated successfully! ";
        public static string DeleteOfferDetail = "Your information has been deleted successfully! ";
        public static string EditOfferDetail = "Your information has been updated successfully! ";
        public static string AddOfferDetail = "Your information has been saved successfully! ";
        public static string EditAccidentDetail = "Your information has been updated successfully! ";
        public static string AddAccidentDetail = "Your information has been saved successfully! ";
        public static string EditPerformanceReview = "Your information has been updated successfully! ";
        public static string AddPerformanceReview = "Your information has been saved successfully! ";

        public static string DefaultUpdateConst = "Your information has been saved successfully! ";
        public const string DefaultDelateConst = "Record delete successfully.";
        public static string ShareInspectionWithMember = "Inspection shared successfully with JHSC member. ";
        public static string ShareInspectionWithMeeting = "Inspection shared successfully with JHSC meeting. ";
        public static string DeleteLayOffs= "Your information has been deleted successfully! ";
        public static string EditLayOffs = "Your information has been updated successfully! ";
        public static string AddLayOffs = "Your information has been saved successfully! ";
        //InspectionForm
        public const string Addsaftyboard = "Your information has been saved successfully!.";
        public const string Editsaftyboard = "Your information has been updated successfully!";
        public const string Deletesaftyboard = "Your information has been deleted successfully!";
        public const string Alreadysaftyboard = "Same name already part of the list";

        //Shift
        public const string AddShift = "Your information has been saved successfully!.";
        public const string UpdateShift= "Your information has been updated successfully!";
        public const string DeleteShift= "Your information has been deleted successfully!";
        public const string AlreadyShiftExist= "Same name already part of the list";

        public const string EmployeeSignupTemplate = "EmployeeSignupTemplate";
        public const string EmployeeOfferAcceptance = "EmployeeOfferAcceptance";
        public const string HRTerminationNotification = "HRTerminationNotification";

        public const string AddAssets = "Your information has been saved successfully!.";
        public const string UpdateAssets = "Your information has been updated successfully!";
        public const string AssetsAlreadyExist = "Same AssetID already part of the list";
        public const string DeleteAssets = "Your information has been deleted successfully!";
        public const string AlreadyAssets = "Same name already part of the list";

        public const string AddLDCertificate = "Your information has been saved successfully!.";
        public const string UpdateLDCertificate = "Your information has been updated successfully!";
        public const string AlreadyExistLDCertificate = "Same AssetID already part of the list";
        public const string DeleteLDCertificate = "Your information has been deleted successfully!";
        public const string AlreadyLDCertificate = "Same name already part of the list";

        public const string AddEquipmentType = "Your information has been saved successfully!.";
        public const string UpdateEquipmentType = "Your information has been updated successfully!";
        public const string AlreadyExistEquipmentType = "Same AssetID already part of the list";
        public const string DeleteEquipmentType = "Your information has been deleted successfully!";
        public const string AlreadyEquipmentType = "Same name already part of the list";
        public const string MailSent= "Email send successfully.";
        //Termination Reason Type
        public const string JustCause = "JUST CAUSE";
        public const string UnsatisfactoryProbationaryPeriod = "UNSATISFACTORY PROBATIONARY PERIOD";
        public const string WithoutCauseRelease = "WITHOUT CAUSE RELEASE";
        public const string Other = "OTHER";
    }
    public enum PerformanceReviewStatus
    {
        Communicated=1,
        Pending=0
    }
    public enum FinalPay
    {
        DirectlyIntoBankAccount = 0,
        Check = 1
    }
    public enum TerminationPayPaid
    {
        LumpSum = 0,
        SalaryContinuance = 1
    }
    public enum SeverancePaid
    {
        LumpSum = 0,
        SalaryContinuance = 1
    }
    public enum AssetStatus
    {
        Pending = 1,
        Approved = 2,
        Damaged = 3,
        Deployed = 4,
        Returned = 5
    }
    public enum InspectionType
    {
        HealthSafety=1,
        Vehicle = 2,
        LiftingVehicle = 3,
        Machinery=4
    }
    public enum SafeUnSafe
    {
        UnSafe = 1,
        Safe = 2
    }
}
