﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMIPayrolModel
{

    public class DepartmentRolePermission
    {
        public DepartmentRolePermission()
        { RoleModuleList = new List<RoleModule>(); }
        public int DepartmentID { get; set; }
        public int CompanyID { get; set; }
        public List<RoleModule> RoleModuleList { get; set; }
    }
    public class RoleModule
    {
        public RoleModule()
        { ModulePermissionList = new List<ModulePermission>(); }
        public int RoleID { get; set; }
        public List<ModulePermission> ModulePermissionList { get; set; }
    }
    public class ModulePermission
    {
        public int ModuleId { get; set; }
        public bool Read { get; set; }
        public bool Write { get; set; }
        public bool Import { get; set; }
        public bool Export { get; set; }
        public bool Create { get; set; }
        public bool Delete { get; set; }
        public bool Selected { get; set; }
    }
}
