﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EMIPayrolModel
{
    public class Country
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class State
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int CountryID { get; set; }

    }
    public class City
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int StateID { get; set; }
    }
    public class CommonClass
    {
        public int ID { get; set; }
        public string Email { get; set; }
        public string Guidtoken { get; set; }
        public string Password{ get; set; }
        public string CompanyOwner { get; set; }
    }
    public class EmailTemplate
    {
        public string Subject { get; set; }
        public string Body { get; set; }
    }
    public class AuthToken
    {
        public string Uat { get; set; }
    }
    public class AppModule
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }
        public bool Read { get; set; }
        public bool Write{ get; set; }
        //public bool Create{ get; set; }
        public bool Delete{ get; set; }
        public bool Import{ get; set; }
        public bool Export{ get; set; }
    }
    public class Role
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int IndustryID { get; set; }
    }
    public class Department
    {
        public int ID { get; set; }
        public int IndustryID { get; set; }
        public string Name { get; set; }
    }
    public class Industry
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }
    }
    public class IndustryQuestion
    {
        public int QuestionID { get; set; }
        public int IndustryID { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
    }
    public class EmailSetting
    {
        public int ID { get; set; }
        public string EmailFrom { get; set; }
        public string DisplayName { get; set; }
        public string SMTPHOST { get; set; }
        public string SMTPUSER { get; set; }
        public string SMTPPASSWORD { get; set; }
        public int SMTPPORT { get; set; }
        public string SMTPSecurity { get; set; }
        public string SMTPAuthenticationDomain { get; set; }
        public bool EnableSSL { get; set; }
        public int CompanyID { get; set; }
    }
    public class SalarySetting
    {
        public int ID { get; set; }
        public decimal DA { get; set; }
        public decimal HRA { get; set; }
        public decimal PFEmployeeShare { get; set; }
        public decimal PFOrganizationShare { get; set; }
        public decimal ESIEmployeeShare { get; set; }
        public decimal ESIOrganizationShare { get; set; }
        public int CompanyID { get; set; }
    }
    public class AdditionalRequirment
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class DeleteDepartment
    {
        public int CompanyID { get; set; }
        public int DepartmentID { get; set; }
    }
    public class LeaveType
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int CompanyID { get; set; }
        public int NoOfDays { get; set; }
        public int IsActive { get; set; }
    }
    
    public class LeaveStatus
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class ChangePassword
    {
        public int ID { get; set; }
        public string OldPassword { get; set; }
        public string Password { get; set; }
    }
    public class Shift
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    
}
