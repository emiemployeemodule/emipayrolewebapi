﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMIPayrolModel
{
    public class Company
    {
        public int ID { get; set; }
        public string CompanyName { get; set; }
        public string CompanyOwner { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNo { get; set; }
        public string MobileNo { get; set; }
        public int CountryID { get; set; }
        public int StateID { get; set; }
        public int CityID { get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
        public string Address { get; set; }
        public string Fax { get; set; }
        public string CompanyGuid { get; set; }
        public string CreatedDate { get; set; }
        public string WebSiteUrl { get; set; }
        public int EmpRangeID { get; set; }
        public string EmpRange { get; set; }
        public int IndustryID { get; set; }
    }
    public class EmployeRange
    {
        public int ID { get; set; }
        public string EmployeeRange { get; set; }
    }
    public class VactionPolicy
    {
        public int ID { get; set; }
        public string PolicyType { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int CompanyID { get; set; }
        public int DaysAllowed { get; set; }
        public int DaysPaid { get; set; }
    }
    public class CompanyPolicy
    {
        public int ID { get; set; }
        public string PolicyType { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int CompanyID { get; set; }
        public int DaysAllowed { get; set; }
        public int DaysPaid { get; set; }
    }
    public class CompanyDepartment
    {
        public int CompanyID { get; set; }
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
    }
    public class CompanyIndustry
    {
        public int CompanyID { get; set; }
        public int IndustryID { get; set; }
        public List<IndustryQuestion> IndustryQuestion { get; set; }
    }
    public class Permission
    {
        public int CompanyID { get; set; }
        public int RoleID { get; set; }
        public int DepartmentID { get; set; }
        public int ModuleID { get; set; }
        public bool Read{ get; set; }
        public bool Write{ get; set; }
        public bool Create { get; set; }
        public bool Delete { get; set; }
        public bool Import { get; set; }
        public bool Export { get; set; }
    }
    public class CompanyHoliday
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Day { get; set; }
        public string HolidayDate{ get; set; }
        public int CompanyID{get;set;}
    }
    public class Designation
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public int CompanyID { get; set; }
    }
    public class InvoiceSettings
    {
        public int CompanyID { get; set; }
        public string InvPerfix { get; set; }
        public string Path { get; set; }
        public string ImageUrl { get; set; }
    }
}
