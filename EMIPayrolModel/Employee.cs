﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMIPayrolModel
{
    public class Status
    {
        public int ID { get; set; }
        public string Type { get; set; }
    }
    public class EmployeeStatus
    {
        public int EmployeeID { get; set; }
        public int StatusID { get; set; }
        public string Status { get; set; }
        public string ChangeDate { get; set; }
        public string CreatedUser { get; set; }
    }
    public class Employee
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string DateOfBirth { get; set; }
        public string Gender { get; set; }
        public string EmployeeID { get; set; }
        public string JoiningDate { get; set; }
        public string Phone { get; set; }
        public int CountryID { get; set; }
        public int StateID { get; set; }
        public int CityID { get; set; }
        public string Address { get; set; }
        public string PinCode { get; set; }
        public string ImageProfilePath { get; set; }
        public string ImageProfileUrl { get; set; }
        public int CompanyID { get; set; }
        public int DesignationID { get; set; }
        public string DesignationName { get; set; }
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public int CreatedBy { get; set; }
        public List<AppModule> ModulePermission { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string CompanyName { get; set; }
        public string CompanyAddress { get; set; }
        public string Owner { get; set; }
        public string StartDate { get; set; }
        public string HireDate { get; set; }
        public int ShiftID { get; set; }
        public string ShiftType { get; set; }
        public int ReportingManagerID { get; set; }
        public string ReportingManager { get; set; }
    }
    public class BulkResult
    {
        public int Success { get; set; }
        public int Failed { get; set; }
        public int TotalRecord { get; set; }
    }
    public class LeaveRequest
    {
        public int ID { get; set; }
        public int LeaveTypeID { get; set; }
        public string LeaveType { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int NoOfDay { get; set; }
        public int RemainingLeave { get; set; }
        public string Reason { get; set; }
        public int LeaveStatusID { get; set; }
        public string LeaveStatus { get; set; }
        public int EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public int CompanyID { get; set; }
    }
    public class LeaveSearch
    {
        public int CompanyID { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public int LeaveTypeID { get; set; }
        public int LeaveStatusID { get; set; }
        public string EmployeeName { get; set; }
    }
    public class EmployeRole
    {
        public int ID { get; set; }
        public int DesignationID { get; set; }
    }
    public class EmergenyInfo
    {
        public int EmployeeID { get; set; }
        public string Name { get; set; }
        public string Relation { get; set; }
        public string Address { get; set; }
        public int CountryID { get; set; }
        public int StateID { get; set; }
        public int CityID { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Cell { get; set; }
        public string Email { get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
    }
    public class PositionInfo
    {
        public int ID { get; set; }
        public string Position { get; set; }
        public string Notes { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string StartTime { get; set; }
        public string Endtime { get; set; }
        public int EmployeeID { get; set; }
        public int CreatedBy { get; set; }
        public string CreatedUser { get; set; }
    }
    public class PayFrequency
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class EmpWagesInfo
    {
        public int ID { get; set; }
        public int EmployeeID { get; set; }
        public int PayFrequencyID { get; set; }
        public string PayFrequency { get; set; }
        public string StartDate { get; set; }
        public decimal PayPerPeriod { get; set; }
        public string Notes { get; set; }
        public int CreatedBy { get; set; }
        public string CreatedUser { get; set; }
    }
    public class DependentInfo
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Relation { get; set; }
        public string DatOfBirth { get; set; }
        public int CountryID { get; set; }
        public string CountryName { get; set; }
        public int StateID { get; set; }
        public string StateName { get; set; }
        public int CityID { get; set; }
        public string CityName { get; set; }
        public string Address { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Cell { get; set; }
        public string Email { get; set; }
        public int EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public int CreatedBy { get; set; }
        public string CreatedUser { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
    }
    public class TrainingCertificate
    {
        public int ID { get; set; }
        public string CertificateType { get; set; }
    }
    public class RenewCertificate
    {
        public int ID { get; set; }
        public string ExpireDate { get; set; }
        public string IssuedBy { get; set; }
    }
    public class EmployeeCertificateInfo
    {
        public int ID { get; set; }
        public int TrainingCertificateID { get; set; }
        public string TrainingCertificate { get; set; }
        public string Acquireddate { get; set; }
        public string Expirydate { get; set; }
        public string RefNumber { get; set; }
        public string IssuedBy { get; set; }
        public string FIlePath { get; set; }
        public int EmployeeID { get; set; }
        public int CreatedBy { get; set; }
        public string CreatedUser { get; set; }
    }
    public class VicationType
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class EmployeeVicationInfo
    {
        public int ID { get; set; }
        public int VicationTypeID { get; set; }
        public string VicationType { get; set; }
        public int YearFrom { get; set; }
        public int YearTo { get; set; }
        public int DayAllow { get; set; }
        public int DayPaid { get; set; }
        public int EmployeeID { get; set; }
        public int CreatedBy { get; set; }
        public string CreatedUser { get; set; }
        public string CreatedDate { get; set; }
    }
    public class DisciplinaryInfraction
    {
        public int ID { get; set; }
        public string Infraction { get; set; }
        public string Expectations { get; set; }
    }
    public class DisciplinaryWarning
    {
        public int ID { get; set; }
        public string WarningType { get; set; }
    }
    public class EmployeeDisciplinaryDetail
    {
        public int ID { get; set; }
        public string DisciplinaryDate { get; set; }
        public int DisciplinaryInfractionID { get; set; }
        public string Infraction { get; set; }
        public string Specify { get; set; }
        public string Expectations { get; set; }
        public int WarningTypeID { get; set; }
        public string WarningType { get; set; }
        public string InfractionDetails { get; set; }
        public string Status { get; set; }
        public string FilePath { get; set; }
        public bool RequireFollowUp { get; set; }
        public int NoOfWeek { get; set; }
        public int NumberOfRepetition { get; set; }
        public int EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string CompanyName { get; set; }
        public int CreatedBy { get; set; }
        public string CreatedUser { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State{ get; set; }
        public string PostalCode{ get; set; }
        public string CompanyLogo { get; set; }
        public string ModifiedUser { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public string StatusValue { get; set; }
        public string EmployeePhone { get; set; }
        public string CompanyAddress { get; set; }
        public int CompanyID { get; set; }
    }
    public class TerminationReasonType
    {
        public int ID { get; set; }
        public string TerminationType { get; set; }
    }
    public class EmployeeTerminationDetail
    {
        public int ID { get; set; }
        public string LastDayWorkDate { get; set; }
        public string BenefitsCeaseDate { get; set; }
        public int TerminationPay { get; set; }
        public int TerminationPayPaid { get; set; }
        public int Severance { get; set; }
        public int SeverancePaid { get; set; }
        public string Status { get; set; }
        public int TerminationTypeID { get; set; }
        public string TerminationType { get; set; }
        public int FinalPay { get; set; }
        public string FilePath { get; set; }
        public int EmployeeID { get; set; }
        public int CreatedBy { get; set; }
        public string CreatedUser { get; set; }
        public string ModifiedUser { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public bool Completed { get; set; }
        public string EmployeeName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string FinalPayValue { get; set; }
        public string TerminationPayPaidValue { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyName { get; set; }
        public string Phone { get; set; }
        public string OtherInfo{ get; set; }
        public string Designation{ get; set; }
        public string ReportingManager { get; set; }
        public int CompanyID{ get; set; }
        public string StatusValue { get; set; }
    }
    public class WorkingDayType
    {
        public int ID { get; set; }
        public string DayType { get; set; }
    }
    public class CompensationType
    {
        public int ID { get; set; }
        public string Type { get; set; }
    }
    public class TerminationClause
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class EmploymentType
    {
        public int ID { get; set; }
        public string Type { get; set; }
    }
    public class OfferVacationType {
        public int ID { get; set; }
        public string Type { get; set; }
    }
    public class OfferBenefitsType
    {
        public int ID { get; set; }
        public string Type { get; set; }
    }
    public class EmployeeOfferDetail
    {
        public int ID { get; set; }
        public int EmploymentTypeID { get; set; }
        public string EmploymentType { get; set; }
        public string Startdate { get; set; }
        public int CompensationTypeID { get; set; }
        public string CompensationType { get; set; }
        public decimal CompensationAmount { get; set; }
        public string BenefitsCeaseDate { get; set; }
        public int WorkingDayTypeID { get; set; }
        public string WorkingDayType { get; set; }
        public string OtherInfo { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Status { get; set; }
        public string JobTitle { get; set; }
        public int OfferBenefitsTypeID { get; set; }
        public string OfferBenefitsType { get; set; }
        public int OfferVacationTypeID { get; set; }
        public string OfferVacationsType { get; set; }
        public string OfferAcceptingDate { get; set; }
        public int ReportingManagerID { get; set; }
        public string FilePath { get; set; }
        public int EmployeeID { get; set; }
        public int CreatedBy { get; set; }
        public string CreatedUser { get; set; }
        public bool PayCommission { get; set; }
        public string CommissionPrevision { get; set; }
        public int TerminationClauseID { get; set; }
        public string TerminationDescription { get; set; }
        public bool OtherClause { get; set; }
        public bool OfferAccepted { get; set; }
        public string ModifiedUser { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public string EmployeeName { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string ReportingManager { get; set; }
        public string FirstName { get; set; }
        public string LastName{ get; set; }
        public int StateID { get; set; }
        public int CityID { get; set; }
        public int CountryID{ get; set; }
        public int OnBoarding { get; set; }
        public string Email { get; set; }
        public int CompanyID{ get; set; }
        public string ExpiryDate { get; set; }
        public bool IsOnBoarding { get; set; }
        public string StatusValue { get; set; }
    }

    public class AccidentType
    {
        public int ID { get; set; }
        public string Type { get; set; }
    }
    public class DiseasesType
    {
        public int ID { get; set; }
        public string Type { get; set; }
    }
    public class BodyPartType
    {
        public int ID { get; set; }
        public string Type { get; set; }
    }
    public class AccidentDetail
    {
        public AccidentDetail()
        {
            this.DiseasesList = new List<EmployeeDiseasesMap>();
            this.AccidentTypeList = new List<EmployeeAccidentTypeMap>();
            this.BodyPartMapList = new List<EmployeeBodyPartMap>();
        }
        public int ID { get; set; }
        public bool Completed { get; set; }
        public string DateOfAccident { get; set; }
        public string DateOfReported { get; set; }
        public string TimeOfAccident { get; set; }
        public string TimeOfReported { get; set; }
        public int ReportedTo { get; set; }
        public string Description { get; set; }
        public int AccidentHappendEmployeerPremises { get; set; }
        public string AccidentHappendEmployeerPremisesDesc { get; set; }
        public int AccidentOutSideProvince { get; set; }
        public string AccidentOutSideProvinceDesc { get; set; }
        public int AnyWitness { get; set; }
        public string WitnessDesc { get; set; }
        public int ResonposibleForAccident { get; set; }
        public string ResonposibleDescritption { get; set; }
        public int AwareAnyInjury { get; set; }
        public string AwareAnyInjuryDesc { get; set; }
        public int ConcernClaim { get; set; }
        public string NameOfPerson { get; set; }
        public string OfficeTitle { get; set; }
        public string RegisterDate { get; set; }
        public string Telephone { get; set; }
        public string Ext { get; set; }
        public int EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public int CreatedBy { get; set; }
        public string CreatedUser { get; set; }
        public string ModifiedUser { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public string ReportedManager { get; set; }
        public string StatusValue { get; set; }
        public int Status { get; set; }
        public string EmployeeCode { get; set; }
        public int CompanyID { get; set; }
        public List<EmployeeDiseasesMap> DiseasesList { get; set; }
        public List<EmployeeAccidentTypeMap> AccidentTypeList { get; set; }
        public List<EmployeeBodyPartMap> BodyPartMapList { get; set; }
    }
    public class EmployeeDiseasesMap
    {
        public int EmployeeID { get; set; }
        public int AccidentDetailId { get; set; }
        public int DiseasesTypeID { get; set; }
        public string DiseasesType { get; set; }
    }
    public class EmployeeAccidentTypeMap
    {
        public int EmployeeID { get; set; }
        public int AccidentDetailId { get; set; }
        public int AccidentTypeId { get; set; }
        public string AccidentType { get; set; }
    }
    public class EmployeeBodyPartMap
    {
        public int EmployeeID { get; set; }
        public int AccidentDetailId { get; set; }
        public int BodyPartId { get; set; }
        public string BodyPartType { get; set; }
    }
    public class TreatmentPlace
    {
        public int ID { get; set; }
        public string PlaceType { get; set; }
    }
    public class MedicalAwarness
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class RateType
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class WorkerJobType
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class HealthCare
    {
        public int AccidentDetailID { get; set; }
        public bool ReceiveHealthCareForInjury { get; set; }
        public int TreatedPlaceID { get; set; }
        public string HealthCareDate { get; set; }
        public string ReceivedHealthCareDate { get; set; }
        public string SpecifyOther { get; set; }
        public string AttendPersonAddress { get; set; }
        public int AwarenessOfIllnessID { get; set; }
        public bool WorkLimitation { get; set; }
        public bool DiscusionWithWoker { get; set; }
        public bool AnyOfferToWorker { get; set; }
        public bool ResponsibleForArrangingWorker { get; set; }
        public string PersonName { get; set; }
        public string Telephone { get; set; }
        public string Ext { get; set; }
        public int IsWorkerJobType { get; set; }
        public decimal RatePay { get; set; }
        public int RatePayTypeID { get; set; }
        public string RatePayOther { get; set; }
        public string LostTimeDate { get; set; }
        public string ReturnDate { get; set; }
        public int ShiftType { get; set; }
        public int InforConfirmBy { get; set; }
        public string ConfirmPersonName { get; set; }
        public string WorkerLostTimeDate { get; set; }
        public string ReturnToWorkDate { get; set; }
        public bool WorkType { get; set; }
        public bool LostTimeConfirmBy { get; set; }
        public string LostTime_Name { get; set; }
        public string LostTime_Telephone { get; set; }
        public string LostTime_Ext { get; set; }
        public bool IsAccepted { get; set; }
        public bool AttchedCopy { get; set; }
    }
    public class AccidentLable
    {
        public string EmployersPremises = "Did the accident/illness happen on the employer's premises (owned, leased or maintained)?";
        public string ProvinceOfOntario = "Did the accident/illness happen outside the Province of Ontario? ";
        public string AwareOfAnyWitnesses = "Are you aware of any witnesses or other employees involved in this accident/illness?";
        public string ResponsibleforAccident = "Was any individual, not in your firm, partially or totally responsible for this accident?";
        public string AwareAnyProblem = "Are you aware of any prior similar or related problem, injury or condition?";
        public string ConcernAboutClaim = "If you have concerns about this claim, attach a written submission to this form.";
        public string AcknowledgHeader = "It is an offence to deliberately make false statements to the Workplace Safety and Insurance Board. I declare that all of the information provided on pages 1, 2, and 3 is true";
        public string NameOfPerson = "Name of person completing this report";
        public string ReceiveHealthCareForInjury = "Did the worker receive health care for this injury?";
        public string TreatmentPlaceLabel = "Where was the worker treated for this injury? (Please check all that apply)";
        public string EmployeeLearnHealthCare = "When did the employer learn that the worker received health care?";
        public string SpecifyOther = "Please specify Other: ";
        public string AttendPersonAddress = "Name, address and phone number of health professional or facility who treated this worker (if known) ";
        public string LosttimeNoLosttimeHeader = "Lost Time - No Lost Time";
        public string FollowingIndicatorAwarenessOfIllness = "Please choose one of the following indicators. After the day of accident/awareness of illness, this worker: ";
        public string Isthisworker = "Is this worker (Please check all that apply)";
        public string BaseWageEmploymentInformation = "Base Wage / Employment Information - (Do not include overtime here)";
        public string RegularrateOfPay = "Regular Rate of Pay";
        public string ReturnedToWork = "Returned To Work";
        public string ProvidedWithWorkLimitations = "Have you been provided with work limitations for this worker's injury?";
        public string DiscussedWorker = "Has modified work been discussed with this worker?";
        public string OfferedtoWorker = "Has modified work been offered to this worker? ";
        public string DeclineLabled = "If Declined please attach a copy of the written offer given to the worker.";
        public string ResponsibleArrangigWorker = "Who is responsible for arranging worker's return to work";
        public string InfoConfirmBy = "This Lost Time - No Lost Time - Modified Work information was confirmed by ";
        public string ProvideDateWorker = "Provide date worker first lost time";
        public string DateWorkerReturnedWork = "Date worker returned to work (if known) ";
    }
    public class PerformenceReview
    {
        public int ID { get; set; }
        public int EmployeeID { get; set; }
        public string EmployeeNumber { get; set; }
        public string EvaluatorName { get; set; }
        public int EvaluatorID { get; set; }
        public string EvaluatorPossition { get; set; }
        public string DateOfReview { get; set; }
        public string FilePath { get; set; }
        public int Status { get; set; }
        public string StatusValue { get; set; }
        public int ExcusableAbsenteeism { get; set; }
        public int InexcusableAbsenteeism { get; set; }
        public int LateDepartures { get; set; }
        public int Occupational { get; set; }
        public int EmployeeRelations { get; set; }
        public int QualityAssurance { get; set; }
        public int JobRequirements { get; set; }
        public int AdherenceProcedure { get; set; }
        public int JobTasks { get; set; }
        public int OrganizationTasks { get; set; }
        public int AbilityProblemSolve { get; set; }
        public int Responsibility { get; set; }
        public int Initiative { get; set; }
        public int AbilityLearn { get; set; }
        public int Adaptability { get; set; }
        public int AbilityWork { get; set; }
        public int CooperationCoWorkers { get; set; }
        public int AttitudeSupervision { get; set; }
        public string EmployeeStrengths { get; set; }
        public string ImprovementDesc { get; set; }
        public string GaolDesc { get; set; }
        public string Comments { get; set; }
        public int CreatedBy { get; set; }
        public string CreatedUser { get; set; }
        public string ModifiedUser { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeePosition{ get; set; }
        public int CompanyID { get; set; }
    }
    public class ReportedUser
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Position { get; set; }
    }
    public class ReviewSectionContent
    {
        public ReviewSectionContent()
        { this.Content = new List<string>(); }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public List<string> Content { get; set; }
    }
    public class EvaluatorIndicatedType
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class PerformanceLable
    {
        public string Timekeeping = "Timekeeping(attach a copy of the Employee Calendar";
        public string ExcusableAbsenteeism = "Excusable absenteeism";
        public string InexcusableAbsenteeism = "Inexcusable absenteeism";
        public string LateDepartures = "Late Arrivals/Early Departures";
        public string JobKnowledge = "Job Knowledge (based on the applicable section of the appropriate Performance Review Checklist)";
        public string Occupational = "Occupational Health and Safety";
        public string EmployeeRelations = "Employee Relations";
        public string QualityAssurance = "Quality Assurance";
        public string JobRequirements = "Job Requirements";
        public string WorkConduct = "Work Conduct";
        public string AdherenceProcedure = "Adherence to Procedures (including documentation)";
        public string JobTasks = "Thoroughness/Accuracy of Job Tasks";
        public string OrganizationTasks = "Organization of Job tasks (i.e. finished goods)";
        public string AbilityProblemSolve = "Ability to Problem Solve (including troubleshooting)";
        public string Responsibility = "Acceptance of Responsibility";
        public string Initiative = "Initiative";
        public string AbilityLearn = "Ability to Learn";
        public string Adaptability = "Adaptability (i.e. acceptance of temporary work assignments)";
        public string AbilityWork = "Ability to Work Independently";
        public string CooperationCoWorkers = "Cooperation with Co-workers";
        public string AttitudeSupervision = "Attitude towards Supervision";
        public string EmployeeStrengths = "Describe the employee's strengths";
        public string ImprovementDesc = "Describe the employee's areas for improvement";
        public string EmployeeComments = "Employee comments";
        public string DescribeGoal = "Describe goal set for AND/OR with the employee";
    }

    public class FilePrivacy
    {
        public int EmployeeID { get; set; }
        public string FolderName { get; set; }
        public bool PrivacyType { get; set; }
    }
    public class FileDetail
    {
        public int ID { get; set; }
        public string FilePath { get; set; }
        public string Name { get; set; }
        public string CreatedDate { get; set; }
    }
    public class Filedto
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string IconUrl { get; set; }
    }
    public class FilterDto
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string SearchText { get; set; }
        public int EmoplyeeID { get; set; }
        public int CompanyID { get; set; }
    }
    public class EmployeSearchRequest
    {
        public int CompanyID{ get; set; }
        public string EmpoyeeID { get; set; }
        public string Name { get; set; }
        public int DepartmentID { get; set; }
        public int ReportingManagerID { get; set; }
    }
    public class HumanResource
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string CompanyName { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeEmail { get; set; }
        public string EmployeeID { get; set; }
    }
    public class LayOffs
    {
        public int ID { get; set; }
        public int EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string LastDayWork { get; set; }
        public string ReturnDate { get; set; }
        public bool PayBenifit { get; set; }
        public int BenifitTypeID { get; set; }
        public string BenifitTypeValue { get; set; }
        public string FilePath { get; set; }
        public int CreatedBy { get; set; }
        public bool Completed { get; set; }
    }
    public class LayoffEmployee
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public string PinCode { get; set; }
        public string CompanyName { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string CompanyAddress { get; set; }
        public string Owner { get; set; }
        public string LastWorkDate { get; set; }
    }
    public class Assets
    {
        public int ID { get; set; }
        public string AssetName { get; set; }
        public int AssetsTypeID { get; set; }
        public string AssetsType { get; set; }
        public string AssetId { get; set; }
        public string PurchaseDate { get; set; }
        public string PurchaseFrom { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string SerialNumber { get; set; }
        public string Supplier { get; set; }
        public string Condition { get; set; }
        public int Warranty { get; set; }
        public decimal Price { get; set; }
        public int EmployeeID { get; set; }
        public int CompanyID { get; set; }
        public string EmployeeName { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public string StatusValue { get; set; }
        public int CreatedBy { get; set; }
    }
    public class AssetsFilter
    {
        public int CompanyID{ get; set; }
        public string EmployeeName { get; set; }
        public int Status { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
    public class AssetsType
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class AssetStatusDto
    {
        public int ID { get; set; }
        public int StatusID { get; set; }
    }
    public class FileDocument
    {
        public int ID { get; set; }
        public string FileUrl { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string CreatedDate { get; set; }
        public bool PublicPrivacy { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
    public class EmployeeEmail
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
