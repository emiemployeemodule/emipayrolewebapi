﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMIPayrolModel
{
    public class JHSCMember
    {
        public int ID { get; set; }
        public int EmployeeID { get; set; }
        public string Name { get; set; }
        public int CompanyID { get; set; }
        public DateTime CertificationDate { get; set; }
        public DateTime ExpirationDate { get; set; }
    }
    public class BulkJHSCMemberdto
    {
        public BulkJHSCMemberdto()
        { this.ParticipantsList = new List<InsParticipant>(); }
        

        public int CompanyID { get; set; }
        public List<InsParticipant> ParticipantsList { get; set; }
    }
    public class HealthAndSaftyEmail
    {

        public int ID { get; set; }
        public int EmployeeID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
        public int CompanyID { get; set; }
        public DateTime CreatedDate { get; set; }
    }
    public class InspectionModule
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string FilePath { get; set; }
        public string HoverFilePath { get; set; }
    }
    public class InspectionModuleCategory
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int InspectionModuleID { get; set; }
        public string FilePath { get; set; }
        public string HoveFilePath { get; set; }
    }
    public class InspectionForm
    {
        public int ID { get; set; }
        public string FormName { get; set; }
        public int InspectionModuleID { get; set; }
        public int CompanyID { get; set; }
        public int ModuleCategoryID { get; set; }
        public string FilePath { get; set; }
        public string HoverFilePath { get; set; }
    }
    public class InspectionFormQuestion
    {
        public int ID { get; set; }
        public string Question { get; set; }
        public int InspectionFormID { get; set; }
        public int InspectionModuleID { get; set; }
        public int CompanyID { get; set; }
        public int ModuleCategoryID { get; set; }
        public string FormName { get; set; }
        public bool Active { get; set; }
    }
    public class QuestionActive
    {
        public int InspectionFormQuestionID { get; set; }
        public bool Active { get; set; }

    }
    public class CompanyInspectionForm
    {
        public int CompanyID { get; set; }
        public int ModuleID { get; set; }
        public string FormName { get; set; }
        public List<InsQuestion> QuestionList { get; set; }
    }
    public class InsQuestion
    {
        public int ID { get; set; }
        public string Question { get; set; }
        public bool Active { get; set; }
        public int Safe { get; set; }
        public List<InsQuestionAnswer> AnswerList { get; set; }
    }
    public class InsQuestionAnswer
    {
        public int ID { get; set; }
        public int QuestionID { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public string Action { get; set; }
        public string HAZARDClass { get; set; }
        public string FilePath { get; set; }
        public string FileUrl { get; set; }
        public int Safe { get; set; }
        public string CompletionDate { get; set; }
        public string PersonResponsible { get; set; }
    }
    public class SaftyBoard
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string ExpirationDate { get; set; }
        public string Decription { get; set; }
        public string FileName { get; set; }
        public string FileUrl { get; set; }
        public string Location { get; set; }
        public int CompanyID { get; set; }
        public int CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedUser { get; set; }
    }
    public class SaftyBoardAnnouncement
    {
        public int ID { get; set; }
        public string Announcement { get; set; }
        public string DurationFrom { get; set; }
        public string DurationTo { get; set; }
        public int CompanyID { get; set; }
        public int CreatedBy { get; set; }
        public string CreatedUser { get; set; }
    }
    public class SaftyDataSheet
    {
        public int ID { get; set; }
        public string ChemicalName { get; set; }
        public string Supplier { get; set; }
        public string ExpirationDate { get; set; }
        public string FileName { get; set; }
        public string FileUrl { get; set; }
        public int CompanyID { get; set; }
        public int CreatedBy { get; set; }
        public string Status { get; set; }
    }
    public class InspectionPriority
    {
        public int ID { get; set; }
        public string Title { get; set; }
    }
    public class EquipmentType
    {
        public int ID { get; set; }
        public string Type { get; set; }
        public int CompanyID { get; set; }
        public int CreatedBy { get; set; }
    }
    public class LiftingDeviceCertification
    {
        public int ID { get; set; }
        public int EquipmentTypeID { get; set; }
        public string EquipmentType { get; set; }
        public string Model { get; set; }
        public string Supplier { get; set; }
        public string ExpirationDate { get; set; }
        public string FileName { get; set; }
        public string FileUrl { get; set; }
        public string Status { get; set; }
        public int CompanyID { get; set; }
        public int CreatedBy { get; set; }
    }
    public class InspectionDetailsRequest
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string DateOfInspection { get; set; }
        public int PriorityID { get; set; }
        public string Description { get; set; }
        public int EmployeeID { get; set; }
        public int CompanyID { get; set; }
        public string Type { get; set; }
        public int CreatedBy { get; set; }
    }
    public class InspectionDetailsResponse
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string InspectionID { get; set; }
        public string DateOfInspection { get; set; }
        public int PriorityID { get; set; }
        public string PriorityValue { get; set; }
        public string Description { get; set; }
        public int EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public int CompanyID { get; set; }
        public string CreatedDate { get; set; }
        public int Status { get; set; }
        public string StatusValue { get; set; }
        public string Type { get; set; }
        public int CreatedBy { get; set; }
    }
    public class Inspectiondto
    {
        public int ID { get; set; }
        public int CompanyID { get; set; }
        public string Type { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
    public class InspectionQuestionAnswerdto
    {
        public int QuestionID { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public string Action { get; set; }
        public string HAZARDClass { get; set; }
        public int Safe { get; set; }
        public string FilePath { get; set; }
    }

    public class Participants
    {
        public Participants()
        { this.ParticipantsList = new List<InsParticipant>(); }
        public int InspectionID { get; set; }
        public string Type { get; set; }
        public int CompanyID { get; set; }
        public List<InsParticipant> ParticipantsList { get; set; }
    }
    public class Participantdto
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public int CompanyID { get; set; }
        public int InspectionID { get; set; }
        public int EmployeeID { get; set; }
    }
    public class InsParticipant
    {
        public int EmployeeID { get; set; }
        public string Email { get; set; }
    }
    public class InsParticipantWithName
    {
        public int EmployeeID { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
    }
    public class InspectionDetails
    {
        public InspectionDetails()
        {
            AllModule = new List<Module>();
            UnsafeModule = new List<Module>();
            SafeModule = new List<Module>();
            UnSafeImageList = new List<UnsafeImageList>();
        }
        public int ID { get; set; }
        public string Name { get; set; }
        public string InspectionID { get; set; }
        public string DateOfInspection { get; set; }
        public string CompleteDate { get; set; }
        public int PriorityID { get; set; }
        public string PriorityValue { get; set; }
        public string Description { get; set; }
        public int CompanyID { get; set; }
        public string CreatedDate { get; set; }
        public int Status { get; set; }
        public string StatusValue { get; set; }
        public string Type { get; set; }
        public int CreatedBy { get; set; }
        public int CompletedTask { get; set; }
        public int PendingTask { get; set; }
        public string CompletedTaskPercentage { get; set; }
        public string PendingTaskPercentage { get; set; }
        public List<Module> AllModule { get; set; }
        public List<Module> UnsafeModule { get; set; }
        public List<Module> SafeModule { get; set; }
        public List<UnsafeImageList> UnSafeImageList { get; set; }
    }

    public class Module
    {
        public Module() { this.FormList = new List<Form>(); }
        public int ID { get; set; }
        public string Name { get; set; }
        public bool Complete { get; set; }
        public List<Form> FormList { get; set; }
    }
    public class Form
    {
        public int ID { get; set; }
        public string FormName { get; set; }
        public bool Complete { get; set; }
    }
    public class Moduledto
    {
        public int ModuleCategoryID { get; set; }
        public string Name { get; set; }
        public int FormID { get; set; }
        public string FormName { get; set; }
        public bool Safe { get; set; }
    }
    public class UnsafeImageList
    {
        public string Title { get; set; }
        public string Image { get; set; }
    }
    public class UnSafeQuestion
    {
        public string CategoryName { get; set; }
        public string FormName { get; set; }
        public string Question { get; set; }
    }
    public class EmailList
    {
        public string Email { get; set; }
    }

    #region Incident 
    public class GernralInformationRequestdto
    {
        public int InspectionID { get; set; }
        public int FormID { get; set; }
        [Required]
        public string Complainant { get; set; }
        [Required]
        public string DateOfReport { get; set; }
        [Required]
        public string ReportedTo { get; set; }
        [Required]
        public string Location { get; set; }
        [Required]
        public string AllegedPerpetators { get; set; }
        public int CreatedBy { get; set; }
    }
    public class AllegationRequestdto
    {
        public int InspectionID { get; set; }
        public int FormID { get; set; }
        [Required]
        public string NatureOfAllegation { get; set; }
        [Required]
        public string AllegationIncidents { get; set; }
        public int CreatedBy { get; set; }
    }
    public class WitnessRequestdto
    {
        public int InspectionID { get; set; }
        public int FormID { get; set; }
        public int AnyWItness { get; set; }
        [Required]
        public string NameOfWitness { get; set; }
        [Required]
        public string RoleOFWitness { get; set; }
        public int CreatedBy { get; set; }
    }
    public class ResponseRequestdto
    {
        public int InspectionID { get; set; }
        public int FormID { get; set; }
        [Required]
        public string ActionOfIncident { get; set; }
        [Required]
        public string DescribePrevIncident { get; set; }
        public int CreatedBy { get; set; }
    }
    #endregion


    #region Accident section
    public class AccidentDatedetaildto
    {
        [Required]
        public int InspectionID { get; set; }
        [Required]
        public int FormID { get; set; }
        [Required]
        public int CompanyID { get; set; }
        [Required]
        public string DateOfInspection { get; set; }
        [Required]
        public string TimeOfInspection { get; set; }
        [Required]
        public string DateReported { get; set; }
        [Required]
        public string TimeReported { get; set; }
        [Required]
        public int ReportedTo { get; set; }
        public string DiseasesType { get; set; }
        public string AccidentType { get; set; }
        [Required]
        public int CreatedBy { get; set; }
    }

    public class LocationWitnessdto
    {
        [Required]
        public int InspectionID { get; set; }
        [Required]
        public int FormID { get; set; }
        [Required]
        public int CompanyID { get; set; }
        public int AccidentHappendInsidePremises { get; set; }
        public string AccidentHappendInsidePremisesDescription { get; set; }
        public int AccidentOutSideProvince { get; set; }
        public string AccidentOutSideProvinceDescription { get; set; }
        public int AwarenessAnyWitness { get; set; }
        public string AwarenessWitnessDescription { get; set; }
        public int ResonposibleForAccident { get; set; }
        public string ResonposibleDescription { get; set; }
        public int AwareAnyInjury { get; set; }
        public string AwareAnyInjuryDescription { get; set; }
        public int ConcernClaim { get; set; }
        [Required]
        public int CreatedBy { get; set; }
    }
    public class AccidentHealtCare
    {
        [Required]
        public int InspectionID { get; set; }
        [Required]
        public int FormID { get; set; }
        public int ReceiveHealthCareForInjury { get; set; }
        public string HealthCareDate { get; set; }
        public string EmployerlearnReceivedHealthCare { get; set; }
        public int WorkerTreated { get; set; }
        public string AddressOfTreatedPerson { get; set; }
        public int AwarenessOfIllnessID { get; set; }
        public string WorkerLostTimeDate { get; set; }
        public string ReturnToWorkDate { get; set; }
        public int WorkType { get; set; }
        public int ConfirmBy { get; set; }
        public string Name { get; set; }
        public string Telephone { get; set; }
        [Required]
        public int CompanyID { get; set; }
        [Required]
        public int CreatedBy { get; set; }
    }
    public class ReturnWorkDetail
    {
        [Required]
        public int InspectionID { get; set; }
        [Required]
        public int FormID { get; set; }
        public int WorkLimitationForInjury { get; set; }
        public int ModifiedworkDiscuss { get; set; }
        public int OfferAnyWork { get; set; }
        public int WorkAccepted { get; set; }
        public int ResponsibleForArangingWorker { get; set; }
        public string Name { get; set; }
        public string Telephone { get; set; }
        [Required]
        public int CompanyID { get; set; }
        [Required]
        public int CreatedBy { get; set; }
    }
    public class AccidentWagesInfo
    {
        [Required]
        public int InspectionID { get; set; }
        [Required]
        public int FormID { get; set; }
        [Required]
        public string Federal { get; set; }
        [Required]
        public string Provincial { get; set; }
        [Required]
        public int VactionPayCheck { get; set; }
        public decimal VactionPayCheckPercentage { get; set; }
        public string LastWorkDate { get; set; }
        public string LastworkTime { get; set; }
        public string LastDayWorkFrom { get; set; }
        public string LastDayWorkTo { get; set; }
        public decimal ActualEarning { get; set; }
        public decimal NormalEarning { get; set; }
        public int AdvanceWagePaid { get; set; }
        public int FullRegular { get; set; }
        public string Other { get; set; }
        public string Week1FromDate { get; set; }
        public string Week2FromDate { get; set; }
        public string Week3FromDate { get; set; }
        public string Week4FromDate { get; set; }
        public string Week1ToDate { get; set; }
        public string Week2ToDate { get; set; }
        public string Week3ToDate { get; set; }
        public string Week4ToDate { get; set; }
        public string MandatoryOverTimePay1 { get; set; }
        public string MandatoryOverTimePay2 { get; set; }
        public string MandatoryOverTimePay3 { get; set; }
        public string MandatoryOverTimePay4 { get; set; }
        public string VoluntaryOverTimePay1 { get; set; }
        public string VoluntaryOverTimePay2 { get; set; }
        public string VoluntaryOverTimePay3 { get; set; }
        public string VoluntaryOverTimePay4 { get; set; }
        [Required]
        public int CompanyID { get; set; }
        [Required]
        public int CreatedBy { get; set; }
        public List<AccidentCommissionInfo> AccidentCommissionInfo { get; set; }
    }
    public class AccidentCommissionInfo
    {
        public int InspectionID { get; set; }
        public string WeekOneName { get; set; }
        public string WeekOneValue { get; set; }
        public string WeekTwoName { get; set; }
        public string WeekTwoValue { get; set; }
        public string WeekThreeName { get; set; }
        public string WeekThreeValue { get; set; }
        public string WeekFourName { get; set; }
        public string WeekFourValue { get; set; }
    }
    public class WorkSchedule
    {
        [Required]
        public int InspectionID { get; set; }
        [Required]
        public int FormID { get; set; }
        public string SundayHour { get; set; }
        public string ModayHour { get; set; }
        public string TuesdayHour { get; set; }
        public string Wednesday { get; set; }
        public string ThrusdayHour { get; set; }
        public string FridayHour { get; set; }
        public string SaturdayHour { get; set; }
        public string DayOn { get; set; }
        public string DayOff { get; set; }
        public string HourPerShift { get; set; }
        public string NumberOfWeek { get; set; }
        public string Week1FromDate { get; set; }
        public string Week1ToDate { get; set; }
        public string Week1TotalHourWork { get; set; }
        public string Week1TotalShiftWork { get; set; }
        public string Week2FromDate { get; set; }
        public string Week2ToDate { get; set; }
        public string Week2TotalHourWork { get; set; }
        public string Week2TotalShiftWork { get; set; }
        public string Week3FromDate { get; set; }
        public string Week3ToDate { get; set; }
        public string Week3TotalHourWork { get; set; }
        public string Week3TotalShiftWork { get; set; }
        public string Week4FromDate { get; set; }
        public string Week4ToDate { get; set; }
        public string Week4TotalHourWork { get; set; }
        public string Week4TotalShiftWork { get; set; }
        [Required]
        public int CompanyID { get; set; }
        [Required]
        public int CreatedBy { get; set; }

    }
    public class AccidentSignOff
    {
        [Required]
        public int InspectionID { get; set; }
        [Required]
        public int FormID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Telephone { get; set;}
        [Required]
        public string Ext { get; set; }
        public string FilePath { get; set; }
        public string Signature { get; set; }
        [Required]
        public string SignOffDate { get; set; }
        public string AdditionalInfo { get; set; }
        [Required]
        public int CompanyID { get; set; }
        [Required]
        public int CreatedBy { get; set; }
    }
    public class JHSCMeeting
    {
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string ScheduleDate { get; set; }
        [Required]
        public int CompanyID { get; set; }
        [Required]
        public int CreatedBy { get; set; }
        public List<InsParticipantWithName> EmployeeList { get; set; }
        public string StatusValues { get; set; }
    }
    public class JHSCMeetingQuestionAnswerdto
    {
        public int QuestionID { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public string Action { get; set; }
        public string HAZARDClass { get; set; }
        public int Safe { get; set; }
        public string CompletionDate { get; set; }
        public string PersonResponsible { get; set; }
    }
    #endregion

    public class UnsafeQuestion
    {
        public int JHSCMeetingID { get; set; }
        public int InspectionID { get; set; }
        public string InspectionName { get; set; }
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public int FormID { get; set; }
        public string FormName { get; set; }
        public int QuestionID { get; set; }
        public string Question { get; set; }
        public int AnswerID { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public string Action { get; set; }
        public string HAZARDClass { get; set; }
        public string CompletionDate { get; set; }
        public int ResponsiblePersionID { get; set; }
        public string InspectionType { get; set; }
    }
    public class JHSCInspection
    {
        public JHSCInspection()
        {
            UnsafeQuestionList = new List<InspectionUnsafeQuestion>();
        }
        public string  Name{ get; set; }
        public List<InspectionUnsafeQuestion> UnsafeQuestionList { get; set; }
    }
    public class JHSCMeetingUnsafeQuestion
    {
        public JHSCMeetingUnsafeQuestion()
        {
            CurrentMeetingQuestion = new List<JHSCInspection>();
            PendingMeetingQuestion=new List<JHSCInspection>();
        }
        public List<JHSCInspection> CurrentMeetingQuestion { get; set; }
        public List<JHSCInspection> PendingMeetingQuestion { get; set; }
    }
    public class InspectionUnsafeQuestion
    {
        public int JHSCmeetingID { get; set; }
        public int InspectionID { get; set; }
        public int CategoryID { get; set; }
        public int FormID { get; set; }
        public int QuestionID { get; set; }
        public string Question { get; set; }
        public int AnswerID { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public string Action { get; set; }
        public string HAZARDClass { get; set; }
        public string CompletionDate { get; set; }
        public int ResponsiblePersionID { get; set; }
    }
    public class Dashboard
    {
        public Dashboard()
        {
            AccidentList = new List<InspectionDetailsResponse>();
            IncidentList = new List<InspectionDetailsResponse>();
            JHSCMeetingList = new List<JHSCMeeting>();
        }
        public List<InspectionDetailsResponse> AccidentList { get; set; }
        public List<InspectionDetailsResponse> IncidentList { get; set; }
        public List<JHSCMeeting> JHSCMeetingList{ get;set;}
        public int TotalHealthSafety { get; set; }
        public int PendingHealthSafety { get; set; }
        public int TotalVehicle { get; set; }
        public int PendingVehicle { get; set; }
        public int TotalLiftingVehicle { get; set; }
        public int PendingLiftingVehicle { get; set; }
        public int TotalMachinery { get; set; }
        public int PendingMachinery { get; set; }
        public int EmployeeCount { get; set; }
        public int TotalMeetingCompleted { get; set; }
    }

}
