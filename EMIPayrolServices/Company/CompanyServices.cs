﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EMIPayrolModel;
using EMIPayrolData;
using System.Data.SqlClient;
using System.Configuration;
namespace EMIPayrolServices
{
    public class CompanyServices : ICompanyServices
    {
        #region company info
        public List<Company> GetCompanyList(int UserID)
        {
            List<Company> companyList = new List<Company>();
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("UserID", UserID) };
            using (SqlDataReader dr = SQL.GetDataReader("GetCompanyList",param, true))
            {
                while (dr.Read())
                {
                    companyList.Add(new Company
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        CompanyName = Convert.ToString(dr["CompanyName"]),
                        CompanyOwner = Convert.ToString(dr["CompanyOwner"]),
                        Email = Convert.ToString(dr["Email"]),
                        Password = CommonHelper.Decrypt(dr["Password"]+""),
                        PostalCode = Convert.ToString(dr["PostalCode"]),
                        PhoneNo = Convert.ToString(dr["PhoneNo"]),
                        MobileNo = Convert.ToString(dr["MobileNo"]),
                        CountryID = Convert.ToInt32(dr["CountryID"]),
                        StateID = Convert.ToInt32(dr["StateID"]),
                        CityID = Convert.ToInt32(dr["CityID"]),
                        CountryName = Convert.ToString(dr["Country"]),
                        StateName = Convert.ToString(dr["State"]),
                        CityName = Convert.ToString(dr["City"]),
                        Address = Convert.ToString(dr["Address"]),
                        Fax = Convert.ToString(dr["Fax"]),
                        CompanyGuid = Convert.ToString(dr["CompanyGuid"]),
                        EmpRangeID = Convert.ToInt32(dr["EmpRangeID"]),
                        EmpRange = Convert.ToString(dr["EmpRange"]),
                        WebSiteUrl = Convert.ToString(dr["WebSiteUrl"]),
                    });
                }
            }
            return companyList;
        }
        public int SaveCompanyDetails(Company company)
        {
            var returnVal = 0;
            if (company.ID == 0)
                company.CompanyGuid = Guid.NewGuid().ToString();

            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("@Result",""),
                new SqlParameter("ID",company.ID),
                new SqlParameter("CompanyName",company.CompanyName),
                new SqlParameter("CompanyOwner",company.CompanyOwner),
                new SqlParameter("Email",company.Email),
                new SqlParameter("PostalCode",company.PostalCode),
                new SqlParameter("PhoneNo",company.PhoneNo),
                new SqlParameter("MobileNo",company.MobileNo),
                new SqlParameter("CountryID",company.CountryID),
                new SqlParameter("StateID",company.StateID),
                new SqlParameter("CityID",company.CityID),
                new SqlParameter("Address",company.Address),
                new SqlParameter("Fax",company.Fax),
                new SqlParameter("CompanyGuid",company.CompanyGuid),
                new SqlParameter("WebSiteUrl",company.WebSiteUrl),
                new SqlParameter("EmpRangeID",company.EmpRangeID),
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            returnVal = SQL.ExceuteStoreProcedure("CompanySaveUpdate", param, true);
            if (company.ID == 0 && returnVal > 0)
            {
                var Link = ConfigurationManager.AppSettings["EmailConfirmUrl"] + "/accountconfirmation/" + company.CompanyGuid;
                var emailtemplate = GetEmailTemplate(Constants.CompanySignupTemplate);
                string emailbody = emailtemplate.Body.Replace("{Link}", Link).Replace("{Name}", company.CompanyOwner).Replace("{assistancelink}", "");
                CommonHelper.SendEmail(company.Email, emailtemplate.Subject, emailbody);
            }
            return returnVal;
        }
        public bool IsEmailExist(string Email)
        {
            bool result = false;
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("Email", Email) };
            using (SqlDataReader dr = SQL.GetDataReader("CheckEmailExist", param, true))
            {
                while (dr.Read())
                {
                    result = Convert.ToBoolean(dr["value"]);
                }
            }
            return result;
        }
        public bool verifyCompanyAccount(CommonClass model)
        {
            bool result = false;
            SqlParameter[] param = new SqlParameter[] {new SqlParameter("Guid", model.Guidtoken),
                new SqlParameter("Password", CommonHelper.Encrypt(model.Password))
            };
            using (SqlDataReader dr = SQL.GetDataReader("verifyCompanyAccount", param, true))
            {
                while (dr.Read())
                {
                    result = Convert.ToBoolean(dr["Result"]);
                }
            }
            return result;
        }
        public Company companyDetail(int CompanyID)
        {
            Company model = null;
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("CompanyID", CompanyID) };
            using (SqlDataReader dr = SQL.GetDataReader("GetCompanyDetail", param, true))
            {
                while (dr.Read())
                {
                    model = new Company()
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        CompanyName = dr["CompanyName"].ToString(),
                        CompanyOwner = dr["CompanyOwner"].ToString(),
                        Email = dr["Email"].ToString(),
                        Password = CommonHelper.Decrypt(dr["Password"].ToString()),
                        PostalCode = dr["PostalCode"].ToString(),
                        PhoneNo = dr["PhoneNo"].ToString(),
                        MobileNo = dr["MobileNo"].ToString(),
                        CountryID = Convert.ToInt32(dr["CountryID"]),
                        CountryName = dr["CountryName"].ToString(),
                        StateID = Convert.ToInt32(dr["StateID"]),
                        StateName = dr["StateName"].ToString(),
                        CityID = Convert.ToInt32(dr["CityID"]),
                        CityName = dr["CityName"].ToString(),
                        Address = dr["Address"].ToString(),
                        Fax = dr["Fax"].ToString(),
                        WebSiteUrl = dr["WebSiteUrl"].ToString(),
                        EmpRangeID = Convert.ToInt32(dr["EmpRangeID"]),
                        EmpRange = dr["EmpRange"].ToString(),
                        CreatedDate = dr["CreatedDate"].ToString(),
                        IndustryID=Convert.ToInt32(dr["IndustryID"])
                    };

                }
            }
            return model;
        }
        public List<EmployeRange> EmployeRange()
        {
            List<EmployeRange> employeRange = new List<EmployeRange>();
            using (SqlDataReader dr = SQL.GetDataReader("CompEmployeRange", true))
            {
                while (dr.Read())
                {
                    employeRange.Add(new EmployeRange
                    {

                        ID = Convert.ToInt32(dr["ID"]),
                        EmployeeRange = dr["EmpRange"].ToString()
                    });

                }
            }
            return employeRange;
        }
        public int CompanyLogin(CommonClass model)
        {
            int result = 0;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("Email",model.Email),
                new SqlParameter("Password",CommonHelper.Encrypt(model.Password))
            };
            using (SqlDataReader dr = SQL.GetDataReader("LoginCompany", param, true))
            {
                while (dr.Read())
                {
                    result = Convert.ToInt32(dr["Value"]);
                }
            }
            return result;
        }
        public bool companyForgotPassword(string email)
        {
            bool result = false;

            var compDetail = GetCompanyGuid(email);
            var Link = ConfigurationManager.AppSettings["EmailConfirmUrl"] + "/companyRestPassword/" + compDetail.Guidtoken;
            var emailtemplate = GetEmailTemplate(Constants.CompanyResetLink);
            string emailbody = emailtemplate.Body.Replace("{Link}", Link).Replace("{Name}", compDetail.CompanyOwner).Replace("{assistancelink}", "");
            CommonHelper.SendEmail(email, emailtemplate.Subject, emailbody);
            result = true;
            return result;
        }
        public bool updateCompanyPassword(CommonClass model)
        {
            bool result = false;
            SqlParameter[] param = new SqlParameter[] {new SqlParameter("Guid", model.Guidtoken),
                new SqlParameter("Password", CommonHelper.Encrypt(model.Password))
            };
            using (SqlDataReader dr = SQL.GetDataReader("UpdateCompanypassword", param, true))
            {
                while (dr.Read())
                {
                    result = Convert.ToBoolean(dr["Result"]);
                }
            }
            return result;
        }
        public int ChangePassword(ChangePassword model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("Result", ""),
                new SqlParameter("Id", model.ID),
                new SqlParameter("OldPassword", CommonHelper.Encrypt(model.OldPassword)),
                new SqlParameter("Password", CommonHelper.Encrypt(model.Password))
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            return SQL.ExceuteStoreProcedure("ChangeCompanypassword", param, true);
        }
        private CommonClass GetCompanyGuid(string email)
        {
            CommonClass compDetail = new CommonClass();
            string SQLQuery = "select CompanyGuid,CompanyOwner from Company Where Email='" + email + "'";
            using (SqlDataReader dr = SQL.GetDataReader(SQLQuery, false))
            {
                while (dr.Read())
                {
                    compDetail.Guidtoken = dr["CompanyGuid"] + "";
                    compDetail.CompanyOwner = dr["CompanyOwner"] + "";
                }
            }
            return compDetail;
        }
        
        private EmailTemplate GetEmailTemplate(string TemplateName)
        {
            EmailTemplate emailTemplate = null;
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("Name", TemplateName) };
            using (SqlDataReader dr = SQL.GetDataReader("GetEmailTeplateByName", param, true))
            {
                while (dr.Read())
                {
                    emailTemplate = new EmailTemplate();
                    emailTemplate.Subject = dr["Subject"].ToString();
                    emailTemplate.Body = dr["Body"].ToString();
                }
            }
            return emailTemplate;
        }
        #endregion

        #region Vaction Policy
        public int SaveVactionPolicy(VactionPolicy vactionPolicy)
        {
            var returnVal = 0;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("@Result",""),
                new SqlParameter("ID",vactionPolicy.ID),
                new SqlParameter("PolicyType",vactionPolicy.PolicyType),
                new SqlParameter("CompanyID",vactionPolicy.CompanyID),
                new SqlParameter("StartDate",vactionPolicy.StartDate),
                new SqlParameter("EndDate",vactionPolicy.EndDate),
                new SqlParameter("DaysAllowed",vactionPolicy.DaysAllowed),
                new SqlParameter("DaysPaid",vactionPolicy.DaysPaid)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            returnVal = SQL.ExceuteStoreProcedure("SaveUpdateVactionPolicy", param, true);
            return returnVal;
        }
        public List<VactionPolicy> getVactionPolicy(int CompanyID, int VactionPolicyID = 0)
        {
            List<VactionPolicy> vactionPolicyList = new List<VactionPolicy>();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",CompanyID),
                new SqlParameter("ID",VactionPolicyID)
            };
            using (SqlDataReader dr = SQL.GetDataReader("GetVactionPolicy", param, true))
            {
                while (dr.Read())
                {
                    vactionPolicyList.Add(new VactionPolicy
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        PolicyType = dr["PolicyType"].ToString(),
                        CompanyID = Convert.ToInt32(dr["CompanyID"]),
                        DaysPaid = Convert.ToInt32(dr["DaysPaid"]),
                        DaysAllowed = Convert.ToInt32(dr["DaysAllowed"]),
                        StartDate = dr["StartDate"].ToString(),
                        EndDate = dr["EndDate"].ToString()
                    });
                }
            }
            return vactionPolicyList;
        }
        public void DeleteVactionPolicy(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("DeleteVactionPolicy", param, false);
        }
        #endregion

        #region Company Policy
        public int SaveCompanyPolicy(CompanyPolicy companyPolicy)
        {
            var returnVal = 0;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("@Result",""),
                new SqlParameter("ID",companyPolicy.ID),
                new SqlParameter("PolicyType",companyPolicy.PolicyType),
                new SqlParameter("CompanyID",companyPolicy.CompanyID),
                new SqlParameter("StartDate",companyPolicy.StartDate),
                new SqlParameter("EndDate",companyPolicy.EndDate),
                new SqlParameter("DaysAllowed",companyPolicy.DaysAllowed),
                new SqlParameter("DaysPaid",companyPolicy.DaysPaid)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            returnVal = SQL.ExceuteStoreProcedure("SaveUpdatePolicySaveUpdate", param, true);
            return returnVal;
        }
        public List<CompanyPolicy> getCompanyPolicy(int CompanyID, int CompanyPolicyID = 0)
        {
            List<CompanyPolicy> companyPolicyList = new List<CompanyPolicy>();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",CompanyID),
                new SqlParameter("ID",CompanyPolicyID)
            };
            using (SqlDataReader dr = SQL.GetDataReader("GetCompanyPolicy", param, true))
            {
                while (dr.Read())
                {
                    companyPolicyList.Add(new CompanyPolicy
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        PolicyType = dr["PolicyType"].ToString(),
                        CompanyID = Convert.ToInt32(dr["CompanyID"]),
                        DaysPaid = Convert.ToInt32(dr["DaysPaid"]),
                        DaysAllowed = Convert.ToInt32(dr["DaysAllowed"]),
                        StartDate = dr["StartDate"].ToString(),
                        EndDate = dr["EndDate"].ToString()
                    });
                }
            }
            return companyPolicyList;
        }
        public void DeleteCompanyPolicy(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("DeleteCompanyPolicy", param, false);
        }
        #endregion

        #region company depaertment
        public void SaveCompanyDepartment(List<CompanyDepartment> model)
        {
            foreach (var obj in model)
            {
                SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",obj.CompanyID),
                new SqlParameter("DepartmentID",obj.DepartmentID),
                new SqlParameter("DepartmentName",obj.DepartmentName)
                };
                SQL.ExceuteStoreProcedure("SaveCompanyDepartment", param, false);
            }
        }
        public List<CompanyDepartment> GetCompanyDepartment(int CompanyID)
        {
            List<CompanyDepartment> companyDepartment = new List<CompanyDepartment>();
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("CompanyID", CompanyID) };
            using (SqlDataReader dr = SQL.GetDataReader("GetCompanyDepartment", param, true))
            {
                while (dr.Read())
                {
                    companyDepartment.Add(new CompanyDepartment
                    {
                        DepartmentID = Convert.ToInt32(dr["DepartmentID"]),
                        CompanyID = Convert.ToInt32(dr["CompanyID"]),
                        DepartmentName = dr["DepartmentName"].ToString()
                    });
                }
            }
            return companyDepartment;
        }
        public void DeleteCompanyDepartment(DeleteDepartment model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",model.CompanyID),
                new SqlParameter("DepartmentID",model.DepartmentID)
            };
            SQL.ExceuteStoreProcedure("DeleteCompanyDepartment", param, false);
        }
        #endregion company depaertment

        #region industry info
        public void SaveCompanyIndustryInfo(CompanyIndustry model)
        {
            foreach (var obj in model.IndustryQuestion)
            {
                SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("QuestionID",obj.QuestionID),
                new SqlParameter("IndustryID",model.IndustryID),
                new SqlParameter("Answer",obj.Answer),
                new SqlParameter("CompanyID",model.CompanyID),
                };
                SQL.ExceuteStoreProcedure("SaveCompanyIndustryInfo", param, false);
            }
        }
        public bool SaveCompanyRolePermission(List<DepartmentRolePermission> model)
        {
            bool result = false;

            deleteExistingPermission(model.FirstOrDefault().CompanyID);
            foreach (var obj in model)
            {
                if (obj.RoleModuleList != null)
                {
                    foreach (var role in obj.RoleModuleList)
                    {
                        if (role.ModulePermissionList != null)
                        {
                           
                            foreach (var objmodule in role.ModulePermissionList)
                            {
                                result = true;
                                SqlParameter[] param = new SqlParameter[] {
                                new SqlParameter("CompanyID",obj.CompanyID),
                                new SqlParameter("DepartmentID",obj.DepartmentID),
                                new SqlParameter("RoleID",role.RoleID),
                                new SqlParameter("ModuleId",objmodule.ModuleId),
                                new SqlParameter("Read",objmodule.Read),
                                new SqlParameter("Write",objmodule.Write),
                                new SqlParameter("Create",objmodule.Create),
                                new SqlParameter("Delete",objmodule.Delete),
                                new SqlParameter("Import",objmodule.Import),
                                new SqlParameter("Export",objmodule.Export),
                                };
                                SQL.ExceuteStoreProcedure("SaveCompanyRolePermission", param, false);
                            }
                        }
                    }
                }
            }
            return result;
        }
        private void deleteExistingPermission(int CompanyID)
        {
            SqlParameter[] param = new SqlParameter[] {
                                new SqlParameter("CompanyID",CompanyID)
                                };
            SQL.ExceuteStoreProcedure("deleteExistingPermission", param, false);
        }
        public List<DepartmentRolePermission> getCompanyRolePermission(int companyID)
        {
            List<Permission> permission = new List<Permission>();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",companyID)
            };
            using (SqlDataReader dr = SQL.GetDataReader("GetPermissionByCompanyID", param, true))
            {
                while (dr.Read())
                {
                    permission.Add(new Permission
                    {
                        CompanyID = Convert.ToInt32(dr["CompanyID"]),
                        DepartmentID = Convert.ToInt32(dr["DepartmentID"]),
                        RoleID = Convert.ToInt32(dr["RoleID"]),
                        ModuleID = Convert.ToInt32(dr["ModuleID"]),
                        Read = Convert.ToBoolean(dr["Read"]),
                        Write = Convert.ToBoolean(dr["Write"]),
                        Delete = Convert.ToBoolean(dr["Delete"]),
                        Create = Convert.ToBoolean(dr["Create"]),
                        Import = Convert.ToBoolean(dr["Import"]),
                        Export = Convert.ToBoolean(dr["Export"])
                    });
                }
            }
            List<DepartmentRolePermission> drplist = new List<DepartmentRolePermission>();
            if (permission != null && permission.Count > 0)
            {
                var disCompany = permission.Select(x => x.CompanyID).Distinct();
                foreach (var company in disCompany)
                {
                    var disDepartment = permission.Where(x => x.CompanyID == company).Select(x => x.DepartmentID).Distinct();
                    foreach (var department in disDepartment)
                    {
                        var disRole = permission.Where(x => x.CompanyID == company && x.DepartmentID == department).Select(x => x.RoleID).Distinct();
                        var roleModule = new List<RoleModule>();
                        foreach (var role in disRole)
                        {
                            var modulePermission = permission.Where(x => x.CompanyID == company && x.DepartmentID == department && x.RoleID == role).ToList();
                            var modulePermissionList = new List<ModulePermission>();
                            foreach (var mPermission in modulePermission)
                            {
                                modulePermissionList.Add(new ModulePermission
                                {
                                    ModuleId = mPermission.ModuleID,
                                    Create = mPermission.Create,
                                    Delete = mPermission.Delete,
                                    Read = mPermission.Read,
                                    Write = mPermission.Write,
                                    Import = mPermission.Import,
                                    Export = mPermission.Export,
                                    Selected = true
                                });
                            }
                            roleModule.Add(new RoleModule
                            {
                                RoleID = role,
                                ModulePermissionList = new List<ModulePermission>(modulePermissionList)
                            });
                        }
                        drplist.Add(new DepartmentRolePermission
                        {
                            CompanyID = company,
                            DepartmentID = department,
                            RoleModuleList = new List<RoleModule>(roleModule)
                        });
                    }
                }
            }
            return drplist;
        }
        public List<AppModule> getCompanyAppModule(int companyID, int roleID, int departMentID)
        {
            List<AppModule> AppModuleList = new List<AppModule>();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",companyID),
                new SqlParameter("RoleID",roleID),
                new SqlParameter("DepartMentID",departMentID)
            };
            using (SqlDataReader dr = SQL.GetDataReader("GetCompanyAppModule", param, true))
            {
                while (dr.Read())
                {
                    AppModuleList.Add(new AppModule
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Name = dr["Name"].ToString(),
                        Selected = Convert.ToBoolean(dr["Selected"]),
                        Read = Convert.ToBoolean(dr["Read"]),
                        Write= Convert.ToBoolean(dr["Write"]),
                        Delete= Convert.ToBoolean(dr["Delete"]),
                        //Create= Convert.ToBoolean(dr["Create"]),
                        Import= Convert.ToBoolean(dr["Import"]),
                        Export= Convert.ToBoolean(dr["Export"])
                    });
                }
            }
            return AppModuleList;
        }
        #endregion

        #region CompanyHoliday
        public int SaveUpdateCompanyHoliday(CompanyHoliday CompanyHoliday)
        {
            var returnVal = 0;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("@Result",""),
                new SqlParameter("ID",CompanyHoliday.ID),
                new SqlParameter("Title",CompanyHoliday.Title),
                new SqlParameter("HolidayDate",CompanyHoliday.HolidayDate),
                new SqlParameter("CompanyID",CompanyHoliday.CompanyID)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            returnVal = SQL.ExceuteStoreProcedure("SaveUpdateCompanyHoliday", param, true);
            return returnVal;
        }
        public List<CompanyHoliday> GetCompanyHolidayByCompanyID(int CompnayID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",CompnayID)
            };
            return GetCompanyHoliday(param);
        }
        
        public CompanyHoliday GetCompanyHolidayByID(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            return GetCompanyHoliday(param).FirstOrDefault();
        }
        private List<CompanyHoliday> GetCompanyHoliday(SqlParameter[] param)
        {
            List<CompanyHoliday> CompanyHoliday = new List<CompanyHoliday>();
            using (SqlDataReader dr = SQL.GetDataReader("GetCompanyHoliday", param, true))
            {
                while (dr.Read())
                {
                    CompanyHoliday.Add(new CompanyHoliday
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Title = Convert.ToString(dr["Title"]),
                        CompanyID = Convert.ToInt32(dr["CompanyID"]),
                        Day = Convert.ToString(dr["Day"]),
                        HolidayDate = CommonHelper.DateToString(dr["holidayDate"])
                    });
                }
            }
            return CompanyHoliday;
        }
        public void DeleteCompanyHoliday(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("DeleteCompanyHoliday", param, false);
        }
        #endregion

        #region InvSettings
        public int SaveInvoiceSettings(InvoiceSettings invoiceSettings)
        {
            var returnVal = 0;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("@Result",""),
                new SqlParameter("ID",invoiceSettings.CompanyID),
                new SqlParameter("InvPerfix",invoiceSettings.InvPerfix),
                new SqlParameter("InvoiceLogoUrl",invoiceSettings.Path)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            returnVal = SQL.ExceuteStoreProcedure("SaveInvoiceSettings", param, true);
            return returnVal;
        }
        public InvoiceSettings GetInvoiceSettings(int CompanyID)
        {
            InvoiceSettings invoiceSettings = new InvoiceSettings();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",CompanyID)
            };
            using (SqlDataReader dr = SQL.GetDataReader("GetInvoiceSettings", param, true))
            {
                while (dr.Read())
                {
                    invoiceSettings.InvPerfix= Convert.ToString(dr["InvPerfix"]);
                    invoiceSettings.Path = Convert.ToString(dr["InvoiceLogoUrl"]);
                    if (!string.IsNullOrEmpty(invoiceSettings.Path))
                        invoiceSettings.ImageUrl = ConfigurationManager.AppSettings["APIBaseUrl"].ToString() + invoiceSettings.Path;
                    else
                        invoiceSettings.ImageUrl = "";
                    invoiceSettings.CompanyID = Convert.ToInt32(dr["CompanyID"]);
                }
            }
            return invoiceSettings;
        }
        #endregion

        #region Miscellaneous  
        public List<FileDocument> GetDocumentList(int CompanyID, string folderName)
        {
            folderName = folderName.Replace("{FolderName}", "");
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("folderName",folderName??""),
                new SqlParameter("CompanyID",CompanyID)
            };
            return GetFilesFromFolder(param);
        }
        public FileDocument GetDocumentByID(int ID)
        {
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("ID",ID)
            };
            return GetFilesFromFolder(param).FirstOrDefault();
        }
        private List<FileDocument> GetFilesFromFolder(SqlParameter[] param)
        {
            List<FileDocument> lst = new List<FileDocument>();
            FileDocument obj = null;
            var baseurl = Convert.ToString(ConfigurationManager.AppSettings["APIBaseUrl"]);
            using (SqlDataReader dr = SQL.GetDataReader("GetCompanyDocument", param, true))
            {
                while (dr.Read())
                {
                    obj = new FileDocument();
                    obj.ID = Convert.ToInt32(dr["ID"]);
                    obj.FilePath = Convert.ToString(dr["FilePath"]);
                    obj.FileUrl = baseurl + Convert.ToString(dr["FilePath"]);
                    obj.FileName = string.IsNullOrEmpty(dr["FilePath"] + "") ? "" : dr["FilePath"].ToString().Split('/')[dr["FilePath"].ToString().Split('/').Length - 1];
                    obj.CreatedDate = CommonHelper.DateToString(dr["CreatedDate"]);
                    obj.PublicPrivacy = Convert.ToBoolean(dr["PrivacyType"]);
                    obj.Title = Convert.ToString(dr["Title"]);
                    if (string.IsNullOrEmpty(obj.Title))
                        obj.Title = obj.FileName;
                    obj.Description= Convert.ToString(dr["Description"]);
                    lst.Add(obj);
                }
            }
            return lst;
        }
        public void SaveDocument(int CompanyID, FileDocument filedoc)
        {
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("ID",filedoc.ID),
                new SqlParameter("CompanyID",CompanyID),
                new SqlParameter("FilePath",filedoc.FilePath),
                new SqlParameter("FolderName",filedoc.FileName),
                new SqlParameter("Privacy",filedoc.PublicPrivacy),
                new SqlParameter("Title",filedoc.Title),
                new SqlParameter("Description",filedoc.Description)
            };
            SQL.ExceuteStoreProcedure("SaveCompanyDocument", param, false);
        }
        #endregion
    }
}
