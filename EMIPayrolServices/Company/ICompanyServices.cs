﻿using EMIPayrolModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMIPayrolServices
{
   public interface ICompanyServices
    {
        #region Company info
        List<Company> GetCompanyList(int UserID);
        int SaveCompanyDetails(Company compay);
        bool IsEmailExist(string Email);
        bool verifyCompanyAccount(CommonClass model);
        Company companyDetail(int CompanyID);
        List<EmployeRange> EmployeRange();
        int CompanyLogin(CommonClass model);
        bool companyForgotPassword(string email);
        bool updateCompanyPassword(CommonClass model);
        int ChangePassword(ChangePassword model);
        void SaveCompanyDepartment(List<CompanyDepartment> model);
        List<CompanyDepartment> GetCompanyDepartment(int CompanyID);
        void DeleteCompanyDepartment(DeleteDepartment model);
        void SaveCompanyIndustryInfo(CompanyIndustry model);
        bool SaveCompanyRolePermission(List<DepartmentRolePermission> model);
        List<DepartmentRolePermission> getCompanyRolePermission(int companyID);
        List<AppModule> getCompanyAppModule(int companyID, int roleID, int departMentID);
        #endregion

        #region CompanyHoliday
        int SaveUpdateCompanyHoliday(CompanyHoliday CompanyHoliday);
        List<CompanyHoliday> GetCompanyHolidayByCompanyID(int CompnayID);
        CompanyHoliday GetCompanyHolidayByID(int ID);
        void DeleteCompanyHoliday(int ID);
        #endregion

        #region  Vication policy
        int SaveVactionPolicy(VactionPolicy vactionPolicy);
        List<VactionPolicy> getVactionPolicy(int CompanyID = 0, int VactionPolicyID = 0);
        void DeleteVactionPolicy(int ID);
        #endregion

        #region  CompanyPolicy
        int SaveCompanyPolicy(CompanyPolicy companyPolicy);
        List<CompanyPolicy> getCompanyPolicy(int CompanyID = 0, int CompanyPolicyID = 0);
        void DeleteCompanyPolicy(int ID);
        #endregion

        #region InvSettings
        int SaveInvoiceSettings(InvoiceSettings invoiceSettings);
        InvoiceSettings GetInvoiceSettings(int CompanyID);
        #endregion

        #region Document Library  
        List<FileDocument> GetDocumentList(int CompanyID, string folderName);
        FileDocument GetDocumentByID(int ID);
        void SaveDocument(int CompanyID, FileDocument filedoc);
        #endregion
    }
}
