﻿using EMIPayrolModel;
using System;
using System.Collections.Generic;
using System.Data;

namespace EMIPayrolServices
{
    public interface IHealthSaftyServices
    {
        #region JSSC Member
        int SaveUpdateJHSCMember(JHSCMember jHSCMember);
        List<JHSCMember> GetJHSCMemberBYCompanyID(int companyID);
        void deleteJHSCMemberByID(int id);
        void SaveBulkJHSCMember(BulkJHSCMemberdto model);
        BulkJHSCMemberdto GetBulkJHSCMember(int CompanyID);
        List<EmployeeEmail> GetNonJhscMember(int CompanyID);
        #endregion

        #region HealthAndSaftyEmail
        void SaveHealthSaftyEmail(List<HealthAndSaftyEmail> healthAndSaftyEmailList);
        List<HealthAndSaftyEmail> GetHealthAndSaftyEmailBYCompanyID(int companyID);
        List<HealthAndSaftyEmail> GetNonHealthAndSaftyEmail(int companyID);
        void deleteHealthAndSaftyEmail(int id);
        #endregion

        #region InspectionModule
        int SaveUpdateInspectionModule(InspectionModule inspectionModule);
        List<InspectionModule> GetInspectionModule(int ID=0);
        void DeleteInspectionModule(int ID);
        #endregion

        #region InspectionModuleCategory
        int SaveUpdateInspectionModuleCategory(InspectionModuleCategory inspectionModuleCategory);
        List<InspectionModuleCategory> GetInsModuleCategoryByModuleID(int InspectionModuleID);
        List<InspectionModuleCategory> GetInsModuleCategoryByName(string Name);
        InspectionModuleCategory GetInsModuleCategoryByID(int ID);
        void DeleteInspectionModuleCategory(int ID);
        #endregion

        #region InspectionForm
        int SaveUpdateInspectionForm(InspectionForm inspectionForm);
        List<InspectionForm> GetInspectionFormByCompanyID(int InspectionModuleID, int CategoryID, int CompanyID);
        List<InspectionForm> GetFormListByCategoryId(int CategoryID, int CompanyID);
        List<InspectionForm> GetInsFormlistByName(int CompanyID,string ModuleName);
        InspectionForm GetInspectionFormByID(int ID);
        void DeleteInspectionForm(int ID);
        #endregion

        #region  InspectionFormQuestion
        int SaveInspectionFormQuestion(InspectionFormQuestion insFormQuestion);
        void setQuestionActive(QuestionActive model);
        List<CompanyInspectionForm> GetCompanyInspectionFormQuestion(int CompanyID, int ModuleID, int CategoryID);
        void DeleteInspectionFormQuestion(int ID);
        #endregion

        #region Saftyboard
        int SaveSaftyBoard(SaftyBoard model);
        List<SaftyBoard> GetSaftyBoard(int CompanyID);
        SaftyBoard GetSaftyBoardbyID(int ID);
        void DeleteSaftyBoard(int id);
        int SaveSaftyBoardAnnouncement(SaftyBoardAnnouncement model);
        List<SaftyBoardAnnouncement> GetSaftyBoardAnnouncement(int CompanyID);
        SaftyBoardAnnouncement GetSaftyBoardAnnouncementbyID(int ID);
        void DeleteSaftyBoardAnnouncement(int id);
        int SaveSaftyDataSheet(SaftyDataSheet model);
        List<SaftyDataSheet> GetSaftyDataSheet(int CompanyID);
        SaftyDataSheet GetSaftyDataSheetbyID(int ID);
        void DeleteSaftyDataSheet(int id);
        #endregion

        #region Equipment Type
        int SaveEquipmentType(EquipmentType model);
        List<EquipmentType> GetEquipmentType(int CompanyID);
        EquipmentType GetEquipmentTypebyID(int ID);
        void DeleteEquipmentType(int Id);
        #endregion

        #region Lifting Device Certification
        int SaveLiftingDeviceCertification(LiftingDeviceCertification model);
        List<LiftingDeviceCertification> GetLiftingDeviceCertification(int CompanyID);
        LiftingDeviceCertification GetLiftingDeviceCertificationbyID(int ID);
        void DeleteLiftingDeviceCertification(int Id);

        #endregion

        #region Save inspection details
        List<InspectionPriority> GetInspectionPriority();
        int SaveInspectionDetails(InspectionDetailsRequest model);
        List<InspectionDetailsResponse> GetInspectionDetails(Inspectiondto model);
        List<HealthAndSaftyEmail> GetNonInspectionParticipants(int CompanyID);
        void saveInspectionParticipants(Participants model, bool IsEmailParticipant = false);
        Participants GetInspectionParticipants(int CompanyID, int InspectionID, string Type, bool IsEmailParticipant = false);
        void sendInspectionParticipantMail(Participants model, int IsEmailParticipant = 0);
        void SaveInspectionQuestionAnswer(List<InsQuestion> model, int InspectionID, int CompanyID, int FormID, int CreatedBy, int ModuleCategoryID);
        List<Participantdto> GetParticipantByInspection(int CompanyID, int InspectionID, bool IsEmailParticipant = false);
        List<InsQuestion> GetInspectionQuestionAnswer(int CompanyID, int InspectionID, int FormID, int CategoryID = 0);
        InspectionDetails GetInspectionDetailsByID(int CompanyID, int InspectionID, string Type);
        void InspectionComplete(int InspectionID,int  CreatedBy,string Type);
        void DeleteInspection(int InspectionID);

        #endregion

        #region Incident
        int SaveGernralInformation(GernralInformationRequestdto model);
        int SaveAllegation(AllegationRequestdto model);
        int SaveWitness(WitnessRequestdto model);
        int SaveResponse(ResponseRequestdto model);
        System.Data.DataTable GetIncidentFormDetail(int InspectionID, int FormID, int Type = 0);
        InspectionDetails GetIncidentDetailsByID(int CompanyID, int InspectionID, string Type);
        #endregion

        #region Accident
        int SaveAccidentDatedetail(AccidentDatedetaildto model);
        AccidentDatedetaildto GetAccidentDatedetailByInspectionID(int InspectionID);
        int SaveLocationWitness(LocationWitnessdto model);
        LocationWitnessdto GetLocationWitnessByInspectionID(int InspectionID);
        int SaveAccidentHealtCare(AccidentHealtCare model);
        AccidentHealtCare GetAccidentHealtCareByInspectionID(int InspectionID);
        int SaveReturnWorkDetail(ReturnWorkDetail model);
        ReturnWorkDetail GetReturnWorkDetailByID(int InspectionID);
        int SaveAccidentWagesInfo(AccidentWagesInfo model);
        AccidentWagesInfo GetAccidentWagesInfoByID(int InspectionID);
        int SaveAccidentWorkScheduleInfo(WorkSchedule model);
        WorkSchedule GetAccidentWorkScheduleByID(int InspectionID);
        int SaveAccidentSignOffInfo(AccidentSignOff model);
        AccidentSignOff GetAccidentSignOffByID(int InspectionID);
        InspectionDetails GetAccidentDetailsByID(int CompanyID, int InspectionID);
        #endregion

        #region JHSC Meeting
        int SaveJHSCMeeting(JHSCMeeting model);
        List<JHSCMeeting> GetJHSCMeetingbyCompanyID(int CompanyID);
        JHSCMeeting GetJHSCMeetingbyID(int ID);
        void DeleteJHSCMeeting(int ID);
        void CompleteJHSCMeeting(int JHSCMeetingID,int CreatedBy);
        JHSCMeetingUnsafeQuestion GetJHSCUnsafeQuestion(int CompanyID, int JHSCMeetingID = 0);
        void SaveJHSCUnsafeQuestion(List<InspectionUnsafeQuestion> model, int CompanyID);
        void ShareInspectionWithjHSCMember(List<InsParticipant> model, int CompanyID,int InspectionID);
        DataSet GetHealthSafetyWSIBDetails(int ID);
        void ShareInspectionWithjHSCMeeting(int CompanyID, int InspectionID, int JHSCMeetingID);
        #endregion

        #region Dashboard API
        Dashboard GetDashboardDetails(int CompanyID);
        #endregion
    }
}