﻿using EMIPayrolData;
using EMIPayrolModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace EMIPayrolServices
{
    public class HealthSaftyServices : IHealthSaftyServices
    {
        static string baseurl = Convert.ToString(ConfigurationManager.AppSettings["APIBaseUrl"]);
        private readonly ICommonServices _commonServices;
        public HealthSaftyServices(ICommonServices commonServices)
        {
            _commonServices = commonServices;
        }

        readonly ICommonServices commonServices = new CommonServices();

        public HealthSaftyServices()
        {
            _commonServices = commonServices;
        }
        #region JHSCMember
        public int SaveUpdateJHSCMember(JHSCMember jHSCMember)
        {
            var returnVal = 0;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("@Result",""),
                new SqlParameter("ID",jHSCMember.ID),
                new SqlParameter("EmployeeID",jHSCMember.EmployeeID),
                new SqlParameter("ExpirationDate",jHSCMember.ExpirationDate),
                new SqlParameter("CertificationDate",jHSCMember.CertificationDate),
                new SqlParameter("CompanyID",jHSCMember.CompanyID)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            returnVal = SQL.ExceuteStoreProcedure("SaveUpdateJHSCMember", param, true);
            return returnVal;
        }
      
        public List<JHSCMember> GetJHSCMemberBYCompanyID(int companyID)
        {
            List<JHSCMember> jHSCMemberList = new List<JHSCMember>();
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("CompanyID", companyID) };
            using (SqlDataReader dr = SQL.GetDataReader("GetJHSCMemberByCompanyID", param, true))
            {
                while (dr.Read())
                {
                    jHSCMemberList.Add(new JHSCMember
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        EmployeeID = Convert.ToInt32(dr["EmployeeID"]),
                        Name = dr["Name"].ToString(),
                        CertificationDate = Convert.ToDateTime(dr["CertificationDate"]),
                        ExpirationDate = Convert.ToDateTime(dr["ExpirationDate"]),
                        CompanyID = Convert.ToInt32(dr["CompanyID"])
                    });
                }
            }
            return jHSCMemberList;
        }
        public void deleteJHSCMemberByID(int id)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",id)
            };
            SQL.ExceuteStoreProcedure("deleteJHSCMember", param, false);
        }
        public void SaveBulkJHSCMember(BulkJHSCMemberdto model)
        {
            var dtemployee = CommonHelper.ToDataTable(model.ParticipantsList);
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",model.CompanyID),
                new SqlParameter("Employeelist",dtemployee)
            };
            SQL.ExceuteStoreProcedure("SaveBulkJHSCMember", param, false);
        }
        public BulkJHSCMemberdto GetBulkJHSCMember(int CompanyID)
        {
            BulkJHSCMemberdto objMember = new BulkJHSCMemberdto();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",CompanyID)
            };
            var dt = SQL.GetDt("GetBulkJHSCMember", param, true);
            if (dt != null && dt.Rows.Count > 0)
            {
                objMember.CompanyID = CompanyID;
                objMember.ParticipantsList.AddRange(CommonHelper.ToList<InsParticipant>(dt));
            }
            return objMember;
        }
        public List<EmployeeEmail> GetNonJhscMember(int CompanyID)
        {
            List<EmployeeEmail> employeeList = new List<EmployeeEmail>();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",CompanyID)
            };
            using (SqlDataReader dr = SQL.GetDataReader("GetNonJHSCMember", param,true))
            {
                while (dr.Read())
                {
                    employeeList.Add(new EmployeeEmail
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Name = dr["Name"].ToString(),
                        Email = dr["Email"].ToString()
                    });
                }
            }
            return employeeList;
        }
        #endregion

        #region HealthAndSaftyEmail
        public void SaveHealthSaftyEmail(List<HealthAndSaftyEmail> healthAndSaftyEmailList)
        {
            foreach (var healthAndSaftyEmail in healthAndSaftyEmailList)
            {
                SqlParameter[] param = new SqlParameter[] {
                    new SqlParameter("EmployeeID",healthAndSaftyEmail.EmployeeID),
                    new SqlParameter("CompanyID",healthAndSaftyEmail.CompanyID)
                };
                SQL.ExceuteStoreProcedure("SaveUpdateHealthAndSaftyEmail", param, false);
            }
        }
        public List<HealthAndSaftyEmail> GetHealthAndSaftyEmailBYCompanyID(int companyID)
        {
            List<HealthAndSaftyEmail> HealthAndSaftyEmailList = new List<HealthAndSaftyEmail>();
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("CompanyID", companyID) };
            using (SqlDataReader dr = SQL.GetDataReader("GetHealthAndSaftyEmailByCompanyID", param, true))
            {
                while (dr.Read())
                {
                    HealthAndSaftyEmailList.Add(new HealthAndSaftyEmail
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        EmployeeID = Convert.ToInt32(dr["EmployeeID"]),
                        Name = dr["Name"].ToString(),
                        Email = dr["Email"].ToString(),
                        Status = dr["Status"].ToString(),
                        CompanyID = Convert.ToInt32(dr["CompanyID"]),
                        CreatedDate = Convert.ToDateTime(dr["CreatedDate"])
                    });
                }
            }
            return HealthAndSaftyEmailList;
        }
        public List<HealthAndSaftyEmail> GetNonHealthAndSaftyEmail(int companyID)
        {
            List<HealthAndSaftyEmail> HealthAndSaftyEmailList = new List<HealthAndSaftyEmail>();
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("CompanyID", companyID) };
            using (SqlDataReader dr = SQL.GetDataReader("GetNonHealthAndSaftyEmail", param, true))
            {
                while (dr.Read())
                {
                    HealthAndSaftyEmailList.Add(new HealthAndSaftyEmail
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        EmployeeID = Convert.ToInt32(dr["EmployeeID"]),
                        Name = dr["Name"].ToString(),
                        Email = dr["Email"].ToString(),
                        Status = dr["Status"].ToString(),
                        CompanyID = Convert.ToInt32(dr["CompanyID"]),
                        CreatedDate = Convert.ToDateTime(dr["CreatedDate"])
                    });
                }
            }
            return HealthAndSaftyEmailList;
        }
        public void deleteHealthAndSaftyEmail(int id)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",id)
            };
            SQL.ExceuteStoreProcedure("deleteHealthAndSaftyEmail", param, false);
        }
        #endregion

        #region InspectionModule
        public int SaveUpdateInspectionModule(InspectionModule inspectionModule)
        {
            var returnVal = 0;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("@Result",""),
                new SqlParameter("ID",inspectionModule.ID),
                new SqlParameter("Name",inspectionModule.Name)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            returnVal = SQL.ExceuteStoreProcedure("SaveUpdateInspectionModule", param, true);
            return returnVal;
        }
        public List<InspectionModule> GetInspectionModule(int ID = 0)
        {
            List<InspectionModule> InspectionModule = new List<InspectionModule>();
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("ID", ID) };
            using (SqlDataReader dr = SQL.GetDataReader("GetInspectionModule", param, true))
            {
                while (dr.Read())
                {
                    InspectionModule.Add(new InspectionModule
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Name = dr["Name"].ToString(),
                        FilePath = string.IsNullOrEmpty(dr["FilePath"] + "") ? "" : (baseurl + dr["FilePath"].ToString()),
                        HoverFilePath = string.IsNullOrEmpty(dr["FilePath"] + "") ? "" : (baseurl + dr["FilePath"].ToString().Replace(".png", "-1.png"))
                    });
                }
            }
            return InspectionModule;
        }
        public void DeleteInspectionModule(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("DeleteInspectionModule", param, false);
        }

        #endregion

        #region InspectionModuleCategory
        public int SaveUpdateInspectionModuleCategory(InspectionModuleCategory inspectionModuleCategory)
        {
            var returnVal = 0;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("@Result",""),
                new SqlParameter("ID",inspectionModuleCategory.ID),
                new SqlParameter("Name",inspectionModuleCategory.Name),
                new SqlParameter("InspectionModuleID",inspectionModuleCategory.InspectionModuleID)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            returnVal = SQL.ExceuteStoreProcedure("SaveUpdateInspectionModuleCategory", param, true);
            return returnVal;
        }
        public List<InspectionModuleCategory> GetInsModuleCategoryByModuleID(int InspectionModuleID)
        {
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("InspectionModuleID", InspectionModuleID) };
            return GetInspectionModuleCategory(param);
        }
        public List<InspectionModuleCategory> GetInsModuleCategoryByName(string Name)
        {
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("Name", Name) };
            return GetInspectionModuleCategory(param);
        }
        public InspectionModuleCategory GetInsModuleCategoryByID(int ID)
        {
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("ID", ID) };
            return GetInspectionModuleCategory(param).FirstOrDefault();
        }
        private List<InspectionModuleCategory> GetInspectionModuleCategory(SqlParameter[] param)
        {
            List<InspectionModuleCategory> inspectionModuleCategory = new List<InspectionModuleCategory>();
            using (SqlDataReader dr = SQL.GetDataReader("GetInspectionModuleCategory", param, true))
            {
                while (dr.Read())
                {
                    inspectionModuleCategory.Add(new InspectionModuleCategory
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Name = dr["Name"].ToString(),
                        InspectionModuleID = Convert.ToInt32(dr["InspectionModuleID"]),
                        FilePath = string.IsNullOrEmpty(dr["FilePath"] + "") ? "" : (baseurl + dr["FilePath"].ToString()),
                        HoveFilePath = string.IsNullOrEmpty(dr["FilePath"] + "") ? "" : (baseurl + dr["FilePath"].ToString().Replace(".png", "-1.png"))

                    });
                }
            }
            return inspectionModuleCategory;
        }
        public void DeleteInspectionModuleCategory(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("DeleteInspectionModuleCategory", param, false);
        }
        #endregion

        #region InspectionForm
        public int SaveUpdateInspectionForm(InspectionForm inspectionForm)
        {
            var returnVal = 0;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("@Result",""),
                new SqlParameter("ID",inspectionForm.ID),
                new SqlParameter("FormName",inspectionForm.FormName),
                new SqlParameter("InspectionModuleID",inspectionForm.InspectionModuleID),
                new SqlParameter("CompanyID",inspectionForm.CompanyID),
                new SqlParameter("ModuleCategoryID",inspectionForm.ModuleCategoryID)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            returnVal = SQL.ExceuteStoreProcedure("SaveUpdateInspectionForm", param, true);
            return returnVal;
        }
        public List<InspectionForm> GetInspectionFormByCompanyID(int InspectionModuleID, int CategoryID, int CompanyID)
        {
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("InspectionModuleID", InspectionModuleID),
            new SqlParameter("ModuleCategoryID",CategoryID),new SqlParameter("CompanyID",CompanyID)};
            return GetInspectionForm(param);
        }
        public List<InspectionForm> GetFormListByCategoryId(int CategoryID, int CompanyID)
        {
            SqlParameter[] param = new SqlParameter[] {
            new SqlParameter("ModuleCategoryID",CategoryID),
                new SqlParameter("CompanyID",CompanyID)
            };
            return GetInspectionForm(param);
        }
        public List<InspectionForm> GetInsFormlistByName(int CompanyID, string ModuleName)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",CompanyID),
                new SqlParameter("ModuleName",ModuleName)
            };
            return GetInspectionForm(param);
        }
        public InspectionForm GetInspectionFormByID(int ID)
        {
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("ID", ID) };
            return GetInspectionForm(param).FirstOrDefault();
        }
        private List<InspectionForm> GetInspectionForm(SqlParameter[] param)
        {
            List<InspectionForm> inspectionForm = new List<InspectionForm>();
            using (SqlDataReader dr = SQL.GetDataReader("GetInspectionForm", param, true))
            {
                while (dr.Read())
                {
                    inspectionForm.Add(new InspectionForm
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        FormName = dr["FormName"].ToString(),
                        InspectionModuleID = Convert.ToInt32(dr["InspectionModuleID"]),
                        CompanyID = Convert.ToInt32(dr["CompanyID"]),
                        ModuleCategoryID = Convert.ToInt32(dr["ModuleCategoryID"]),
                        FilePath = string.IsNullOrEmpty(dr["FilePath"] + "") ? "" : (baseurl + dr["FilePath"].ToString()),
                        HoverFilePath = string.IsNullOrEmpty(dr["FilePath"] + "") ? "" : (baseurl + dr["FilePath"].ToString().Replace(".png", "-1.png"))
                    });
                }
            }
            return inspectionForm;
        }
        public void DeleteInspectionForm(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("DeleteInspectionForm", param, false);
        }
        #endregion

        #region InspectionFormQuestion
        public int SaveInspectionFormQuestion(InspectionFormQuestion insFormQuestion)
        {
            var returnVal = 0;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("@Result",""),
                new SqlParameter("ID",insFormQuestion.ID),
                new SqlParameter("Question",insFormQuestion.Question),
                new SqlParameter("InspectionModuleID",insFormQuestion.InspectionModuleID),
                new SqlParameter("InspectionFormID",insFormQuestion.InspectionFormID),
                new SqlParameter("CompanyID",insFormQuestion.CompanyID),
                new SqlParameter("ModuleCategoryID",insFormQuestion.ModuleCategoryID)

            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            returnVal = SQL.ExceuteStoreProcedure("SaveUpdateInspectionFormQuestion", param, true);
            return returnVal;
        }
        public void setQuestionActive(QuestionActive model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("@Result",""),
                new SqlParameter("ID",model.InspectionFormQuestionID),
                new SqlParameter("Active",model.Active)

            };
            SQL.ExecuteQuery("update InspectionFormQuestion set Active =@Active WHERE ID=@ID", param);
        }
        public List<CompanyInspectionForm> GetCompanyInspectionFormQuestion(int CompanyID, int ModuleID, int CategoryID)
        {
            List<CompanyInspectionForm> compInspectionForm = new List<CompanyInspectionForm>();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",CompanyID),
                new SqlParameter("InspectionModuleID",ModuleID),
                new SqlParameter("ModuleCategoryID",CategoryID)
            };
            List<InspectionFormQuestion> inspFormQuestion = new List<InspectionFormQuestion>();
            using (SqlDataReader dr = SQL.GetDataReader("GetCompanyInspectionFormQuestion", param, true))
            {
                while (dr.Read())
                {
                    inspFormQuestion.Add(new InspectionFormQuestion
                    {
                        ID = Convert.ToInt32(dr["QuestionID"]),
                        Question = dr["Question"].ToString(),
                        InspectionFormID = Convert.ToInt32(dr["InspectionFormID"]),
                        CompanyID = Convert.ToInt32(dr["CompanyID"]),
                        FormName = Convert.ToString(dr["FormName"]),
                        InspectionModuleID = Convert.ToInt32(dr["InspectionModuleID"]),
                        Active = Convert.ToBoolean(dr["Active"])
                    });
                }
            }
            if (inspFormQuestion != null && inspFormQuestion.Count > 0)
            {
                List<InsQuestion> insQuestion = null;
                var dstform = inspFormQuestion.Select(x => x.FormName).Distinct();
                foreach (var form in dstform)
                {
                    insQuestion = new List<InsQuestion>();
                    var formquestion = inspFormQuestion.Where(x => x.FormName == form);
                    foreach (var qes in formquestion.Where(x => x.ID > 0 && !string.IsNullOrEmpty(x.Question)))
                    {
                        insQuestion.Add(new InsQuestion
                        {
                            ID = qes.ID,
                            Question = qes.Question,
                            Active = qes.Active
                        });
                    }
                    compInspectionForm.Add(new CompanyInspectionForm
                    {
                        CompanyID = formquestion.FirstOrDefault().CompanyID,
                        ModuleID = formquestion.FirstOrDefault().InspectionModuleID,
                        FormName = form,
                        QuestionList = new List<InsQuestion>(insQuestion)
                    });
                }
            }
            return compInspectionForm;
        }
        public void DeleteInspectionFormQuestion(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("DeleteInspectionFormQuestion", param, false);
        }
        #endregion

        #region Saftyboard
        public int SaveSaftyBoard(SaftyBoard model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("Result",""),
                new SqlParameter("ID",model.ID),
                new SqlParameter("Title",model.Title),
                new SqlParameter("ExpirationDate",model.ExpirationDate),
                new SqlParameter("Decription",model.Decription),
                new SqlParameter("FilePath",model.FileName),
                new SqlParameter("Location",model.Location),
                new SqlParameter("CreatedBy",model.CreatedBy),
                new SqlParameter("CompanyID",model.CompanyID)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            int result = SQL.ExceuteStoreProcedure("SaveSaftyBoard", param, true);
            if (result > 0)
                _commonServices.SaveFileRecord(model.FileName, model.CompanyID, "HeathSafety", true);

            return result;
        }
        public List<SaftyBoard> GetSaftyBoard(int CompanyID)
        {
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("CompanyID", CompanyID) };
            return GetSaftyBoard(param);
        }
        public SaftyBoard GetSaftyBoardbyID(int ID)
        {
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("ID", ID) };
            return GetSaftyBoard(param).FirstOrDefault();
        }
        private List<SaftyBoard> GetSaftyBoard(SqlParameter[] param)
        {
            List<SaftyBoard> saftyBoard = new List<SaftyBoard>();
            using (SqlDataReader dr = SQL.GetDataReader("GetSaftyBoard", param, true))
            {
                while (dr.Read())
                {
                    saftyBoard.Add(new SaftyBoard
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Title = Convert.ToString(dr["Title"]),
                        ExpirationDate = CommonHelper.DateToString(dr["ExpirationDate"]),
                        Decription = Convert.ToString(dr["Decription"]),
                        FileName = Convert.ToString(dr["FilePath"]),
                        FileUrl = string.IsNullOrEmpty(dr["FilePath"] + "") ? "" : (baseurl + Convert.ToString(dr["FilePath"])),
                        Location = Convert.ToString(dr["Location"]),
                        CompanyID = Convert.ToInt32(dr["CompanyID"]),
                        CreatedBy = Convert.ToInt32(dr["CreatedBy"]),
                        CreatedDate = CommonHelper.DateToString(dr["CreatedDate"]),
                        CreatedUser = Convert.ToString(dr["CreatedUser"]),
                    });
                }
            }
            return saftyBoard;
        }
        public void DeleteSaftyBoard(int id)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",id)
            };
            SQL.ExceuteStoreProcedure("DeleteSaftyBoard", param, false);
        }
        #endregion

        #region SaftyBoardAnnouncement
        public int SaveSaftyBoardAnnouncement(SaftyBoardAnnouncement model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("Result",""),
                new SqlParameter("ID",model.ID),
                new SqlParameter("Announcement",model.Announcement),
                new SqlParameter("DurationFrom",model.DurationFrom),
                new SqlParameter("DurationTo",model.DurationTo),
                new SqlParameter("CreatedBy",model.CreatedBy),
                new SqlParameter("CompanyID",model.CompanyID)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            return SQL.ExceuteStoreProcedure("SaveSaftyBoardAnnouncement", param, true);
        }
        public List<SaftyBoardAnnouncement> GetSaftyBoardAnnouncement(int CompanyID)
        {
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("CompanyID", CompanyID) };
            return GetSaftyBoardAnnouncement(param);
        }
        public SaftyBoardAnnouncement GetSaftyBoardAnnouncementbyID(int ID)
        {
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("ID", ID) };
            return GetSaftyBoardAnnouncement(param).FirstOrDefault();
        }
        private List<SaftyBoardAnnouncement> GetSaftyBoardAnnouncement(SqlParameter[] param)
        {
            List<SaftyBoardAnnouncement> SaftyBoardAnnouncement = new List<SaftyBoardAnnouncement>();
            using (SqlDataReader dr = SQL.GetDataReader("GetSaftyBoardAnnouncement", param, true))
            {
                while (dr.Read())
                {
                    SaftyBoardAnnouncement.Add(new SaftyBoardAnnouncement
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Announcement = Convert.ToString(dr["Announcement"]),
                        DurationFrom = Convert.ToString(dr["DurationFrom"]),
                        DurationTo = Convert.ToString(dr["DurationTo"]),
                        CompanyID = Convert.ToInt32(dr["CompanyID"]),
                        CreatedBy = Convert.ToInt32(dr["CreatedBy"]),
                        CreatedUser = Convert.ToString(dr["CreatedUser"]),
                    });
                }
            }
            return SaftyBoardAnnouncement;
        }
        public void DeleteSaftyBoardAnnouncement(int id)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",id)
            };
            SQL.ExceuteStoreProcedure("DeleteSaftyBoardAnnouncement", param, false);
        }
        #endregion

        #region SaftyDataSheet
        public int SaveSaftyDataSheet(SaftyDataSheet model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("Result",""),
                new SqlParameter("ID",model.ID),
                new SqlParameter("ChemicalName",model.ChemicalName),
                new SqlParameter("Supplier",model.Supplier),
                new SqlParameter("ExpirationDate",model.ExpirationDate),
                new SqlParameter("FilePath",model.FileName),
                new SqlParameter("CreatedBy",model.CreatedBy),
                new SqlParameter("CompanyID",model.CompanyID)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            int result = SQL.ExceuteStoreProcedure("SaveSaftyDataSheet", param, true);
            if (result > 0)
                _commonServices.SaveFileRecord(model.FileName, model.CompanyID, "HeathSafety", true);

            return result;
        }
        public List<SaftyDataSheet> GetSaftyDataSheet(int CompanyID)
        {
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("CompanyID", CompanyID) };
            return GetSaftyDataSheet(param);
        }
        public SaftyDataSheet GetSaftyDataSheetbyID(int ID)
        {
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("ID", ID) };
            return GetSaftyDataSheet(param).FirstOrDefault();
        }
        private List<SaftyDataSheet> GetSaftyDataSheet(SqlParameter[] param)
        {
            List<SaftyDataSheet> SaftyDataSheet = new List<SaftyDataSheet>();
            using (SqlDataReader dr = SQL.GetDataReader("GetSaftyDataSheet", param, true))
            {
                while (dr.Read())
                {
                    SaftyDataSheet.Add(new SaftyDataSheet
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        ChemicalName = Convert.ToString(dr["ChemicalName"]),
                        ExpirationDate = CommonHelper.DateToString(dr["ExpirationDate"]),
                        Supplier = Convert.ToString(dr["Supplier"]),
                        FileName = Convert.ToString(dr["FilePath"]),
                        FileUrl = string.IsNullOrEmpty(dr["FilePath"] + "") ? "" : (baseurl + Convert.ToString(dr["FilePath"])),
                        Status = Convert.ToString(dr["Status"]),
                        CompanyID = Convert.ToInt32(dr["CompanyID"]),
                        CreatedBy = Convert.ToInt32(dr["CreatedBy"])
                    });
                }
            }
            return SaftyDataSheet;
        }
        public void DeleteSaftyDataSheet(int id)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",id)
            };
            SQL.ExceuteStoreProcedure("DeleteSaftyDataSheet", param, false);
        }
        #endregion

        #region Equipment Type
        public int SaveEquipmentType(EquipmentType model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("Result",""),
                new SqlParameter("ID",model.ID),
                new SqlParameter("Type",model.Type),
                new SqlParameter("CreatedBy",model.CreatedBy),
                new SqlParameter("CompanyID",model.CompanyID)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            return SQL.ExceuteStoreProcedure("SaveEquipmentType", param, true);
        }
        public List<EquipmentType> GetEquipmentType(int CompanyID)
        {
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("CompanyID", CompanyID) };
            return GetEquipmentType(param);
        }
        public EquipmentType GetEquipmentTypebyID(int ID)
        {
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("ID", ID) };
            return GetEquipmentType(param).FirstOrDefault();
        }
        private List<EquipmentType> GetEquipmentType(SqlParameter[] param)
        {
            List<EquipmentType> equipmentType = new List<EquipmentType>();
            using (SqlDataReader dr = SQL.GetDataReader("GetEquipmentType", param, true))
            {
                while (dr.Read())
                {
                    equipmentType.Add(new EquipmentType
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Type = Convert.ToString(dr["Type"]),
                        CompanyID = Convert.ToInt32(dr["CompanyID"]),
                        CreatedBy = Convert.ToInt32(dr["CreatedBy"])
                    });
                }
            }
            return equipmentType;
        }
        public void DeleteEquipmentType(int Id)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",Id)
            };
            SQL.ExecuteQuery("Update EquipmentType SET Isdeleted=1 WHERE ID=@ID", param);
        }
        #endregion

        #region Lifting Device Certification
        public int SaveLiftingDeviceCertification(LiftingDeviceCertification model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("Result",""),
                new SqlParameter("ID",model.ID),
                new SqlParameter("EquipmentTypeID",model.EquipmentTypeID),
                new SqlParameter("Model",model.Model),
                new SqlParameter("Supplier",model.Supplier),
                new SqlParameter("ExpirationDate",model.ExpirationDate),
                new SqlParameter("FilePath",model.FileName),
                new SqlParameter("CreatedBy",model.CreatedBy),
                new SqlParameter("CompanyID",model.CompanyID)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            int result = SQL.ExceuteStoreProcedure("SaveLiftingDeviceCertification", param, true);
            if (result > 0)
                _commonServices.SaveFileRecord(model.FileName, model.CompanyID, "HeathSafety", true);

            return result;
        }
        public List<LiftingDeviceCertification> GetLiftingDeviceCertification(int CompanyID)
        {
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("CompanyID", CompanyID) };
            return GetLiftingDeviceCertification(param);
        }
        public LiftingDeviceCertification GetLiftingDeviceCertificationbyID(int ID)
        {
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("ID", ID) };
            return GetLiftingDeviceCertification(param).FirstOrDefault();
        }
        private List<LiftingDeviceCertification> GetLiftingDeviceCertification(SqlParameter[] param)
        {
            List<LiftingDeviceCertification> liftingDeviceCertification = new List<LiftingDeviceCertification>();
            using (SqlDataReader dr = SQL.GetDataReader("GetLiftingDeviceCertificationList", param, true))
            {
                while (dr.Read())
                {
                    liftingDeviceCertification.Add(new LiftingDeviceCertification
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        EquipmentTypeID = Convert.ToInt32(dr["EquipmentTypeID"]),
                        EquipmentType = Convert.ToString(dr["EquipmentType"]),
                        ExpirationDate = CommonHelper.DateToString(dr["ExpirationDate"]),
                        Model = Convert.ToString(dr["Model"]),
                        Supplier = Convert.ToString(dr["Supplier"]),
                        FileName = Convert.ToString(dr["FilePath"]),
                        FileUrl = string.IsNullOrEmpty(dr["FilePath"] + "") ? "" : (baseurl + Convert.ToString(dr["FilePath"])),
                        Status = Convert.ToString(dr["Status"]),
                        CompanyID = Convert.ToInt32(dr["CompanyID"]),
                        CreatedBy = Convert.ToInt32(dr["CreatedBy"])
                    });
                }
            }
            return liftingDeviceCertification;
        }
        public void DeleteLiftingDeviceCertification(int Id)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",Id)
            };
            SQL.ExecuteQuery("UPDATE LiftingDeviceCertification SET Isdeleted=1 WHERE ID=@ID", param);
        }
        #endregion

        #region Save inspection details
        public List<InspectionPriority> GetInspectionPriority()
        {
            List<InspectionPriority> lstInspectionPriority = new List<InspectionPriority>();
            using (SqlDataReader dr = SQL.GetDataReader("GetInspectionPriority", true))
            {
                while (dr.Read())
                {
                    lstInspectionPriority.Add(new InspectionPriority {
                        ID = Convert.ToInt32(dr["ID"]),
                        Title = Convert.ToString(dr["Title"])
                    });
                }
            }
            return lstInspectionPriority;
        }
        public int SaveInspectionDetails(InspectionDetailsRequest model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("Result",""),
                new SqlParameter("ID",model.ID),
                new SqlParameter("Name",model.Name),
                new SqlParameter("PriorityID",model.PriorityID),
                new SqlParameter("Description",model.Description),
                new SqlParameter("DateOfInspection",model.DateOfInspection),
                new SqlParameter("Type",model.Type),
                new SqlParameter("EmployeeID",model.EmployeeID),
                new SqlParameter("CreatedBy",model.CreatedBy),
                new SqlParameter("CompanyID",model.CompanyID)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            return SQL.ExceuteStoreProcedure("SaveInspectionDetails", param, true);
        }
        public List<InspectionDetailsResponse> GetInspectionDetails(Inspectiondto model)
        {
            List<InspectionDetailsResponse> objlist = new List<InspectionDetailsResponse>();
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("CompanyID",model.CompanyID),
                new SqlParameter("Type",model.Type),
                new SqlParameter("ID",model.ID),
                new SqlParameter("StartDate",model.StartDate),
                new SqlParameter("EndDate",model.EndDate)
            };
            using (SqlDataReader dr = SQL.GetDataReader("GetInspectionDetails", param, true))
            {
                while (dr.Read())
                {
                    objlist.Add(new InspectionDetailsResponse
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Name = Convert.ToString(dr["Name"]),
                        InspectionID = Convert.ToString(dr["InspectionID"]),
                        DateOfInspection = CommonHelper.DateToString(dr["DateOfInspection"]),
                        PriorityValue = Convert.ToString(dr["PriorityValue"]),
                        PriorityID = Convert.ToInt32(dr["PriorityID"]),
                        Description = Convert.ToString(dr["Description"]),
                        Status = Convert.ToInt32(dr["Status"]),
                        StatusValue = Convert.ToString(dr["StatusValue"]),
                        EmployeeID = Convert.ToInt32(dr["EmployeeID"]),
                        CompanyID = Convert.ToInt32(dr["CompanyID"]),
                        CreatedBy = Convert.ToInt32(dr["CreatedBy"]),
                        CreatedDate = CommonHelper.DateToString(dr["CreatedDate"]),
                        Type = Convert.ToString(dr["Type"]),
                        EmployeeName = dr["EmployeeName"]+""
                    });
                }
            }
            return objlist;
        }
        public List<HealthAndSaftyEmail> GetNonInspectionParticipants(int CompanyID)
        {
            List<HealthAndSaftyEmail> inspectionParticipant = new List<HealthAndSaftyEmail>();
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("CompanyID", CompanyID) };
            using (SqlDataReader dr = SQL.GetDataReader("GetNonInspectionParticipants", param, true))
            {
                while (dr.Read())
                {
                    inspectionParticipant.Add(new HealthAndSaftyEmail
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        EmployeeID = Convert.ToInt32(dr["EmployeeID"]),
                        Name = dr["Name"].ToString(),
                        Email = dr["Email"].ToString(),
                        Status = dr["Status"].ToString(),
                        CompanyID = Convert.ToInt32(dr["CompanyID"]),
                        CreatedDate = Convert.ToDateTime(dr["CreatedDate"])
                    });
                }
            }
            return inspectionParticipant;
        }
        public void saveInspectionParticipants(Participants model, bool IsEmailParticipant = false)
        {
            var dtemployee = CommonHelper.ToDataTable(model.ParticipantsList);
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",model.CompanyID),
                new SqlParameter("InspectionID",model.InspectionID),
                new SqlParameter("Type",model.Type),
                new SqlParameter("IsEmailParticipant",IsEmailParticipant),
                new SqlParameter("Employeelist",dtemployee)
            };
            SQL.ExceuteStoreProcedure("SaveInspectionParticipants", param, false);
        }
        public Participants GetInspectionParticipants(int CompanyID, int InspectionID, string Type, bool IsEmailParticipant = false)
        {
            Participants objParticipants = new Participants();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",CompanyID),
                new SqlParameter("InspectionID",InspectionID),
                new SqlParameter("Type",Type),
                new SqlParameter("IsEmailParticipant",IsEmailParticipant)
            };
            var dt = SQL.GetDt("GetInspectionParticipants", param, true);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    objParticipants.ParticipantsList.Add(new InsParticipant
                    {
                        EmployeeID = Convert.ToInt32(dr["ID"]),
                        Email = Convert.ToString(dr["Email"])
                    });
                    if (dt.Rows.Count - 1 == dt.Rows.IndexOf(dr))
                    {
                        objParticipants.CompanyID = Convert.ToInt32(dr["CompanyID"]);
                        objParticipants.InspectionID = Convert.ToInt32(dr["InspectionID"]);
                        objParticipants.Type = Convert.ToString(dr["Type"]);
                    }
                }
            }
            return objParticipants;
        }
        public List<Participantdto> GetParticipantByInspection(int CompanyID, int InspectionID, bool IsEmailParticipant = false)
        {
           List<Participantdto> objParticipants = new List<Participantdto>();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",CompanyID),
                new SqlParameter("InspectionID",InspectionID),
                new SqlParameter("IsEmailParticipant",IsEmailParticipant)
            };
            var dt = SQL.GetDt("GetInspectionParticipants", param, true);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    objParticipants.Add(new Participantdto
                    {
                        InspectionID = Convert.ToInt32(dr["InspectionID"]),
                        CompanyID = Convert.ToInt32(dr["CompanyID"]),
                        EmployeeID = Convert.ToInt32(dr["EmployeeID"]),
                        Email = Convert.ToString(dr["Email"]),
                        Name = Convert.ToString(dr["Name"])
                    });
                }
            }
            return objParticipants;
        }
        public void sendInspectionParticipantMail(Participants model, int IsEmailParticipant = 0)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",model.InspectionID),
                new SqlParameter("CompanyID",model.CompanyID),
                new SqlParameter("@IsEmailParticipant",IsEmailParticipant)
            };
            DataSet ds = SQL.GetDs("GetUnsafeQuestionByInspectionID", param, true);
            List<UnSafeQuestion> unSafeQuestionlist = new List<UnSafeQuestion>();
            string html = string.Empty;
            if (ds != null && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
            {
                var objList = CommonHelper.ToList<UnSafeQuestion>(ds.Tables[0]);
                if (model.Type == "HealthSafety")
                {
                    var categoryList = objList.Select(x => x.CategoryName).Distinct();
                    var healthsaftyQuestion = objList.Where(x => !string.IsNullOrEmpty(x.CategoryName));
                    html = "<ul>";
                    foreach (var category in categoryList)
                    {
                        html += "<li>" + category;
                        var formList = healthsaftyQuestion.Where(x => x.CategoryName == category).ToList();
                        var dstFormlist = formList.Select(x => x.FormName).Distinct().ToList();
                        if (dstFormlist.Count > 0)
                        {
                            html += "<ul>";
                            foreach (var form in dstFormlist)
                            {
                                html += "<li>" + form;
                                var questionList = formList.Where(x => x.FormName == form).ToList();
                                if (questionList.Count > 0)
                                {
                                    html += "<ul>";
                                    foreach (var question in questionList)
                                    {
                                        html += "<li>" + question.Question + "</li>";
                                    }
                                    html += "</ul>";
                                }
                                html += "</li>";
                            }
                            html += "</ul>";
                        }
                        html += "</li>";
                    }
                    html += "</ul>";
                }
                else
                {
                    var formList = objList.Select(x => x.FormName).Distinct();
                    html = "<ul>";
                    foreach (var form in formList)
                    {
                        html += "<li>" + form;
                        var questionlist = objList.Where(x => x.FormName == form).ToList();
                        if (questionlist.Count > 0)
                        {
                            html += "<ul>";
                            foreach (var question in questionlist)
                            {
                                html += "<li>" + question.Question + "</li>";
                            }
                            html += "</ul>";
                        }
                        html += "</li>";
                    }
                    html += "</ul>";
                }
                var emailList = CommonHelper.ToList<EmailList>(ds.Tables[1]);
                string emails = string.Join(",", emailList.Select(x => x.Email));
                string emailbody = html;
                CommonHelper.SendEmail(emails, model.Type +" Inspection", emailbody);
            }
        }
        public void SaveInspectionQuestionAnswer(List<InsQuestion> modellist, int InspectionID, int CompanyID, int FormID, int CreatedBy, int ModuleCategoryID = 0)
        {
            string sourcePath = Path.Combine(HttpContext.Current.Server.MapPath("~" + "/Files/tmpimg_" + InspectionID + "_" + CompanyID + "/"));
            if (Directory.Exists(sourcePath))
            {
                string destinationPath = Path.Combine(HttpContext.Current.Server.MapPath("~" + "/Files/HeathSafety/"));
                CommonHelper.CopyFiles(sourcePath, destinationPath);
            }
            DeleteInspectionanswer(InspectionID, CompanyID, FormID, ModuleCategoryID);
            List<InspectionQuestionAnswerdto> objlist = new List<InspectionQuestionAnswerdto>();
            foreach (var model in modellist)
            {
                if (model.AnswerList.Count > 0)
                {
                    foreach (var ans in model.AnswerList)
                    {
                        if (!string.IsNullOrEmpty(ans.FilePath) && !ans.FilePath.Contains("HeathSafety"))
                        { ans.FilePath = "/Files/HeathSafety/" + ans.FilePath.Split('/')[ans.FilePath.Split('/').Length - 1]; }

                        if (!string.IsNullOrEmpty(ans.FilePath)) { _commonServices.SaveFileRecord(ans.FilePath, CompanyID, "HeathSafety", true); }

                        objlist.Add(new InspectionQuestionAnswerdto
                        {
                            QuestionID = model.ID,
                            Location = ans.Location,
                            Description = ans.Description,
                            Action = ans.Action,
                            HAZARDClass = ans.HAZARDClass,
                            Safe = model.Safe,
                            FilePath = ans.FilePath
                        });
                    }
                }
                else
                {
                    objlist.Add(new InspectionQuestionAnswerdto
                    {
                        QuestionID = model.ID,
                        Location = string.Empty,
                        Description = string.Empty,
                        Action = string.Empty,
                        HAZARDClass = string.Empty,
                        Safe = model.Safe,
                        FilePath = string.Empty
                    });
                }
            }
            var dt = CommonHelper.ToDataTable(objlist);
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("InspectionID",InspectionID),
                new SqlParameter("CompanyID",CompanyID),
                new SqlParameter("FormID",FormID),
                new SqlParameter("ModuleCategoryID",ModuleCategoryID),
                new SqlParameter("CreatedBy",CreatedBy),
                new SqlParameter("InspectionQuestionAnswer",dt)
            };
            SQL.ExceuteStoreProcedure("SaveInspectionFormQusAnswer", param, false);
            
        }
        private void DeleteInspectionanswer(int InspectionID, int CompanyID, int FormID, int ModuleCategoryID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("InspectionID", InspectionID),
                new SqlParameter("ModuleCategoryID", ModuleCategoryID),
                new SqlParameter("FormID", FormID),
                new SqlParameter("CompanyID", CompanyID),
            };
            SQL.ExceuteStoreProcedure("DeleteInspectionAnswer", param, false);
        }
        public List<InsQuestion> GetInspectionQuestionAnswer(int CompanyID, int InspectionID,int FormID,int CategoryID=0)
        {
            List<InsQuestion> insQuestion = new List<InsQuestion>();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("InspectionID",InspectionID),
                new SqlParameter("CompanyID",CompanyID),
                new SqlParameter("FormID",FormID),
                new SqlParameter("ModuleCategoryID",CategoryID)
            };
            DataSet  ds= SQL.GetDs("GetInspectionQuestionAnswer", param, true);
            
            if (ds!=null&&ds.Tables.Count==2)
            {
                List<InsQuestionAnswer> answerList = null;
                var dtAnswer = ds.Tables[1];
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    answerList = new List<InsQuestionAnswer>();
                    if (dtAnswer != null && dtAnswer.Rows.Count > 0)
                    {
                        DataRow[] result = dtAnswer.Select("QuestionID = '" + Convert.ToInt32(row["ID"]) + "'");
                        foreach (DataRow dr in result)
                        {
                            answerList.Add(new InsQuestionAnswer
                            {
                                ID = Convert.ToInt32(dr["ID"]),
                                QuestionID = Convert.ToInt32(dr["QuestionID"]),
                                Location = Convert.ToString(dr["Location"] + ""),
                                Description = Convert.ToString(dr["Description"] + ""),
                                Action = Convert.ToString(dr["Action"] + ""),
                                HAZARDClass = Convert.ToString(dr["HAZARDClass"] + ""),
                                FilePath = Convert.ToString(dr["FilePath"] + ""),
                                Safe=Convert.ToInt32(dr["Safe"]),
                                FileUrl = string.IsNullOrEmpty(dr["FilePath"] + "") ? "" : (baseurl + Convert.ToString(dr["FilePath"])),
                            });
                        }
                    }
                    insQuestion.Add(new InsQuestion
                    {
                        ID = Convert.ToInt32(row["ID"]),
                        Question = Convert.ToString(row["Question"]),
                        Active = Convert.ToBoolean(row["Active"]),
                        Safe = answerList.Count > 0 ? answerList.FirstOrDefault().Safe : 0,
                        AnswerList = new List<InsQuestionAnswer>(answerList)
                    });
                }
                
            }
            return insQuestion;
        }
        public InspectionDetails GetInspectionDetailsByID(int CompanyID, int InspectionID, string Type)
        {
            InspectionDetails inspectionDetails = new InspectionDetails();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("InspectionID",InspectionID),
                new SqlParameter("CompanyID",CompanyID),
                new SqlParameter("Type",Type)
            };
            DataSet ds = SQL.GetDs("GetInspectionDetailsByID", param, true);
            if (ds != null && ds.Tables.Count == 3)
            {
                List<UnsafeImageList> unsafeimageslist = new List<UnsafeImageList>();
                List<Module> modulelist = new List<Module>();
                if (ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                {
                    List<Form> formlist = null;
                    //COnvert Datatable to list
                    var objList =  CommonHelper.ToList<Moduledto>(ds.Tables[1]);
                    var distIds = objList.Select(x => x.ModuleCategoryID).Distinct();
                    bool flag = true;
                    foreach (var id in distIds)
                    {
                        flag = true;
                        formlist = new List<Form>();
                        var categoryFormList = objList.Where(x => x.ModuleCategoryID == id).ToList();
                        foreach(var obj in categoryFormList)
                        {
                            formlist.Add(new Form
                            {
                                ID = obj.FormID,
                                FormName = obj.FormName,
                                Complete= obj.Safe
                            });
                            if (flag) { flag=obj.Safe; }
                        }
                        var singleobj = categoryFormList.FirstOrDefault();
                        modulelist.Add(new Module
                        {
                            ID = singleobj.ModuleCategoryID,
                            Name = singleobj.Name==""?"Gernal Inspection":singleobj.Name,
                            Complete = flag,
                            FormList = formlist
                        });
                    }
                }
                if (ds.Tables[2] != null && ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[2].Rows)
                    {
                        inspectionDetails.UnSafeImageList.Add(new UnsafeImageList
                        {
                            Title = Convert.ToString(dr["Question"]),
                            Image = baseurl + Convert.ToString(dr["FilePath"])
                        });
                    }
                }
                if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    inspectionDetails.ID = Convert.ToInt32(dr["ID"]);
                    inspectionDetails.Name = Convert.ToString(dr["Name"]);
                    inspectionDetails.InspectionID = Convert.ToString(dr["InspectionID"]);
                    inspectionDetails.DateOfInspection = CommonHelper.DateToString(dr["DateOfInspection"]);
                    inspectionDetails.CompleteDate = CommonHelper.DateToString(dr["CompleteDate"]);
                    inspectionDetails.PriorityValue = Convert.ToString(dr["PriorityValue"]);
                    inspectionDetails.PriorityID = Convert.ToInt32(dr["PriorityID"]);
                    inspectionDetails.Description = Convert.ToString(dr["Description"]);
                    inspectionDetails.StatusValue = Convert.ToString(dr["StatusValue"]);
                    inspectionDetails.CompanyID = Convert.ToInt32(dr["CompanyID"]);
                    inspectionDetails.CreatedBy = Convert.ToInt32(dr["CreatedBy"]);
                    inspectionDetails.CompletedTask = Convert.ToInt32(dr["CompletedTask"]);
                    inspectionDetails.PendingTask= Convert.ToInt32(dr["PendingTask"]);
                    inspectionDetails.PendingTaskPercentage = ((inspectionDetails.PendingTask / ((float)(inspectionDetails.PendingTask + inspectionDetails.CompletedTask))) * 100.0).ToString("0.00");
                    inspectionDetails.CompletedTaskPercentage = ((inspectionDetails.CompletedTask / ((float)(inspectionDetails.PendingTask + inspectionDetails.CompletedTask))) * 100.0).ToString("0.00");
                    inspectionDetails.AllModule = modulelist;
                    inspectionDetails.UnsafeModule = modulelist.Where(x=>!x.Complete).ToList();
                    inspectionDetails.SafeModule = modulelist.Where(x => x.Complete).ToList();
                }
            }
            return inspectionDetails;
        }
        public void InspectionComplete(int InspectionID, int CreatedBy,string Type)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",InspectionID),
                new SqlParameter("CreatedBy",CreatedBy)
            };
            DataSet ds = SQL.GetDs("CompleteInspection", param, true);
            List<UnSafeQuestion> unSafeQuestionlist = new List<UnSafeQuestion>();
            string html = string.Empty;
            if (ds!=null&& ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
            {
                var objList = CommonHelper.ToList<UnSafeQuestion>(ds.Tables[0]);
                if (Type == "HealthSafety")
                {
                    var categoryList = objList.Select(x => x.CategoryName).Distinct();
                    var healthsaftyQuestion = objList.Where(x => !string.IsNullOrEmpty(x.CategoryName));
                    html = "<ul>";
                    foreach (var category in categoryList)
                    {
                        html += "<li>" + category;
                        var formList = healthsaftyQuestion.Where(x => x.CategoryName == category).ToList();
                        var dstFormlist = formList.Select(x => x.FormName).Distinct().ToList();
                        if (dstFormlist.Count > 0)
                        {
                            html += "<ul>";
                            foreach (var form in dstFormlist)
                            {
                                html += "<li>" + form;
                                var questionList = formList.Where(x => x.FormName == form).ToList();
                                if (questionList.Count > 0)
                                {
                                    html += "<ul>";
                                    foreach (var question in questionList)
                                    {
                                        html += "<li>" + question.Question + "</li>";
                                    }
                                    html += "</ul>";
                                }
                                html += "</li>";
                            }
                            html += "</ul>";
                        }
                        html += "</li>";
                    }
                    html += "</ul>";
                }
                else
                {
                    var formList = objList.Select(x => x.FormName).Distinct();
                    html = "<ul>";
                    foreach (var form in formList)
                    {
                        html += "<li>" + form;
                        var questionlist = objList.Where(x => x.FormName == form).ToList();
                        if (questionlist.Count > 0)
                        {
                            html += "<ul>";
                            foreach (var question in questionlist)
                            {
                                html += "<li>" + question.Question + "</li>";
                            }
                            html += "</ul>";
                        }
                        html += "</li>";
                    }
                    html += "</ul>";
                }
                var emailList = CommonHelper.ToList<EmailList>(ds.Tables[1]);
                string emails = string.Join(",", emailList.Select(x=>x.Email));
                string emailbody = html;
                CommonHelper.SendEmail(emails, "Inspection Complete", emailbody);
            }
        }
        public void DeleteInspection(int InspectionID)
        {
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("InspectionID",InspectionID)
            };
            SQL.ExceuteStoreProcedure("DeleteInspection", param, false);
        }
        #endregion

        #region Incident API's

        public int SaveGernralInformation(GernralInformationRequestdto model)
        {
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("Result",""),
                new SqlParameter("InspectionID",model.InspectionID),
                new SqlParameter("FormID",model.FormID),
                new SqlParameter("DateOfReport",model.DateOfReport),
                new SqlParameter("AllegedPerpetators",model.AllegedPerpetators),
                new SqlParameter("Complainant",model.Complainant),
                new SqlParameter("Location",model.Location),
                new SqlParameter("ReportedTo",model.ReportedTo),
                new SqlParameter("CreatedBy",model.CreatedBy)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            return SQL.ExceuteStoreProcedure("SaveGernralInformation", param, true);
        }
        public int SaveAllegation(AllegationRequestdto model)
        {
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("Result",""),
                new SqlParameter("InspectionID",model.InspectionID),
                new SqlParameter("FormID",model.FormID),
                new SqlParameter("AllegationIncidents",model.AllegationIncidents),
                new SqlParameter("NatureOfAllegation",model.NatureOfAllegation),
                new SqlParameter("CreatedBy",model.CreatedBy)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            return SQL.ExceuteStoreProcedure("SaveAllegation", param, true);
        }
        public int SaveWitness(WitnessRequestdto model)
        {
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("Result",""),
                new SqlParameter("InspectionID",model.InspectionID),
                new SqlParameter("FormID",model.FormID),
                new SqlParameter("AnyWItness",model.AnyWItness),
                new SqlParameter("NameOfWitness",model.NameOfWitness),
                new SqlParameter("RoleOFWitness",model.RoleOFWitness),
                new SqlParameter("CreatedBy",model.CreatedBy)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            return SQL.ExceuteStoreProcedure("SaveWitness", param, true);
        }
        public int SaveResponse(ResponseRequestdto model)
        {
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("Result",""),
                new SqlParameter("InspectionID",model.InspectionID),
                new SqlParameter("FormID",model.FormID),
                new SqlParameter("ActionOfIncident",model.ActionOfIncident),
                new SqlParameter("DescribePrevIncident",model.DescribePrevIncident),
                new SqlParameter("CreatedBy",model.CreatedBy)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            return SQL.ExceuteStoreProcedure("SaveIncidentResponse", param, true);
        }
        public DataTable GetIncidentFormDetail(int InspectionID, int FormID, int Type = 0)
        {
            List<InsQuestion> insQuestion = new List<InsQuestion>();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("InspectionID",InspectionID),
                new SqlParameter("FormID",FormID),
                new SqlParameter("Type",Type)
            };
            return SQL.GetDt("GetIncidentFormDetail", param, true);
        }
        public InspectionDetails GetIncidentDetailsByID(int CompanyID, int InspectionID, string Type)
        {
            InspectionDetails inspectionDetails = new InspectionDetails();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("InspectionID",InspectionID),
                new SqlParameter("CompanyID",CompanyID),
                new SqlParameter("Type",Type)
            };
            DataSet ds = SQL.GetDs("GetIncidentDetailsByID", param, true);
            if (ds != null && ds.Tables.Count == 2)
            {
                int PendingTask = 0, CompletedTask = 0;
                List<Module> modulelist = new List<Module>();
                List<Form> formlist = new List<Form>();
                bool flag = true;
                if (ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        formlist.Add(new Form
                        {
                            ID = Convert.ToInt32(dr["FormID"]),
                            FormName = Convert.ToString(dr["FormName"]),
                            Complete = Convert.ToBoolean(dr["Safe"])
                        });
                        PendingTask += Convert.ToBoolean(dr["Safe"]) ? 0 : 1;
                        CompletedTask += Convert.ToBoolean(dr["Safe"]) ? 1 : 0;
                        if (flag) { flag = Convert.ToBoolean(dr["Safe"]); }
                    }
                    modulelist.Add(new Module
                    {
                        Name = "Gernal Incpection",
                        Complete = flag,
                        FormList = formlist
                    });
                }
                if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    inspectionDetails.ID = Convert.ToInt32(dr["ID"]);
                    inspectionDetails.Name = Convert.ToString(dr["Name"]);
                    inspectionDetails.InspectionID = Convert.ToString(dr["InspectionID"]);
                    inspectionDetails.DateOfInspection = CommonHelper.DateToString(dr["DateOfInspection"]);
                    inspectionDetails.CompleteDate = CommonHelper.DateToString(dr["CompleteDate"]);
                    inspectionDetails.PriorityValue = Convert.ToString(dr["PriorityValue"]);
                    inspectionDetails.PriorityID = Convert.ToInt32(dr["PriorityID"]);
                    inspectionDetails.Description = Convert.ToString(dr["Description"]);
                    inspectionDetails.StatusValue = Convert.ToString(dr["StatusValue"]);
                    inspectionDetails.CompanyID = Convert.ToInt32(dr["CompanyID"]);
                    inspectionDetails.CreatedBy = Convert.ToInt32(dr["CreatedBy"]);
                    inspectionDetails.PendingTask = PendingTask;
                    inspectionDetails.CompletedTask = CompletedTask;
                    inspectionDetails.PendingTaskPercentage = ((PendingTask / ((float)(PendingTask + CompletedTask))) * 100.0).ToString("0.00");
                    inspectionDetails.CompletedTaskPercentage = ((CompletedTask / ((float)(PendingTask + CompletedTask))) * 100.0).ToString("0.00");
                    inspectionDetails.AllModule = modulelist;
                    inspectionDetails.UnsafeModule.Add(new Module
                    {
                        Name = "Gernal Incpection",
                        Complete = flag,
                        FormList = formlist.Where(x=>!x.Complete).ToList()
                    });
                    inspectionDetails.SafeModule.Add(new Module
                    {
                        Name = "Gernal Incpection",
                        Complete = flag,
                        FormList = formlist.Where(x => x.Complete).ToList()
                    });
                }
            }
            return inspectionDetails;
        }
        #endregion

        #region Accident
        public int SaveAccidentDatedetail(AccidentDatedetaildto model)
        {
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("Result",""),
                new SqlParameter("InspectionID",model.InspectionID),
                new SqlParameter("FormID",model.FormID),
                new SqlParameter("CompanyID",model.CompanyID),
                new SqlParameter("DateOfInspection",model.DateOfInspection),
                new SqlParameter("TimeOfInspection",model.TimeOfInspection),
                new SqlParameter("DateReported",model.DateReported),
                new SqlParameter("TimeReported",model.TimeReported),
                new SqlParameter("ReportedTo",model.ReportedTo),
                new SqlParameter("DiseasesType",model.DiseasesType),
                new SqlParameter("AccidentType",model.AccidentType),
                new SqlParameter("CreatedBy",model.CreatedBy)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            return SQL.ExceuteStoreProcedure("SaveAccidentDatedetail", param, true);
        }
        public AccidentDatedetaildto GetAccidentDatedetailByInspectionID(int InspectionID)
        {
            AccidentDatedetaildto model = null;
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("InspectionID",InspectionID)
            };
            using (SqlDataReader dr = SQL.GetDataReader("GetAccidentDatedetailByInspectionID", param, true))
            {
                while (dr.Read())
                {
                    model = new AccidentDatedetaildto();
                    model.InspectionID=Convert.ToInt32(dr["InspectionID"]);
                    model.DateOfInspection = CommonHelper.DateToString(dr["DateOfInspection"]);
                    model.TimeOfInspection = Convert.ToString(dr["TimeOfInspection"]);
                    model.DateReported= CommonHelper.DateToString(dr["DateReported"]);
                    model.TimeReported = Convert.ToString(dr["TimeReported"]);
                    model.ReportedTo = Convert.ToInt32(dr["ReportedTo"]);
                    model.AccidentType = Convert.ToString(dr["AccidentType"]);
                    model.DiseasesType= Convert.ToString(dr["DiseasesType"]);
                    model.CreatedBy = Convert.ToInt32(dr["CreatedBy"]);
                }
            }
            return model;
        }
        public int SaveLocationWitness(LocationWitnessdto model)
        {
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("Result",""),
                new SqlParameter("InspectionID",model.InspectionID),
                new SqlParameter("FormID",model.FormID),
                new SqlParameter("CompanyID",model.CompanyID),
                new SqlParameter("AccidentHappendInsidePremises",model.AccidentHappendInsidePremises),
                new SqlParameter("AccidentHappendInsidePremisesDescription",model.AccidentHappendInsidePremisesDescription),
                new SqlParameter("AwarenessAnyWitness",model.AwarenessAnyWitness),
                new SqlParameter("AwarenessWitnessDescription",model.AwarenessWitnessDescription),
                new SqlParameter("AccidentOutSideProvince",model.AccidentOutSideProvince),
                new SqlParameter("AccidentOutSideProvinceDescription",model.AccidentOutSideProvinceDescription),
                new SqlParameter("ResonposibleForAccident",model.ResonposibleForAccident),
                new SqlParameter("ResonposibleDescription",model.ResonposibleDescription),
                new SqlParameter("AwareAnyInjury",model.AwareAnyInjury),
                new SqlParameter("AwareAnyInjuryDescription",model.AwareAnyInjuryDescription),
                new SqlParameter("ConcernClaim",model.ConcernClaim),
                new SqlParameter("CreatedBy",model.CreatedBy)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            return SQL.ExceuteStoreProcedure("SaveAccidentLocationWitnesses", param, true);
        }
        public LocationWitnessdto GetLocationWitnessByInspectionID(int InspectionID)
        {
            LocationWitnessdto model = null; ;
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("InspectionID",InspectionID)
            };
            using (SqlDataReader dr = SQL.GetDataReader("GetAccidentLocationWitnesses", param, true))
            {
                while (dr.Read())
                {
                    model = new LocationWitnessdto();
                    model.InspectionID = Convert.ToInt32(dr["InspectionID"]);
                    model.FormID = Convert.ToInt32(dr["FormID"]);
                    model.CompanyID= Convert.ToInt32(dr["CompanyID"]);
                    model.AccidentHappendInsidePremises = Convert.ToInt32(dr["AccidentHappendInsidePremises"]);
                    model.AccidentHappendInsidePremisesDescription= Convert.ToString(dr["AccidentHappendInsidePremisesDescription"]);
                    model.AccidentOutSideProvince = Convert.ToInt32(dr["AccidentOutSideProvince"]);
                    model.AccidentOutSideProvinceDescription = Convert.ToString(dr["AccidentOutSideProvinceDescription"]);
                    model.AwarenessAnyWitness= Convert.ToInt32(dr["AwarenessAnyWitness"]);
                    model.AwarenessWitnessDescription = Convert.ToString(dr["AwarenessWitnessDescription"]);
                    model.ResonposibleForAccident = Convert.ToInt32(dr["ResonposibleForAccident"]);
                    model.ResonposibleDescription = Convert.ToString(dr["ResonposibleDescription"]);
                    model.AwareAnyInjury = Convert.ToInt32(dr["AwareAnyInjury"]);
                    model.AwareAnyInjuryDescription = Convert.ToString(dr["AwareAnyInjuryDescription"]);
                    model.ConcernClaim= Convert.ToInt32(dr["ConcernClaim"]);
                    model.CreatedBy = Convert.ToInt32(dr["CreatedBy"]);
                }
            }
            return model;
        }
        public int SaveAccidentHealtCare(AccidentHealtCare model)
        {
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("Result",""),
                new SqlParameter("InspectionID",model.InspectionID),
                new SqlParameter("FormID",model.FormID),
                new SqlParameter("CompanyID",model.CompanyID),
                new SqlParameter("ReceiveHealthCareForInjury",model.ReceiveHealthCareForInjury),
                new SqlParameter("HealthCareDate",model.HealthCareDate),
                new SqlParameter("EmployerlearnReceivedHealthCare",model.EmployerlearnReceivedHealthCare),
                new SqlParameter("WorkerTreated",model.WorkerTreated),
                new SqlParameter("AddressOfTreatedPerson",model.AddressOfTreatedPerson),
                new SqlParameter("AwarenessOfIllnessID",model.AwarenessOfIllnessID),
                new SqlParameter("WorkerLostTimeDate",model.WorkerLostTimeDate),
                new SqlParameter("ReturnToWorkDate",model.ReturnToWorkDate),
                new SqlParameter("WorkType",model.WorkType),
                new SqlParameter("ConfirmBy",model.ConfirmBy),
                new SqlParameter("Name",model.Name),
                new SqlParameter("Telephone",model.Telephone),
                new SqlParameter("CreatedBy",model.CreatedBy)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            return SQL.ExceuteStoreProcedure("SaveAccidentHealthCare", param, true);
        }
        public AccidentHealtCare GetAccidentHealtCareByInspectionID(int InspectionID)
        {
            AccidentHealtCare model = null;
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("InspectionID",InspectionID)
            };
            using (SqlDataReader dr = SQL.GetDataReader("GetAccidentHealthCareByID", param, true))
            {
                while (dr.Read())
                {
                    model = new AccidentHealtCare();
                    model.InspectionID = Convert.ToInt32(dr["InspectionID"]);
                    model.FormID = Convert.ToInt32(dr["FormID"]);
                    model.CompanyID = Convert.ToInt32(dr["CompanyID"]);
                    model.ReceiveHealthCareForInjury = Convert.ToInt32(dr["ReceiveHealthCareForInjury"]);
                    model.HealthCareDate = CommonHelper.DateToString(dr["HealthCareDate"]);
                    model.EmployerlearnReceivedHealthCare = CommonHelper.DateToString(dr["EmployerlearnReceivedHealthCare"]);
                    model.WorkerTreated = Convert.ToInt32(dr["WorkerTreated"]);
                    model.AddressOfTreatedPerson = Convert.ToString(dr["AddressOfTreatedPerson"]);
                    model.AwarenessOfIllnessID = Convert.ToInt32(dr["AwarenessOfIllnessID"]);
                    model.WorkerLostTimeDate = CommonHelper.DateToString(dr["WorkerLostTimeDate"]);
                    model.ReturnToWorkDate = CommonHelper.DateToString(dr["ReturnToWorkDate"]);
                    model.WorkType = Convert.ToInt32(dr["WorkType"]);
                    model.ConfirmBy = Convert.ToInt32(dr["ConfirmBy"]);
                    model.Name = Convert.ToString(dr["Name"]);
                    model.Telephone = Convert.ToString(dr["Telephone"]);
                    model.CreatedBy = Convert.ToInt32(dr["CreatedBy"]);
                }
            }
            return model;
        }
        public int SaveReturnWorkDetail(ReturnWorkDetail model)
        {
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("Result",""),
                new SqlParameter("InspectionID",model.InspectionID),
                new SqlParameter("FormID",model.FormID),
                new SqlParameter("CompanyID",model.CompanyID),
                new SqlParameter("WorkLimitationForInjury",model.WorkLimitationForInjury),
                new SqlParameter("ModifiedworkDiscuss",model.ModifiedworkDiscuss),
                new SqlParameter("OfferAnyWork",model.OfferAnyWork),
                new SqlParameter("WorkAccepted",model.WorkAccepted),
                new SqlParameter("ResponsibleForArangingWorker",model.ResponsibleForArangingWorker),
                new SqlParameter("Name",model.Name),
                new SqlParameter("Telephone",model.Telephone),
                new SqlParameter("CreatedBy",model.CreatedBy)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            return SQL.ExceuteStoreProcedure("SaveAccidentReturnWorkDetail", param, true);
        }
        public ReturnWorkDetail GetReturnWorkDetailByID(int InspectionID)
        {
            ReturnWorkDetail model = null;
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("InspectionID",InspectionID)
            };
            using (SqlDataReader dr = SQL.GetDataReader("GetAccidentReturnWorkDetailByID", param, true))
            {
                while (dr.Read())
                {
                    model = new ReturnWorkDetail();
                    model.InspectionID = Convert.ToInt32(dr["InspectionID"]);
                    model.FormID = Convert.ToInt32(dr["FormID"]);
                    model.CompanyID = Convert.ToInt32(dr["CompanyID"]);
                    model.WorkLimitationForInjury = Convert.ToInt32(dr["WorkLimitationForInjury"]);
                    model.ModifiedworkDiscuss = Convert.ToInt32(dr["ModifiedworkDiscuss"]);
                    model.OfferAnyWork = Convert.ToInt32(dr["OfferAnyWork"]);
                    model.WorkAccepted = Convert.ToInt32(dr["WorkAccepted"]);
                    model.ResponsibleForArangingWorker= Convert.ToInt32(dr["ResponsibleForArangingWorker"]);
                    model.Name = Convert.ToString(dr["Name"]);
                    model.Telephone= Convert.ToString(dr["Telephone"]);
                    model.CreatedBy = Convert.ToInt32(dr["CreatedBy"]);
                }
            }
            return model;
        }
        public int SaveAccidentWagesInfo(AccidentWagesInfo model)
        {

            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("Result",""),
                new SqlParameter("InspectionID",model.InspectionID),
                new SqlParameter("FormID",model.FormID),
                new SqlParameter("Federal",model.Federal),
                new SqlParameter("Provincial",model.Provincial),
                new SqlParameter("VactionPayCheck",model.VactionPayCheck),
                new SqlParameter("VactionPayCheckPercentage",model.VactionPayCheckPercentage),
                new SqlParameter("LastWorkDate",model.LastWorkDate),
                new SqlParameter("LastworkTime",model.LastworkTime),
                new SqlParameter("LastDayWorkFrom",model.LastDayWorkFrom),
                new SqlParameter("LastDayWorkTo",model.LastDayWorkTo),
                new SqlParameter("ActualEarning",model.ActualEarning),
                new SqlParameter("NormalEarning",model.NormalEarning),
                new SqlParameter("AdvanceWagePaid",model.AdvanceWagePaid),
                new SqlParameter("FullRegular",model.FullRegular),
                new SqlParameter("Other",model.Other),
                new SqlParameter("Week1FromDate",model.Week1FromDate),
                new SqlParameter("Week2FromDate",model.Week2FromDate),
                new SqlParameter("Week3FromDate",model.Week3FromDate),
                new SqlParameter("Week4FromDate",model.Week4FromDate),
                new SqlParameter("Week1ToDate",model.Week1ToDate),
                new SqlParameter("Week2ToDate",model.Week2ToDate),
                new SqlParameter("Week3ToDate",model.Week3ToDate),
                new SqlParameter("Week4ToDate",model.Week4ToDate),
                new SqlParameter("MandatoryOverTimePay1",model.MandatoryOverTimePay1),
                new SqlParameter("MandatoryOverTimePay2",model.MandatoryOverTimePay2),
                new SqlParameter("MandatoryOverTimePay3",model.MandatoryOverTimePay3),
                new SqlParameter("MandatoryOverTimePay4",model.MandatoryOverTimePay4),
                new SqlParameter("VoluntaryOverTimePay1",model.VoluntaryOverTimePay1),
                new SqlParameter("VoluntaryOverTimePay2",model.VoluntaryOverTimePay2),
                new SqlParameter("VoluntaryOverTimePay3",model.VoluntaryOverTimePay3),
                new SqlParameter("VoluntaryOverTimePay4",model.VoluntaryOverTimePay4),
                new SqlParameter("CompanyID",model.CompanyID),
                new SqlParameter("CreatedBy",model.CreatedBy),
                new SqlParameter("CommissionInfo",CommonHelper.ToDataTable(model.AccidentCommissionInfo))
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            return SQL.ExceuteStoreProcedure("SaveAccidentWagesInfo", param, true);
        }
        public AccidentWagesInfo GetAccidentWagesInfoByID(int InspectionID)
        {
            AccidentWagesInfo model = null;
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("InspectionID",InspectionID)
            };
            DataSet ds = SQL.GetDs("GetAccidentWagesInfoByID", param, true);
            if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
            {
                model = new AccidentWagesInfo();
                model.InspectionID = Convert.ToInt32(ds.Tables[0].Rows[0]["InspectionID"]);
                model.FormID = Convert.ToInt32(ds.Tables[0].Rows[0]["FormID"]);
                model.Federal = Convert.ToString(ds.Tables[0].Rows[0]["Federal"]);
                model.Provincial = Convert.ToString(ds.Tables[0].Rows[0]["Provincial"]);
                model.VactionPayCheck = Convert.ToInt32(ds.Tables[0].Rows[0]["VactionPayCheck"]);
                model.VactionPayCheckPercentage = Convert.ToDecimal(ds.Tables[0].Rows[0]["VactionPayCheckPercentage"]);
                model.LastWorkDate = Convert.ToString(ds.Tables[0].Rows[0]["LastWorkDate"]);
                model.LastworkTime = Convert.ToString(ds.Tables[0].Rows[0]["LastworkTime"]);
                model.LastDayWorkFrom = Convert.ToString(ds.Tables[0].Rows[0]["LastDayWorkFrom"]);
                model.LastDayWorkTo = Convert.ToString(ds.Tables[0].Rows[0]["LastDayWorkTo"]);
                model.ActualEarning = Convert.ToDecimal(ds.Tables[0].Rows[0]["ActualEarning"]);
                model.NormalEarning = Convert.ToDecimal(ds.Tables[0].Rows[0]["NormalEarning"]);
                model.AdvanceWagePaid = Convert.ToInt32(ds.Tables[0].Rows[0]["AdvanceWagePaid"]);
                model.FullRegular = Convert.ToInt32(ds.Tables[0].Rows[0]["FullRegular"]);
                model.Other = Convert.ToString(ds.Tables[0].Rows[0]["Other"]);
                model.Week1FromDate = Convert.ToString(ds.Tables[0].Rows[0]["Week1FromDate"]);
                model.Week2FromDate = Convert.ToString(ds.Tables[0].Rows[0]["Week2FromDate"]);
                model.Week3FromDate = Convert.ToString(ds.Tables[0].Rows[0]["Week3FromDate"]);
                model.Week4FromDate = Convert.ToString(ds.Tables[0].Rows[0]["Week4FromDate"]);
                model.Week1ToDate = Convert.ToString(ds.Tables[0].Rows[0]["Week1ToDate"]);
                model.Week2ToDate = Convert.ToString(ds.Tables[0].Rows[0]["Week2ToDate"]);
                model.Week3ToDate = Convert.ToString(ds.Tables[0].Rows[0]["Week3ToDate"]);
                model.Week4ToDate = Convert.ToString(ds.Tables[0].Rows[0]["Week4ToDate"]);
                model.MandatoryOverTimePay1 = Convert.ToString(ds.Tables[0].Rows[0]["MandatoryOverTimePay1"]);
                model.MandatoryOverTimePay2 = Convert.ToString(ds.Tables[0].Rows[0]["MandatoryOverTimePay2"]);
                model.MandatoryOverTimePay3 = Convert.ToString(ds.Tables[0].Rows[0]["MandatoryOverTimePay3"]);
                model.MandatoryOverTimePay4 = Convert.ToString(ds.Tables[0].Rows[0]["MandatoryOverTimePay4"]);
                model.VoluntaryOverTimePay1 = Convert.ToString(ds.Tables[0].Rows[0]["VoluntaryOverTimePay1"]);
                model.VoluntaryOverTimePay2 = Convert.ToString(ds.Tables[0].Rows[0]["VoluntaryOverTimePay2"]);
                model.VoluntaryOverTimePay3 = Convert.ToString(ds.Tables[0].Rows[0]["VoluntaryOverTimePay3"]);
                model.VoluntaryOverTimePay4 = Convert.ToString(ds.Tables[0].Rows[0]["VoluntaryOverTimePay4"]);
                model.CompanyID = Convert.ToInt32(ds.Tables[0].Rows[0]["CompanyID"]);
                model.CreatedBy = Convert.ToInt32(ds.Tables[0].Rows[0]["CreatedBy"]);
                if (ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                {
                    model.AccidentCommissionInfo = new List<AccidentCommissionInfo>();
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        model.AccidentCommissionInfo.Add(new AccidentCommissionInfo
                        {
                            InspectionID = Convert.ToInt32(dr["InspectionID"]),
                            WeekOneName = Convert.ToString(dr["WeekOneName"]),
                            WeekOneValue = Convert.ToString(dr["WeekOneValue"]),
                            WeekTwoName = Convert.ToString(dr["WeekTwoName"]),
                            WeekTwoValue = Convert.ToString(dr["WeekTwoValue"]),
                            WeekThreeName = Convert.ToString(dr["WeekThreeName"]),
                            WeekThreeValue = Convert.ToString(dr["WeekThreeValue"]),
                            WeekFourName = Convert.ToString(dr["WeekFourName"]),
                            WeekFourValue = Convert.ToString(dr["WeekFourValue"])
                        });
                    }
                }
            }
            return model;
        }
        public int SaveAccidentWorkScheduleInfo(WorkSchedule model)
        {
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("Result",""),
                new SqlParameter("InspectionID",model.InspectionID),
                new SqlParameter("FormID",model.FormID),
                new SqlParameter("CompanyID",model.CompanyID),
                new SqlParameter("SundayHour",model.SundayHour),
                new SqlParameter("ModayHour",model.ModayHour),
                new SqlParameter("TuesdayHour",model.TuesdayHour),
                new SqlParameter("Wednesday",model.Wednesday),
                new SqlParameter("ThrusdayHour",model.ThrusdayHour),
                new SqlParameter("FridayHour",model.FridayHour),
                new SqlParameter("SaturdayHour",model.SaturdayHour),
                new SqlParameter("DayOn",model.DayOn),
                new SqlParameter("DayOff",model.DayOff),
                new SqlParameter("HourPerShift",model.HourPerShift),
                new SqlParameter("NumberOfWeek",model.NumberOfWeek),
                new SqlParameter("Week1FromDate",model.Week1FromDate),
                new SqlParameter("Week1ToDate",model.Week1ToDate),
                new SqlParameter("Week1TotalHourWork",model.Week1TotalHourWork),
                new SqlParameter("Week1TotalShiftWork",model.Week1TotalShiftWork),
                new SqlParameter("Week2FromDate",model.Week2FromDate),
                new SqlParameter("Week2ToDate",model.Week2ToDate),
                new SqlParameter("Week2TotalHourWork",model.Week2TotalHourWork),
                new SqlParameter("Week2TotalShiftWork",model.Week2TotalShiftWork),
                new SqlParameter("Week3FromDate",model.Week3FromDate),
                new SqlParameter("Week3ToDate",model.Week3ToDate),
                new SqlParameter("Week3TotalHourWork",model.Week3TotalHourWork),
                new SqlParameter("Week3TotalShiftWork",model.Week3TotalShiftWork),
                new SqlParameter("Week4FromDate",model.Week4FromDate),
                new SqlParameter("Week4ToDate",model.Week4ToDate),
                new SqlParameter("Week4TotalHourWork",model.Week4TotalHourWork),
                new SqlParameter("Week4TotalShiftWork",model.Week4TotalShiftWork),
                new SqlParameter("CreatedBy",model.CreatedBy)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            return SQL.ExceuteStoreProcedure("SaveAccidentWorkSchedule", param, true);
        }
        public WorkSchedule GetAccidentWorkScheduleByID(int InspectionID)
        {
            WorkSchedule model = null;
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("InspectionID",InspectionID)
            };
            using (SqlDataReader dr = SQL.GetDataReader("GetWorkScheduleByID", param, true))
            {
                while (dr.Read())
                {
                    model = new WorkSchedule();
                    model.InspectionID = Convert.ToInt32(dr["InspectionID"]);
                    model.FormID = Convert.ToInt32(dr["FormID"]);
                    model.SundayHour= Convert.ToString(dr["SundayHour"]);
                    model.ModayHour= Convert.ToString(dr["ModayHour"]);
                    model.TuesdayHour= Convert.ToString(dr["TuesdayHour"]);
                    model.Wednesday = Convert.ToString(dr["Wednesday"]);
                    model.ThrusdayHour= Convert.ToString(dr["ThrusdayHour"]);
                    model.FridayHour = Convert.ToString(dr["FridayHour"]);
                    model.SaturdayHour= Convert.ToString(dr["SaturdayHour"]);
                    model.DayOn = Convert.ToString(dr["DayOn"]);
                    model.DayOff = Convert.ToString(dr["DayOff"]);
                    model.HourPerShift = Convert.ToString(dr["HourPerShift"]);
                    model.NumberOfWeek = Convert.ToString(dr["NumberOfWeek"]);
                    model.Week1FromDate = CommonHelper.DateToString(dr["Week1FromDate"]);
                    model.Week1ToDate = CommonHelper.DateToString(dr["Week1ToDate"]);
                    model.Week1TotalHourWork = Convert.ToString(dr["Week1TotalHourWork"]);
                    model.Week1TotalShiftWork = Convert.ToString(dr["Week1TotalShiftWork"]);
                    model.Week2FromDate = CommonHelper.DateToString(dr["Week2FromDate"]);
                    model.Week2ToDate = CommonHelper.DateToString(dr["Week2ToDate"]);
                    model.Week2TotalHourWork = Convert.ToString(dr["Week2TotalHourWork"]);
                    model.Week2TotalShiftWork = Convert.ToString(dr["Week2TotalShiftWork"]);
                    model.Week3FromDate = CommonHelper.DateToString(dr["Week3FromDate"]);
                    model.Week3ToDate = CommonHelper.DateToString(dr["Week3ToDate"]);
                    model.Week3TotalHourWork = Convert.ToString(dr["Week3TotalHourWork"]);
                    model.Week3TotalShiftWork = Convert.ToString(dr["Week3TotalShiftWork"]);
                    model.Week4FromDate= CommonHelper.DateToString(dr["Week4FromDate"]);
                    model.Week4ToDate = CommonHelper.DateToString(dr["Week4ToDate"]);
                    model.Week4TotalHourWork = Convert.ToString(dr["Week4TotalHourWork"]);
                    model.Week4TotalShiftWork= Convert.ToString(dr["Week4TotalShiftWork"]);
                    model.CompanyID= Convert.ToInt32(dr["CompanyID"]);
                    model.CreatedBy = Convert.ToInt32(dr["CreatedBy"]);
                }
            }
            return model;
        }
        public int SaveAccidentSignOffInfo(AccidentSignOff model)
        {
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("Result",""),
                new SqlParameter("InspectionID",model.InspectionID),
                new SqlParameter("FormID",model.FormID),
                new SqlParameter("CompanyID",model.CompanyID),
                new SqlParameter("Name",model.Name),
                new SqlParameter("Title",model.Title),
                new SqlParameter("Telephone",model.Telephone),
                new SqlParameter("Ext",model.Ext),
                new SqlParameter("SignOffDate",model.SignOffDate),
                new SqlParameter("Signature",model.FilePath),
                new SqlParameter("AdditionalInfo",model.AdditionalInfo),
                new SqlParameter("CreatedBy",model.CreatedBy)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            int returVal = SQL.ExceuteStoreProcedure("SaveAccidentSignOff", param, true);
            if (returVal == 1)
                _commonServices.SaveFileRecord(model.FilePath, model.CompanyID, "HeathSafety", true);
            return returVal;
        }
        public AccidentSignOff GetAccidentSignOffByID(int InspectionID)
        {
            AccidentSignOff model = null;
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("InspectionID",InspectionID)
            };
            using (SqlDataReader dr = SQL.GetDataReader("GetAccidentSignOffByID", param, true))
            {
                while (dr.Read())
                {
                    model = new AccidentSignOff();
                    model.InspectionID = Convert.ToInt32(dr["InspectionID"]);
                    model.FormID = Convert.ToInt32(dr["FormID"]);
                    model.Name = Convert.ToString(dr["Name"]);
                    model.Title = Convert.ToString(dr["Title"]);
                    model.Telephone = Convert.ToString(dr["Telephone"]);
                    model.Ext = Convert.ToString(dr["Ext"]);
                    model.FilePath = Convert.ToString(dr["Signature"]);
                    model.Signature = string.IsNullOrEmpty(dr["Signature"] + "") ? "" : (baseurl + dr["Signature"].ToString());
                    model.SignOffDate = CommonHelper.DateToString(dr["SignOffDate"]);
                    model.AdditionalInfo= Convert.ToString(dr["AdditionalInfo"]);
                    model.CompanyID = Convert.ToInt32(dr["CompanyID"]);
                    model.CreatedBy = Convert.ToInt32(dr["CreatedBy"]);
                }
            }
            return model;
        }
        public InspectionDetails GetAccidentDetailsByID(int CompanyID, int InspectionID)
        {
            InspectionDetails inspectionDetails = new InspectionDetails();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("InspectionID",InspectionID),
                new SqlParameter("CompanyID",CompanyID)
            };
            DataSet ds = SQL.GetDs("GetAccidentDetailsByID", param, true);
            if (ds != null && ds.Tables.Count == 2)
            {
                int pendingTask = 0, completeTask = 0;
                List<Module> modulelist = new List<Module>();
                List<Form> formlist = new List<Form>();
                bool flag = true;
                if (ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                {   
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        formlist.Add(new Form
                        {
                            ID = Convert.ToInt32(dr["FormID"]),
                            FormName = Convert.ToString(dr["FormName"]),
                            Complete = Convert.ToBoolean(dr["Safe"])
                        });
                        pendingTask += Convert.ToBoolean(dr["Safe"]) ? 0 : 1;
                        completeTask += Convert.ToBoolean(dr["Safe"]) ? 1 : 0;
                        if (flag) { flag = Convert.ToBoolean(dr["Safe"]); }
                    }
                    modulelist.Add(new Module
                    {
                        Name = "Gernal Incpection",
                        Complete = flag,
                        FormList = formlist
                    });
                }
                if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    inspectionDetails.ID = Convert.ToInt32(dr["ID"]);
                    inspectionDetails.Name = Convert.ToString(dr["Name"]);
                    inspectionDetails.InspectionID = Convert.ToString(dr["InspectionID"]);
                    inspectionDetails.DateOfInspection = CommonHelper.DateToString(dr["DateOfInspection"]);
                    inspectionDetails.CompleteDate = CommonHelper.DateToString(dr["CompleteDate"]);
                    inspectionDetails.PriorityValue = Convert.ToString(dr["PriorityValue"]);
                    inspectionDetails.PriorityID = Convert.ToInt32(dr["PriorityID"]);
                    inspectionDetails.Description = Convert.ToString(dr["Description"]);
                    inspectionDetails.StatusValue = Convert.ToString(dr["StatusValue"]);
                    inspectionDetails.CompanyID = Convert.ToInt32(dr["CompanyID"]);
                    inspectionDetails.CreatedBy = Convert.ToInt32(dr["CreatedBy"]);
                    inspectionDetails.PendingTask = pendingTask;
                    inspectionDetails.CompletedTask = completeTask;
                    inspectionDetails.PendingTaskPercentage = ((pendingTask / ((float)(pendingTask + completeTask))) * 100.0).ToString("0.00");
                    inspectionDetails.CompletedTaskPercentage = ((completeTask / ((float)(pendingTask + completeTask))) * 100.0).ToString("0.00");
                    inspectionDetails.AllModule = modulelist;
                    inspectionDetails.UnsafeModule.Add(new Module
                    {
                        Name = "Gernal Incpection",
                        Complete = flag,
                        FormList = formlist.Where(x => !x.Complete).ToList()
                    });
                    inspectionDetails.SafeModule.Add(new Module
                    {
                        Name = "Gernal Incpection",
                        Complete = flag,
                        FormList = formlist.Where(x => x.Complete).ToList()
                    });
                }
            }
            return inspectionDetails;
        }
        #endregion

        #region JHSC Meeting
        public int SaveJHSCMeeting(JHSCMeeting model)
        {
            var dtemployee = CommonHelper.ToDataTable(model.EmployeeList);
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("Result",""),
                new SqlParameter("ID",model.ID),
                new SqlParameter("Name",model.Name),
                new SqlParameter("ScheduleDate",model.ScheduleDate),
                new SqlParameter("CreatedBy",model.CreatedBy),
                new SqlParameter("CompanyID",model.CompanyID),
                new SqlParameter("Employeelist",dtemployee)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            int result = SQL.ExceuteStoreProcedure("SaveJHSCMeetings", param, true);
            return result;
        }
        public List<JHSCMeeting> GetJHSCMeetingbyCompanyID(int CompanyID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",CompanyID)
            };
            return GetJHSCMeetingList(param);
        }
        public JHSCMeeting GetJHSCMeetingbyID(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            return GetJHSCMeetingList(param).FirstOrDefault();
        }
        private List<JHSCMeeting> GetJHSCMeetingList(SqlParameter[] param)
        {
            List<JHSCMeeting> model = new List<JHSCMeeting>();
            DataSet ds = SQL.GetDs("GetJHSCMeetingList", param, true);

            if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
            {
                List<InsParticipantWithName> employeeList = null;
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    employeeList = new List<InsParticipantWithName>();
                    if (ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                    {
                        DataRow[] result = ds.Tables[1].Select("ID = '" + Convert.ToInt32(dr["ID"]) + "'");
                        foreach (DataRow dr1 in result)
                        {
                            employeeList.Add(new InsParticipantWithName
                            {
                                Email = Convert.ToString(dr1["Email"]),
                                EmployeeID = Convert.ToInt32(dr1["EmployeeID"] + ""),
                                Name = Convert.ToString(dr1["Name"] + "")
                            });
                        }
                    }
                    model.Add(new JHSCMeeting
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Name = dr["Name"] + "",
                        ScheduleDate = CommonHelper.DateToString(dr["ScheduleDate"]),
                        CompanyID = Convert.ToInt32(dr["CompanyID"]),
                        CreatedBy = Convert.ToInt32(dr["CreatedBy"]),
                        StatusValues = dr["Status"] + "",
                        EmployeeList = employeeList
                    });
                }
            }
            return model;
        }
        public void DeleteJHSCMeeting(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("DeleteJHSCMEeeting", param, false);
        }
        public void CompleteJHSCMeeting(int JHSCMeetingID, int CreatedBy)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",JHSCMeetingID),
                new SqlParameter("UpdatedBy",CreatedBy)
            };
            SQL.ExceuteStoreProcedure("CompleteJHSCMeeting", param, false);
        }
        public JHSCMeetingUnsafeQuestion GetJHSCUnsafeQuestion(int CompanyID,int JHSCMeetingID = 0)
        {
            JHSCMeetingUnsafeQuestion obj = new JHSCMeetingUnsafeQuestion();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",CompanyID),
                new SqlParameter("JHSCMeetingID",JHSCMeetingID)
            };
            DataTable dt = SQL.GetDt("GetJHSCUnSafeQuestion",param,true);
            var objList = CommonHelper.ToList<UnsafeQuestion>(dt);
            var dstInspection = objList.Where(x=>x.InspectionType=="New").Select(r => r.InspectionID).Distinct().ToList();
            foreach (var inspectionID in dstInspection)
            {
                var inspectionQuestion = objList.Where(r => r.InspectionID == inspectionID).ToList();
                obj.CurrentMeetingQuestion.Add(new JHSCInspection
                {
                    Name = inspectionQuestion.FirstOrDefault().InspectionName,
                    UnsafeQuestionList = BindUnsafeQuestion(inspectionQuestion)
                });
            }
            var PendingInspection = objList.Where(x => x.InspectionType == "Pending");
            if (PendingInspection != null)
            {
                var dstInspectionPending = PendingInspection.Select(r => r.InspectionID).Distinct().ToList();
                List<JHSCInspection> jhscPendingInspection = new List<JHSCInspection>();
                foreach (var inspectionID in dstInspectionPending)
                {
                    var inspectionQuestion = objList.Where(r => r.InspectionID == inspectionID).ToList();
                    obj.PendingMeetingQuestion.Add(new JHSCInspection
                    {
                        Name = inspectionQuestion.FirstOrDefault().InspectionName,
                        UnsafeQuestionList = BindUnsafeQuestion(inspectionQuestion)
                    });
                }
            }
            return obj;
        }
        private static List<InspectionUnsafeQuestion> BindUnsafeQuestion(List<UnsafeQuestion> inspectionQuestion)
        {
            List<InspectionUnsafeQuestion> unsafeQuestion = new List<InspectionUnsafeQuestion>();
            foreach (var ques in inspectionQuestion)
            {
                unsafeQuestion.Add(new InspectionUnsafeQuestion
                {
                    JHSCmeetingID = ques.JHSCMeetingID,
                    InspectionID = ques.InspectionID,
                    CategoryID = ques.CategoryID,
                    FormID = ques.FormID,
                    QuestionID = ques.QuestionID,
                    Question = ques.Question,
                    AnswerID = ques.AnswerID,
                    Location = ques.Location,
                    Description = ques.Description,
                    Action = ques.Action,
                    HAZARDClass = ques.HAZARDClass,
                    CompletionDate = CommonHelper.DateToString(ques.CompletionDate),
                    ResponsiblePersionID = ques.ResponsiblePersionID,
                });
            }
            return unsafeQuestion;
        }
        public void SaveJHSCUnsafeQuestion(List<InspectionUnsafeQuestion> model, int CompanyID)
        {
            var dt = CommonHelper.ToDataTable(model);
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("CompanyID",CompanyID),
                new SqlParameter("InspectionUnsafeQuestion",dt)
            };
            SQL.ExceuteStoreProcedure("SaveJHSCUnsafeQuestion ", param, false);
        }
        public DataSet GetHealthSafetyWSIBDetails(int ID)
        {
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("InspectionID",ID)
            };
            return SQL.GetDs("GetHealthSafetyWSIBDetails ", param, true);
        }
        public void ShareInspectionWithjHSCMember(List<InsParticipant> model, int CompanyID, int InspectionID)
        {
            DataTable dtemployee = CommonHelper.ToDataTable(model);
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",CompanyID),
                new SqlParameter("InspectionID",InspectionID),
                new SqlParameter("dtEmployee",dtemployee)
            };
            SQL.ExceuteStoreProcedure("SaveInspectionWithjHSCMember", param, false);
        }
        public void ShareInspectionWithjHSCMeeting(int CompanyID, int InspectionID,int JHSCMeetingID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",CompanyID),
                new SqlParameter("InspectionID",InspectionID),
                new SqlParameter("JHSCMeetingID",JHSCMeetingID)
            };
            SQL.ExceuteStoreProcedure("SaveInspectionWithjHSCMeeting", param, false);
        }
        #endregion

        #region Dashboard API
        public Dashboard GetDashboardDetails(int CompanyID)
        {
            Dashboard obj = new Dashboard();
            Inspectiondto model = new Inspectiondto { CompanyID = CompanyID };
            var objInspection = GetInspectionDetails(model);
            obj.IncidentList = objInspection.Where(x => x.Type == "Incident").Take(10).ToList();
            obj.AccidentList = objInspection.Where(x => x.Type == "Accident").Take(10).ToList();
            var healthsafety = objInspection.Where(x => x.Type == "HealthSafety").ToList();
            if (healthsafety != null && healthsafety.Count > 0)
            {
                obj.TotalHealthSafety = healthsafety.Count;
                obj.PendingHealthSafety = healthsafety.Where(x => x.Status == 1).Count();
            }
            var vehicle = objInspection.Where(x => x.Type == "Vehicle").ToList();
            if (vehicle != null && vehicle.Count > 0)
            {
                obj.TotalVehicle = vehicle.Count;
                obj.PendingVehicle = vehicle.Where(x => x.Status == 1).Count();
            }
            var LiftingVehicle = objInspection.Where(x => x.Type == "LiftingVehicle").ToList();
            if (vehicle != null && vehicle.Count > 0)
            {
                obj.TotalLiftingVehicle = LiftingVehicle.Count;
                obj.PendingLiftingVehicle = LiftingVehicle.Where(x => x.Status == 1).Count();
            }
            var Machinery = objInspection.Where(x => x.Type == "Machinery").ToList();
            if (vehicle != null && vehicle.Count > 0)
            {
                obj.TotalMachinery = Machinery.Count;
                obj.PendingMachinery = Machinery.Where(x => x.Status == 1).Count();
            }
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID", CompanyID)
            };
            var ds = SQL.GetDs("GetHealthSafetyDashboard", param, true);
            obj.EmployeeCount = Convert.ToInt32(ds.Tables[0].Rows[0]["EmployeeCount"]);
            foreach (DataRow dr in ds.Tables[1].Rows)
            {
                obj.JHSCMeetingList.Add(new JHSCMeeting {
                    ID = Convert.ToInt32(dr["ID"]),
                    Name = dr["Name"] + "",
                    ScheduleDate = CommonHelper.DateToString(dr["ScheduleDate"]),
                    CompanyID = Convert.ToInt32(dr["CompanyID"]),
                    CreatedBy = Convert.ToInt32(dr["CreatedBy"]),
                    StatusValues = dr["Status"] + ""
                });
                obj.TotalMeetingCompleted += (dr["Status"].ToString() == "Complete" ? 1 : 0);
            }
            return obj;
        }
        #endregion
    }
}
