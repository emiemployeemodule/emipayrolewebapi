﻿using EMIPayrolData;
using EMIPayrolModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMIPayrolServices
{
   public class UserServices:IUserServices
    {
        public User Login(login model)
        {
            User result =null;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("Email",model.Email),
                new SqlParameter("Password",CommonHelper.Encrypt(model.Password))
            };
            using (SqlDataReader dr = SQL.GetDataReader("loginUser", param, true))
            {
                while (dr.Read())
                {
                    result = new User
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        FirstName = Convert.ToString(dr["FirstName"]),
                        LastName = Convert.ToString(dr["LastName"]),
                        Email = Convert.ToString(dr["Email"]),
                        Password = CommonHelper.Decrypt(dr["Password"].ToString()),
                    };
                }
            }
            return result;
        }
    }
}
