﻿using EMIPayrolData;
using EMIPayrolModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMIPayrolServices
{
    public class CommonServices : ICommonServices
    {
        #region Country,State ,city
        public List<Country> getCounteryList()
        {
            List<Country> model = new List<Country>();
            model.Add(new Country
            {
                Name = "Select Country"
            });
            using (SqlDataReader dr = SQL.GetDataReader("FetchCountryList", true))
            {
                while (dr.Read())
                {
                    model.Add(new Country
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Name = dr["Name"].ToString()
                    });
                }
            }
            return model;
        }
        public int SaveCountry(Country model)
        {
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("ID",model.ID),
                new SqlParameter("Name",model.Name)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            return SQL.ExceuteStoreProcedure("SaveCountry", param, true);
        }
        public List<State> getStateList(int CountryID)
        {
            List<State> model = new List<State>();
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("CountryID", CountryID) };
            model.Add(new State
            {
                Name = "Select State"
            });
            using (SqlDataReader dr = SQL.GetDataReader("FetchStatebyCountryID", param, true))
            {
                while (dr.Read())
                {
                    model.Add(new State
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Name = dr["Name"].ToString(),
                        CountryID = Convert.ToInt32(dr["CountryID"])
                    });
                }
            }
            return model;
        }
        public int SaveState(State model)
        {
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("ID",model.ID),
                new SqlParameter("Name",model.Name),
                new SqlParameter("CountryID",model.CountryID)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            return SQL.ExceuteStoreProcedure("SaveState", param, true);
        }
        public List<City> getCityByStateID(int StateID)
        {
            List<City> model = new List<City>();
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("StateID", StateID) };
            model.Add(new City
            {
                Name = "Select City"
            });
            using (SqlDataReader dr = SQL.GetDataReader("FetchCitybyStateID", param, true))
            {
                while (dr.Read())
                {
                    model.Add(new City
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Name = dr["Name"].ToString(),
                        StateID = Convert.ToInt32(dr["StateID"])
                    });
                }
            }
            return model;
        }
        public int SaveCIty(City model)
        {
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("ID",model.ID),
                new SqlParameter("Name",model.Name),
                new SqlParameter("StateID",model.StateID)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            return SQL.ExceuteStoreProcedure("SaveCity", param, true);
        }
        #endregion

        #region Company department
        public int SaveDepartment(Department Department)
        {
            var returnVal = 0;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("@Result",""),
                new SqlParameter("ID",Department.ID),
                new SqlParameter("Name",Department.Name),
                new SqlParameter("IndustryID",Department.IndustryID)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            returnVal = SQL.ExceuteStoreProcedure("SaveUpdateDepartment", param, true);
            return returnVal;
        }
        public List<Department> getDepartment(int IndustryID=0, int ID = 0)
        {
            List<Department> DepartmentList = new List<Department>();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("IndustryID",IndustryID),
                new SqlParameter("ID",ID)
            };
            using (SqlDataReader dr = SQL.GetDataReader("GetDepartment", param, true))
            {
                while (dr.Read())
                {
                    DepartmentList.Add(new Department
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Name = dr["Name"].ToString(),
                        IndustryID = Convert.ToInt32(dr["IndustryID"])
                    });
                }
            }
            return DepartmentList;
        }

        public void DeleteDepartment(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("DeleteDepartment", param, false);
        }
        #endregion Copany Department

        #region Role
        public int SaveUpdateRoles(Role Roles)
        {
            var returnVal = 0;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("@Result",""),
                new SqlParameter("ID",Roles.ID),
                new SqlParameter("Name",Roles.Name),
                new SqlParameter("IndustryID",Roles.IndustryID)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            returnVal = SQL.ExceuteStoreProcedure("SaveUpdateRole", param, true);
            return returnVal;
        }
        public List<Role> getRoles(int IndustryID=0,int ID = 0)
        {
            List<Role> RolesList = new List<Role>();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID),
                new SqlParameter("IndustryID",IndustryID)
            };
            using (SqlDataReader dr = SQL.GetDataReader("GetRoles", param, true))
            {
                while (dr.Read())
                {
                    RolesList.Add(new Role
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Name = dr["Name"].ToString(),
                        IndustryID= Convert.ToInt32(dr["IndustryID"])
                    });
                }
            }
            return RolesList;
        }
        public void DeleteRole(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("DeleteRoles", param, false);
        }
        #endregion Roles

        #region AppModule
        public int SaveUpdateAppModule(AppModule AppModule)
        {
            var returnVal = 0;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("@Result",""),
                new SqlParameter("ID",AppModule.ID),
                new SqlParameter("Name",AppModule.Name)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            returnVal = SQL.ExceuteStoreProcedure("SaveUpdateAppModule", param, true);
            return returnVal;
        }
        public List<AppModule> getAppModule(int ID = 0)
        {
            List<AppModule> AppModuleList = new List<AppModule>();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            using (SqlDataReader dr = SQL.GetDataReader("GetAppModule", param, true))
            {
                while (dr.Read())
                {
                    AppModuleList.Add(new AppModule
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Name = dr["Name"].ToString()
                    });
                }
            }
            return AppModuleList;
        }
        public void DeleteAppModule(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("DeleteAppModule", param, false);
        }
        #endregion AppModule

        #region Industry
        public int SaveUpdateIndustry(Industry Industry)
        {
            var returnVal = 0;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("@Result",""),
                new SqlParameter("ID",Industry.ID),
                new SqlParameter("Name",Industry.Name)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            returnVal = SQL.ExceuteStoreProcedure("SaveUpdateIndustry", param, true);
            return returnVal;
        }
        public List<Industry> getIndustry(int ID = 0,int CompanyID=0)
        {
            List<Industry> IndustryList = new List<Industry>();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID),
                new SqlParameter("CompanyID",CompanyID)
            };
            using (SqlDataReader dr = SQL.GetDataReader("GetIndustry", param, true))
            {
                while (dr.Read())
                {
                    IndustryList.Add(new Industry
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Name = dr["Name"].ToString(),
                        Selected = Convert.ToBoolean(dr["Selected"])
                    });
                }
            }
            return IndustryList;
        }
        public void DeleteIndustry(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("DeleteIndustry", param, false);
        }
        #endregion Industry

        #region EmailSetting
        public int SaveUpdateEmailSetting(EmailSetting EmailSetting)
        {
            var returnVal = 0;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("@Result",""),
                new SqlParameter("ID",EmailSetting.ID),
            new SqlParameter("EmailFrom",EmailSetting.EmailFrom),
            new SqlParameter("EnableSSL",EmailSetting.EnableSSL),
            new SqlParameter("DisplayName",EmailSetting.DisplayName),
            new SqlParameter("CompanyID",EmailSetting.CompanyID),
            new SqlParameter("SMTPAuthenticationDomain",EmailSetting.SMTPAuthenticationDomain),
            new SqlParameter("SMTPHOST",EmailSetting.SMTPHOST),
            new SqlParameter("SMTPPASSWORD",EmailSetting.SMTPPASSWORD),
            new SqlParameter("SMTPPORT",EmailSetting.SMTPPORT),
            new SqlParameter("SMTPSecurity",EmailSetting.SMTPSecurity),
            new SqlParameter("SMTPUSER",EmailSetting.SMTPUSER)
        };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            returnVal = SQL.ExceuteStoreProcedure("SaveUpdateCompanyEmailSettings", param, true);
            return returnVal;
        }
        public EmailSetting getEmailSetting(int CompanyID)
        {
            EmailSetting emailSetting = null;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",CompanyID)
            };
            using (SqlDataReader dr = SQL.GetDataReader("GetEmailSetting", param, true))
            {
                while (dr.Read())
                {
                    emailSetting = new EmailSetting();
                    emailSetting.ID = Convert.ToInt32(dr["ID"]);
                    emailSetting.EmailFrom = dr["EmailFrom"].ToString();
                    emailSetting.EnableSSL = Convert.ToBoolean(dr["EnableSSL"]);
                    emailSetting.DisplayName = dr["DisplayName"].ToString();
                    emailSetting.CompanyID = Convert.ToInt32(dr["CompanyID"]);
                    emailSetting.SMTPAuthenticationDomain = dr["SMTPAuthenticationDomain"].ToString();
                    emailSetting.SMTPHOST = dr["SMTPHOST"].ToString();
                    emailSetting.SMTPPASSWORD = dr["SMTPPASSWORD"].ToString();
                    emailSetting.SMTPPORT = Convert.ToInt32(dr["SMTPPORT"]);
                    emailSetting.SMTPSecurity = dr["SMTPSecurity"].ToString();
                    emailSetting.SMTPUSER = dr["SMTPUSER"].ToString();
                }
            }
            return emailSetting;
        }
        #endregion EmailSetting

        #region SalarySetting
        public int SaveUpdateSalarySetting(SalarySetting SalarySetting)
        {
            var returnVal = 0;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("@Result",""),
                new SqlParameter("ID",SalarySetting.ID),
            new SqlParameter("DA",SalarySetting.DA),
            new SqlParameter("HRA",SalarySetting.HRA),
            new SqlParameter("PFEmployeeShare",SalarySetting.PFEmployeeShare),
            new SqlParameter("PFOrganizationShare",SalarySetting.PFOrganizationShare),
            new SqlParameter("ESIEmployeeShare",SalarySetting.ESIEmployeeShare),
            new SqlParameter("ESIOrganizationShare",SalarySetting.ESIOrganizationShare),
            new SqlParameter("CompanyID",SalarySetting.CompanyID)
        };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            returnVal = SQL.ExceuteStoreProcedure("SaveUpdateSalarySetting", param, true);
            return returnVal;
        }
        public SalarySetting getSalarySetting(int CompanyID)
        {
            SalarySetting SalarySetting = null;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",CompanyID)
            };
            using (SqlDataReader dr = SQL.GetDataReader("GetSalarySetting", param, true))
            {
                while (dr.Read())
                {
                    SalarySetting = new SalarySetting();
                    SalarySetting.ID = Convert.ToInt32(dr["ID"]);
                    SalarySetting.DA = Convert.ToDecimal(dr["DA"]);
                    SalarySetting.HRA = Convert.ToDecimal(dr["HRA"]);
                    SalarySetting.PFEmployeeShare= Convert.ToDecimal(dr["PFEmployeeShare"]);
                    SalarySetting.PFOrganizationShare = Convert.ToDecimal(dr["PFOrganizationShare"]);
                    SalarySetting.ESIEmployeeShare = Convert.ToDecimal(dr["ESIEmployeeShare"]);
                    SalarySetting.ESIOrganizationShare = Convert.ToDecimal(dr["ESIOrganizationShare"]);
                    SalarySetting.CompanyID = Convert.ToInt32(dr["CompanyID"]);
                }
            }
            return SalarySetting;
        }
        #endregion SalarySetting

        #region AdditionalRequirment
        public int SaveUpdateAdditionalRequirment(AdditionalRequirment AdditionalRequirment)
        {
            var returnVal = 0;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("@Result",""),
                new SqlParameter("ID",AdditionalRequirment.ID),
                new SqlParameter("Name",AdditionalRequirment.Name)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            returnVal = SQL.ExceuteStoreProcedure("SaveUpdateAdditionalRequirment", param, true);
            return returnVal;
        }
        public List<AdditionalRequirment> getAdditionalRequirment(int ID = 0)
        {
            List<AdditionalRequirment> AdditionalRequirmentList = new List<AdditionalRequirment>();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            using (SqlDataReader dr = SQL.GetDataReader("GetAdditionalRequirment", param, true))
            {
                while (dr.Read())
                {
                    AdditionalRequirmentList.Add(new AdditionalRequirment
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Name = dr["Name"].ToString()
                    });
                }
            }
            return AdditionalRequirmentList;
        }
        public void DeleteAdditionalRequirment(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("DeleteAdditionalRequirment", param, false);
        }
        #endregion AdditionalRequirment

        #region IndustryQuestion
        public int SaveUpdateIndustryQuestion(IndustryQuestion IndustryQuestion)
        {
            var returnVal = 0;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("@Result",""),
                new SqlParameter("ID",IndustryQuestion.QuestionID),
                new SqlParameter("Question",IndustryQuestion.Question),
                new SqlParameter("IndustryID",IndustryQuestion.IndustryID),
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            returnVal = SQL.ExceuteStoreProcedure("SaveUpdateIndustryQuestion", param, true);
            return returnVal;
        }
        public List<IndustryQuestion> getIndustryQuestion(int ID = 0,int IndustryID=0,int CompanyID=0)
        {
            List<IndustryQuestion> IndustryQuestionList = new List<IndustryQuestion>();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID),
                new SqlParameter("IndustryID",IndustryID),
                new SqlParameter("CompanyID",CompanyID)
            };
            using (SqlDataReader dr = SQL.GetDataReader("GetIndustryQuestion", param, true))
            {
                while (dr.Read())
                {
                    IndustryQuestionList.Add(new IndustryQuestion
                    {
                        QuestionID = Convert.ToInt32(dr["ID"]),
                        Question = dr["Question"].ToString(),
                        Answer = dr["Answer"].ToString(),
                        IndustryID = Convert.ToInt32(dr["IndustryID"]),
                    });
                }
            }
            return IndustryQuestionList;
        }
        public void DeleteIndustryQuestion(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("DeleteIndustryQuestion", param, false);
        }
        #endregion IndustryQuestion

        #region leave Type
        public List<LeaveType> getLeaveType(int CompanyID)
        {
            List<LeaveType> leaveType = new List<LeaveType>();
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("CompanyID", CompanyID) };
            using (SqlDataReader dr = SQL.GetDataReader("GetLeaveType",param, true))
            {
                while (dr.Read())
                {
                    leaveType.Add(new LeaveType
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Name = dr["Name"].ToString(),
                        CompanyID = Convert.ToInt32(dr["CompanyID"]),
                        NoOfDays = Convert.ToInt32(dr["NoOfDays"]),
                        IsActive = Convert.ToInt32(dr["IsActive"])
                    });
                }
            }
            return leaveType;
        }
        public int SaveLeaveType(LeaveType model)
        {
            var returnVal = 0;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("Result",""),
                new SqlParameter("ID",model.ID),
                new SqlParameter("Name",model.Name),
                new SqlParameter("NoOfDays ",model.NoOfDays),
                new SqlParameter("CompanyID",model.CompanyID),
                new SqlParameter("IsActive",model.IsActive)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            returnVal = SQL.ExceuteStoreProcedure("SaveUpdateLeaveType", param, true);
            return returnVal;
        }
        public void UpdateLeaevTypeStatus(LeaveType model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",model.ID),
                new SqlParameter("IsActive",model.IsActive)
            };
            SQL.ExceuteStoreProcedure("UpdateLeaveTypeStatus", param, false);
        }
        public void DeleteLeaveType(int ID)
        {

            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("DeleteLeaveType", param, false);
        }
        public List<LeaveStatus> getLeaveStatus()
        {
            List<LeaveStatus> leaveStatus = new List<LeaveStatus>();

            using (SqlDataReader dr = SQL.GetDataReader("GetLeaveStatus", true))
            {
                while (dr.Read())
                {
                    leaveStatus.Add(new LeaveStatus
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Name = dr["Name"].ToString()
                    });
                }
            }
            return leaveStatus;
        }
        #endregion leaveType

        #region Shift
        public int SaveUpdateShift(Shift shift)
        {
            var returnVal = 0;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("@Result",""),
                new SqlParameter("ID",shift.ID),
                new SqlParameter("Name",shift.Name)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            returnVal = SQL.ExceuteStoreProcedure("SaveUpdateShift", param, true);
            return returnVal;
        }
        public List<Shift> GetShift(int ID = 0)
        {
            List<Shift> lstShift = new List<Shift>();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            return GetShiftList(param);
        }
        public Shift GetShiftByID(int ID)
        {
            List<Shift> lstShift = new List<Shift>();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            return GetShiftList(param).FirstOrDefault();
        }
        private List<Shift> GetShiftList(SqlParameter[] param)
        {
            List<Shift> lstShift = new List<Shift>();
            using (SqlDataReader dr = SQL.GetDataReader("GetShift", param, true))
            {
                while (dr.Read())
                {
                    lstShift.Add(new Shift
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Name = dr["Name"].ToString()
                    });
                }
            }
            return lstShift;
        }
        public void DeleteShift(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("DeleteShift", param, false);
        }
        #endregion

        public void SaveFileRecord(string fIlePath, int ID, string foldername,bool IsCompanyRecord=false)
        {
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("filePath",fIlePath),
                new SqlParameter("ID",ID),
                new SqlParameter("foldername",foldername),
                new SqlParameter("IsCompanyRecord",IsCompanyRecord)
            };
            SQL.ExceuteStoreProcedure("SaveFileRecord", param, false);
        }

        /// <summary>
        /// Check login credtional with Decripted password
        /// </summary>
        /// <returns></returns>

        public List<Tuple<string, string>> getUserLoginCredtional()
        {
            List<Tuple<string, string>> list = new List<Tuple<string, string>>();
            using (SqlDataReader dr = SQL.GetDataReader("select Email,Password from employee where isdeleted=0", false))
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        list.Add(new Tuple<string, string>(dr["Email"].ToString(),CommonHelper.Decrypt(dr["Password"].ToString())));
                    }
                }
            }
            return list;
        }
        public List<Tuple<int, string, string, string>> getCompanyLoginCredtional()
        {
            List<Tuple<int, string, string, string>> list = new List<Tuple<int, string, string, string>>();
            using (SqlDataReader dr = SQL.GetDataReader("select ID,CompanyName,Email,Password from COmpany where isdeleted=0", false))
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        list.Add(new Tuple<int,string,string, string>(Convert.ToInt16(dr["ID"]), dr["CompanyName"].ToString(),dr["Email"].ToString(), CommonHelper.Decrypt(dr["Password"].ToString())));
                    }
                }
            }
            return list;
        }
        public List<EmployeeEmail> getEmployeeEmail(int CompanyID)
        {
            List<EmployeeEmail> employeeList = new List<EmployeeEmail>();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",CompanyID)
            };
            string SQLQuery = @"select ID,FirstName+' '+ISNULL(LastName,'') AS Name,Email from Employee 
                            WHERE Isdeleted=0 AND Status<>16 AND CompanyID=@CompanyID";
            using (SqlDataReader dr = SQL.GetDataReader(SQLQuery, param, false))
            {
                while (dr.Read())
                {
                    employeeList.Add(new EmployeeEmail
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Name = dr["Name"].ToString(),
                        Email = dr["Email"].ToString()
                    });
                }
            }
            return employeeList;
        }
    }
}
