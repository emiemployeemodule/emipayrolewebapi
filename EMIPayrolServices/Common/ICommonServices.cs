﻿using EMIPayrolModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMIPayrolServices
{
    public interface ICommonServices
    {
        #region Country,State City
        List<Country> getCounteryList();
        int SaveCountry(Country model);
        List<State> getStateList(int CountryID);
        int SaveState(State model);
        List<City> getCityByStateID(int StateID);
        int SaveCIty(City model);
        #endregion

        #region Department
        int SaveDepartment(Department companyOrgainsation);
        List<Department> getDepartment(int IndustryID, int ID = 0);
        void DeleteDepartment(int ID);
        int SaveUpdateRoles(Role Roles);
        #endregion

        #region Roles
        List<Role> getRoles(int IndustryID=0,int ID = 0);
        void DeleteRole(int ID);
        #endregion

        #region AppModule
        int SaveUpdateAppModule(AppModule AppModule);
        List<AppModule> getAppModule(int ID = 0);
        void DeleteAppModule(int ID);
        #endregion

        #region Industry
        int SaveUpdateIndustry(Industry Industry);
        List<Industry> getIndustry(int ID = 0,int CompanyID=0);
        void DeleteIndustry(int ID);
        #endregion

        #region EmialSettings
        EmailSetting getEmailSetting(int CompanyID);
        int SaveUpdateEmailSetting(EmailSetting EmailSetting);
        #endregion

        #region Salary Settings
        SalarySetting getSalarySetting(int CompanyID);
        int SaveUpdateSalarySetting(SalarySetting SalarySetting);
        #endregion

        #region AdditionalRequirment
        int SaveUpdateAdditionalRequirment(AdditionalRequirment AdditionalRequirment);
        List<AdditionalRequirment> getAdditionalRequirment(int ID = 0);
        void DeleteAdditionalRequirment(int ID);
        #endregion

        #region Industry Question
        int SaveUpdateIndustryQuestion(IndustryQuestion IndustryQuestion);
        List<IndustryQuestion> getIndustryQuestion(int ID = 0, int IndustryID = 0,int CompanyID=0);
        void DeleteIndustryQuestion(int ID);
        #endregion

        #region Leaver type
        List<LeaveType> getLeaveType(int CompanyID);
        int SaveLeaveType(LeaveType model);
        List<LeaveStatus> getLeaveStatus();
        void UpdateLeaevTypeStatus(LeaveType model);
        void DeleteLeaveType(int ID);
        #endregion

        #region Shift
        int SaveUpdateShift(Shift shift);        
        List<Shift> GetShift(int ID = 0);
        Shift GetShiftByID(int ID);
        void DeleteShift(int ID);
        #endregion

        void SaveFileRecord(string fIlePath, int ID, string foldername, bool IsCompanyRecord = false);
        List<Tuple<string, string>> getUserLoginCredtional();
        List<Tuple<int,string,string, string>> getCompanyLoginCredtional();
        List<EmployeeEmail> getEmployeeEmail(int CompanyID);
    }
}
