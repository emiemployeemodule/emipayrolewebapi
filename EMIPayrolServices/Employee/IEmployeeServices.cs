﻿using EMIPayrolModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EMIPayrolServices
{
    public interface IEmployeeServices
    {
        #region employee
        int SaveEmployeeDetails(Employee employee);
        List<Employee> getEmployeeByCompanyID(int companyID);
        Employee getEmployeeByID(int ID);
        List<Employee> GetEmployeeByFilter(EmployeSearchRequest searchRequest);
        void DeleteEmployeeByID(int ID);
        int EmployeeLogin(CommonClass model);
        List<Status>GetStatusList();
        void UpdateEmployeeStatus(EmployeeStatus model);
        List<EmployeeStatus> GetStatusHistory(int EmployeeID);
        List<ReportedUser> getReportedUser(int CompanyID);
        
        #endregion

        #region Leave Request
        int SaveUpdateLeaevRequest(LeaveRequest leaveRequest);
        void UpdateLeaevRequestStatus(int ID, int LeaveStatusID);
        List<LeaveRequest> GetLeaevRequestByCompanyID(LeaveSearch model);
        LeaveRequest GetLeaevRequestByID(int ID);
        void DeleteLeaveRequest(int ID);
        #endregion

        #region Designation
        int SaveUpdateDesignation(Designation designation);
        List<Designation> GetDesignationByCompanyID(int CompnayID);
        List<Designation> GetDesignationByDepartment(int CompnayID,int Department);
        Designation GetDesignationByID(int ID);
        void DeleteDesignation(int ID);
        void UpdateEmployeeDesignation(EmployeRole employeerole);
        #endregion

        #region Bulk upload & GerateEmployeeID
        BulkResult BulkUpload(DataTable dtRecord, int CompanyID);
        string GerateEmployeeID(int CompanyID);
        void UpdateEployeePhoto(int employeeID, string Filename);
        #endregion

        #region emrgency info
        void SaveEmergenyInfo(EmergenyInfo model);
        EmergenyInfo GetEmergenyInfo(int EmployeeID);
        #endregion

        #region position info
        int SavePositionInfo(PositionInfo model);
        List<PositionInfo> GetPositionInfo(int EmployeeID);
        PositionInfo GetPositionInfoByID(int ID);
        void DeletePositionInfo(int ID);
        #endregion

        #region EmpWagesInfo
        List<PayFrequency> GetPayFrequencyList();
        void SaveEmpWagesInfo(EmpWagesInfo model);
        List<EmpWagesInfo> GetEmpWagesInfo(int EmployeeID);
        EmpWagesInfo GetEmpWagesInfoByID(int ID);
        void DeleteEmpWagesInfo(int ID);
        #endregion

        #region Dependent info
        int SaveDependentInfo(DependentInfo model);
        List<DependentInfo> GetDependentInfo(int EmployeeID);
        DependentInfo GetDependentInfoByID(int ID);
        void DeleteDependentInfo(int ID);
        #endregion

        #region Certification info        
        List<TrainingCertificate> GetCertificateType();        
        int SaveEmployeeCertificateInfo(EmployeeCertificateInfo model);
        List<EmployeeCertificateInfo> GetEmployeeCertificateInfo(int EmployeeID);
        EmployeeCertificateInfo GetEmployeeCertificateInfoByID(int ID);
        void DeleteEmployeeCertificateInfo(int ID);
        void RenewCertificate(RenewCertificate model);
        void SetCertificateClossOff(int ID);
        #endregion

        #region EmployeeVicationInfo
        List<VicationType> GetVicationType();
        int SaveEmployeeVicationInfo(EmployeeVicationInfo model);
        List<EmployeeVicationInfo> GetEmployeeVicationInfo(int EmployeeID);
        EmployeeVicationInfo GetEmployeeVicationInfoByID(int ID);
        void DeleteEmployeeVicationInfo(int ID);
        #endregion

        #region EmployeeDisciplinaryDetail
        List<DisciplinaryInfraction> GetDisciplinaryInfractionType();
        List<DisciplinaryWarning> GetDisciplinaryWarningType();
        int SaveEmployeeDisciplinaryDetail(EmployeeDisciplinaryDetail model);
        List<EmployeeDisciplinaryDetail> GetDisciplinaryDetailByCompanyID(int CompanyID);
        List<EmployeeDisciplinaryDetail> GetEmployeeDisciplinaryDetail(int EmployeeID);
        List<EmployeeDisciplinaryDetail> GetFilterDisciplinaryDetail(FilterDto model);
        EmployeeDisciplinaryDetail GetEmployeeDisciplinaryDetailByID(int ID);
        void DeleteEmployeeDisciplinaryDetail(int ID);
        void DisciplinaryDetailCompleted(int Id);
        #endregion

        #region EmployeeTerminationDetail
        bool SendNotoficationTermination(int EmployeeID);
        List<TerminationReasonType> GetTerminationReasonType();
        int SaveEmployeeTerminationDetail(EmployeeTerminationDetail model);
        List<EmployeeTerminationDetail> GetTerminationDetailByCompanyID(int CompanyID);
        List<EmployeeTerminationDetail> GetEmployeeTerminationDetail(int EmployeeID);        
        EmployeeTerminationDetail GetEmployeeTerminationDetailByID(int ID);
        List<EmployeeTerminationDetail> GetFilterTerminationDetail(FilterDto model);
        void DeleteEmployeeTerminationDetail(int ID);
        void TerminationDetailCompleted(int ID);
        #endregion

        #region Layoffs
        int SaveLayoffsDetail(LayOffs model);
        List<LayOffs> getLayoffsByCompanyID(int CompanyID);
        List<LayOffs> getLayoffsDetail(int EmployeeID);        
        LayOffs getLayoffsByID(int Id);
        LayoffEmployee GetLayOffEmpDetails(int ID);
        void DeleteLayoffsByID(int Id);
        void LayoffsCompleted(int Id);
        #endregion

        #region EmployeeOfferDetails
        List<OfferBenefitsType> GetOfferBenefitsType();
        List<OfferVacationType> GetOfferVacationType();
        List<EmploymentType> GetEmploymentType();
        List<CompensationType> GetCompensationType();
        List<WorkingDayType> GetWorkingDayType();
        List<TerminationClause> GetTerminationClause();
        int SaveEmployeeOfferDetail(EmployeeOfferDetail model);
        List<EmployeeOfferDetail> GetEmployeeOfferDetailsByCompanyID(int CompanyID, string SearcthText=null);
        List<EmployeeOfferDetail> GetEmployeeOfferDetails(int EmployeeID,string SearcthText=null);
        EmployeeOfferDetail GetEmployeeOfferDetailsByID(int ID);
        void DeleteEmployeeOfferDetails(int ID);
        void OfferAccepted(int ID);
        void OfferDecline(int ID);
        #endregion

        #region Accindent detail

        List<AccidentType> GetAccidentType();
        List<DiseasesType> GetDiseasesType();        
        List<BodyPartType> GetBodyPartType();
        List<AccidentDetail> GetAccidentDetailByCompanyID(int CompanyID, string SearchText = null);
        List<AccidentDetail> GetAccidentDetail(int EmployeeID, string SearchText = null);
        AccidentDetail GetAccidentDetailByID(int ID);
        int SaveAccidentDetail(AccidentDetail model);
        List<TreatmentPlace> GetTreatmentPlace();
        List<MedicalAwarness> GetMedicalAwarnessList();
        List<RateType> GetRateType();
        List<WorkerJobType> GetWorkerJobType();
        List<HealthCare> GetHealthcare(int AccidentDetailID);
        void SaveHealthCare(HealthCare model);
        void AccidentCompleted(int ID);
        #endregion

        #region Miscellaneous  
        List<FileDetail> GetFilesFromFolderByEmployeeID(int employeeID, string folderName);
        void UpdateFilePrivacy(FilePrivacy model);
        void SavePublicFile(int employeeID, string filePath,string folderName=null);
        #endregion

        #region PerofrmenceReview
        List<EvaluatorIndicatedType> GetPerEvaluatorIndicatedType();
        List<PerformenceReview> GetPerformenceReview(int EmployeeID);
        PerformenceReview GetPerformenceReviewByID(int ID);
        int SavePerformenceReview(PerformenceReview model);
        int UpdatePerReviewStatus(int ID);
        List<ReviewSectionContent> GetPerformanceReviewContent();
        void PerformenceReviewCompleted(int ID);
        List<PerformenceReview> getFilterPerformenceReview(FilterDto model);

        #endregion

        #region Asset
        int SaveAssets(Assets Assets);
        List<Assets> GetAssetList(AssetsFilter assetsFilter);
        Assets GetAssetList(int ID);
        int SaveAssetsType(AssetsType model);
        List<AssetsType> GetAssetTypeList();
        void DeleteAssets(int ID);
        void UpdateAssetStatus(AssetStatusDto model);
        #endregion
    }
}
