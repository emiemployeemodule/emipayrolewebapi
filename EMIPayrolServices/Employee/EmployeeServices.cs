﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EMIPayrolModel;
using EMIPayrolData;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Web;
using System.IO;

namespace EMIPayrolServices
{
    public class EmployeeServices : IEmployeeServices
    {
        private readonly ICompanyServices _companyServices;
        private readonly ICommonServices _commonServices;
        public EmployeeServices(ICompanyServices companyServices,ICommonServices commonServices)
        {
            _companyServices = companyServices;
            _commonServices = commonServices;
        }

        readonly ICompanyServices companyServices = new CompanyServices();
        readonly ICommonServices commonServices = new CommonServices();

        public EmployeeServices()
        {
            _companyServices = companyServices;
            _commonServices = commonServices;
        }
        #region Employee
        public int SaveEmployeeDetails(Employee employee)
        {
            if (employee.ID == 0)
                employee.Password = CommonHelper.AutogenratePassord();
            var returnVal = 0;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("@Result",""),
                new SqlParameter("ID",employee.ID),
                new SqlParameter("FirstName",employee.FirstName),
                new SqlParameter("LastName",employee.LastName),
                new SqlParameter("UserName",employee.UserName),
                new SqlParameter("Email",employee.Email),
                new SqlParameter("Password",employee.Password),
                new SqlParameter("DateOfBirth",employee.DateOfBirth),
                new SqlParameter("Gender",employee.Gender),
                new SqlParameter("EmployeeID",employee.EmployeeID),
                new SqlParameter("JoiningDate",employee.JoiningDate),
                new SqlParameter("Phone",employee.Phone),
                new SqlParameter("CountryID",employee.CountryID),
                new SqlParameter("StateID",employee.StateID),
                new SqlParameter("CityID",employee.CityID),
                new SqlParameter("Address",employee.Address),
                new SqlParameter("PinCode",employee.PinCode),
                new SqlParameter("ImageProfile",employee.ImageProfilePath),
                new SqlParameter("CompanyID",employee.CompanyID),
                new SqlParameter("DesignationID",employee.DesignationID),
                new SqlParameter("DepartmentID",employee.DepartmentID),
                new SqlParameter("CreatedBy",employee.CreatedBy),
                new SqlParameter("StartDate",employee.StartDate),
                new SqlParameter("HireDate",employee.HireDate),
                new SqlParameter("ShiftID",employee.ShiftID),
                new SqlParameter("ReportingManager",employee.ReportingManagerID)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            returnVal = SQL.ExceuteStoreProcedure("SaveUpdateEmployee", param, true);
            if (returnVal > 0 && employee.ModulePermission != null)
            {
                deleteEmployeePermission(returnVal);
                foreach (var permission in employee.ModulePermission)
                {
                    param = new SqlParameter[] {
                    new SqlParameter("EmployeeID",returnVal),
                    new SqlParameter("ModuleID",permission.ID),
                    new SqlParameter("Read",permission.Read),
                    new SqlParameter("Write",permission.Write),
                    new SqlParameter("Delete",permission.Delete),
                    //new SqlParameter("Create",permission.Create),
                    new SqlParameter("Import",permission.Import),
                    new SqlParameter("Export",permission.Export)
                    };
                    SQL.ExceuteStoreProcedure("SaveEmployeePermission", param, false);
                }
            }
            if (employee.ID == 0 && returnVal > 0)
            {
                var objcompany = _companyServices.companyDetail(employee.CompanyID);
                //var Link = ConfigurationManager.AppSettings["EmailConfirmUrl"] + "/accountconfirmation/" + company.CompanyGuid;
                var emailtemplate = GetEmailTemplate(Constants.EmployeeSignupTemplate);
                string emailbody = emailtemplate.Body.Replace("{{CompanyName}}", objcompany.CompanyName).Replace("{{EmployeeName}}", (employee.FirstName + " " + employee.LastName)).Replace("{{Email}}", employee.Email).Replace("{{Password}}", CommonHelper.Decrypt(employee.Password));
                CommonHelper.SendEmail(employee.Email, emailtemplate.Subject.Replace("{{CompanyName}}", objcompany.CompanyName), emailbody);
            }
            return returnVal;
        }
        private EmailTemplate GetEmailTemplate(string TemplateName)
        {
            EmailTemplate emailTemplate = null;
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("Name", TemplateName) };
            using (SqlDataReader dr = SQL.GetDataReader("GetEmailTeplateByName", param, true))
            {
                while (dr.Read())
                {
                    emailTemplate = new EmailTemplate();
                    emailTemplate.Subject = dr["Subject"].ToString();
                    emailTemplate.Body = dr["Body"].ToString();
                }
            }
            return emailTemplate;
        }
        private void deleteEmployeePermission(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("DeleteEmployeePermission", param, false);
        }
        public List<Employee> getEmployeeByCompanyID(int companyID)
        {
            List<Employee> employeeList = new List<Employee>();
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("CompanyID", companyID) };
            return getEmployee(param);
        }
        public Employee getEmployeeByID(int ID)
        {
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("ID", ID) };
            return getEmployee(param).FirstOrDefault();
        }
        public List<Employee> GetEmployeeByFilter(EmployeSearchRequest searchRequest)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID", searchRequest.CompanyID),
                new SqlParameter("EmployeeID", searchRequest.EmpoyeeID),
                new SqlParameter("DepartmentID", searchRequest.DepartmentID),
                new SqlParameter("Name", searchRequest.Name),
                new SqlParameter("ReportingManagerID", searchRequest.ReportingManagerID)
            };
            return getEmployee(param);
        }
        private List<Employee> getEmployee(SqlParameter[] param)
        {
            var baseurl = Convert.ToString(ConfigurationManager.AppSettings["APIBaseUrl"]);
            List<Employee> employeeList = new List<Employee>();
            using (SqlDataReader dr = SQL.GetDataReader("GetEmployeeByCompanyID", param, true))
            {
                while (dr.Read())
                {
                    employeeList.Add(new Employee
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        FirstName = dr["FirstName"].ToString(),
                        LastName = dr["LastName"].ToString(),
                        UserName = dr["UserName"].ToString(),
                        Email = dr["Email"].ToString(),
                        DateOfBirth = CommonHelper.DateToString(dr["DateOfBirth"]),
                        Gender = Convert.ToString(dr["Gender"]),
                        EmployeeID = dr["EmployeeID"].ToString(),
                        JoiningDate = CommonHelper.DateToString(dr["JoiningDate"]),
                        Phone = dr["Phone"].ToString(),
                        CountryID = Convert.ToInt32(dr["CountryID"]),
                        StateID = Convert.ToInt32(dr["StateID"]),
                        CityID = Convert.ToInt32(dr["CityID"]),
                        Address = Convert.ToString(dr["Address"]),
                        PinCode = Convert.ToString(dr["PinCode"]),
                        ImageProfilePath = Convert.ToString(dr["ImageProfile"]),
                        ImageProfileUrl = string.IsNullOrEmpty(dr["ImageProfile"]+"")?"":(baseurl+ Convert.ToString(dr["ImageProfile"])),
                        CompanyID = Convert.ToInt32(dr["CompanyID"]),
                        DesignationID = Convert.ToInt32(dr["DesignationID"]),
                        DesignationName = Convert.ToString(dr["DesignationName"]),
                        CreatedBy = Convert.ToInt32(dr["CreatedBy"]),
                        CreatedDate = CommonHelper.DateToString(dr["CreatedDate"]),
                        ModifiedDate = CommonHelper.DateToString(dr["ModifiedDate"]),
                        CompanyName = Convert.ToString(dr["CompanyName"]),
                        Country = Convert.ToString(dr["Country"]),
                        State = Convert.ToString(dr["State"]),
                        City = Convert.ToString(dr["CIty"]),
                        DepartmentID = Convert.ToInt32(dr["DepartmentID"]),
                        DepartmentName = Convert.ToString(dr["DepartmentName"]),
                        CompanyAddress = Convert.ToString(dr["CompanyAddress"]),
                        Owner=Convert.ToString(dr["CompanyOwner"]),
                        StartDate = CommonHelper.DateToString(dr["StartDate"]),
                        HireDate = CommonHelper.DateToString(dr["HireDate"]),
                        ShiftID = Convert.ToInt32(dr["ShiftID"]),
                        ShiftType = Convert.ToString(dr["ShiftType"]),
                        ReportingManager=Convert.ToString(dr["ReportingManager"]),
                        ReportingManagerID = Convert.ToInt32(dr["ReportingManagerID"]),
                        ModulePermission = new List<AppModule>(getPermissionByID(Convert.ToInt32(dr["ID"]))),
                    });
                }
            }
            return employeeList;
        }
        private List<AppModule> getPermissionByID(int EmployeeID)
        {
            List<AppModule> permission = new List<AppModule>();
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("EmployeeID",EmployeeID)
            };
            using (SqlDataReader dr = SQL.GetDataReader("GetPermissionByEmployeeID", param, true))
            {
                while (dr.Read())
                {
                    permission.Add(new AppModule
                    {
                        ID = Convert.ToInt32(dr["ModuleID"]),
                        Name = dr["Name"].ToString(),
                        Read = Convert.ToBoolean(dr["Read"]),
                        Write = Convert.ToBoolean(dr["Write"]),
                        Delete = Convert.ToBoolean(dr["Delete"]),
                        //Create = Convert.ToBoolean(dr["Create"]),
                        Import = Convert.ToBoolean(dr["Import"]),
                        Export = Convert.ToBoolean(dr["Export"])
                    });
                }
            }
            if (permission == null && permission.Count == 0)
                permission.Add(new AppModule());
            return permission;
        }
        public void DeleteEmployeeByID(int ID)
        {
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("ID", ID) };
            SQL.ExceuteStoreProcedure("DeleteEmployeeByID", param, false);
            deleteEmployeePermission(ID);
        }
        public int EmployeeLogin(CommonClass model)
        {
            int result = 0;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("Email",model.Email),
                new SqlParameter("Password",CommonHelper.Encrypt(model.Password))
            };
            using (SqlDataReader dr = SQL.GetDataReader("LoginEmployee", param, true))
            {
                while (dr.Read())
                {
                    result = Convert.ToInt32(dr["Value"]);
                }
            }
            return result;
        }
        public List<Status> GetStatusList()
        {
            List<Status> statusList = new List<Status>();
            using (SqlDataReader dr = SQL.GetDataReader("GetStatusList", true))
            {
                while (dr.Read())
                {
                    statusList.Add(new Status
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Type = dr["Value"].ToString()
                    });
                }
            }
            return statusList;
        }
        public void UpdateEmployeeStatus(EmployeeStatus model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("EmployeeID",model.EmployeeID),
                new SqlParameter("StatusID",model.StatusID)
            };
            SQL.ExceuteStoreProcedure("UpdateEmployeeStatus", param, false);
        }
        public List<EmployeeStatus> GetStatusHistory(int EmployeeID)
        {
            List<EmployeeStatus> lstEmployeeStatus = new List<EmployeeStatus>();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("EmployeeID",EmployeeID)
            };
            using (SqlDataReader dr = SQL.GetDataReader("GetEmployeeStatusHistory", param, true))
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        lstEmployeeStatus.Add(new EmployeeStatus
                        {
                            StatusID = Convert.ToInt32(dr["StatusID"]),
                            Status = dr["Status"].ToString(),
                            EmployeeID = Convert.ToInt32(dr["EmployeeID"]),
                            ChangeDate = CommonHelper.DateToString(dr["CreatedDate"]),
                            CreatedUser = Convert.ToString(dr["CreatedBy"])
                        });
                    }
                }
            }
            return lstEmployeeStatus;
        }

        public List<ReportedUser> getReportedUser(int CompanyID)
        {
            List<ReportedUser> reportedUserList = new List<ReportedUser>();
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("CompanyID", CompanyID) };
            using (SqlDataReader dr = SQL.GetDataReader("GetReportedUser", param, true))
            {
                while (dr.Read())
                {
                    reportedUserList.Add(new ReportedUser
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Name = dr["FirstName"].ToString() + " " + dr["LastName"].ToString(),
                        Position = Convert.ToString(dr["Title"])
                    });
                }
            }
            return reportedUserList;
        }
      
        #endregion

        #region Leave request
        public int SaveUpdateLeaevRequest(LeaveRequest leaveRequest)
        {
            var returnVal = 0;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("@Result",""),
                new SqlParameter("ID",leaveRequest.ID),
                new SqlParameter("LeaveTypeID",leaveRequest.LeaveTypeID),
                new SqlParameter("FromDate",leaveRequest.FromDate),
                new SqlParameter("ToDate",leaveRequest.ToDate),
                new SqlParameter("NoOfDay",leaveRequest.NoOfDay),
                new SqlParameter("RemainingLeave",leaveRequest.RemainingLeave),
                new SqlParameter("Reason",leaveRequest.Reason),
                new SqlParameter("LeaveStatusID",leaveRequest.LeaveStatusID),
                new SqlParameter("EmployeeID",leaveRequest.EmployeeID),
                new SqlParameter("CompanyID",leaveRequest.CompanyID)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            returnVal = SQL.ExceuteStoreProcedure("SaveUpdateLeaveRequest", param, true);
            return returnVal;
        }
        public void UpdateLeaevRequestStatus(int ID, int LeaveStatusID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID),
                new SqlParameter("LeaveStatusID",LeaveStatusID)
            };
            SQL.ExceuteStoreProcedure("UpdateLeaveRequestStatus", param, false);

        }
        public List<LeaveRequest> GetLeaevRequestByCompanyID(LeaveSearch model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",model.CompanyID),
                new SqlParameter("LeaveTypeID",model.LeaveTypeID),
                new SqlParameter("LeaveStatusID",model.LeaveStatusID),
                new SqlParameter("FromDate",model.FromDate),
                new SqlParameter("ToDate",model.ToDate),
                new SqlParameter("EmployeeName",model.EmployeeName)
            };
            return GetLeaveRequest(param);
        }

        private List<LeaveRequest> GetLeaveRequest(SqlParameter[] param)
        {
            List<LeaveRequest> leaveRequest = new List<LeaveRequest>();
            using (SqlDataReader dr = SQL.GetDataReader("GetLeaveRequest", param, true))
            {
                while (dr.Read())
                {
                    leaveRequest.Add(new LeaveRequest
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        LeaveTypeID = Convert.ToInt32(dr["LeaveTypeID"]),
                        LeaveType = Convert.ToString(dr["LeaveType"]),
                        FromDate = CommonHelper.DateToString(dr["FromDate"]),
                        ToDate = CommonHelper.DateToString(dr["ToDate"]),
                        NoOfDay = Convert.ToInt32(dr["NoOfDay"]),
                        RemainingLeave = Convert.ToInt32(dr["RemainingLeave"]),
                        Reason = Convert.ToString(dr["Reason"]),
                        LeaveStatusID = Convert.ToInt32(dr["LeaveStatusID"]),
                        LeaveStatus = Convert.ToString(dr["LeaveStatus"]),
                        CompanyID = Convert.ToInt32(dr["CompanyID"]),
                        EmployeeID = Convert.ToInt32(dr["EmployeeID"]),
                        EmployeeName = Convert.ToString(dr["EmployeeName"])
                    });
                }
            }
            return leaveRequest;
        }

        public LeaveRequest GetLeaevRequestByID(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            return GetLeaveRequest(param).FirstOrDefault();
        }
        public void DeleteLeaveRequest(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("DeleteLeaveRequest", param, false);
        }
        #endregion

        #region Designation
        public int SaveUpdateDesignation(Designation designation)
        {
            var returnVal = 0;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("@Result",""),
                new SqlParameter("ID",designation.ID),
                new SqlParameter("Title",designation.Title),
                new SqlParameter("DepartmentID",designation.DepartmentID),
                new SqlParameter("CompanyID",designation.CompanyID)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            returnVal = SQL.ExceuteStoreProcedure("SaveUpdateDesignation", param, true);
            return returnVal;
        }
        public List<Designation> GetDesignationByCompanyID(int CompnayID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",CompnayID)
            };
            return GetDesignation(param);
        }
        public List<Designation> GetDesignationByDepartment(int CompnayID, int DepartmentID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",CompnayID),
                new SqlParameter("DepartmentID",DepartmentID)
            };
            return GetDesignation(param);
        }
        private List<Designation> GetDesignation(SqlParameter[] param)
        {
            List<Designation> Designation = new List<Designation>();
            using (SqlDataReader dr = SQL.GetDataReader("GetDesignation", param, true))
            {
                while (dr.Read())
                {
                    Designation.Add(new Designation
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Title = Convert.ToString(dr["Title"]),
                        CompanyID = Convert.ToInt32(dr["CompanyID"]),
                        DepartmentID = Convert.ToInt32(dr["DepartmentID"]),
                        DepartmentName = Convert.ToString(dr["DepartmentName"])
                    });
                }
            }
            return Designation;
        }

        public Designation GetDesignationByID(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            return GetDesignation(param).FirstOrDefault();
        }
        public void DeleteDesignation(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("DeleteDesignation", param, false);
        }
        public void UpdateEmployeeDesignation(EmployeRole employeerole)
        {
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("ID",employeerole.ID),
                new SqlParameter("DesignationID",employeerole.DesignationID)
            };
            SQL.ExceuteStoreProcedure("updateEmployeeDesignation", param, false);
        }
        #endregion

        #region bulkUpload & Auto EmployeeID
        public BulkResult BulkUpload(DataTable dtRecord, int CompanyID)
        {
            BulkResult bulkResult = new BulkResult();
            foreach (DataRow row in dtRecord.Rows)
            {
                if (!string.IsNullOrEmpty(row["FirstName"] + ""))
                {
                    if (validateBulkRow(row))
                    {
                        var returnVal = 0;
                        SqlParameter[] param = new SqlParameter[] {
                            new SqlParameter("@Result",""),
                            new SqlParameter("FirstName",row["FirstName"].ToString()),
                            new SqlParameter("LastName",row["LastName"].ToString()),
                            new SqlParameter("UserName",row["UserName"].ToString()),
                            new SqlParameter("Email",row["Email"].ToString()),
                            new SqlParameter("Password",CommonHelper.AutogenratePassord()),
                            new SqlParameter("DateOfBirth",row["DateOfBirth"].ToString()),
                            new SqlParameter("Gender",row["Gender"].ToString()),
                            new SqlParameter("JoiningDate",row["JoiningDate"].ToString()),
                            new SqlParameter("Phone",row["Phone"].ToString()),
                            new SqlParameter("CompanyID",CompanyID),
                            new SqlParameter("Country",row["Country"].ToString()),
                            new SqlParameter("State",row["State"].ToString()),
                            new SqlParameter("City",row["City"].ToString()),
                            new SqlParameter("Address",row["Address"].ToString()),
                            new SqlParameter("Designation",row["Designation"].ToString()),
                            new SqlParameter("PinCode",row["PostalCode"].ToString())
                            };
                        param[0].Direction = System.Data.ParameterDirection.ReturnValue;
                        returnVal = SQL.ExceuteStoreProcedure("BulkInsertEmployee", param, true);
                        if (returnVal > 0) bulkResult.Success++;
                        else
                            bulkResult.Failed++;
                    }
                    else { bulkResult.Failed++; }
                }
            }
            bulkResult.TotalRecord = bulkResult.Failed + bulkResult.Success;
            return bulkResult;
        }

        private bool validateBulkRow(DataRow row)
        {
            return (!string.IsNullOrEmpty(row["FirstName"] + "") && !string.IsNullOrEmpty(row["LastName"] + "")
                && !string.IsNullOrEmpty(row["UserName"] + "") && !string.IsNullOrEmpty(row["Email"] + "")
                && !string.IsNullOrEmpty(row["JoiningDate"] + "") && !string.IsNullOrEmpty(row["Phone"] + "")
                && !string.IsNullOrEmpty(row["Designation"] + ""));
        }

        public string GerateEmployeeID(int CompanyID)
        {
            string employeeID = "";
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("CompanyID", CompanyID) };
            employeeID = SQL.ExecuteScalar("EXEC GenrateEmployeeID " + CompanyID + "") + "";
            return employeeID;
        }
        public void UpdateEployeePhoto(int employeeID, string filename)
        {
            SqlParameter[] param = new SqlParameter[] {
            new SqlParameter("filename",filename),
            new SqlParameter("employeeID",employeeID) };
            SQL.ExceuteStoreProcedure("UpdateEployeePhoto", param, false);
        }
        #endregion

        #region emergency info
        public void SaveEmergenyInfo(EmergenyInfo model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("EmployeeID",model.EmployeeID),
                new SqlParameter("Name",model.Name),
                new SqlParameter("Relation",model.Relation),
                new SqlParameter("Address",model.Address),
                new SqlParameter("CountryID",model.CountryID),
                new SqlParameter("StateID",model.StateID),
                new SqlParameter("CityID",model.CityID),
                new SqlParameter("PostalCode",model.PostalCode),
                new SqlParameter("Phone",model.Phone),
                new SqlParameter("Cell",model.Cell),
                new SqlParameter("Email",model.Email),
            };
            SQL.ExceuteStoreProcedure("SaveEmrgencyContInfo", param, false);
        }
        public EmergenyInfo GetEmergenyInfo(int EmployeeID)
        {
            EmergenyInfo emergenyInfo = new EmergenyInfo() { EmployeeID = EmployeeID };
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("EmployeeID",EmployeeID)
            };
            using (SqlDataReader dr = SQL.GetDataReader("GetEmrgencyContInfo", param, true))
            {
                while (dr.Read())
                {
                    emergenyInfo.Name = Convert.ToString(dr["Name"]);
                    emergenyInfo.Relation = dr["Relation"] + "";
                    emergenyInfo.CountryID = Convert.ToInt32(dr["CountryID"]);
                    emergenyInfo.CountryName = dr["Country"] + "";
                    emergenyInfo.StateID = Convert.ToInt32(dr["StateID"]);
                    emergenyInfo.StateName = dr["State"] + "";
                    emergenyInfo.CityID = Convert.ToInt32(dr["CityID"]);
                    emergenyInfo.CityName = dr["City"] + "";
                    emergenyInfo.Address = dr["Address"] + "";
                    emergenyInfo.PostalCode = dr["PostalCode"] + "";
                    emergenyInfo.Phone = dr["Phone"] + "";
                    emergenyInfo.Cell = dr["Cell"] + "";
                    emergenyInfo.Email = dr["Email"] + "";
                }
            }
            return emergenyInfo;
        }
        #endregion

        #region position info
        public int SavePositionInfo(PositionInfo model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("@Result",""),
                new SqlParameter("ID",model.ID),
                new SqlParameter("EmployeeID",model.EmployeeID),
                new SqlParameter("Position",model.Position),
                new SqlParameter("Notes",model.Notes),
                new SqlParameter("StartDate",model.StartDate),
                new SqlParameter("EndDate",model.EndDate),
                new SqlParameter("StartTime",model.StartTime),
                new SqlParameter("Endtime",model.Endtime),
                new SqlParameter("CreatedBy",model.CreatedBy)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            return SQL.ExceuteStoreProcedure("SaveEmployeePositionInfo", param, true);
        }
        public List<PositionInfo> GetPositionInfo(int EmployeeID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("EmployeeID",EmployeeID)
            };
            return GetPositionInfo(param);
        }
        public PositionInfo GetPositionInfoByID(int ID)
        {
            List<PositionInfo> positionInfo = new List<PositionInfo>();

            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            return GetPositionInfo(param).FirstOrDefault();
        }
        private List<PositionInfo> GetPositionInfo(SqlParameter[] param)
        {
            List<PositionInfo> positionInfo = new List<PositionInfo>();
            using (SqlDataReader dr = SQL.GetDataReader("GetEmployeePositionInfo", param, true))
            {
                while (dr.Read())
                {
                    positionInfo.Add(new PositionInfo
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        EmployeeID = Convert.ToInt32(dr["EmployeeID"]),
                        Position = Convert.ToString(dr["Position"]),
                        Notes = Convert.ToString(dr["Notes"]),
                        StartDate = CommonHelper.DateToString(dr["StartDate"]),
                        EndDate = CommonHelper.DateToString(dr["EndDate"]),
                        StartTime = dr["StartTime"] + "",
                        Endtime = dr["Endtime"] + "",
                        CreatedUser = dr["CreatedUser"] + ""
                    });
                }
            }
            return positionInfo;
        }
        public void DeletePositionInfo(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("DeleteEmployeePositionInfo", param, false);
        }
        #endregion

        #region Wages info
        public List<PayFrequency> GetPayFrequencyList()
        {
            List<PayFrequency> payFrequency = new List<PayFrequency>();
            using (SqlDataReader dr = SQL.GetDataReader("GetPayFrequency", true))
            {
                while (dr.Read())
                {
                    payFrequency.Add(new PayFrequency
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Name = Convert.ToString(dr["Name"])
                    });
                }
            }
            return payFrequency;
        }
        public void SaveEmpWagesInfo(EmpWagesInfo model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",model.ID),
                new SqlParameter("EmployeeID",model.EmployeeID),
                new SqlParameter("StartDate",model.StartDate),
                new SqlParameter("Notes",model.Notes),
                new SqlParameter("PayFrequencyID",model.PayFrequencyID),
                new SqlParameter("PayPerPeriod",model.PayPerPeriod),
                new SqlParameter("CreatedBy",model.CreatedBy)
            };
            SQL.ExceuteStoreProcedure("SaveEmpWagesInfo", param, false);
        }
        public List<EmpWagesInfo> GetEmpWagesInfo(int EmployeeID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("EmployeeID",EmployeeID)
            };
            return GetEmpWagesInfo(param);
        }
        public EmpWagesInfo GetEmpWagesInfoByID(int ID)
        {
            List<EmpWagesInfo> EmpWagesInfo = new List<EmpWagesInfo>();

            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            return GetEmpWagesInfo(param).FirstOrDefault();
        }
        private List<EmpWagesInfo> GetEmpWagesInfo(SqlParameter[] param)
        {
            List<EmpWagesInfo> EmpWagesInfo = new List<EmpWagesInfo>();
            using (SqlDataReader dr = SQL.GetDataReader("GetEmpWagesInfo", param, true))
            {
                while (dr.Read())
                {
                    EmpWagesInfo.Add(new EmpWagesInfo
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        EmployeeID = Convert.ToInt32(dr["EmployeeID"]),
                        PayFrequencyID = Convert.ToInt32(dr["PayFrequencyID"]),
                        PayFrequency = Convert.ToString(dr["PayFrequency"]),
                        StartDate = CommonHelper.DateToString(dr["StartDate"]),
                        PayPerPeriod = Convert.ToDecimal(dr["PayPerPeriod"]),
                        Notes = Convert.ToString(dr["Notes"]),
                        CreatedBy = Convert.ToInt32(dr["CreatedBy"]),
                        CreatedUser = Convert.ToString(dr["CreatedUser"])
                    });
                }
            }
            return EmpWagesInfo;
        }
        public void DeleteEmpWagesInfo(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("DeleteEmpWagesInfo", param, false);
        }
        #endregion

        #region Dependent info        
        public int SaveDependentInfo(DependentInfo model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("Result",""),
                new SqlParameter("ID",model.ID),
                new SqlParameter("Name",model.Name),
                new SqlParameter("Relation",model.Relation),
                new SqlParameter("DateOfBirth",model.DatOfBirth),
                new SqlParameter("CountryID",model.CountryID),
                new SqlParameter("StateID",model.StateID),
                new SqlParameter("CityID",model.CityID),
                new SqlParameter("Address",model.Address),
                new SqlParameter("PostalCode",model.PostalCode),
                new SqlParameter("Phone",model.Phone),
                new SqlParameter("Cell",model.Cell),
                new SqlParameter("Email",model.Email),
                new SqlParameter("EmployeeID",model.EmployeeID),
                new SqlParameter("CreatedBy",model.CreatedBy)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            return SQL.ExceuteStoreProcedure("SaveDependentInfo", param, true);
        }
        public List<DependentInfo> GetDependentInfo(int EmployeeID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("EmployeeID",EmployeeID)
            };
            return GetDependentInfo(param);
        }
        public DependentInfo GetDependentInfoByID(int ID)
        {
            List<DependentInfo> DependentInfo = new List<DependentInfo>();

            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            return GetDependentInfo(param).FirstOrDefault();
        }
        private List<DependentInfo> GetDependentInfo(SqlParameter[] param)
        {
            List<DependentInfo> DependentInfo = new List<DependentInfo>();
            using (SqlDataReader dr = SQL.GetDataReader("GetDependentInfo", param, true))
            {
                while (dr.Read())
                {
                    DependentInfo.Add(new DependentInfo
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        EmployeeID = Convert.ToInt32(dr["EmployeeID"]),
                        EmployeeName = Convert.ToString(dr["EmployeeName"]),
                        Name = Convert.ToString(dr["Name"]),
                        Relation = Convert.ToString(dr["Relation"]),
                        DatOfBirth = CommonHelper.DateToString(dr["DateOfBirth"]),
                        Address = Convert.ToString(dr["Address"]),
                        CountryID = Convert.ToInt32(dr["CountryID"]),
                        CountryName = Convert.ToString(dr["Country"]),
                        StateID = Convert.ToInt32(dr["StateID"]),
                        StateName = Convert.ToString(dr["State"]),
                        CityID = Convert.ToInt32(dr["CityID"]),
                        CityName = Convert.ToString(dr["City"]),
                        PostalCode = Convert.ToString(dr["PostalCode"]),
                        Phone = Convert.ToString(dr["Phone"]),
                        Cell = Convert.ToString(dr["Cell"]),
                        Email = Convert.ToString(dr["Email"]),
                        CreatedBy = Convert.ToInt32(dr["CreatedBy"]),
                        CreatedUser = Convert.ToString(dr["CreatedUser"]),
                        CreatedDate = CommonHelper.DateToString(dr["CreatedDate"]),
                        ModifiedDate = CommonHelper.DateToString(dr["ModifiedDate"]),
                    });
                }
            }
            return DependentInfo;
        }
        public void DeleteDependentInfo(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("DeleteDependentInfo", param, false);
        }
        #endregion

        #region EmployeeCertificateInfo
        public List<TrainingCertificate> GetCertificateType()
        {
            List<TrainingCertificate> lstTrainingCertificate = new List<TrainingCertificate>();
            using (SqlDataReader dr = SQL.GetDataReader("GetTrainingCertificate", true))
            {
                while (dr.Read())
                {
                    lstTrainingCertificate.Add(new TrainingCertificate
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        CertificateType = Convert.ToString(dr["CertificateType"])
                    });
                }
            }
            return lstTrainingCertificate;
        }
        public int SaveEmployeeCertificateInfo(EmployeeCertificateInfo model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("Result",""),
                new SqlParameter("ID",model.ID),
                new SqlParameter("TrainingCertificateID",model.TrainingCertificateID),
                new SqlParameter("Acquireddate",model.Acquireddate),
                new SqlParameter("Expirydate",model.Expirydate),
                new SqlParameter("RefNumber",model.RefNumber),
                new SqlParameter("IssuedBy",model.IssuedBy),
                new SqlParameter("FIlePath",model.FIlePath),
                new SqlParameter("EmployeeID",model.EmployeeID),
                new SqlParameter("CreatedBy",model.CreatedBy)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            int result = SQL.ExceuteStoreProcedure("SaveEmployeeCertificateInfo", param, true);
            if (result > 0)
                _commonServices.SaveFileRecord(model.FIlePath, model.EmployeeID, "Certificate");
            return result;
        }

        

        public List<EmployeeCertificateInfo> GetEmployeeCertificateInfo(int EmployeeID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("EmployeeID",EmployeeID)
            };
            return GetEmployeeCertificateInfo(param);
        }
        public EmployeeCertificateInfo GetEmployeeCertificateInfoByID(int ID)
        {
            List<EmployeeCertificateInfo> EmployeeCertificateInfo = new List<EmployeeCertificateInfo>();

            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            return GetEmployeeCertificateInfo(param).FirstOrDefault();
        }
        private List<EmployeeCertificateInfo> GetEmployeeCertificateInfo(SqlParameter[] param)
        {
            List<EmployeeCertificateInfo> EmployeeCertificateInfo = new List<EmployeeCertificateInfo>();
            using (SqlDataReader dr = SQL.GetDataReader("GetEmployeeCertificateInfo", param, true))
            {
                while (dr.Read())
                {
                    EmployeeCertificateInfo.Add(new EmployeeCertificateInfo
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        TrainingCertificateID = Convert.ToInt32(dr["TrainingCertificateID"]),
                        TrainingCertificate = Convert.ToString(dr["TrainingCertificate"]),
                        Acquireddate = CommonHelper.DateToString(dr["Acquireddate"]),
                        Expirydate = CommonHelper.DateToString(dr["Expirydate"]),
                        RefNumber = Convert.ToString(dr["RefNumber"]),
                        IssuedBy = Convert.ToString(dr["IssuedBy"]),
                        FIlePath = Convert.ToString(dr["FIlePath"]),
                        EmployeeID = Convert.ToInt32(dr["EmployeeID"]),
                        CreatedBy = Convert.ToInt32(dr["CreatedBy"]),
                        CreatedUser = Convert.ToString(dr["CreatedUser"])
                    });
                }
            }
            return EmployeeCertificateInfo;
        }
        public void DeleteEmployeeCertificateInfo(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("DeleteEmployeeCertificateInfo", param, false);
        }
        public void RenewCertificate(RenewCertificate model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",model.ID),
                new SqlParameter("ExpiryDate",model.ExpireDate),
                new SqlParameter("IssuedBy",model.IssuedBy)
            };
            SQL.ExceuteStoreProcedure("CertificateRenew", param, false);
        }
        public void SetCertificateClossOff(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("SetCertificateClossOff", param, false);
        }
        #endregion

        #region EmployeeVicationInfo
        public List<VicationType> GetVicationType()
        {
            List<VicationType> lstVicationType = new List<VicationType>();
            using (SqlDataReader dr = SQL.GetDataReader("GetMasterVication", true))
            {
                while (dr.Read())
                {
                    lstVicationType.Add(new VicationType
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Name = Convert.ToString(dr["Name"])
                    });
                }
            }
            return lstVicationType;
        }
        public int SaveEmployeeVicationInfo(EmployeeVicationInfo model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("Result",""),
                new SqlParameter("ID",model.ID),
                new SqlParameter("VicationTypeID",model.VicationTypeID),
                new SqlParameter("YearFrom",model.YearFrom),
                new SqlParameter("YearTo",model.YearTo),
                new SqlParameter("DayAllow",model.DayAllow),
                new SqlParameter("DayPaid",model.DayPaid),
                new SqlParameter("EmployeeID",model.EmployeeID),
                new SqlParameter("CreatedBy",model.CreatedBy)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            return SQL.ExceuteStoreProcedure("SaveEmployeeVicationInfo", param, true);
        }
        public List<EmployeeVicationInfo> GetEmployeeVicationInfo(int EmployeeID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("EmployeeID",EmployeeID)
            };
            return GetEmployeeVicationInfo(param);
        }
        public EmployeeVicationInfo GetEmployeeVicationInfoByID(int ID)
        {
            List<EmployeeVicationInfo> EmployeeVicationInfo = new List<EmployeeVicationInfo>();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            return GetEmployeeVicationInfo(param).FirstOrDefault();
        }
        private List<EmployeeVicationInfo> GetEmployeeVicationInfo(SqlParameter[] param)
        {
            List<EmployeeVicationInfo> employeeVicationInfo = new List<EmployeeVicationInfo>();
            using (SqlDataReader dr = SQL.GetDataReader("GetEmployeeVicationInfo", param, true))
            {
                while (dr.Read())
                {
                    employeeVicationInfo.Add(new EmployeeVicationInfo
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        VicationTypeID = Convert.ToInt32(dr["VicationTypeID"]),
                        VicationType = Convert.ToString(dr["VicationType"]),
                        YearFrom = Convert.ToInt32(dr["YearFrom"]),
                        YearTo = Convert.ToInt32(dr["YearTo"]),
                        DayAllow = Convert.ToInt32(dr["DayAllow"]),
                        DayPaid = Convert.ToInt32(dr["DayPaid"]),
                        EmployeeID = Convert.ToInt32(dr["EmployeeID"]),
                        CreatedBy = Convert.ToInt32(dr["CreatedBy"]),
                        CreatedUser = Convert.ToString(dr["CreatedUser"]),
                        CreatedDate = CommonHelper.DateToString(dr["CreatedDate"])
                    });
                }
            }
            return employeeVicationInfo;
        }
        public void DeleteEmployeeVicationInfo(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("DeleteEmployeeVicationInfo", param, false);
        }
        #endregion

        #region EmployeeDisciplinaryDetail
        public List<DisciplinaryInfraction> GetDisciplinaryInfractionType()
        {
            List<DisciplinaryInfraction> lstDisciplinaryInfraction = new List<DisciplinaryInfraction>();
            using (SqlDataReader dr = SQL.GetDataReader("GetMasterDisciplinaryInfraction", true))
            {
                while (dr.Read())
                {
                    lstDisciplinaryInfraction.Add(new DisciplinaryInfraction
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Expectations = Convert.ToString(dr["Expectations"]),
                        Infraction = Convert.ToString(dr["Infraction"])
                    });
                }
            }
            return lstDisciplinaryInfraction;
        }
        public List<DisciplinaryWarning> GetDisciplinaryWarningType()
        {
            List<DisciplinaryWarning> lstDisciplinaryWarning = new List<DisciplinaryWarning>();
            using (SqlDataReader dr = SQL.GetDataReader("GetMasterDisciplinaryWarning", true))
            {
                while (dr.Read())
                {
                    lstDisciplinaryWarning.Add(new DisciplinaryWarning
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        WarningType = Convert.ToString(dr["WarningType"])
                    });
                }
            }
            return lstDisciplinaryWarning;
        }
        public int SaveEmployeeDisciplinaryDetail(EmployeeDisciplinaryDetail model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("Result",""),
                new SqlParameter("ID",model.ID),
                new SqlParameter("DisciplinaryDate",model.DisciplinaryDate),
                new SqlParameter("DisciplinaryInfractionID",model.DisciplinaryInfractionID),
                new SqlParameter("Specify",model.Specify),
                new SqlParameter("Expectations",model.Expectations),
                new SqlParameter("WarningTypeID",model.WarningTypeID),
                new SqlParameter("InfractionDetails",model.InfractionDetails),
                new SqlParameter("Status",model.Status),
                new SqlParameter("FilePath",model.FilePath),
                new SqlParameter("RequireFollowUp",model.RequireFollowUp),
                new SqlParameter("NoOfWeek",model.NoOfWeek),
                new SqlParameter("NumberOfRepetition",model.NumberOfRepetition),
                new SqlParameter("EmployeeID",model.EmployeeID),
                new SqlParameter("CreatedBy",model.CreatedBy)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            int result = SQL.ExceuteStoreProcedure("SaveEmpDisciplinaryDetail", param, true);
            if (result > 0)
                _commonServices.SaveFileRecord(model.FilePath, model.EmployeeID, "Disciplinary");
            return result;
        }
        public List<EmployeeDisciplinaryDetail> GetDisciplinaryDetailByCompanyID(int CompanyID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",CompanyID)
            };
            return GetEmployeeDisciplinaryDetail(param);
        }
        public List<EmployeeDisciplinaryDetail> GetEmployeeDisciplinaryDetail(int EmployeeID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("EmployeeID",EmployeeID)
            };
            return GetEmployeeDisciplinaryDetail(param);
        }
        public List<EmployeeDisciplinaryDetail> GetFilterDisciplinaryDetail(FilterDto model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("EmployeeID",model.EmoplyeeID),
                new SqlParameter("FromDate",model.FromDate),
                new SqlParameter("ToDate",model.ToDate),
                new SqlParameter("SearchText",model.SearchText),
            };
            return GetEmployeeDisciplinaryDetail(param);
        }
        public EmployeeDisciplinaryDetail GetEmployeeDisciplinaryDetailByID(int ID)
        {
            List<EmployeeDisciplinaryDetail> EmployeeDisciplinaryDetail = new List<EmployeeDisciplinaryDetail>();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            return GetEmployeeDisciplinaryDetail(param).FirstOrDefault();
        }
        private List<EmployeeDisciplinaryDetail> GetEmployeeDisciplinaryDetail(SqlParameter[] param)
        {
            List<EmployeeDisciplinaryDetail> employeeDisciplinaryDetail = new List<EmployeeDisciplinaryDetail>();
            using (SqlDataReader dr = SQL.GetDataReader("GetEmpDisciplinaryDetail", param, true))
            {
                while (dr.Read())
                {
                    employeeDisciplinaryDetail.Add(new EmployeeDisciplinaryDetail
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        DisciplinaryDate = CommonHelper.DateToString(dr["DisciplinaryDate"]),
                        DisciplinaryInfractionID = Convert.ToInt32(dr["DisciplinaryInfractionID"]),
                        Infraction = Convert.ToString(dr["Infraction"]),
                        Specify = Convert.ToString(dr["Specify"]),
                        Expectations = Convert.ToString(dr["Expectations"]),
                        WarningTypeID = Convert.ToInt32(dr["WarningTypeID"]),
                        WarningType = Convert.ToString(dr["WarningType"]),
                        InfractionDetails = Convert.ToString(dr["InfractionDetails"]),
                        Status = Convert.ToString(dr["Status"]),
                        FilePath = Convert.ToString(dr["FilePath"]),
                        RequireFollowUp = Convert.ToBoolean(dr["RequireFollowUp"]),
                        NoOfWeek = Convert.ToInt32(dr["NoOfWeek"]),
                        NumberOfRepetition = Convert.ToInt32(dr["NumberOfRepetition"]),
                        EmployeeID = Convert.ToInt32(dr["EmployeeID"]),
                        CreatedBy = Convert.ToInt32(dr["CreatedBy"]),
                        CreatedUser = Convert.ToString(dr["CreatedUser"]),
                        ModifiedUser = Convert.ToString(dr["ModifiedUser"]),
                        CreatedDate = CommonHelper.DateToString(dr["CreatedDate"]),
                        ModifiedDate = Convert.ToString(dr["ModifiedDate"]),
                        EmployeeName = Convert.ToString(dr["EmployeeName"]),
                        CompanyName=Convert.ToString(dr["CompanyName"]),
                        Address = Convert.ToString(dr["Address"]),
                        State = Convert.ToString(dr["State"]),
                        City = Convert.ToString(dr["City"]),
                        PostalCode = Convert.ToString(dr["PinCode"]),
                        CompanyLogo=Convert.ToString(dr["InvoiceLogoUrl"]),
                        StatusValue=Convert.ToString(dr["StatusValue"]),
                        CompanyAddress = Convert.ToString(dr["CompanyAddress"]),
                        EmployeePhone = Convert.ToString(dr["EmployeePhone"]),
                        CompanyID = Convert.ToInt32(dr["CompanyID"])
                    });
                }
            }
            return employeeDisciplinaryDetail;
        }
        public void DeleteEmployeeDisciplinaryDetail(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("DeleteEmpDisciplinaryDetail", param, false);
        }
        public void DisciplinaryDetailCompleted(int Id)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",Id)
            };
            SQL.ExceuteStoreProcedure("DisciplinaryDetailCompleted", param, false);
        }
        #endregion

        #region EmployeeTerminationDetail
        public bool SendNotoficationTermination(int EmployeeID)
        {
            bool result = true;
            var objDetail = GetCompanyHRDetail(EmployeeID);
            if (objDetail != null)
            {
                    var emailtemplate = GetEmailTemplate(Constants.HRTerminationNotification);
                    string emailbody = emailtemplate.Body.Replace("{Name}", objDetail.Name).Replace("{EmployeeName}", objDetail.EmployeeName)
                        .Replace("{Email}", objDetail.EmployeeEmail).Replace("{EmployeeID}", objDetail.EmployeeID);
                    CommonHelper.SendEmail(objDetail.Email, emailtemplate.Subject, emailbody);
            }
            else { result = false; }
            return result;
        }

        private HumanResource GetCompanyHRDetail(int EmployeeID)
        {
            HumanResource objHR = null;
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("EmployeeID", EmployeeID) };
            using (SqlDataReader dr = SQL.GetDataReader("GetHRDetails", param, true))
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        objHR = new HumanResource {
                            Name=Convert.ToString(dr["Name"]),
                            Email= Convert.ToString(dr["Email"]),
                            CompanyName = Convert.ToString(dr["CompanyName"]),
                            EmployeeName = Convert.ToString(dr["EmployeeName"]),
                            EmployeeEmail = Convert.ToString(dr["EmployeeEmail"]),
                            EmployeeID=Convert.ToString("EmployeeID")
                        }; 
                    }
                }
            }
            return objHR;
        }

        public List<TerminationReasonType> GetTerminationReasonType()
        {
            List<TerminationReasonType> lstTerminationReasonType = new List<TerminationReasonType>();
            using (SqlDataReader dr = SQL.GetDataReader("GetTerminationReasionType", true))
            {
                while (dr.Read())
                {
                    lstTerminationReasonType.Add(new TerminationReasonType
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        TerminationType = Convert.ToString(dr["TerminationType"])
                    });
                }
            }
            return lstTerminationReasonType;
        }
        public int SaveEmployeeTerminationDetail(EmployeeTerminationDetail model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("Result",""),
                new SqlParameter("ID",model.ID),
                new SqlParameter("LastDayWorkDate",model.LastDayWorkDate),
                new SqlParameter("BenefitsCeaseDate",model.BenefitsCeaseDate),
                new SqlParameter("TerminationPay",model.TerminationPay),
                new SqlParameter("TerminationPayPaid",model.TerminationPayPaid),
                new SqlParameter("Severance",model.Severance),
                new SqlParameter("SeverancePaid",model.SeverancePaid),
                new SqlParameter("Status",model.Status),
                new SqlParameter("TerminationReasonID",model.TerminationTypeID),
                new SqlParameter("FinalPay",model.FinalPay),
                new SqlParameter("FilePath",model.FilePath),
                new SqlParameter("EmployeeID",model.EmployeeID),
                new SqlParameter("CreatedBy",model.CreatedBy)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            return SQL.ExceuteStoreProcedure("SaveEmpTerminationDetail", param, true);
        }
        public List<EmployeeTerminationDetail> GetTerminationDetailByCompanyID(int CompanyID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",CompanyID)
            };
            return GetEmployeeTerminationDetail(param);
        }
        public List<EmployeeTerminationDetail> GetEmployeeTerminationDetail(int EmployeeID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("EmployeeID",EmployeeID)
            };
            return GetEmployeeTerminationDetail(param);
        }
        public EmployeeTerminationDetail GetEmployeeTerminationDetailByID(int ID)
        {
            List<EmployeeTerminationDetail> EmployeeTerminationDetail = new List<EmployeeTerminationDetail>();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            return GetEmployeeTerminationDetail(param).FirstOrDefault();
        }
        public List<EmployeeTerminationDetail> GetFilterTerminationDetail(FilterDto model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("EmployeeID",model.EmoplyeeID),
                new SqlParameter("FromDate",model.FromDate),
                new SqlParameter("ToDate",model.ToDate),
                new SqlParameter("SearchText",model.SearchText),
            };
            return GetEmployeeTerminationDetail(param);
        }
        private List<EmployeeTerminationDetail> GetEmployeeTerminationDetail(SqlParameter[] param)
        {
            List<EmployeeTerminationDetail> employeeTerminationDetail = new List<EmployeeTerminationDetail>();
            using (SqlDataReader dr = SQL.GetDataReader("GetEmpTerminationDetail", param, true))
            {
                while (dr.Read())
                {
                    employeeTerminationDetail.Add(new EmployeeTerminationDetail
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        LastDayWorkDate = CommonHelper.DateToString(dr["LastDayWorkDate"]),
                        BenefitsCeaseDate = CommonHelper.DateToString(dr["BenefitsCeaseDate"]),
                        TerminationPay = Convert.ToInt32(dr["TerminationPay"]),
                        TerminationPayPaid = Convert.ToInt32(dr["TerminationPayPaid"]),
                        Severance = Convert.ToInt32(dr["Severance"]),
                        SeverancePaid = Convert.ToInt32(dr["SeverancePaid"]),
                        Status = Convert.ToString(dr["Status"]),
                        StatusValue = Convert.ToString(dr["StatusValue"]),
                        TerminationTypeID = Convert.ToInt32(dr["TerminationTypeID"]),
                        TerminationType = Convert.ToString(dr["TerminationType"]),
                        FinalPay = Convert.ToInt32(dr["FinalPay"]),
                        FilePath = Convert.ToString(dr["FilePath"]),
                        EmployeeID = Convert.ToInt32(dr["EmployeeID"]),
                        CreatedBy = Convert.ToInt32(dr["CreatedBy"]),
                        CreatedUser = Convert.ToString(dr["CreatedUser"]),
                        ModifiedUser = Convert.ToString(dr["ModifiedUser"]),
                        CreatedDate = CommonHelper.DateToString(dr["CreatedDate"]),
                        ModifiedDate = CommonHelper.DateToString(dr["ModifiedDate"]),
                        Completed = Convert.ToBoolean(dr["Completed"]),
                        EmployeeName = Convert.ToString(dr["EmployeeName"]),
                        Address = Convert.ToString(dr["Address"]),
                        City= Convert.ToString(dr["City"]),
                        PostalCode= Convert.ToString(dr["PinCode"]),
                        FinalPayValue=Convert.ToString(dr["FinalPayValue"]),
                        TerminationPayPaidValue=Convert.ToString(dr["TerminationPayPaidValue"]),
                        CompanyAddress = Convert.ToString(dr["CompanyAddress"]),
                        CompanyName = Convert.ToString(dr["CompanyName"]),
                        Phone = Convert.ToString(dr["Phone"]),
                        OtherInfo = Convert.ToString(dr["OtherInfo"]),
                        Designation = Convert.ToString(dr["Designation"]),
                        ReportingManager=Convert.ToString(dr["ReportingManager"]),
                        CompanyID = Convert.ToInt32(dr["CompanyID"])
                    });
                }
            }
            return employeeTerminationDetail;
        }
        public void DeleteEmployeeTerminationDetail(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("DeleteEmpTerminationDetail", param, false);
        }
        public void TerminationDetailCompleted(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("TerminationDetailCompleted", param, false);
        }
        #endregion

        #region Layoffs
        public int SaveLayoffsDetail(LayOffs model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("Result",""),
                new SqlParameter("ID",model.ID),
                new SqlParameter("LastDayWork",model.LastDayWork),
                new SqlParameter("ReturnDate",model.ReturnDate),
                new SqlParameter("PayBenifit",model.PayBenifit),
                new SqlParameter("BenifitType",model.BenifitTypeID),
                new SqlParameter("FilePath",model.FilePath),
                new SqlParameter("EmployeeID",model.EmployeeID),
                new SqlParameter("CreatedBy",model.CreatedBy)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            int result= SQL.ExceuteStoreProcedure("SaveLayOffs", param, true);
            if (result > 0)
                _commonServices.SaveFileRecord(model.FilePath, model.EmployeeID, "LayOffs");
            return result;
        }
        public List<LayOffs> getLayoffsByCompanyID(int CompanyID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",CompanyID)
            };
            return GetLayOffList(param);
        }
        public List<LayOffs> getLayoffsDetail(int EmployeeID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("EmployeeID",EmployeeID)
            };
            return GetLayOffList(param);
        }
        public LayOffs getLayoffsByID(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            return GetLayOffList(param).FirstOrDefault();
        }
        private List<LayOffs> GetLayOffList(SqlParameter[] param)
        {
            List<LayOffs> objlst = new List<LayOffs>();
            using (SqlDataReader dr = SQL.GetDataReader("GetLayOffList", param, true))
            {
                while (dr.Read())
                {

                    objlst.Add(new LayOffs {
                        ID =Convert.ToInt32(dr["ID"]),
                        LastDayWork=CommonHelper.DateToString(dr["LastDayWork"]),
                        ReturnDate = CommonHelper.DateToString(dr["ReturnDate"]),
                        EmployeeID=Convert.ToInt32(dr["EmployeeID"]),
                        EmployeeName = Convert.ToString(dr["EmployeeName"]),
                        PayBenifit = Convert.ToBoolean(dr["PayBenifit"]),
                        BenifitTypeID = Convert.ToInt32(dr["BenifitTypeID"]),
                        BenifitTypeValue=Convert.ToString(dr["BenifitTypeValue"]),
                        FilePath=Convert.ToString(dr["FilePath"]),
                        CreatedBy=Convert.ToInt32(dr["CreatedBy"]),
                        Completed=Convert.ToBoolean(dr["Completed"])
                    });
                }
            }
            return objlst;
        }
        public LayoffEmployee GetLayOffEmpDetails(int ID)
        {
            LayoffEmployee employee = null;
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("ID", ID) };
            using (SqlDataReader dr = SQL.GetDataReader("GetLayoffEmployeeDetails", param, true))
            {
                while (dr.Read())
                {
                    employee = new LayoffEmployee();
                    employee.FirstName = dr["FirstName"].ToString();
                    employee.LastName = dr["LastName"].ToString();
                    employee.Phone = dr["Phone"].ToString();
                    employee.Address = Convert.ToString(dr["Address"]);
                    employee.PinCode = Convert.ToString(dr["PinCode"]);
                    employee.CompanyName = Convert.ToString(dr["CompanyName"]);
                    employee.Country = Convert.ToString(dr["Country"]);
                    employee.State = Convert.ToString(dr["State"]);
                    employee.City = Convert.ToString(dr["CIty"]);
                    employee.CompanyAddress = Convert.ToString(dr["CompanyAddress"]);
                    employee.Owner = Convert.ToString(dr["CompanyOwner"]);
                    employee.LastWorkDate = CommonHelper.DateToString(dr["LastWorkDate"]);
                }
            }
            return employee;
        }
        public void DeleteLayoffsByID(int Id)
        {
            SQL.ExecuteQuery("UPDATE LayOffs SET Isdeleted=1 WHERE ID=" + Id);
        }
        public void LayoffsCompleted(int Id)
        {
            SQL.ExecuteQuery("UPDATE LayOffs SET Complated=1 WHERE ID=" + Id);
        }

        #endregion

        #region EmployeeOfferDetails
        public List<OfferBenefitsType> GetOfferBenefitsType()
        {
            List<OfferBenefitsType> lstOfferBenefitsType = new List<OfferBenefitsType>();
            using (SqlDataReader dr = SQL.GetDataReader("GetOfferBenefitsType", true))
            {
                while (dr.Read())
                {
                    lstOfferBenefitsType.Add(new OfferBenefitsType
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Type = Convert.ToString(dr["BenefitsType"])
                    });
                }
            }
            return lstOfferBenefitsType;
        }
        public List<OfferVacationType> GetOfferVacationType()
        {
            List<OfferVacationType> lstOfferVacationType = new List<OfferVacationType>();
            using (SqlDataReader dr = SQL.GetDataReader("GetOfferVacationType", true))
            {
                while (dr.Read())
                {
                    lstOfferVacationType.Add(new OfferVacationType
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Type = Convert.ToString(dr["VacationType"])
                    });
                }
            }
            return lstOfferVacationType;
        }
        public List<EmploymentType> GetEmploymentType()
        {
            List<EmploymentType> lstEmploymentType = new List<EmploymentType>();
            using (SqlDataReader dr = SQL.GetDataReader("GetEmploymentType", true))
            {
                while (dr.Read())
                {
                    lstEmploymentType.Add(new EmploymentType
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Type = Convert.ToString(dr["EmploymentType"])
                    });
                }
            }
            return lstEmploymentType;
        }
        public List<CompensationType> GetCompensationType()
        {
            List<CompensationType> lstCompensationType = new List<CompensationType>();
            using (SqlDataReader dr = SQL.GetDataReader("GetCompensationType", true))
            {
                while (dr.Read())
                {
                    lstCompensationType.Add(new CompensationType
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Type = Convert.ToString(dr["CompensationType"])
                    });
                }
            }
            return lstCompensationType;
        }
        public List<TerminationClause> GetTerminationClause()
        {
            List<TerminationClause> lstTerminationClause = new List<TerminationClause>();
            using (SqlDataReader dr = SQL.GetDataReader("GetTerminationClause", true))
            {
                while (dr.Read())
                {
                    lstTerminationClause.Add(new TerminationClause
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Name = Convert.ToString(dr["Name"])
                    });
                }
            }
            return lstTerminationClause;
        }
        public List<WorkingDayType> GetWorkingDayType()
        {
            List<WorkingDayType> lstWorkingDayType = new List<WorkingDayType>();
            using (SqlDataReader dr = SQL.GetDataReader("GetWorkingDayType", true))
            {
                while (dr.Read())
                {
                    lstWorkingDayType.Add(new WorkingDayType
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        DayType = Convert.ToString(dr["WorkingDayType"])
                    });
                }
            }
            return lstWorkingDayType;
        }
        public int SaveEmployeeOfferDetail(EmployeeOfferDetail model)
        {
            if (model.IsOnBoarding && model.ID==0)
                model.EmployeeID = SaveOnboardingDetails(model);
                
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("Result",""),
                new SqlParameter("ID",model.ID),
                new SqlParameter("EmploymentTypeID",model.EmploymentTypeID),
                new SqlParameter("Startdate",model.Startdate),
                new SqlParameter("CompensationTypeID",model.CompensationTypeID),
                new SqlParameter("CompensationAmount",model.CompensationAmount),
                new SqlParameter("WorkingDayTypeID",model.WorkingDayTypeID),
                new SqlParameter("WorkingDayOther",model.OtherInfo),
                new SqlParameter("StartTime",model.StartTime),
                new SqlParameter("EndTime",model.EndTime),
                new SqlParameter("Status",model.Status),
                new SqlParameter("JobTitle",model.JobTitle),
                new SqlParameter("OfferBenefitsTypeID",model.OfferBenefitsTypeID),
                new SqlParameter("OfferVacationTypeID",model.OfferVacationTypeID),
                new SqlParameter("OfferAcceptingDate",model.OfferAcceptingDate),
                new SqlParameter("ReportingManagerID",model.ReportingManagerID),
                new SqlParameter("FilePath",model.FilePath),
                new SqlParameter("EmployeeID",model.EmployeeID),
                new SqlParameter("CreatedBy",model.CreatedBy),
                new SqlParameter("PayCommission",model.PayCommission),
                new SqlParameter("CommissionPrevision",model.CommissionPrevision),
                new SqlParameter("TerminationClauseID",model.TerminationClauseID),
                new SqlParameter("TerminationDescription",model.TerminationDescription),
                new SqlParameter("OtherClause",model.OtherClause),
                new SqlParameter("ExpiryDate",model.ExpiryDate)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            int result = SQL.ExceuteStoreProcedure("SaveEmpOfferDetails", param, true);
            if (result > 0 && !string.IsNullOrEmpty(model.FilePath))
                _commonServices.SaveFileRecord(model.FilePath, model.EmployeeID, "OfferLetter");
            return result;
        }
        private int SaveOnboardingDetails(EmployeeOfferDetail model)
        {
            int returnVal = 0;
            string EmployeeID = GerateEmployeeID(model.CompanyID);
            string Password = CommonHelper.AutogenratePassord();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("@Result",""),
                new SqlParameter("FirstName",model.FirstName),
                new SqlParameter("LastName",model.LastName),
                new SqlParameter("Email",model.Email),
                new SqlParameter("Password",Password),
                new SqlParameter("EmployeeID",EmployeeID),
                new SqlParameter("JoiningDate",DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss")),
                new SqlParameter("CountryID",model.CountryID),
                new SqlParameter("StateID",model.StateID),
                new SqlParameter("CityID",model.CityID),
                new SqlParameter("CompanyID",model.CompanyID),
                new SqlParameter("CreatedBy",model.CreatedBy)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            returnVal = SQL.ExceuteStoreProcedure("SaveUpdateEmployee", param, true);

            if (returnVal > 0)
            {
                var objcompany = _companyServices.companyDetail(model.CompanyID);
                //var Link = ConfigurationManager.AppSettings["EmailConfirmUrl"] + "/accountconfirmation/" + company.CompanyGuid;
                var emailtemplate = GetEmailTemplate(Constants.EmployeeSignupTemplate);
                string emailbody = emailtemplate.Body.Replace("{{CompanyName}}", objcompany.CompanyName).Replace("{{EmployeeName}}", (model.FirstName + " " + model.LastName)).Replace("{{Email}}", model.Email).Replace("{{Password}}", CommonHelper.Decrypt(Password));
                CommonHelper.SendEmail(model.Email, emailtemplate.Subject.Replace("{{CompanyName}}", objcompany.CompanyName), emailbody);
            }
            return returnVal;
        }
        public List<EmployeeOfferDetail> GetEmployeeOfferDetailsByCompanyID(int CompanyID, string SearchText)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",CompanyID),
                new SqlParameter("SearchText",SearchText)
            };
            return GetEmployeeOfferDetailList(param);
        }
        public List<EmployeeOfferDetail> GetEmployeeOfferDetails(int EmployeeID, string SearchText)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("EmployeeID",EmployeeID),
                new SqlParameter("SearchText",SearchText)
            };
            return GetEmployeeOfferDetailList(param);
        }
        public EmployeeOfferDetail GetEmployeeOfferDetailsByID(int ID)
        {
            List<EmployeeOfferDetail> EmployeeOfferDetails = new List<EmployeeOfferDetail>();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            return GetEmployeeOfferDetailList(param).FirstOrDefault();
        }
        private List<EmployeeOfferDetail> GetEmployeeOfferDetailList(SqlParameter[] param)
        {
            List<EmployeeOfferDetail> employeeOfferDetail = new List<EmployeeOfferDetail>();
            using (SqlDataReader dr = SQL.GetDataReader("GetEmpOfferDetails", param, true))
            {
                while (dr.Read())
                {
                    employeeOfferDetail.Add(new EmployeeOfferDetail
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        EmploymentTypeID = Convert.ToInt32(dr["EmploymentTypeID"]),
                        EmploymentType = Convert.ToString(dr["EmploymentType"]),
                        Startdate = CommonHelper.DateToString(dr["Startdate"]),
                        CompensationTypeID = Convert.ToInt32(dr["CompensationTypeID"]),
                        CompensationType = Convert.ToString(dr["CompensationType"]),
                        CompensationAmount = Convert.ToDecimal(dr["CompensationAmount"]),
                        OtherInfo = Convert.ToString(dr["WorkingDayOther"]),
                        WorkingDayType = Convert.ToString(dr["WorkingDayType"]),
                        WorkingDayTypeID = Convert.ToInt32(dr["WorkingDayTypeID"]),
                        StartTime = Convert.ToString(dr["StartTime"]),
                        EndTime = Convert.ToString(dr["EndTime"]),
                        Status = Convert.ToString(dr["Status"]),
                        JobTitle = Convert.ToString(dr["JobTitle"]),
                        OfferBenefitsTypeID = Convert.ToInt32(dr["OfferBenefitsTypeID"]),
                        OfferVacationTypeID = Convert.ToInt32(dr["OfferVacationTypeID"]),
                        OfferBenefitsType = Convert.ToString(dr["OfferBenefitsType"]),
                        OfferVacationsType = Convert.ToString(dr["OfferVacationType"]),
                        OfferAcceptingDate = CommonHelper.DateToString(dr["OfferAcceptingDate"]),
                        ReportingManagerID = Convert.ToInt32(dr["ReportingManagerID"]),
                        FilePath = Convert.ToString(dr["FIlePath"]),
                        EmployeeID = Convert.ToInt32(dr["EmployeeID"]),
                        CreatedUser = Convert.ToString(dr["CreatedUser"]),
                        CreatedBy = Convert.ToInt32(dr["CreatedBy"]),
                        PayCommission = Convert.ToBoolean(dr["PayCommission"]),
                        CommissionPrevision = Convert.ToString(dr["CommissionPrevision"]),
                        TerminationClauseID = Convert.ToInt32(dr["TerminationClauseID"]),
                        TerminationDescription = Convert.ToString(dr["TerminationDescription"]),
                        OtherClause = Convert.ToBoolean(dr["OtherClause"]),
                        OfferAccepted = Convert.ToBoolean(dr["OfferAccepted"]),
                        ModifiedUser = Convert.ToString(dr["ModifiedUser"]),
                        CreatedDate = CommonHelper.DateToString(dr["CreatedDate"]),
                        ModifiedDate = CommonHelper.DateToString(dr["ModifiedDate"]),
                        EmployeeName = Convert.ToString(dr["EmployeeName"]),
                        CompanyName = Convert.ToString(dr["CompanyName"]),
                        City = Convert.ToString(dr["City"]),
                        State= Convert.ToString(dr["State"]),
                        Address= Convert.ToString(dr["Address"]),
                        PostalCode = Convert.ToString(dr["PinCode"]),
                        ReportingManager=Convert.ToString(dr["ReportingManager"]),
                        CompanyID = Convert.ToInt32(dr["CompanyID"]),
                        ExpiryDate = CommonHelper.DateToString(dr["ExpiryDate"]),
                        StatusValue= Convert.ToString(dr["StatusValue"])
                    });
                }
            }
            return employeeOfferDetail;
        }
        public void DeleteEmployeeOfferDetails(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("DeleteEmpOfferDetails", param, false);
        }
        public void OfferAccepted(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            DataTable dt = SQL.GetDt("OfferAccepted", param, true);
            if (dt != null && dt.Rows.Count > 0)
            {
                var emailtemplate = GetEmailTemplate(Constants.EmployeeOfferAcceptance);
                string emailbody = emailtemplate.Body.Replace("{{CompanyName}}", dt.Rows[0]["CompanyName"].ToString()).Replace("{{EmployeeName}}", dt.Rows[0]["EmployeeName"].ToString())
                    .Replace("{{RecipientName}}", dt.Rows[0]["RecipientName"].ToString()).Replace("{{JobTitle}}", dt.Rows[0]["JobTitle"].ToString());
                CommonHelper.SendEmail(dt.Rows[0]["RecipientEmail"].ToString(), emailtemplate.Subject, emailbody);
            }
        }
        public void OfferDecline(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExecuteQuery("Update EmpOfferDetails SET Status=2 WHERE ID=@ID", param);
        }
        #endregion

        #region Accindent detail

        public List<AccidentType> GetAccidentType()
        {
            List<AccidentType> lstAccidentType = new List<AccidentType>();
            using (SqlDataReader dr = SQL.GetDataReader("GetAccidentType", true))
            {
                while (dr.Read())
                {
                    lstAccidentType.Add(new AccidentType
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Type = Convert.ToString(dr["Type"])
                    });
                }
            }
            return lstAccidentType;
        }
        public List<DiseasesType> GetDiseasesType()
        {
            List<DiseasesType> lstDiseasesType = new List<DiseasesType>();
            using (SqlDataReader dr = SQL.GetDataReader("GetIllnessType", true))
            {
                while (dr.Read())
                {
                    lstDiseasesType.Add(new DiseasesType
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Type = Convert.ToString(dr["Type"])
                    });
                }
            }
            return lstDiseasesType;
        }
        public List<BodyPartType> GetBodyPartType()
        {
            List<BodyPartType> lstBodyPartType = new List<BodyPartType>();
            using (SqlDataReader dr = SQL.GetDataReader("GetBodyPart", true))
            {
                while (dr.Read())
                {
                    lstBodyPartType.Add(new BodyPartType
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Type = Convert.ToString(dr["BodyPartType"])
                    });
                }
            }
            return lstBodyPartType;
        }
        public List<AccidentDetail> GetAccidentDetailByCompanyID(int CompanyID, string SearchText = null)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("CompanyID",CompanyID),
                new SqlParameter("SearchText",SearchText)
            };
            return GetAccidentDetailList(param);
        }
        public List<AccidentDetail> GetAccidentDetail(int EmployeeID,string SearchText=null)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("EmployeeID",EmployeeID),
                new SqlParameter("SearchText",SearchText)
            };
            return GetAccidentDetailList(param);
        }
        public AccidentDetail GetAccidentDetailByID(int ID)
        {
            List<AccidentDetail> AccidentDetail = new List<AccidentDetail>();
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            return GetAccidentDetailList(param).FirstOrDefault();
        }
        private List<AccidentDetail> GetAccidentDetailList(SqlParameter[] param)
        {
            List<AccidentDetail> AccidentDetail = new List<AccidentDetail>();
            DataSet ds = SQL.GetDs("GetAccidentDetails", param, true);
            if (ds != null && ds.Tables.Count == 4)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    AccidentDetail.Add(new AccidentDetail
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        DateOfAccident = CommonHelper.DateToString(dr["DateOfAccident"]),
                        DateOfReported = CommonHelper.DateToString(dr["DateOfReported"]),
                        TimeOfAccident = Convert.ToString(dr["TimeOfAccident"]),
                        TimeOfReported = Convert.ToString(dr["TimeOfReported"]),
                        ReportedTo = Convert.ToInt32(dr["ReportedTo"]),
                        Description = Convert.ToString(dr["Description"]),
                        AccidentHappendEmployeerPremises = Convert.ToInt32(dr["AccidentHappendEmployeerPremises"]),
                        AccidentHappendEmployeerPremisesDesc = Convert.ToString(dr["AccidentHappendEmployeerPremisesDesc"]),
                        AccidentOutSideProvince = Convert.ToInt32(dr["AccidentOutSideProvince"]),
                        AccidentOutSideProvinceDesc = Convert.ToString(dr["AccidentOutSideProvinceDesc"]),
                        AnyWitness = Convert.ToInt32(dr["AnyWitness"]),
                        WitnessDesc = Convert.ToString(dr["WitnessDesc"]),
                        ResonposibleForAccident = Convert.ToInt32(dr["ResonposibleForAccident"]),
                        ResonposibleDescritption = Convert.ToString(dr["ResonposibleDescritption"]),
                        AwareAnyInjury = Convert.ToInt32(dr["AwareAnyInjury"]),
                        AwareAnyInjuryDesc = Convert.ToString(dr["AwareAnyInjuryDesc"]),
                        ConcernClaim = Convert.ToInt32(dr["ConcernClaim"]),
                        NameOfPerson = Convert.ToString(dr["NameOfPerson"]),
                        OfficeTitle = Convert.ToString(dr["OfficeTitle"]),
                        RegisterDate = CommonHelper.DateToString(dr["RegisterDate"]),
                        Telephone = Convert.ToString(dr["Telephone"]),
                        Ext = Convert.ToString(dr["Ext"]),
                        EmployeeID = Convert.ToInt32(dr["EmployeeID"]),
                        DiseasesList = DiseasesList(Convert.ToInt32(dr["EmployeeID"]), Convert.ToInt32(dr["ID"]), ds.Tables[1]),
                        AccidentTypeList = AccidentTypeList(Convert.ToInt32(dr["EmployeeID"]), Convert.ToInt32(dr["ID"]), ds.Tables[2]),
                        BodyPartMapList = BodyPartMapList(Convert.ToInt32(dr["EmployeeID"]), Convert.ToInt32(dr["ID"]), ds.Tables[3]),
                        EmployeeName = Convert.ToString(dr["EmployeeName"]),
                        CreatedUser = Convert.ToString(dr["CreatedUser"]),
                        ModifiedUser = Convert.ToString(dr["ModifiedUser"]),
                        CreatedDate = CommonHelper.DateToString(dr["CreatedDate"]),
                        ModifiedDate = CommonHelper.DateToString(dr["ModifiedDate"]),
                        ReportedManager = Convert.ToString(dr["ReportedManager"]),
                        Status = Convert.ToInt32(dr["Status"]),
                        StatusValue = Convert.ToString(dr["StatusValue"]),
                        EmployeeCode = Convert.ToString(dr["EmployeeCode"]),
                        CompanyID = Convert.ToInt32(dr["CompanyID"])
                    });
                }
            }
            return AccidentDetail;
        }
        private List<EmployeeDiseasesMap> DiseasesList(int employeeid, int id, DataTable dataTable)
        {
            List<EmployeeDiseasesMap> objlist = new List<EmployeeDiseasesMap>();
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                DataRow[] result = dataTable.Select("EmployeeID = '" + employeeid + "' AND AccidentDetailsID = '" + id + "'");
                foreach (DataRow row in result)
                {
                    objlist.Add(new EmployeeDiseasesMap { EmployeeID = employeeid, AccidentDetailId = id, DiseasesTypeID = Convert.ToInt32(row["IllnessTypeID"]),DiseasesType= Convert.ToString(row["DiseasesType"]) });
                }
            }
            return objlist;
        }
        private List<EmployeeAccidentTypeMap> AccidentTypeList(int employeeid, int id, DataTable dataTable)
        {
            List<EmployeeAccidentTypeMap> objlist = new List<EmployeeAccidentTypeMap>();
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                DataRow[] result = dataTable.Select("EmployeeID = '" + employeeid + "' AND AccidentDetailsID = '" + id + "'");
                foreach (DataRow row in result)
                {
                    objlist.Add(new EmployeeAccidentTypeMap { EmployeeID = employeeid, AccidentDetailId = id, AccidentTypeId = Convert.ToInt32(row["AccidentTypeId"]),AccidentType=Convert.ToString(row["AccidentType"]) });
                }
            }
            return objlist;
        }
        private List<EmployeeBodyPartMap> BodyPartMapList(int employeeid, int id, DataTable dataTable)
        {
            List<EmployeeBodyPartMap> objlist = new List<EmployeeBodyPartMap>();
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                DataRow[] result = dataTable.Select("EmployeeID = '" + employeeid + "' AND AccidentDetailsID = '" + id + "'");
                foreach (DataRow row in result)
                {
                    objlist.Add(new EmployeeBodyPartMap { EmployeeID = employeeid, AccidentDetailId = id, BodyPartId = Convert.ToInt32(row["BodyPartId"]), BodyPartType=Convert.ToString(row["BodyPartType"]) });
                }
            }
            return objlist;
        }
        public int SaveAccidentDetail(AccidentDetail model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("Return",""),
                new SqlParameter("ID",model.ID),
                new SqlParameter("DateOfAccident",model.DateOfAccident),
                new SqlParameter("DateOfReported",model.DateOfReported),
                new SqlParameter("TimeOfAccident",model.TimeOfAccident),
                new SqlParameter("TimeOfReported",model.TimeOfReported),
                new SqlParameter("ReportedTo",model.ReportedTo),
                new SqlParameter("Description",model.Description),
                new SqlParameter("AccidentHappendEmployeerPremises",model.AccidentHappendEmployeerPremises),
                new SqlParameter("AccidentHappendEmployeerPremisesDesc",model.AccidentHappendEmployeerPremisesDesc),
                new SqlParameter("AccidentOutSideProvince",model.AccidentOutSideProvince),
                new SqlParameter("AccidentOutSideProvinceDesc",model.AccidentOutSideProvinceDesc),
                new SqlParameter("AnyWitness",model.AnyWitness),
                new SqlParameter("WitnessDesc",model.WitnessDesc),
                new SqlParameter("ResonposibleForAccident",model.ResonposibleForAccident),
                new SqlParameter("ResonposibleDescritption",model.ResonposibleDescritption),
                new SqlParameter("AwareAnyInjury",model.AwareAnyInjury),
                new SqlParameter("AwareAnyInjuryDesc",model.AwareAnyInjuryDesc),
                new SqlParameter("ConcernClaim",model.ConcernClaim),
                new SqlParameter("NameOfPerson",model.NameOfPerson),
                new SqlParameter("OfficeTitle",model.OfficeTitle),
                new SqlParameter("RegisterDate",model.RegisterDate),
                new SqlParameter("Telephone",model.Telephone),
                new SqlParameter("Ext",model.Ext),
                new SqlParameter("EmployeeID",model.EmployeeID),
                new SqlParameter("CreatedBy",model.CreatedBy),
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            int AccidentID = SQL.ExceuteStoreProcedure("SaveAccidentDetails", param, true);

            if (AccidentID > 0)
            {
                string sqlQury = "Delete From EmployeeAccident WHERE EmployeeID=" + model.EmployeeID + " AND AccidentDetailsID=" + AccidentID + ";";
                foreach (var objAcc in model.AccidentTypeList)
                {
                    sqlQury += @" INSERT INTO EmployeeAccident(EmployeeID,AccidentTypeID,AccidentDetailsID)
                                 VALUES(" + model.EmployeeID + @"," + objAcc.AccidentTypeId + @"," + AccidentID + @"); ";
                }
                sqlQury += " Delete From EmployeeIllness WHERE EmployeeID=" + model.EmployeeID + " AND AccidentDetailsID=" + AccidentID + ";";
                foreach (var objDis in model.DiseasesList)
                {
                    sqlQury += @"  INSERT INTO EmployeeIllness(EmployeeID,IllnessTypeID,AccidentDetailsID)
                                 VALUES(" + model.EmployeeID + @"," + objDis.DiseasesTypeID + @"," + AccidentID + @");";
                }
                sqlQury += " Delete From EmployeeBodyPartInjury WHERE EmployeeID=" + model.EmployeeID + " AND AccidentDetailsID=" + AccidentID + ";";
                foreach (var objbody in model.BodyPartMapList)
                {
                    sqlQury += @" INSERT INTO EmployeeBodyPartInjury(EmployeeID,BodyPartID,AccidentDetailsID)
                                 VALUES(" + model.EmployeeID + @"," + objbody.BodyPartId + @"," + AccidentID + @");";
                }
                SQL.ExecuteQuery(sqlQury);
            }
            return AccidentID;
        }
        public List<TreatmentPlace> GetTreatmentPlace()
        {
            List<TreatmentPlace> lstTreatmentPlace = new List<TreatmentPlace>();
            using (SqlDataReader dr = SQL.GetDataReader("GetTreatmentPlace", true))
            {
                while (dr.Read())
                {
                    lstTreatmentPlace.Add(new TreatmentPlace
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        PlaceType = Convert.ToString(dr["PlaceType"])
                    });
                }
            }
            return lstTreatmentPlace;
        }
        public List<MedicalAwarness> GetMedicalAwarnessList()
        {
            List<MedicalAwarness> lstMedicalAwarness = new List<MedicalAwarness>();
            using (SqlDataReader dr = SQL.GetDataReader("GetMedicalAwarnessList", true))
            {
                while (dr.Read())
                {
                    lstMedicalAwarness.Add(new MedicalAwarness
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Name = Convert.ToString(dr["Name"])
                    });
                }
            }
            return lstMedicalAwarness;
        }
        public List<RateType> GetRateType()
        {
            List<RateType> lstRateType = new List<RateType>();
            using (SqlDataReader dr = SQL.GetDataReader("GetRateType", true))
            {
                while (dr.Read())
                {
                    lstRateType.Add(new RateType
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Name = Convert.ToString(dr["Name"])
                    });
                }
            }
            return lstRateType;
        }
        public List<WorkerJobType> GetWorkerJobType()
        {
            List<WorkerJobType> lstWorkerJobType = new List<WorkerJobType>();
            using (SqlDataReader dr = SQL.GetDataReader("GetWorkerJobType", true))
            {
                while (dr.Read())
                {
                    lstWorkerJobType.Add(new WorkerJobType
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Name = Convert.ToString(dr["Name"])
                    });
                }
            }
            return lstWorkerJobType;
        }
        public List<HealthCare> GetHealthcare(int AccidentDetailID)
        {
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("AccidentDetailID", AccidentDetailID) };
            List<HealthCare> lstHealthCare = new List<HealthCare>();
            using (SqlDataReader dr = SQL.GetDataReader("GetHealthcare", param, true))
            {
                while (dr.Read())
                {
                    lstHealthCare.Add(new HealthCare
                    {
                        AccidentDetailID = Convert.ToInt32(dr["AccidentDetailID"]),
                        TreatedPlaceID = Convert.ToInt32(dr["TreatedPlaceID"]),
                        ReceiveHealthCareForInjury = Convert.ToBoolean(dr["ReceiveHealthCareForInjury"]),
                        HealthCareDate = CommonHelper.DateToString(dr["HealthCareDate"]),
                        ReceivedHealthCareDate = Convert.ToString(dr["EmployerlearnReceivedHealthCare"]),
                        SpecifyOther = Convert.ToString(dr["SpecifyOther"]),
                        AttendPersonAddress = Convert.ToString(dr["AttendPersonAddress"]),
                        AwarenessOfIllnessID = Convert.ToInt32(dr["AwarenessOfIllnessID"]),
                        WorkLimitation = Convert.ToBoolean(dr["WorkLimitation"]),
                        DiscusionWithWoker = Convert.ToBoolean(dr["DiscusionWithWoker"]),
                        AnyOfferToWorker = Convert.ToBoolean(dr["AnyOfferToWorker"]),
                        ResponsibleForArrangingWorker = Convert.ToBoolean(dr["ResponsibleForArangingWorker"]),
                        PersonName = Convert.ToString(dr["PersonName"]),
                        Telephone = Convert.ToString(dr["Telephone"]),
                        Ext = Convert.ToString(dr["Ext"]),
                        IsWorkerJobType = Convert.ToInt32(dr["IsWorkerJobType"]),
                        RatePay = Convert.ToDecimal(dr["RatePay"]),
                        RatePayTypeID = Convert.ToInt32(dr["RatePayTypeID"]),
                        RatePayOther = Convert.ToString(dr["RatePayOther"]),
                        LostTimeDate = CommonHelper.DateToString(dr["LostTimeDate"]),
                        ReturnDate = CommonHelper.DateToString(dr["LostTimeDate"]),
                        ShiftType = Convert.ToInt32(dr["ShiftType"]),
                        InforConfirmBy = Convert.ToInt32(dr["InforConfirmBy"]),
                        ConfirmPersonName = Convert.ToString(dr["ConfirmPersonName"]),
                        WorkerLostTimeDate = CommonHelper.DateToString(dr["WorkerLostTimeDate"]),
                        ReturnToWorkDate = CommonHelper.DateToString(dr["ReturnToWorkDate"]),
                        WorkType = Convert.ToBoolean(dr["WorkType"]),
                        LostTimeConfirmBy = Convert.ToBoolean(dr["LostTimeConfirmBy"]),
                        LostTime_Name = Convert.ToString(dr["LostTime_Name"]),
                        LostTime_Telephone = Convert.ToString(dr["LostTime_Telephone"]),
                        LostTime_Ext = Convert.ToString(dr["LostTime_Ext"]),
                        AttchedCopy= Convert.ToBoolean(dr["AttchedCopy"]),
                        IsAccepted = Convert.ToBoolean(dr["IsAccepted"])
                    });
                }
            }
            return lstHealthCare;
        }
        public void SaveHealthCare(HealthCare model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("AccidentDetailID",model.AccidentDetailID),
                new SqlParameter("ReceiveHealthCareForInjury",model.ReceiveHealthCareForInjury),
                new SqlParameter("TreatedPlaceID",model.TreatedPlaceID),
                new SqlParameter("HealthCareDate",model.HealthCareDate),
                new SqlParameter("EmployerlearnReceivedHealthCare",model.ReceivedHealthCareDate),
                new SqlParameter("SpecifyOther",model.SpecifyOther),
                new SqlParameter("AttendPersonAddress",model.AttendPersonAddress),
                new SqlParameter("AwarenessOfIllnessID",model.AwarenessOfIllnessID),
                new SqlParameter("WorkLimitation",model.WorkLimitation),
                new SqlParameter("DiscusionWithWoker",model.DiscusionWithWoker),
                new SqlParameter("AnyOfferToWorker",model.AnyOfferToWorker),
                new SqlParameter("ResponsibleForArangingWorker",model.ResponsibleForArrangingWorker),
                new SqlParameter("PersonName",model.PersonName),
                new SqlParameter("Telephone",model.Telephone),
                new SqlParameter("Ext",model.Ext),
                new SqlParameter("IsWorkerJobType",model.IsWorkerJobType),
                new SqlParameter("RatePay",model.RatePay),
                new SqlParameter("RatePayTypeID",model.RatePayTypeID),
                new SqlParameter("RatePayOther",model.RatePayOther),
                new SqlParameter("LostTimeDate",model.LostTimeDate),
                new SqlParameter("ReturnDate",model.ReturnDate),
                new SqlParameter("ShiftType",model.ShiftType),
                new SqlParameter("InforConfirmBy",model.InforConfirmBy),
                new SqlParameter("ConfirmPersonName",model.ConfirmPersonName),
                new SqlParameter("WorkerLostTimeDate",model.WorkerLostTimeDate),
                new SqlParameter("ReturnToWorkDate",model.ReturnToWorkDate),
                new SqlParameter("WorkType",model.WorkType),
                new SqlParameter("LostTimeConfirmBy",model.LostTimeConfirmBy),
                new SqlParameter("LostTime_Name",model.LostTime_Name),
                new SqlParameter("LostTime_Telephone",model.LostTime_Telephone),
                new SqlParameter("LostTime_Ext",model.LostTime_Ext),
                new SqlParameter("IsAccepted",model.IsAccepted),
                new SqlParameter("AttchedCopy",model.AttchedCopy)
            };
            SQL.ExceuteStoreProcedure("SaveHealthCare", param, true);
        }
        public void AccidentCompleted(int ID)
        {
            SqlParameter[] param = new SqlParameter[] { new SqlParameter("ID", ID) };
            SQL.ExceuteStoreProcedure("AccidentCompleted", param, false);
        }
        #endregion

        #region Miscellaneous  
        public List<FileDetail> GetFilesFromFolderByEmployeeID(int employeeID, string folderName)
        {
            folderName = folderName.Replace("{FolderName}", "");
            List<FileDetail> lst = new List<FileDetail>();
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("folderName",folderName??""),
                new SqlParameter("EmployeeID",employeeID)
            };
            FileDetail obj = null;
            var baseurl = Convert.ToString(ConfigurationManager.AppSettings["APIBaseUrl"]);
            using (SqlDataReader dr = SQL.GetDataReader("GetFileFromFolder", param, true))
            {
                while (dr.Read())
                {
                    obj = new FileDetail();
                    obj.ID = Convert.ToInt32(dr["ID"]);
                    obj.FilePath = baseurl + Convert.ToString(dr["FilePath"]);
                    obj.Name = dr["FilePath"].ToString().Split('/').Length==3?dr["FilePath"].ToString().Split('/')[2]: dr["FilePath"].ToString().Split('/')[3];
                    obj.CreatedDate = CommonHelper.DateToString(dr["CreatedDate"]);
                    lst.Add(obj);
                }
            }
            return lst;
        }
        public void UpdateFilePrivacy(FilePrivacy model)
        {
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("folderName",model.FolderName),
                new SqlParameter("PrivacyType",model.PrivacyType),
                new SqlParameter("EmployeeID",model.EmployeeID),
            };
            SQL.ExceuteStoreProcedure("UpdateFilePrivacy", param, false);
        }
        public void SavePublicFile(int employeeID, string filePath, string folderName=null)
        {
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("EmployeeID",employeeID),
                new SqlParameter("FilePath",filePath),
                new SqlParameter("folderName",folderName)
            };
            SQL.ExceuteStoreProcedure("SavePublicFile", param, false);
        }
        #endregion

        #region PerofrmenceReview
        public List<EvaluatorIndicatedType> GetPerEvaluatorIndicatedType()
        {
            List<EvaluatorIndicatedType> evaluatorIndicatedType = new List<EvaluatorIndicatedType>();
            using (SqlDataReader dr = SQL.GetDataReader("GetPerEvaluatorIndicatedType", true))
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        evaluatorIndicatedType.Add(new EvaluatorIndicatedType
                        {
                            ID = Convert.ToInt32(dr["ID"]),
                            Name = Convert.ToString(dr["Name"])
                        });
                    }
                }
            }
            return evaluatorIndicatedType;
        }
        public List<PerformenceReview> GetPerformenceReview(int EmployeeID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("EmployeeID",EmployeeID)
            };
            return GetPerformenceReviewList(param);
        }
        public PerformenceReview GetPerformenceReviewByID(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            return GetPerformenceReviewList(param).FirstOrDefault();
        }
        public List<PerformenceReview> getFilterPerformenceReview(FilterDto model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("EmployeeID",model.EmoplyeeID),
                new SqlParameter("FromDate",model.FromDate),
                new SqlParameter("ToDate",model.ToDate),
                new SqlParameter("SearchText",model.SearchText),
                new SqlParameter("CompanyID",model.CompanyID),
            };
            return GetPerformenceReviewList(param);
        }
        private List<PerformenceReview> GetPerformenceReviewList(SqlParameter[] param)
        {
            List<PerformenceReview> PerformenceReview = new List<PerformenceReview>();
            using (SqlDataReader dr = SQL.GetDataReader("GetPerformenceReview", param, true))
            {
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        PerformenceReview.Add(new PerformenceReview
                        {
                            ID = Convert.ToInt32(dr["ID"]),
                            EmployeeID = Convert.ToInt32(dr["EmployeeID"]),
                            EvaluatorID = Convert.ToInt32(dr["EvaluatorID"]),
                            EvaluatorName=Convert.ToString(dr["EvaluatorName"]),
                            EmployeeNumber=Convert.ToString(dr["EmployeeNumber"]),
                            EvaluatorPossition = Convert.ToString(dr["EvaluatorPossition"]),
                            DateOfReview = CommonHelper.DateToString(dr["DateOfReview"]),
                            FilePath = Convert.ToString(dr["FilePath"]),
                            Status = Convert.ToInt32(dr["Status"]),
                            ExcusableAbsenteeism = Convert.ToInt32(dr["ExcusableAbsenteeism"]),
                            InexcusableAbsenteeism = Convert.ToInt32(dr["InexcusableAbsenteeism"]),
                            LateDepartures = Convert.ToInt32(dr["LateDepartures"]),
                            Occupational = Convert.ToInt32(dr["Occupational"]),
                            EmployeeRelations = Convert.ToInt32(dr["EmployeeRelations"]),
                            QualityAssurance = Convert.ToInt32(dr["QualityAssurance"]),
                            JobRequirements = Convert.ToInt32(dr["JobRequirements"]),
                            AdherenceProcedure = Convert.ToInt32(dr["AdherenceProcedure"]),
                            JobTasks = Convert.ToInt32(dr["JobTasks"]),
                            OrganizationTasks = Convert.ToInt32(dr["OrganizationTasks"]),
                            AbilityProblemSolve = Convert.ToInt32(dr["AbilityProblemSolve"]),
                            Responsibility = Convert.ToInt32(dr["Responsibility"]),
                            Initiative = Convert.ToInt32(dr["Initiative"]),
                            AbilityLearn = Convert.ToInt32(dr["AbilityLearn"]),
                            Adaptability = Convert.ToInt32(dr["Adaptability"]),
                            AbilityWork = Convert.ToInt32(dr["AbilityWork"]),
                            CooperationCoWorkers = Convert.ToInt32(dr["CooperationCoWorkers"]),
                            AttitudeSupervision = Convert.ToInt32(dr["AttitudeSupervision"]),
                            EmployeeStrengths = Convert.ToString(dr["EmployeeStrengths"]),
                            ImprovementDesc = Convert.ToString(dr["ImprovementDesc"]),
                            GaolDesc = Convert.ToString(dr["GaolDesc"]),
                            Comments = Convert.ToString(dr["Comments"]),
                            CreatedBy = Convert.ToInt32(dr["CreatedBy"]),
                            CreatedUser = Convert.ToString(dr["CreatedUser"]),
                            ModifiedUser = Convert.ToString(dr["ModifiedUser"]),
                            CreatedDate = CommonHelper.DateToString(dr["CreatedDate"]),
                            ModifiedDate = CommonHelper.DateToString(dr["ModifiedDate"]),
                            EmployeeName = Convert.ToString(dr["EmployeeName"]),
                            EmployeePosition = Convert.ToString(dr["Title"]),
                            StatusValue=Convert.ToString(dr["StatusValue"]),
                            CompanyID = Convert.ToInt32(dr["CompanyID"])
                        });
                    }
                }
            }
            return PerformenceReview;
        }
        public int SavePerformenceReview(PerformenceReview model)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("Return",""),
                new SqlParameter("ID",model.ID),
                        new SqlParameter("EmployeeID",model.EmployeeID),
                        new SqlParameter("EvaluatorID",model.EvaluatorID),
                        new SqlParameter("EvaluatorPossition",model.EvaluatorPossition),
                        new SqlParameter("DateOfReview",model.DateOfReview),
                        new SqlParameter("FilePath",model.FilePath),
                        new SqlParameter("Status",model.Status),
                        new SqlParameter("ExcusableAbsenteeism",model.ExcusableAbsenteeism),
                        new SqlParameter("InexcusableAbsenteeism",model.InexcusableAbsenteeism),
                        new SqlParameter("LateDepartures",model.LateDepartures),
                        new SqlParameter("Occupational",model.Occupational),
                        new SqlParameter("EmployeeRelations",model.EmployeeRelations),
                        new SqlParameter("QualityAssurance",model.QualityAssurance),
                        new SqlParameter("JobRequirements",model.JobRequirements),
                        new SqlParameter("AdherenceProcedure",model.AdherenceProcedure),
                        new SqlParameter("JobTasks",model.JobTasks),
                        new SqlParameter("OrganizationTasks",model.OrganizationTasks),
                        new SqlParameter("AbilityProblemSolve",model.AbilityProblemSolve),
                        new SqlParameter("Responsibility",model.Responsibility),
                        new SqlParameter("Initiative",model.Initiative),
                        new SqlParameter("AbilityLearn",model.AbilityLearn),
                        new SqlParameter("Adaptability",model.Adaptability),
                        new SqlParameter("AbilityWork",model.AbilityWork ),
                        new SqlParameter("CooperationCoWorkers",model.CooperationCoWorkers),
                        new SqlParameter("AttitudeSupervision",model.AttitudeSupervision),
                        new SqlParameter("EmployeeStrengths",model.EmployeeStrengths),
                        new SqlParameter("ImprovementDesc",model.ImprovementDesc),
                        new SqlParameter("GaolDesc",model.GaolDesc),
                        new SqlParameter("Comments",model.Comments),
                        new SqlParameter("CreatedBy",model.CreatedBy)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            int result = SQL.ExceuteStoreProcedure("SavePerformenceReview", param, true);
            if (result > 0)
                _commonServices.SaveFileRecord(model.FilePath, model.EmployeeID, "PerformancReview");

            return result;

        }
        public int UpdatePerReviewStatus(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            return SQL.ExceuteStoreProcedure("UpdatePerReviewStatus", param, true);

        }
        public List<ReviewSectionContent> GetPerformanceReviewContent()
        {
            List<ReviewSectionContent> lst = new List<ReviewSectionContent>();
            DataSet ds = SQL.GetDs("PerReviewSectonContent", true);
            if (ds != null && ds.Tables[0].Rows.Count>0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    lst.Add(new ReviewSectionContent
                    {
                        Title= Convert.ToString(dr["Title"]),
                        SubTitle= Convert.ToString(dr["SubTitle"]),
                        Content = GetContentList(Convert.ToInt32(dr["ID"]), ds.Tables[1])
                    });
                }
            }
            return lst;
        }
        private List<string> GetContentList(int id, DataTable dataTable)
        {
            List<string> objlist = new List<string>();
            if (dataTable != null && dataTable.Rows.Count > 0)
            {
                DataRow[] result = dataTable.Select("ID = '" + id + "'");
                foreach (DataRow row in result)
                {
                    objlist.Add(Convert.ToString(row["Title"]));
                }
            }
            return objlist;
        }
        public void PerformenceReviewCompleted(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            SQL.ExceuteStoreProcedure("PerformenceReviewCompleted", param, true);
        }
        #endregion

        #region Asset
        public int SaveAssets(Assets Assets)
        {
            var returnVal = 0;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("@Result",""),
                new SqlParameter("ID",Assets.ID),
                new SqlParameter("AssetName",Assets.AssetName),
                new SqlParameter("AssetsTypeID",Assets.AssetsTypeID),
                new SqlParameter("AssetId",Assets.AssetId),
                new SqlParameter("CompanyID",Assets.CompanyID),
                new SqlParameter("Condition",Assets.Condition),
                new SqlParameter("CreatedBy",Assets.CreatedBy),
                new SqlParameter("Description",Assets.Description),
                new SqlParameter("EmployeeID",Assets.EmployeeID),
                new SqlParameter("Manufacturer",Assets.Manufacturer),
                new SqlParameter("Model",Assets.Model),
                new SqlParameter("Price",Assets.Price),
                new SqlParameter("PurchaseDate",Assets.PurchaseDate),
                new SqlParameter("PurchaseFrom",Assets.PurchaseFrom),
                new SqlParameter("SerialNumber",Assets.SerialNumber),
                new SqlParameter("Status",Assets.Status),
                new SqlParameter("Supplier",Assets.Supplier),
                new SqlParameter("Warranty",Assets.Warranty)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            returnVal = SQL.ExceuteStoreProcedure("SaveAssets", param, true);
            return returnVal;
        }
        public List<Assets> GetAssetList(AssetsFilter assetsFilter)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("EmployeeName",assetsFilter.EmployeeName),
                new SqlParameter("CompanyID",assetsFilter.CompanyID),
                new SqlParameter("FromDate",assetsFilter.FromDate),
                new SqlParameter("ToDate",assetsFilter.ToDate),
                new SqlParameter("Status",assetsFilter.Status),
            };
            return AssetLsist(param);
        }
        public Assets GetAssetList(int ID)
        {
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("ID",ID)
            };
            return AssetLsist(param).FirstOrDefault();
        }
        private List<Assets> AssetLsist(SqlParameter[] param)
        {
            List<Assets> objlist = new List<Assets>();
            using (SqlDataReader dr = SQL.GetDataReader("GetAssetsList", param, true))
            {
                while (dr.Read())
                {
                    objlist.Add(new Assets {
                        ID=Convert.ToInt32(dr["ID"]),
                        AssetName= Convert.ToString(dr["AssetName"]),
                        AssetsTypeID = Convert.ToInt32(dr["AssetsTypeID"]),
                        AssetsType = Convert.ToString(dr["AssetsType"]),
                        AssetId = Convert.ToString(dr["AssetID"]),
                        PurchaseDate = CommonHelper.DateToString(dr["PurchaseDate"]),
                        PurchaseFrom = Convert.ToString(dr["PurchaseFrom"]),
                        Manufacturer = Convert.ToString(dr["Manufacturer"]),
                        Model = Convert.ToString(dr["Model"]),
                        SerialNumber = Convert.ToString(dr["SerialNumber"]),
                        Supplier = Convert.ToString(dr["Supplier"]),
                        Condition = Convert.ToString(dr["Condition"]),
                        Warranty = Convert.ToInt32(dr["Warranty"]),
                        Price = Convert.ToDecimal(dr["Price"]),
                        EmployeeID = Convert.ToInt32(dr["EmployeeID"]),
                        Description = Convert.ToString(dr["Description"]),
                        Status = Convert.ToInt32(dr["Status"]),
                        StatusValue = ((AssetStatus)Convert.ToInt32(dr["Status"])).ToString(),
                        CreatedBy = Convert.ToInt32(dr["CreatedBy"]),
                        CompanyID = Convert.ToInt32(dr["CompanyID"]),
                        EmployeeName = Convert.ToString(dr["EmployeeName"]),
                    });
                }
            }
            return objlist;
        }
        public int SaveAssetsType(AssetsType model)
        {
            var returnVal = 0;
            SqlParameter[] param = new SqlParameter[] {
                new SqlParameter("@Result",""),
                new SqlParameter("ID",model.ID),
                new SqlParameter("Name",model.Name)
            };
            param[0].Direction = System.Data.ParameterDirection.ReturnValue;
            returnVal = SQL.ExceuteStoreProcedure("SaveAssetsType", param, true);
            return returnVal;
        }
        public List<AssetsType> GetAssetTypeList()
        {
            List<AssetsType> objlist = new List<AssetsType>();
            using (SqlDataReader dr = SQL.GetDataReader("GetAssetsTypeList", true))
            {
                while (dr.Read())
                {
                    objlist.Add(new AssetsType
                    {
                        ID = Convert.ToInt32(dr["ID"]),
                        Name = Convert.ToString(dr["Name"])
                    });
                }
            }
            return objlist;
        }
        public void DeleteAssets(int ID)
        {
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("ID",ID)
            };
            SQL.ExecuteQuery("UPDATE Assets SET Isdeleted=1 WHERE ID=@ID", param);
        }
        public void UpdateAssetStatus(AssetStatusDto model)
        {
            SqlParameter[] param = new SqlParameter[]
           {
                new SqlParameter("ID",model.ID),
                new SqlParameter("Status",model.StatusID)
           };
            SQL.ExecuteQuery("UPDATE Assets SET Status=@Status WHERE ID=@ID", param);
        }
        #endregion
    }
}
